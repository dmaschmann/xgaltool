# xgaltool - The extra galactic tool kit


This main objective of this package is to provide an easy data access and data handling for further analysis using already published works. 
It is not though to provide a general tool for extra galactic data tables but to track the authors work, make their analysis methods public available and provide transparent coding.


---


### Dependencies
numpy 
scipy
astropy
matplotlib.pyplot
pathlib, urllib3

### linux or Mac OS
1. clone git repo
2. add to your .bashrc  
   **export PYTHONPATH=/ add your project path here! /xgaltool**
3. you should create a data path where you store all data like galaxy tables spectra etc. (standard woud we ~/data)
### Windows
1. Delete your Windows distribution, install a linux ditribution and go back to the step before.

---

## Tutorials

... will come in the future


