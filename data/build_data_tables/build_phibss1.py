import numpy as np
import os

from galtool import analysis_tools, coordinate_utils
from astropy.table import QTable, hstack, vstack, Column

from astropy.coordinates import SkyCoord
from astropy import units as u


"""
This macro will create an galaxy table for the galtool package of the CO observation published in 
Tacconi et al. (2013) DOI:10.1088/0004-637X/768/1/74

We are using Table 1 and 2 of the Publication.
"""


# get galtool access
empty_access = analysis_tools.AnalysisTools(object_type='empty')

# get galaxy coordinates and data
# first table is only coordinates and redshift
data_table_1 = np.genfromtxt(empty_access.project_path / 'data/co_data/phibss1/tacconi_2013_table_1.txt',
                                    dtype=object)

# second table has all observation parameters of CO
data_table_2 = np.genfromtxt(empty_access.project_path / 'data/co_data/phibss1/tacconi_2013_table_2.txt',
                                    dtype=object)

print('length table 1 ', len(data_table_1))
print('length table 2 ', len(data_table_2))

# get names for data table
names = np.array(['ra', 'dec', 'ra_co_peak', 'dec_co_peak', 'z_ned', 'major_beam_axis', 'minor_beam_axis',

                  'flux_co32', 'flux_co32_err', 'L_co32', 'limit_flag_co32',

                  'literature_m_h2', 'literature_f_gas', 'literature_surface_dens_mol_gas',
                  'literature_surface_dens_sfr',

                  'literature_v_rot', 'literature_r_12_opt', 'literature_r_12_co',
                  'literature_log_sfr', 'literature_log_m_star'])



# space holder for final table
final_table = None

for index, galaxy_identifier in zip(range(len(data_table_1)), data_table_1[:, 0]):
    # erase all - characters in the name space
    galaxy_identifier = str(galaxy_identifier, encoding='utf-8').replace('-', '').encode('utf-8', 'ignore')
    print(index, galaxy_identifier)
    index_data_table2 = data_table_2[:, 0] == galaxy_identifier

    ra_raw = str(data_table_1[index, 3], encoding='utf-8').replace('^', '').replace('h', ':').replace('m', ':').replace('s', '')
    dec_raw = str(data_table_1[index, 4], encoding='utf-8').replace('^', '').replace('d', ':').replace('\'', ':').replace('\"', '')

    co_ra_raw = str(data_table_1[index, 5], encoding='utf-8').replace('^', '').replace('h', ':').replace('m', ':').replace('s', '')
    co_dec_raw = str(data_table_1[index, 6], encoding='utf-8').replace('^', '').replace('d', ':').replace('\'', ':').replace('\"', '')

    # check if optical coordinates are nan
    print(ra_raw)
    if ra_raw == '...':
        ra_raw = co_ra_raw
        dec_raw = co_dec_raw

    coordinates = ra_raw + ' ' + dec_raw
    coord_table = SkyCoord(coordinates,  unit=(u.hourangle, u.deg))
    ra = coord_table.ra.deg
    dec = coord_table.dec.deg
    if co_ra_raw != '...':
        co_coordinates = co_ra_raw + ' ' + co_dec_raw
        co_coord_table = SkyCoord(co_coordinates,  unit=(u.hourangle, u.deg))
        co_ra = co_coord_table.ra.deg
        co_dec = co_coord_table.dec.deg
    else:
        co_ra = np.nan
        co_dec = np.nan

    print(ra, dec)
    print(co_ra, co_dec)
    name_column = Column(np.array([galaxy_identifier]), name='literature_name', dtype='S12')

    # create empty CO table
    co_data = np.zeros(len(names))
    co_data[:] = np.nan  # fill with NaN values

    # coordinates
    co_data[0] = ra
    co_data[1] = dec
    co_data[2] = co_ra
    co_data[3] = co_dec
    # redshift
    if data_table_1[index, 7] != b'...':
        co_data[4] = float(data_table_1[index, 7])
    # beam axis
    co_data[5] = float(str(data_table_1[index, 8], encoding='utf-8').replace('\"', ''))
    co_data[6] = float(str(data_table_1[index, 10], encoding='utf-8').replace('\"', ''))

    if float(data_table_2[index_data_table2, 6]) > 0:
        # co32 flux
        co_data[7] = float(data_table_2[index_data_table2, 6])
        co_data[8] = float(data_table_2[index_data_table2, 7])
        # luminosity
        co_data[9] = float(data_table_2[index_data_table2, 8]) * 1e-9
        # limit flag
        co_data[10] = 0
        # molecular gas mass
        co_data[11] = float(data_table_2[index_data_table2, 9]) * 1e-9
        # gas fraction
        co_data[12] = float(data_table_2[index_data_table2, 11])
        # literature_surface_dens_mol_gas
        co_data[13] = float(data_table_2[index_data_table2, 12])
        # literature_surface_dens_sfr
        co_data[14] = float(data_table_2[index_data_table2, 13])

    else:
        # co32 flux
        co_data[7] = float(data_table_2[index_data_table2, 6]) * -1
        co_data[8] = float(data_table_2[index_data_table2, 7])
        # luminosity
        co_data[9] = float(data_table_2[index_data_table2, 8]) * 1e-9 * -1
        # limit flag
        co_data[10] = 1
        # molecular gas mass
        co_data[11] = float(data_table_2[index_data_table2, 9]) * 1e-9 * -1
        # gas fraction
        co_data[12] = float(data_table_2[index_data_table2, 11])
        # literature_surface_dens_mol_gas
        co_data[13] = float(data_table_2[index_data_table2, 12])
        # literature_surface_dens_sfr
        co_data[14] = float(data_table_2[index_data_table2, 13]) * -1

    # literature_v_rot
    if data_table_2[index_data_table2, 2] != b'...':
        co_data[15] = float(data_table_2[index_data_table2, 2])
    # literature_r_12_opt
    co_data[16] = float(data_table_2[index_data_table2, 3])
    # literature_r_12_co
    if data_table_2[index_data_table2, 4] != b'...':
        co_data[17] = float(data_table_2[index_data_table2, 4])
    # literature_log_sfr
    co_data[18] = np.log10(float(data_table_2[index_data_table2, 5]))
    # literature_log_m_star
    co_data[19] = np.log10(float(data_table_2[index_data_table2, 10]))


    co_data_table = QTable(data=co_data, names=names)

    co_data_table = hstack([name_column, co_data_table])

    if final_table is None:
        final_table = co_data_table
    else:
        final_table = vstack([final_table, co_data_table])


# add units to table
final_table['ra'].unit = u.deg
final_table['dec'].unit = u.deg
final_table['ra_co_peak'].unit = u.deg
final_table['dec_co_peak'].unit = u.deg

final_table['major_beam_axis'].unit = u.arcsec
final_table['minor_beam_axis'].unit = u.arcsec


final_table['flux_co32'].unit = u.Jy * u.km / u.s
final_table['flux_co32_err'].unit = u.Jy * u.km / u.s
final_table['L_co32'].unit = 1e9 * u.K * u.km / u.s

final_table['literature_m_h2'].unit = 1e9 * u.M_sun
final_table['literature_surface_dens_mol_gas'].unit = u.M_sun / (u.pc ** (2))
final_table['literature_surface_dens_sfr'].unit = u.M_sun / u.yr / (u.kpc ** (2))

final_table['literature_v_rot'].unit = u.km / u.s
final_table['literature_r_12_opt'].unit = u.kpc
final_table['literature_r_12_co'].unit = u.kpc
final_table['literature_log_sfr'].unit = u.M_sun / u.yr
final_table['literature_log_m_star'].unit = u.M_sun

# print final table
print(final_table)

# create galtool object
final_table_access = analysis_tools.AnalysisTools(table=final_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'tacconi_2013'):
    os.makedirs(final_table_access.data_path / 'tables' / 'tacconi_2013')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'tacconi_2013' / 'tacconi_2013.fits',
                               overwrite=True)

