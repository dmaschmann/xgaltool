from galtool import analysis_tools
import numpy as np
from astropy.table import hstack, Column
import os
import pickle

"""
This macro will create an object table of all gathered observation from by Tacconi et al. 2018 
doi:10.3847/1538-4357/aaa4b4 presented in Table 2
"""

empty_access = analysis_tools.AnalysisTools(object_type='empty')

# accurate data
[tacconi_source, tacconi_survey, tacconi_redshift,
 tacconi_logMstar, tacconi_logSFR, tacconi_DMS,
 tacconi_logtdepl ,tacconi_logmu, tacconi_R, tacconi_DR] = pickle.load(
    open(str(empty_access.project_path / 'data/co_data/tacconi_2018/tacconi_2018_data.pkl'), 'rb'),  encoding='latin1')

co_sample = ((tacconi_survey == b'ALMA') |
             (tacconi_survey == b'Bauermeister_EGNOG') |
             (tacconi_survey == b'COLDGASS') |
             (tacconi_survey == b'COLDGASS_LOW') |
             (tacconi_survey == b'Combes_ULIRGs') |
             (tacconi_survey == b'Daddi_Magdis') |
             (tacconi_survey == b'PEP') |
             (tacconi_survey == b'Gracia_GOALS') |
             (tacconi_survey == b'_Gracia_GOALS') |
             (tacconi_survey == b'PHIBSS1') |
             (tacconi_survey == b'PHIBSS2') |
             (tacconi_survey == b'SMG') |
             (tacconi_survey == b'Saintonge_lenses') |
             (tacconi_survey == b'Silverman_FMOS_Herschel'))

tacconi_method = np.zeros(len(tacconi_source), dtype='S5')
tacconi_method[co_sample] = b'CO'
tacconi_method[~co_sample] = b'dust'

source_column = Column(np.array(tacconi_source), name='source', dtype='S35')
survey_column = Column(np.array(tacconi_survey), name='survey', dtype='S35')
method_column = Column(np.array(tacconi_method), name='method', dtype='S5')
redshift_column = Column(np.array(tacconi_redshift), name='redshift', dtype=float)
logMstar_column = Column(np.array(tacconi_logMstar), name='log_m_star', dtype=float)
logSFR_column = Column(np.array(tacconi_logSFR), name='log_sfr', dtype=float)
DMS_column = Column(np.array(tacconi_DMS), name='DMS', dtype=float)
logtdepl_column = Column(np.array(tacconi_logtdepl), name='log_t_depl', dtype=float)
logmu_column = Column(np.array(tacconi_logmu), name='log_mu', dtype=float)
R_column = Column(np.array(tacconi_R), name='R', dtype=float)
DR_column = Column(np.array(tacconi_DR), name='DR', dtype=float)

final_table = hstack([source_column, survey_column, method_column, redshift_column, logMstar_column,
                      logSFR_column, DMS_column, logtdepl_column, logmu_column, R_column, DR_column])

# create galtool object
final_table_access = analysis_tools.AnalysisTools(table=final_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'tacconi_2018'):
    os.makedirs(final_table_access.data_path / 'tables' / 'tacconi_2018')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'tacconi_2018' / 'tacconi_2018.fits',
                               overwrite=True)

exit()


r"""
Below you find a discarded version of data access using the provided PDF version
"""

#
#
# # open pdf taken from https://www.iram.fr/~phibss2/Scaling-bigtable-final-forweb.pdf
# # this data is not very accurate since not enough digits are provided in the file
# raw_pdf_data = parser.from_file(str(empty_access.project_path / 'data/co_data/tacconi_2018/tacconi_2018_table_2.pdf'))


# # this is one long string with the whole table
# pdf_content = raw_pdf_data['content']
# # split each line
# pdf_content = np.array(pdf_content.split('\n'), dtype="S100")
#
# # erase empty lines
# empty_spaces = np.where(pdf_content == b'')
# pdf_content = np.delete(pdf_content, empty_spaces)
#
# # erase header and footer
# header_and_footer = np.where(((pdf_content == b'Source Survey or Galaxy Type Method z log Mgas logM* logSFR R_eff') |
#                               (pdf_content == b'Msun Msun Msun/yr kpc') |
#                               (pdf_content == b'\tSheet1')))
# pdf_content = np.delete(pdf_content, header_and_footer)
#
# # create array
# print('Length of table: ', len(pdf_content))
#
# list_surveys = ['LIRGS and ULIRGS', 'xCOLD GASS', 'Bauermeister EGNOG', 'Combes ULIRGs', 'PHIBSS2', 'Daddi-Magdis',
#                 'PHIBSS1', ' PEP', 'ALMA', 'SMG', 'Saintonge-lenses', 'Silverman FMOS-Herschel',
#                 'GOODS-N/S+COSMOS PEP+HerMES', 'COSMOS PEP', 'DeCarli,Barro,Dunlop 1mm', 'Scoville, Tadaki, Lilly 1mm',
#                 'Scoville, Tadaki, Lilly 1mm stacks', 'PHIBBS2-NOEMA-dust-1mm', 'schinnerer-stack']
#
# pdf_data = np.zeros((len(pdf_content), 8), dtype=object)
#
# for index in range(len(pdf_content)):
#     # get line string
#     line_string = str(pdf_content[index], encoding='utf-8')
#
#     # get survey
#     survey = None
#     for running_survey in list_surveys:
#         if running_survey in line_string:
#             survey = running_survey
#     if survey is None:
#         raise RuntimeError('Survey not found !!!!!')
#     # get source name
#     source = line_string.split(survey)[0].rstrip()
#     # get data
#     data_string = line_string.split(survey)[1].split(' ')
#     method = data_string[1]
#     redshift = float(data_string[2])
#     log_m_gas = float(data_string[3])
#     log_m_star = float(data_string[4])
#     if len(data_string) > 5:
#         log_sfr = float(data_string[5])
#     else:
#         log_sfr = np.nan
#     if len(data_string) > 6:
#         r_eff = float(data_string[6])
#     else:
#         r_eff = np.nan
#
#     print(source, survey, method, redshift, log_m_gas, log_m_star, log_sfr, r_eff)
#
#     # create data table
#     pdf_data[index] = np.array([source, survey, method, redshift, log_m_gas, log_m_star, log_sfr, r_eff])

#
# tacconi_method = np.zeros(len(tacconi_source), dtype='S5')
#
# print(tacconi_source)
#
#
# # for index in range(len(tacconi_method)):
# #     print(str(tacconi_source[index], encoding='utf-8'), str(tacconi_survey[index], encoding='utf-8'))
# #     index_pdf_data = np.where((pdf_data[:, 0] == str(tacconi_source[index], encoding='utf-8')) & (pdf_data[:, 0] == str(tacconi_survey[index], encoding='utf-8')))
# #     print(index_pdf_data)
# #     exit()
#
#
# print(pdf_data)


# # create galtool object
# final_table_access = analysis_tools.AnalysisTools(table=final_table)
#
# # check if folder exists
# if not os.path.isdir(final_table_access.data_path / 'tables' / 'tacconi_2018'):
#     os.makedirs(final_table_access.data_path / 'tables' / 'tacconi_2018')
#
# # save table
# final_table_access.table.write(final_table_access.data_path / 'tables' / 'tacconi_2018' / 'tacconi_2018.fits',
#                                overwrite=True)


