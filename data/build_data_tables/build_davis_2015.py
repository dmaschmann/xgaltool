import numpy as np
import os

from galtool import analysis_tools, coordinate_utils
from astropy.table import QTable, hstack, vstack, Column
import astropy.units as u
"""
This macro will create an galaxy tableDavis et al 2015
DOI:10.1093/mnras/stv597 
We are using Table 2 and 3 of the Publication.
"""

# get empty galtool object to access data structure
empty_access = analysis_tools.AnalysisTools(object_type='empty')
# load publication tables
davis_table_1 = np.genfromtxt(empty_access.project_path /
                                     'data/co_data/davis_2015/davis_2015_table_1.dat', dtype=object)
davis_table_2 = np.genfromtxt(empty_access.project_path /
                                     'data/co_data/davis_2015/davis_2015_table_2.dat', dtype=float)

print('length of table 1 = ', len(davis_table_1))
print('length of table 2 = ', len(davis_table_2))

# space holder for final table
final_table = None

# get all data names
names = np.array(['flux_co10', 'flux_co10_err', 'delta_v_co10',
                  'L_co10', 'L_co10_err', 'limit_flag_co10',
                  'flux_co21', 'flux_co21_err', 'delta_v_co21',
                  'L_co21', 'L_co21_err', 'limit_flag_co21',
                  'literature_m_h2', 'literature_m_h2_err', 'literature_limit_flag_m_h2'])

# loop over all galaxies and create Astropy tables
for index, galaxy_cross_id in zip(range(0, len(davis_table_2)), davis_table_1[:, 2]):

    # get name
    literature_name = str(davis_table_1[index, 0], 'utf-8') + " " + str(davis_table_1[index, 1], 'utf-8')
    ra, dec, ned_redshift = coordinate_utils.get_galaxy_ned_coords_and_z(galaxy_name=literature_name)
    print(ra, dec, ned_redshift)

    # column with name and tables with coordinates and redshift
    name_column = Column(np.array([literature_name]), name='literature name', dtype='S25')
    identifier_column = QTable(data=np.array([ra, dec, ned_redshift]), names=['ra', 'dec', 'z_ned'], dtype=(float, float, float))

    # create empty CO table
    co_data = np.zeros(len(names))
    co_data[:] = np.nan  # fill with NaN values

    # get data position in table 2
    mask_data_table = davis_table_2[:, 0] == int(galaxy_cross_id)

    # fill data into co data _table
    # CO10 data
    if not np.isnan(float(davis_table_2[mask_data_table, 5])):
        # flux
        co_data[0] = float(davis_table_2[mask_data_table, 5])
        co_data[1] = float(davis_table_2[mask_data_table, 6])
        # delta v of emission line
        co_data[2] = float(davis_table_2[mask_data_table, 4])

        # line luminosity
        co_data[3] = empty_access.compute_co_luminosity(s_co_vel=co_data[0], redshift=ned_redshift,
                                                        conversion_factor=1)
        co_data[4] = empty_access.compute_co_luminosity(s_co_vel=co_data[1], redshift=ned_redshift,
                                                        conversion_factor=1)
        # limit flag
        co_data[5] = 0
    else:
        # flux
        co_data[0] = float(davis_table_2[mask_data_table, 3]) * 3 * 300 * 1e-3

        # line luminosity
        co_data[3] = empty_access.compute_co_luminosity(s_co_vel=co_data[0], redshift=ned_redshift,
                                                        conversion_factor=1)

        # limit flag
        co_data[5] = 1

    # CO21
    if not np.isnan(float(davis_table_2[mask_data_table, 5])):
        # flux
        co_data[6] = float(davis_table_2[mask_data_table, 5])
        co_data[7] = float(davis_table_2[mask_data_table, 6])
        # delta v of emission line
        co_data[8] = float(davis_table_2[mask_data_table, 4])

        # line luminosity
        co_data[9] = empty_access.compute_co_luminosity(s_co_vel=co_data[6], redshift=ned_redshift,
                                                        observed_co_frequency=empty_access.co21_line_frequency /
                                                                              (1 + ned_redshift),
                                                        conversion_factor=1)
        co_data[10] = empty_access.compute_co_luminosity(s_co_vel=co_data[7], redshift=ned_redshift,
                                                         observed_co_frequency=empty_access.co21_line_frequency /
                                                                               (1 + ned_redshift),
                                                         conversion_factor=1)
        # limit flag
        co_data[11] = 0
    else:
        # flux
        co_data[6] = float(davis_table_2[mask_data_table, 3]) * 3 * 300 * 1e-3

        # line luminosity
        co_data[9] = empty_access.compute_co_luminosity(s_co_vel=co_data[6], redshift=ned_redshift,
                                                        observed_co_frequency=empty_access.co21_line_frequency /
                                                                              (1 + ned_redshift),
                                                        conversion_factor=1)

        # limit flag
        co_data[11] = 1

    # get literature h2
    co_data[12] = float(davis_table_2[mask_data_table, 12])
    co_data[13] = float(davis_table_2[mask_data_table, 13])
    if np.isnan(co_data[13]):
        co_data[14] = 1
    else:
        co_data[14] = 0

    # create data table and put everything together
    co_data_table = QTable(data=co_data, names=names)
    co_data_table = hstack([name_column, identifier_column, co_data_table])

    if final_table is None:
        final_table = co_data_table
    else:
        final_table = vstack([final_table, co_data_table])

# add units to table
final_table['ra'].unit = u.deg
final_table['dec'].unit = u.deg

final_table['flux_co10'].unit = u.Jy * u.km / u.s
final_table['flux_co10_err'].unit = u.Jy * u.km / u.s
final_table['delta_v_co10'].unit = u.km / u.s
final_table['L_co10'].unit = 1e9 * u.K * u.km / u.s * u.pc**2
final_table['L_co10_err'].unit = 1e9 * u.K * u.km / u.s * u.pc**2

final_table['flux_co21'].unit = u.Jy * u.km / u.s
final_table['flux_co21_err'].unit = u.Jy * u.km / u.s
final_table['delta_v_co21'].unit = u.km / u.s
final_table['L_co21'].unit = 1e9 * u.K * u.km / u.s * u.pc**2
final_table['L_co21_err'].unit = 1e9 * u.K * u.km / u.s * u.pc**2

final_table['literature_m_h2'].unit = 1e9 * u.M_sun
final_table['literature_m_h2_err'].unit = 1e9 * u.M_sun

print(final_table.colnames)
print(final_table)

# create galtool object
final_table_access = analysis_tools.AnalysisTools(table=final_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'davis_2015'):
    os.makedirs(final_table_access.data_path / 'tables' / 'davis_2015')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'davis_2015' / 'davis_2015.fits',
                               overwrite=True)

