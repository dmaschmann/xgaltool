import numpy as np
import os

from xgaltool import analysis_tools
from astropy.table import QTable
import astropy.units as u


"""
This macro will create an object table of the observation sample published by French et al. 2015 
doi:10.1088/0004-637X/801/1/1
Table 1, 2 and 3
"""

rcsed_access = analysis_tools.AnalysisTools(object_type='rcsed', writable_table=True)

french_table_1 = np.genfromtxt(rcsed_access.project_path /
                                      'data/co_data/french_2015/french_2015_table_1.txt', dtype=object)
french_table_2 = np.genfromtxt(rcsed_access.project_path /
                                      'data/co_data/french_2015/french_2015_table_2.txt', dtype=object)
french_table_3 = np.genfromtxt(rcsed_access.project_path /
                                      'data/co_data/french_2015/french_2015_table_3.txt', dtype=object)

# print table properties
print(len(french_table_1))
print(len(french_table_2))
print(len(french_table_3))

# data table names
names = np.array(['mjd', 'plate', 'fiberid', 'ra', 'dec', 'z_ned',

                  'flux_co10', 'flux_co10_err', 'delta_v_co10', 'delta_v_co10_err',
                  'L_co10', 'L_co10_err', 'limit_flag_co10',

                  'flux_co21', 'flux_co21_err', 'delta_v_co21', 'delta_v_co21_err', 'limit_flag_co21',

                  'literature_m_h2', 'literature_m_h2_err', 'literature_limit_flag_m_h2'])

# get empty data table and fill it up with nan values
co_data = np.zeros((len(french_table_1), len(names)))
co_data[:, :] = np.nan

# loop over all galaxies and create Astropy tables
for index in range(0, len(french_table_1)):

    # mjd plate and fiberid
    co_data[index, 0] = int(french_table_1[index, 5])
    co_data[index, 1] = int(french_table_1[index, 4])
    co_data[index, 2] = int(french_table_1[index, 6])

    # ra and dec
    co_data[index, 3] = float(french_table_1[index, 1])
    co_data[index, 4] = float(french_table_1[index, 2])

    # redshift
    co_data[index, 5] = float(french_table_1[index, 3])

    # co data
    # CO10
    if float(french_table_2[index, 2]) > 0:
        # flux
        co_data[index, 6] = float(french_table_2[index, 2])
        co_data[index, 7] = float(french_table_2[index, 4])
        # FWHM
        co_data[index, 8] = float(french_table_2[index, 11])
        co_data[index, 9] = float(french_table_2[index, 13])
        # luminosity in 1e9 K km/s pc^2
        co_data[index, 10] = float(french_table_2[index, 5]) * 1e-2
        co_data[index, 11] = float(french_table_2[index, 7]) * 1e-2
        # flag
        co_data[index, 12] = 0
        # H2 mass  in 1e9 sol masses
        co_data[index, 18] = float(french_table_2[index, 8]) * 1e-2
        co_data[index, 19] = float(french_table_2[index, 10]) * 1e-2
        co_data[index, 20] = 0
    else:
        # flux
        co_data[index, 6] = float(french_table_2[index, 2]) * -1
        # luminosity in 1e9 K km/s pc^2
        co_data[index, 10] = float(french_table_2[index, 5]) * 1e-2 * -1
        # flag
        co_data[index, 12] = 1
        # H2 mass in 1e9 sol masses
        co_data[index, 18] = float(french_table_2[index, 8]) * -1 * 1e-2
        co_data[index, 20] = 1

    # CO21
    if float(french_table_3[index, 2]) > 0:
        # flux
        co_data[index, 13] = float(french_table_3[index, 2])
        co_data[index, 14] = float(french_table_3[index, 4])
        # FWHM
        co_data[index, 15] = float(french_table_3[index, 5])
        co_data[index, 16] = float(french_table_3[index, 7])
        # flag
        co_data[index, 17] = 0
    else:
        # flux
        co_data[index, 13] = float(french_table_3[index, 2]) * -1
        # flag
        co_data[index, 17] = 1

# create astropy table
co_data_table = QTable(data=co_data, names=names)

# add column wiith identifier
co_data_table.add_column(np.array(french_table_1[:, 0], dtype='S20'), name='literature name', index=0)

# set units
co_data_table['ra'].unit = u.deg
co_data_table['dec'].unit = u.deg

co_data_table['flux_co10'].unit = u.K * u.km / u.s
co_data_table['flux_co10_err'].unit = u.K * u.km / u.s
co_data_table['delta_v_co10'].unit = u.km / u.s
co_data_table['delta_v_co10_err'].unit = u.km / u.s
co_data_table['L_co10'].unit = 1e9 * u.K * u.km / u.s * u.pc**2
co_data_table['L_co10_err'].unit = 1e9 * u.K * u.km / u.s * u.pc**2

co_data_table['flux_co21'].unit = u.K * u.km / u.s
co_data_table['flux_co21_err'].unit = u.K * u.km / u.s
co_data_table['delta_v_co21'].unit = u.km / u.s
co_data_table['delta_v_co21_err'].unit = u.km / u.s

co_data_table['literature_m_h2'].unit = 1e9 * u.M_sun
co_data_table['literature_m_h2_err'].unit = 1e9 * u.M_sun

print(co_data_table)

# create galtool object
final_table_access = analysis_tools.AnalysisTools(table=co_data_table)


# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'french_2015'):
    os.makedirs(final_table_access.data_path / 'tables' / 'french_2015')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'french_2015' /
                               'french_2015.fits', overwrite=True)

