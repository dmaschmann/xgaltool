import numpy as np
import os

from galtool import analysis_tools
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.table import Table, QTable, hstack, Column


"""
This macro will create an galaxy table for the galtool package of the CO observation published in 
Combes et al. (2013) DOI:10.1051/0004-6361/201220392  
We are using Table 1 and 2 taken from Vizier (https://vizier.u-strasbg.fr/viz-bin/VizieR?-source=J/A+A/550/A41) 
and Table 3 directly taken from the Publication.
"""

# get galtool access
empty_access = analysis_tools.AnalysisTools(object_type='empty')

# get galaxy coordinates and data
# Vizier table J/A+A/550/A41/table1
table_1 = Table.read(empty_access.project_path / 'data/co_data/combes_2013/combes_2013_table_1.fit')
# Vizier table 	J/A+A/550/A41/table2
table_2 = Table.read(empty_access.project_path / 'data/co_data/combes_2013/combes_2013_table_2.fit')
# table 3 directly from Combes et al. (2013)
table_3 = np.genfromtxt(empty_access.project_path / 'data/co_data/combes_2013/combes_2013_table_3.dat', dtype=object)

# print table properties
print('length table_1 ', len(table_1))
print('length table_2 ', len(table_2))
print('length table_3 ', len(table_3))
print('colnames table_1 ', table_1.colnames)
print('colnames table_2 ', table_2.colnames)

# data table names
names = np.array(['ra', 'dec', 'z_ned', 'S60', 'S100', 'limit_flag_S100', 'log_L_FIR', 'flux_1_4GHz',

                  'flux_co21', 'flux_co21_err', 'vel_co21', 'vel_co21_err',
                  'delta_v_co21', 'delta_v_co21_err', 'L_co21', 'limit_flag_co21',

                  'flux_co32', 'flux_co32_err', 'vel_co32', 'vel_co32_err',
                  'delta_v_co32', 'delta_v_co32_err', 'L_co32', 'limit_flag_co32',

                  'flux_co43', 'flux_co43_err', 'vel_co43', 'vel_co43_err',
                  'delta_v_co43', 'delta_v_co43_err', 'L_co43', 'limit_flag_co43',

                  'literature_m_h2', 'literature_SFE', 'literature_limit_flag_m_h2',
                  'literature_T_d', 'literature_M_d', 'literature_limit_flag_dust',
                  'literature_R12', 'literature_log_m_star'
                  ])

# get empty data table and fill it up with nan values
co_data = np.zeros((len(table_1), len(names)))
co_data[:, :] = np.nan

# loop over all galaxies and create Astropy tables
for index in range(0, len(table_1)):

    # coordinates
    coordinates = str(table_1['RAJ2000'][index]) + ' ' + str(table_1['DEJ2000'][index])
    coord_table = SkyCoord(coordinates,  unit=(u.hourangle, u.deg))
    ra = coord_table.ra.deg
    dec = coord_table.dec.deg
    co_data[index, 0] = ra
    co_data[index, 1] = dec
    # redshift
    co_data[index, 2] = table_1['z'][index]

    # get galaxy name / number
    galaxy_name = table_1['Gal'][index]

    # get mask of different emission line observation in Table 2
    mask_co21 = (table_2['Gal'] == galaxy_name) & (table_2['Line'] == 'CO(2-1)')
    mask_co32 = (table_2['Gal'] == galaxy_name) & (table_2['Line'] == 'CO(3-2)')
    mask_co43 = (table_2['Gal'] == galaxy_name) & (table_2['Line'] == 'CO(4-3)')

    # using first occurence of IR data in table 2 (be aware that galaxies can occur multiple times but
    # the second time the IR flux is set to 0)
    if sum(mask_co21) == 1:
        ir_mask = mask_co21
    elif sum(mask_co32) == 1:
        ir_mask = mask_co32
    elif sum(mask_co43) == 1:
        ir_mask = mask_co43
    else:
        ir_mask = None
    # fir IR data into table
    co_data[index, 3] = table_2['S60'][ir_mask]
    co_data[index, 4] = table_2['S100'][ir_mask]
    # flag for S100
    if table_2['l_S100'][ir_mask] == '<':
        co_data[index, 5] = 1
    else:
        co_data[index, 5] = 0
    co_data[index, 6] = table_2['logFIR'][ir_mask]

    # get 1.4 GHz flux
    co_data[index, 7] = table_2['F1_4GHz'][ir_mask]

    # fill data into co data _table
    # CO21 data
    if sum(mask_co21) > 0:
        if table_2['l_S_CO_'][mask_co21] != '<':
            # flux + error
            co_data[index, 8] = table_2['S_CO_'][mask_co21]
            co_data[index, 9] = table_2['e_S_CO_'][mask_co21]
            # velocity + error
            co_data[index, 10] = table_2['V'][mask_co21]
            co_data[index, 11] = table_2['e_V'][mask_co21]
            # delta v + error
            co_data[index, 12] = table_2['DVFWHM'][mask_co21]
            co_data[index, 13] = table_2['e_DVFWHM'][mask_co21]
            # luminosity we transform it from units of 1e10 to 1e9 to be consistent with other tables
            co_data[index, 14] = table_2['L_CO'][mask_co21] * 10
            # limit flag
            co_data[index, 15] = 0
        else:
            # flux
            co_data[index, 8] = table_2['S_CO_'][mask_co21]
            # luminosity we transform it from units of 1e10 to 1e9 to be consistent with other tables
            co_data[index, 14] = table_2['L_CO'][mask_co21] * 10
            # limit flag
            co_data[index, 15] = 1

    # CO32 data
    if sum(mask_co32) > 0:
        if table_2['l_S_CO_'][mask_co32] != '<':
            # flux + error
            co_data[index, 16] = table_2['S_CO_'][mask_co32]
            co_data[index, 17] = table_2['e_S_CO_'][mask_co32]
            # velocity + error
            co_data[index, 18] = table_2['V'][mask_co32]
            co_data[index, 19] = table_2['e_V'][mask_co32]
            # delta v + error
            co_data[index, 20] = table_2['DVFWHM'][mask_co32]
            co_data[index, 21] = table_2['e_DVFWHM'][mask_co32]
            # luminosity we transform it from units of 1e10 to 1e9 to be consistent with other tables
            co_data[index, 22] = table_2['L_CO'][mask_co32] * 10
            # limit flag
            co_data[index, 23] = 0
        else:
            # flux
            co_data[index, 16] = table_2['S_CO_'][mask_co32]
            # luminosity we transform it from units of 1e10 to 1e9 to be consistent with other tables
            co_data[index, 22] = table_2['L_CO'][mask_co32] * 10
            # limit flag
            co_data[index, 23] = 1

    # CO43 data
    if sum(mask_co43) > 0:
        if table_2['l_S_CO_'][mask_co43] != '<':
            # flux + error
            co_data[index, 24] = table_2['S_CO_'][mask_co43]
            co_data[index, 25] = table_2['e_S_CO_'][mask_co43]
            # velocity + error
            co_data[index, 26] = table_2['V'][mask_co43]
            co_data[index, 27] = table_2['e_V'][mask_co43]
            # delta v + error
            co_data[index, 28] = table_2['DVFWHM'][mask_co43]
            co_data[index, 29] = table_2['e_DVFWHM'][mask_co43]
            # luminosity we transform it from units of 1e10 to 1e9 to be consistent with other tables
            co_data[index, 30] = table_2['L_CO'][mask_co43] * 10
            # limit flag
            co_data[index, 31] = 0
        else:
            # flux
            co_data[index, 24] = table_2['S_CO_'][mask_co43]
            # luminosity we transform it from units of 1e10 to 1e9 to be consistent with other tables
            co_data[index, 30] = table_2['L_CO'][mask_co43] * 10
            # limit flag
            co_data[index, 31] = 1

    # get literature values derived in the paper
    # H2 mass and SFE
    if float(table_3[index][1]) > 0:
        # m H2
        co_data[index, 32] = float(table_3[index][1])
        # SFE
        co_data[index, 33] = float(table_3[index][2])
        # flag
        co_data[index, 34] = 0
    else:
        # m H2
        co_data[index, 32] = float(table_3[index][1]) * -1
        # SFE
        co_data[index, 33] = float(table_3[index][2]) * -1
        # flag
        co_data[index, 34] = 1

    # dust temperature and mass
    if float(table_3[index][3]) > 0:
        # temperature
        co_data[index, 35] = float(table_3[index][3])
        # mass
        co_data[index, 36] = float(table_3[index][4])
        # flag
        co_data[index, 37] = 0
    else:
        # temperature
        co_data[index, 35] = float(table_3[index][3]) * -1
        # mass
        co_data[index, 36] = float(table_3[index][4]) * -1
        # flag
        co_data[index, 37] = 1

    # half-light ratio
    co_data[index, 38] = float(table_3[index][5])
    # stellar mass (Here we use the same mask routine as for the IR measurment)
    co_data[index, 39] = table_2['logM_'][ir_mask]



co_data_table = QTable(data=co_data, names=names)

# add galaxy identifier column and galaxy type column
galaxy_type = Column(np.array([table_3[:, 6]], dtype=object).T, name='literature_galaxy_type',  dtype='S8')
# stack all columns
co_data_table = hstack([table_1['Gal'], co_data_table, galaxy_type])
# rename Gal column
co_data_table.rename_column('Gal', 'literature name')
# remove nana values in the galaxy type
co_data_table['literature_galaxy_type'][co_data_table['literature_galaxy_type'] == 'nan'] = ' '

# set units
co_data_table['ra'].unit = u.deg
co_data_table['dec'].unit = u.deg

co_data_table['S60'].unit = u.Jy
co_data_table['S100'].unit = u.Jy
co_data_table['log_L_FIR'].unit = u.L_sun
co_data_table['flux_1_4GHz'].unit = u.mJy

co_data_table['flux_co21'].unit = u.Jy * u.km / u.s
co_data_table['flux_co21_err'].unit = u.Jy * u.km / u.s
co_data_table['vel_co21'].unit = u.km / u.s
co_data_table['vel_co21_err'].unit = u.km / u.s
co_data_table['delta_v_co21'].unit = u.km / u.s
co_data_table['delta_v_co21_err'].unit = u.km / u.s
co_data_table['L_co21'].unit = 1e9 * u.K * u.km / u.s * u.pc**2

co_data_table['flux_co32'].unit = u.Jy * u.km / u.s
co_data_table['flux_co32_err'].unit = u.Jy * u.km / u.s
co_data_table['vel_co32'].unit = u.km / u.s
co_data_table['vel_co32_err'].unit = u.km / u.s
co_data_table['delta_v_co32'].unit = u.km / u.s
co_data_table['delta_v_co32_err'].unit = u.km / u.s
co_data_table['L_co32'].unit = 1e9 * u.K * u.km / u.s * u.pc**2

co_data_table['flux_co43'].unit = u.Jy * u.km / u.s
co_data_table['flux_co43_err'].unit = u.Jy * u.km / u.s
co_data_table['vel_co43'].unit = u.km / u.s
co_data_table['vel_co43_err'].unit = u.km / u.s
co_data_table['delta_v_co43'].unit = u.km / u.s
co_data_table['delta_v_co43_err'].unit = u.km / u.s
co_data_table['L_co43'].unit = 1e9 * u.K * u.km / u.s * u.pc**2

co_data_table['literature_m_h2'].unit = 1e9 * u.M_sun
co_data_table['literature_SFE'].unit = u.L_sun / u.M_sun
co_data_table['literature_T_d'].unit = u.K
co_data_table['literature_M_d'].unit = 1e8 * u.M_sun
co_data_table['literature_R12'].unit = u.kpc
co_data_table['literature_log_m_star'].unit = u.M_sun

# print final table
print(co_data_table)

final_table_access = analysis_tools.AnalysisTools(table=co_data_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'combes_2013'):
    os.makedirs(final_table_access.data_path / 'tables' / 'combes_2013')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'combes_2013' / 'combes_2013.fits',
                               overwrite=True)

