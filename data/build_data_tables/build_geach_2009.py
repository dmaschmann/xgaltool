import numpy as np
import os

from galtool import analysis_tools
from astropy.table import Table, QTable, hstack, Column
from astropy import units as u

"""
This macro will create an object table of the observation sample published by 
Geach et al. 2009 doi:10.1111/j.1745-3933.2009.00642.x
We use Table 1 
"""

# get galtool access
empty_access = analysis_tools.AnalysisTools(object_type='empty')
# load Table 1
geach_2009_table_1 = np.genfromtxt(empty_access.project_path / 'data/co_data/geach_2009/geach_2009_table_1.dat', dtype=object)


# 'literature name'
names = np.array(['ra', 'dec', 'z_ned',
                  'L_FIR', 'L_FIR_err', 'literature_sfr', 'literature_sfr_err',
                  'L_co10', 'L_co10_err', 'literature_m_h2', 'literature_m_h2_err'])
co_data = np.zeros((2, len(names)))

for index in range(0, len(geach_2009_table_1)):
    # ra dec and z
    co_data[index, 0] = float(geach_2009_table_1[index, 3])
    co_data[index, 1] = float(geach_2009_table_1[index, 4])
    co_data[index, 2] = float(geach_2009_table_1[index, 5])
    # FIR luminosity
    co_data[index, 3] = float(geach_2009_table_1[index, 6])
    co_data[index, 4] = float(geach_2009_table_1[index, 8])
    # SFR
    co_data[index, 5] = float(geach_2009_table_1[index, 9])
    co_data[index, 6] = float(geach_2009_table_1[index, 11])
    # L_CO10 and transform it to 1e9
    co_data[index, 7] = float(geach_2009_table_1[index, 17]) * 10
    co_data[index, 8] = float(geach_2009_table_1[index, 19]) * 10
    # MH2
    co_data[index, 9] = float(geach_2009_table_1[index, 20])
    co_data[index, 10] = float(geach_2009_table_1[index, 22])


co_data_table = QTable(data=co_data, names=names)

# add column with identifier
co_data_table.add_column(np.array(geach_2009_table_1[:, 0], dtype='S20'), name='literature name', index=0)

co_data_table['ra'].unit = u.deg
co_data_table['dec'].unit = u.deg

co_data_table['L_FIR'].unit = 1e11 * u.L_sun
co_data_table['L_FIR_err'].unit = 1e11 * u.L_sun
co_data_table['literature_sfr'].unit = u.M_sun / u.yr
co_data_table['literature_sfr_err'].unit = u.M_sun / u.yr
co_data_table['L_co10'].unit = 1e9 * u.K * u.km / u.s
co_data_table['L_co10_err'].unit = 1e9 * u.K * u.km / u.s
co_data_table['literature_m_h2'].unit = 1e9 * u.M_sun
co_data_table['literature_m_h2_err'].unit = 1e9 * u.M_sun

print(co_data_table)

final_table_access = analysis_tools.AnalysisTools(table=co_data_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'geach_2009'):
    os.makedirs(final_table_access.data_path / 'tables' / 'geach_2009')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'geach_2009' /
                               'geach_2009.fits', overwrite=True)
