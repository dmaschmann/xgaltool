import numpy as np
import os

from galtool import analysis_tools
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.table import Table, QTable, hstack, Column


"""
This macro will create an galaxy table for the galtool package of the 1.4 GHz radio table in the COSMOS deep field
Schinnerer et al. (2007)  10.1086/516587
We ascii table taken from   https://irsa.ipac.caltech.edu/data/COSMOS/tables/vla/vla-cosmos_lp_sources_v2_20080615.tbl
"""

empty_access = analysis_tools.AnalysisTools(object_type='empty')

radio_table = np.genfromtxt(empty_access.project_path / 'data/radio_data/schinnerer_2007/vla-cosmos_lp_sources_v2_20080615.tbl', dtype=object)

print(radio_table.shape)

galaxy_names = radio_table[:, 0]
ra = radio_table[:, 20]
dec = radio_table[:, 21]
peak_flux_vla = radio_table[:, 9]
peak_flux_err_vla = radio_table[:, 10]
int_flux_err_vla = radio_table[:, 11]
int_flux_vla = radio_table[:, 12]
rms_vla = radio_table[:, 13]

galaxy_name_column = Column(np.array(galaxy_names, dtype=object).T, name='galaxy_name_vla',  dtype='S30')
ra_column = Column(np.array(ra, dtype=object).T, name='ra',  dtype=float)
dec_column = Column(np.array(dec, dtype=object).T, name='dec',  dtype=float)
peak_flux_vla_column = Column(np.array(peak_flux_vla, dtype=object).T, name='peak_flux_vla',  dtype=float)
peak_flux_err_vla_column = Column(np.array(peak_flux_err_vla, dtype=object).T, name='peak_flux_err_vla',  dtype=float)
int_flux_err_vla_column = Column(np.array(int_flux_err_vla, dtype=object).T, name='int_flux_err_vla',  dtype=float)
int_flux_vla_column = Column(np.array(int_flux_vla, dtype=object).T, name='int_flux_vla',  dtype=float)
rms_vla_column = Column(np.array(rms_vla, dtype=object).T, name='rms_vla',  dtype=float)

radio_data_table = hstack([galaxy_name_column, ra_column, dec_column, peak_flux_vla_column, peak_flux_err_vla_column,
                           int_flux_err_vla_column, int_flux_vla_column, rms_vla_column])


# print final table
print(radio_data_table)

final_table_access = analysis_tools.AnalysisTools(table=radio_data_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'schinnerer_2007'):
    os.makedirs(final_table_access.data_path / 'tables' / 'schinnerer_2007')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'schinnerer_2007' / 'schinnerer_2007.fits',
                               overwrite=True)

