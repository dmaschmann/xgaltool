import numpy as np
import os

from galtool import analysis_tools, coordinate_utils
from astropy.table import Table
import astropy.units as u
from scipy import constants

"""
This macro will create an object table of the observation sample published by Young et al. 2011 
doi:10.1111/j.1365-2966.2011.18561.x
Table 1
In fact this table is an assembly from: 
Young et al. (2011), Combes et al. (2007), Welch & Sage (2003), 
Sage et al. (2007), Wiklind et al. (1995), Young (2002), and Welch et al.(2010)



this was created very quick qith more patience we should include more measurements from Combes et al 2007


"""

empty_access = analysis_tools.AnalysisTools(object_type='empty')

atlas_3d_galaxy_list = np.genfromtxt(empty_access.project_path /
                                     'data/co_data/Atlas_3D/Cappellari2011a_Atlas3D_Paper1_Table3.txt', dtype=object)
atlas_3d_molecular_gas = np.genfromtxt(empty_access.project_path /
                                       'data/co_data/Atlas_3D/Young2011_Atlas3D_Paper4_Table1.txt', dtype=object)

print(len(atlas_3d_galaxy_list))
print(len(atlas_3d_molecular_gas))


names = np.array(['ra', 'dec', 'z_ned',
                  'flux_co10', 'flux_co10_err',
                  'L_co10', 'L_co10_err', 'limit_flag_co10',
                  'flux_co21', 'flux_co21_err',
                  'L_co21', 'L_co21_err', 'limit_flag_co21',
                  'literature_m_h2', 'literature_m_h2_err', 'literature_limit_flag_m_h2'])

co_data = np.zeros((len(atlas_3d_galaxy_list), len(names)))
co_data[:, :] = np.nan


for index, galaxy_name in zip(range(len(atlas_3d_galaxy_list)), atlas_3d_galaxy_list[:, 0]):

    # skip the A option. there is one merger which is not findable in the NED data base
    if str(galaxy_name, 'utf-8')[-1] == 'A':
        continue
    mask_co_data_file = atlas_3d_molecular_gas[:, 0] == galaxy_name
    mask_identifier_file = atlas_3d_galaxy_list[:, 0] == galaxy_name

    ra = float(atlas_3d_galaxy_list[mask_identifier_file][0, 1])
    dec = float(atlas_3d_galaxy_list[mask_identifier_file][0, 2])
    redshift = float(atlas_3d_galaxy_list[mask_identifier_file][0, 6]) / (constants.c * 1e-3)
    distance = float(atlas_3d_galaxy_list[mask_identifier_file][0, 7])

    # print(str(galaxy_name, 'utf-8'), ra, dec, redshift)
    co_data[index, 0] = ra
    co_data[index, 1] = dec
    co_data[index, 2] = redshift

    # get literature H2 mass
    if atlas_3d_molecular_gas[mask_co_data_file][0, 9] == b'=':
        co_data[index, 13] = float(atlas_3d_molecular_gas[mask_co_data_file][0, 14])
        co_data[index, 14] = float(atlas_3d_molecular_gas[mask_co_data_file][0, 15])
        co_data[index, 15] = 0
    else:
        co_data[index, 13] = float(atlas_3d_molecular_gas[mask_co_data_file][0, 14])
        co_data[index, 15] = 1

    # get CO luminosities
    # If there is no flux this measurement is from another source
    if atlas_3d_molecular_gas[mask_co_data_file][0, 5] == b'...':
        continue

    # get CO10 flux
    if atlas_3d_molecular_gas[mask_co_data_file][0, 9] == b'=':
        co_data[index, 3] = float(atlas_3d_molecular_gas[mask_co_data_file][0, 5])
        co_data[index, 4] = float(atlas_3d_molecular_gas[mask_co_data_file][0, 6])
        co_data[index, 7] = 0

    else:
        co_data[index, 3] = float(atlas_3d_molecular_gas[mask_co_data_file][0, 6]) * 3
        co_data[index, 7] = 1
    co_data[index, 5] = empty_access.compute_co_luminosity(s_co_vel=co_data[index, 3], luminosity_dist=distance,
                                                           redshift=0,
                                                           observed_co_frequency=empty_access.co10_line_frequency,
                                                           conversion_factor=5)

    # get CO21 flux
    if float(atlas_3d_molecular_gas[mask_co_data_file][0, 7]) > 0:
        if (float(atlas_3d_molecular_gas[mask_co_data_file][0, 7]) /
                float(atlas_3d_molecular_gas[mask_co_data_file][0, 8]) > 3):
            co_data[index, 8] = float(atlas_3d_molecular_gas[mask_co_data_file][0, 7])
            co_data[index, 9] = float(atlas_3d_molecular_gas[mask_co_data_file][0, 8])
            co_data[index, 12] = 0

        else:
            co_data[index, 8] = float(atlas_3d_molecular_gas[mask_co_data_file][0, 8]) * 3
            co_data[index, 12] = 1
        co_data[index, 10] = empty_access.compute_co_luminosity(s_co_vel=co_data[index, 8], luminosity_dist=distance,
                                                                redshift=0,
                                                                observed_co_frequency=empty_access.co21_line_frequency,
                                                                conversion_factor=5)



final_table = Table(data=co_data, names=names)

# add units to table
final_table['ra'].unit = u.deg
final_table['dec'].unit = u.deg

final_table['flux_co10'].unit = u.Jy * u.km / u.s
final_table['flux_co10_err'].unit = u.Jy * u.km / u.s
final_table['L_co10'].unit = 1e9 * u.K * u.km / u.s * u.pc**2
final_table['L_co10_err'].unit = 1e9 * u.K * u.km / u.s * u.pc**2

final_table['flux_co21'].unit = u.Jy * u.km / u.s
final_table['flux_co21_err'].unit = u.Jy * u.km / u.s
final_table['L_co21'].unit = 1e9 * u.K * u.km / u.s * u.pc**2
final_table['L_co21_err'].unit = 1e9 * u.K * u.km / u.s * u.pc**2

final_table['literature_m_h2'].unit = 1e9 * u.M_sun
final_table['literature_m_h2_err'].unit = 1e9 * u.M_sun

final_table.add_column(np.array(atlas_3d_galaxy_list[:, 0], dtype='S20'), name='Galaxy_name', index=0)

# get rif of nan value
bad_index = np.where(np.isnan(final_table['literature_limit_flag_m_h2']))
print(bad_index[0][0])
final_table.remove_row(index=bad_index[0][0])



print(final_table)

final_table_access = analysis_tools.AnalysisTools(table=final_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'young_2011'):
    os.makedirs(final_table_access.data_path / 'tables' / 'young_2011')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'young_2011' /
                               'young_2011_atlas3d_co_sample.fits', overwrite=True)

