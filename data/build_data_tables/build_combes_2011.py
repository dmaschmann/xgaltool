import numpy as np
import os

from galtool import analysis_tools
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.table import QTable, hstack, vstack, Column

"""
This macro will create an galaxy table for the galtool package of the CO observation published in 
Combes et al. (2011) DOI:10.1051/0004-6361/201015739   
We are using Table 1, 2 and 3 of the Publication.
"""

# get galtool access
empty_access = analysis_tools.AnalysisTools(object_type='empty')

# get galaxy coordinates and data
# first table is only coordinates and redshift
combes_2011_table_1 = np.genfromtxt(empty_access.project_path / 'data/co_data/combes_2011/combes_2011_table_1.dat',
                                    dtype=object)
# second table has all observation parameters of CO
combes_2011_table_2 = np.genfromtxt(empty_access.project_path / 'data/co_data/combes_2011/combes_2011_table_2.dat',
                                    dtype=object)
# third table has H2 masses and other values like dust maps
combes_2011_table_3 = np.genfromtxt(empty_access.project_path / 'data/co_data/combes_2011/combes_2011_table_3.dat',
                                    dtype=object)
# additional table provided by the author with metadata like depletion time and half light radius
combes_2011_additional_data = np.genfromtxt(empty_access.project_path /
                                            'data/co_data/combes_2011/combes_2011_additional_data.dat',  dtype=float)

print('length table 1 ', len(combes_2011_table_1))
print('length table 2 ', len(combes_2011_table_2))
print('length table 3 ', len(combes_2011_table_3))
print('length table 3 ', len(combes_2011_additional_data))

# get names for data table
names = np.array(['ra', 'dec', 'z_ned', 'S60', 'S100', 'log_L_FIR',

                  'flux_co10', 'flux_co10_err', 'vel_co10', 'vel_co10_err',
                  'delta_v_co10', 'delta_v_co10_err', 'L_co10', 'limit_flag_co10',

                  'flux_co21', 'flux_co21_err', 'vel_co21', 'vel_co21_err',
                  'delta_v_co21', 'delta_v_co21_err', 'L_co21', 'limit_flag_co21',

                  'flux_co32', 'flux_co32_err', 'vel_co32', 'vel_co32_err',
                  'delta_v_co32', 'delta_v_co32_err', 'L_co32', 'limit_flag_co32',

                  'literature_m_h2', 'literature_SFE', 'literature_limit_flag_m_h2',
                  'literature_T_d', 'literature_M_d', 'literature_limit_flag_dust',

                  'literature_log_m_star', 'literature_log_sfr', 'literature_log_t_depl', 'literature_R12'
                  ])

# space holder for final table
final_table = None

# loop over all galaxies and create Astropy tables
for index in range(0, len(combes_2011_table_1)):

    # get galaxy identifier
    galaxy_identifier = str(combes_2011_table_1[index, 0], 'utf-8')
    # get the index for the additional data_table
    index_additional_data_table = combes_2011_additional_data[:, 0] == float(galaxy_identifier.replace('G', ''))
    # get ra and dec in degree and redshift
    ra_raw = combes_2011_table_1[index, 1]
    dec_raw = combes_2011_table_1[index, 2]
    coordinates = str(ra_raw, 'utf-8') + ' ' + str(dec_raw, 'utf-8')
    coord_table = SkyCoord(coordinates,  unit=(u.hourangle, u.deg))
    ra = coord_table.ra.deg
    dec = coord_table.dec.deg
    redshift = float(combes_2011_table_1[index, 3])
    # print values
    print('galaxy identifier = ', galaxy_identifier, 'coordinates ', ra_raw, dec_raw)
    print('Coordinates in deg = ', ra, dec, ', redshift = ', redshift)

    # create column with galaxy identifier used in publication
    name_column = Column(np.array([galaxy_identifier]), name='literature_name', dtype='S8')

    # create empty CO table
    co_data = np.zeros(len(names))
    co_data[:] = np.nan  # fill with NaN values

    # get mask for observation in CO10
    data_table_co10_index = ((combes_2011_table_2[:, 0] == combes_2011_table_1[index, 0]) &
                             (combes_2011_table_2[:, 1] == b'CO(1-0)'))
    # get mask for observation in CO21
    data_table_co21_index = ((combes_2011_table_2[:, 0] == combes_2011_table_1[index, 0]) &
                             (combes_2011_table_2[:, 1] == b'CO(2-1)'))
    # get mask for observation in CO32
    data_table_co32_index = ((combes_2011_table_2[:, 0] == combes_2011_table_1[index, 0]) &
                             (combes_2011_table_2[:, 1] == b'CO(3-2)'))

    # fill coordinates and redshift into table
    co_data[0] = ra
    co_data[1] = dec
    co_data[2] = redshift

    # fill IR data
    single_table_element = np.where((data_table_co10_index + data_table_co21_index + data_table_co32_index) ==
                                    True)[0][0]
    co_data[3] = float(combes_2011_table_2[single_table_element, 13])
    co_data[4] = float(combes_2011_table_2[single_table_element, 14])
    co_data[5] = float(combes_2011_table_2[single_table_element, 15])

    # fill data into co data _table
    # CO10 data
    if sum(data_table_co10_index) > 0:
        if float(combes_2011_table_2[data_table_co10_index, 3]) > 0:

            # flux + error
            co_data[6] = float(combes_2011_table_2[data_table_co10_index, 3])
            co_data[7] = float(combes_2011_table_2[data_table_co10_index, 5])
            # velocity + error
            co_data[8] = float(combes_2011_table_2[data_table_co10_index, 6])
            co_data[9] = float(combes_2011_table_2[data_table_co10_index, 8])
            # delta v + error
            co_data[10] = float(combes_2011_table_2[data_table_co10_index, 9])
            co_data[11] = float(combes_2011_table_2[data_table_co10_index, 11])
            # luminosity we transform it from units of 1e10 to 1e9 to be consistent with other tables
            co_data[12] = float(combes_2011_table_2[data_table_co10_index, 12]) * 10
            # limit flag
            co_data[13] = 0

        else:
            # flux + error
            co_data[6] = float(combes_2011_table_2[data_table_co10_index, 3]) * -1
            # luminosity we transform it from units of 1e10 to 1e9 to be consistent with other tables
            co_data[12] = float(combes_2011_table_2[data_table_co10_index, 12]) * -1 * 10
            # limit flag
            co_data[13] = 1

    # CO21 data
    if sum(data_table_co21_index) > 0:
        if float(combes_2011_table_2[data_table_co21_index, 3]) > 0:

            # flux + error
            co_data[14] = float(combes_2011_table_2[data_table_co21_index, 3])
            co_data[15] = float(combes_2011_table_2[data_table_co21_index, 5])
            # velocity + error
            co_data[16] = float(combes_2011_table_2[data_table_co21_index, 6])
            co_data[17] = float(combes_2011_table_2[data_table_co21_index, 8])
            # delta v + error
            co_data[18] = float(combes_2011_table_2[data_table_co21_index, 9])
            co_data[19] = float(combes_2011_table_2[data_table_co21_index, 11])
            # luminosity we transform it from units of 1e10 to 1e9 to be consistent with other tables
            co_data[20] = float(combes_2011_table_2[data_table_co21_index, 12]) * 10
            # limit flag
            co_data[21] = 0
        else:
            # flux + error
            co_data[14] = float(combes_2011_table_2[data_table_co21_index, 3]) * -1
            # luminosity we transform it from units of 1e10 to 1e9 to be consistent with other tables
            co_data[20] = float(combes_2011_table_2[data_table_co21_index, 12]) * -1 * 10
            # limit flag
            co_data[21] = 1

    # CO32 data
    if sum(data_table_co32_index) > 0:
        if float(combes_2011_table_2[data_table_co32_index, 3]) > 0:

            # flux + error
            co_data[22] = float(combes_2011_table_2[data_table_co32_index, 3])
            co_data[23] = float(combes_2011_table_2[data_table_co32_index, 5])
            # velocity + error
            co_data[24] = float(combes_2011_table_2[data_table_co32_index, 6])
            co_data[25] = float(combes_2011_table_2[data_table_co32_index, 8])
            # delta v + error
            co_data[26] = float(combes_2011_table_2[data_table_co32_index, 9])
            co_data[27] = float(combes_2011_table_2[data_table_co32_index, 11])
            # luminosity we transform it from units of 1e10 to 1e9 to be consistent with other tables
            co_data[28] = float(combes_2011_table_2[data_table_co32_index, 12]) * 10
            # limit flag
            co_data[29] = 0

        else:
            # flux + error
            co_data[22] = float(combes_2011_table_2[data_table_co32_index, 3]) * -1
            # luminosity we transform it from units of 1e10 to 1e9 to be consistent with other tables
            co_data[28] = float(combes_2011_table_2[data_table_co32_index, 12]) * -1 * 10
            # limit flag
            co_data[29] = 1

    # fill  data
    # H2 mass and SFE
    co_data[30] = np.abs(float(combes_2011_table_3[index, 1]))
    co_data[31] = np.abs(float(combes_2011_table_3[index, 2]))
    if float(combes_2011_table_3[index, 1]) < 0:
        co_data[32] = 1
    else:
        co_data[32] = 0

    # dust temperature and mass
    co_data[33] = np.abs(float(combes_2011_table_3[index, 3]))
    co_data[34] = np.abs(float(combes_2011_table_3[index, 4]))
    if float(combes_2011_table_3[index, 1]) < 0:
        co_data[35] = 1
    else:
        co_data[35] = 0

    # get column with galaxy type
    if combes_2011_table_3[index, 5] != b'nan':
        galaxy_type = Column(np.array([str(combes_2011_table_3[index, 5], 'utf-8')]), name='literature_galaxy_type',
                             dtype='S8')
    else:
        galaxy_type = Column(np.array(['']), name='literature_galaxy_type',
                             dtype='S8')

    # add meta data
    if sum(index_additional_data_table) == 1:
        # log m_star
        co_data[36] = combes_2011_additional_data[index_additional_data_table, 3]
        # log sfr
        co_data[37] = combes_2011_additional_data[index_additional_data_table, 2]
        # t_depl
        co_data[38] = combes_2011_additional_data[index_additional_data_table, 7]
        # half_light radius
        co_data[39] = combes_2011_additional_data[index_additional_data_table, 8]

    co_data_table = QTable(data=co_data, names=names)

    co_data_table = hstack([name_column, co_data_table, galaxy_type])


    if final_table is None:
        final_table = co_data_table
    else:
        final_table = vstack([final_table, co_data_table])

# # add units to table
# final_table['ra'].unit = u.deg
# final_table['dec'].unit = u.deg

final_table['S60'].unit = u.Jy
final_table['S100'].unit = u.Jy
final_table['log_L_FIR'].unit = u.L_sun

final_table['flux_co10'].unit = u.Jy * u.km / u.s
final_table['flux_co10_err'].unit = u.Jy * u.km / u.s
final_table['vel_co10'].unit = u.km / u.s
final_table['vel_co10_err'].unit = u.km / u.s
final_table['delta_v_co10'].unit = u.km / u.s
final_table['delta_v_co10_err'].unit = u.km / u.s
final_table['L_co10'].unit = 1e9 * u.K * u.km / u.s

final_table['flux_co21'].unit = u.Jy * u.km / u.s
final_table['flux_co21_err'].unit = u.Jy * u.km / u.s
final_table['vel_co21'].unit = u.km / u.s
final_table['vel_co21_err'].unit = u.km / u.s
final_table['delta_v_co21'].unit = u.km / u.s
final_table['delta_v_co21_err'].unit = u.km / u.s
final_table['L_co21'].unit = 1e9 * u.K * u.km / u.s

final_table['flux_co32'].unit = u.Jy * u.km / u.s
final_table['flux_co32_err'].unit = u.Jy * u.km / u.s
final_table['vel_co32'].unit = u.km / u.s
final_table['vel_co32_err'].unit = u.km / u.s
final_table['delta_v_co32'].unit = u.km / u.s
final_table['delta_v_co32_err'].unit = u.km / u.s
final_table['L_co32'].unit = 1e9 * u.K * u.km / u.s

final_table['literature_m_h2'].unit = 1e9 * u.M_sun
final_table['literature_SFE'].unit = u.L_sun / u.M_sun
final_table['literature_T_d'].unit = u.K
final_table['literature_M_d'].unit = 1e8 * u.M_sun

final_table['literature_log_m_star'].unit = u.M_sun
final_table['literature_log_sfr'].unit = u.M_sun / u.yr
final_table['literature_log_t_depl'].unit = u.yr
final_table['literature_R12'].unit = u.arcsec

# print final table
print(final_table)

# create galtool object
final_table_access = analysis_tools.AnalysisTools(table=final_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'combes_2011'):
    os.makedirs(final_table_access.data_path / 'tables' / 'combes_2011')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'combes_2011' / 'combes_2011.fits',
                               overwrite=True)

