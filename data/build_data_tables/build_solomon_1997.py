import numpy as np
import os

from xgaltool import analysis_tools, coordinate_utils
from astropy.table import Table
import astropy.units as u
from astropy.coordinates import SkyCoord, FK4, FK5, Galactic
from scipy import constants

"""
This macro will create an galaxy table for the galtool package of the CO observation published in 
Solomon et al. (1997) ApJ 478 144

We are using Table 1 and 2 of the Publication.
"""

# get empty galtool object to access data structure
empty_access = analysis_tools.AnalysisTools(object_type='empty')
# load publication tables
solomon_1997_table_1 = np.genfromtxt(empty_access.project_path / 'data/co_data/solomon_1997/solomon_1997_table_1.txt',
                                   dtype=object)

solomon_1997_table_2 = np.genfromtxt(empty_access.project_path / 'data/co_data/solomon_1997/solomon_1997_table_2.txt',
                                   dtype=float)

# print('length of iras name table ', len(solomon_1997_iras_names))
print('length of table 1 ', len(solomon_1997_table_1))
print('length of table 3 ', len(solomon_1997_table_2))

# names for final data table
names = np.array(['ra', 'dec', 'z_ned', 'S25',  'S60', 'S100', 'L_FIR',
                  'flux_co10', 'delta_v_co10', 'L_co10', 'literature_m_h2'])
# get empty data tables
co_data = np.zeros((len(solomon_1997_table_1), len(names)))
co_data[:, :] = np.nan
# empty table for galaxy names
galaxy_names = np.zeros(len(solomon_1997_table_1), dtype='S20')

# file tables with data by looping over each galaxy
for index in range(0, len(solomon_1997_table_1)):
    # compose IRAS name
    galaxy_names[index] = str(solomon_1997_table_1[index, 0], 'utf-8')
    # get coordinates and redshift
    ra_raw = solomon_1997_table_1[index, 1]
    dec_raw = solomon_1997_table_1[index, 2]
    coordinates = str(ra_raw, 'utf-8') + ' ' + str(dec_raw, 'utf-8')

    c1 = SkyCoord(coordinates,  unit=(u.hourangle, u.deg), frame=FK4)
    c2 = c1.transform_to(FK5(equinox='J2000'))
    ra = c2.ra.deg
    dec = c2.dec.deg
    # print(ra, dec)
    # exit()

    # coord_table = SkyCoord(coordinates,  unit=(u.hourangle, u.deg), equinox='B1950')
    # ra = coord_table.ra.deg
    # dec = coord_table.dec.deg
    redshift = float(solomon_1997_table_1[index, 3]) / (constants.c * 1e-3)
    # ra, dec, redshift = coordinate_utils.get_galaxy_ned_coords_and_z(galaxy_name=galaxy_names[index])
    print(ra, dec, redshift)
    # fill data
    co_data[index, 0] = ra
    co_data[index, 1] = dec
    co_data[index, 2] = redshift
    # IR flux
    co_data[index, 3] = solomon_1997_table_2[index, 3]
    co_data[index, 4] = solomon_1997_table_2[index, 4]
    co_data[index, 5] = solomon_1997_table_2[index, 5]
    co_data[index, 6] = solomon_1997_table_2[index, 7]

    # get flux, luminosity and limit flag for the CO10 measurements
    co_data[index, 7] = solomon_1997_table_1[index, 5]
    co_data[index, 8] = solomon_1997_table_1[index, 4]
    co_data[index, 9] = solomon_1997_table_2[index, 1]
    co_data[index, 10] = solomon_1997_table_2[index, 2] * 10  # change to units of 1e9

# create table and combine them with name coumn
co_data_table = Table(data=co_data, names=names)
co_data_table.add_column(galaxy_names, name='literature_name', index=0)

# add units
co_data_table['ra'].unit = u.deg
co_data_table['dec'].unit = u.deg

co_data_table['S25'].unit = u.Jy
co_data_table['S60'].unit = u.Jy
co_data_table['S100'].unit = u.Jy
co_data_table['L_FIR'].unit = 1e12 * u.L_sun
co_data_table['flux_co10'].unit = u.Jy * u.km / u.s
co_data_table['delta_v_co10'].unit = u.km / u.s
co_data_table['L_co10'].unit = 1e9 * u.K * u.km / u.s * u.pc**2
co_data_table['literature_m_h2'].unit = 1e9 * u.M_sun

print(co_data_table)

exit()

final_table_access = analysis_tools.AnalysisTools(table=co_data_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'solomon_1997'):
    os.makedirs(final_table_access.data_path / 'tables' / 'solomon_1997')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'solomon_1997' /
                               'solomon_1997.fits', overwrite=True)
