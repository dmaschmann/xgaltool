import numpy as np
import os

from xgaltool import analysis_tools, coordinate_utils
from astropy.table import Table
import astropy.units as u
from astropy.coordinates import SkyCoord
from scipy import constants

"""
This macro will create an galaxy table for the galtool package of the CO observation published in 
Chung et al. (2009) doi:10.1088/0004-637X/768/2/132

We are using Table 1 and 3 of the Publication.
"""

# get empty galtool object to access data structure
empty_access = analysis_tools.AnalysisTools(object_type='empty')
# load publication tables
# chung_2009_iras_names = np.genfromtxt(empty_access.project_path / 'data/co_data/chung_2009/chung_2009_iras_names.txt',
#                                       dtype='S20')


chung_2009_table_1 = np.genfromtxt(empty_access.project_path / 'data/co_data/chung_2009/chung_2009_table_1.txt',
                                   dtype=object)

chung_2009_table_3 = np.genfromtxt(empty_access.project_path / 'data/co_data/chung_2009/chung_2009_table_3.txt',
                                   dtype=float)

# print('length of iras name table ', len(chung_2009_iras_names))
print('length of table 1 ', len(chung_2009_table_1))
print('length of table 3 ', len(chung_2009_table_3))


# names for final data table
names = np.array(['ra', 'dec', 'z_ned', 'S60', 'S100', 'L_FIR', 'flux_co10', 'L_co10', 'limit_flag_co10', 'literature_d_opt'])
# get empty data tables
co_data = np.zeros((len(chung_2009_table_1), len(names)))
co_data[:, :] = np.nan
# empty table for galaxy names
galaxy_names = np.zeros(len(chung_2009_table_1), dtype='S20')

# file tables with data by looping over each galaxy
for index in range(0, len(chung_2009_table_1)):
    # compose IRAS name
    galaxy_names[index] = 'IRAS' + str(chung_2009_table_1[index, 0], 'utf-8')
    # get coordinates and redshift
    ra_raw = chung_2009_table_1[index, 2]
    dec_raw = chung_2009_table_1[index, 3]
    coordinates = str(ra_raw, 'utf-8') + ' ' + str(dec_raw, 'utf-8')
    coord_table = SkyCoord(coordinates,  unit=(u.hourangle, u.deg))
    ra = coord_table.ra.deg
    dec = coord_table.dec.deg
    redshift = float(chung_2009_table_1[index, 4]) / (constants.c * 1e-3)
    # ra, dec, redshift = coordinate_utils.get_galaxy_ned_coords_and_z(galaxy_name=galaxy_names[index])
    print(ra, dec, redshift)
    # fill data
    co_data[index, 0] = ra
    co_data[index, 1] = dec
    co_data[index, 2] = redshift
    # IR flux
    co_data[index, 3] = chung_2009_table_3[index, 5]
    co_data[index, 4] = chung_2009_table_3[index, 6]
    co_data[index, 5] = chung_2009_table_3[index, 7]
    # get flux, luminosity and limit flag for the CO10 measurements (note that limits are stated as negative values)
    if chung_2009_table_3[index, 1] < 0:
        co_data[index, 6] = chung_2009_table_3[index, 1] * -1
        co_data[index, 7] = chung_2009_table_3[index, 3]
        co_data[index, 8] = 1

    else:
        co_data[index, 6] = chung_2009_table_3[index, 1]
        co_data[index, 7] = chung_2009_table_3[index, 3]
        co_data[index, 8] = 0

    co_data[index, 9] = chung_2009_table_1[index, 6]

# create table and combine them with name coumn
co_data_table = Table(data=co_data, names=names)
co_data_table.add_column(galaxy_names, name='iras name', index=0)

co_data_table.add_column(np.array(chung_2009_table_1[:, 9], dtype='S20'), name='literature_galaxy_type', index=-1)

# add units
co_data_table['ra'].unit = u.deg
co_data_table['dec'].unit = u.deg

co_data_table['S60'].unit = u.Jy
co_data_table['S100'].unit = u.Jy
co_data_table['L_FIR'].unit = 1e12 * u.L_sun
co_data_table['flux_co10'].unit = u.Jy * u.km / u.s
co_data_table['L_co10'].unit = 1e9 * u.K * u.km / u.s * u.pc**2
co_data_table['literature_d_opt'].unit = u.arcsec

print(co_data_table)

final_table_access = analysis_tools.AnalysisTools(table=co_data_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'chung_2009'):
    os.makedirs(final_table_access.data_path / 'tables' / 'chung_2009')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'chung_2009' /
                               'chung_2009.fits', overwrite=True)
