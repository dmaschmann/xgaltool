import numpy as np
import os

from galtool import analysis_tools
from astropy.table import QTable, hstack, vstack, Column

from astropy.coordinates import SkyCoord
from astropy import units as u
from scipy import constants

"""
This macro will create an galaxy table for the galtool package of the CO observation published in 
Freundlich et al. (2019) DOI:10.1051/0004-6361/201732223

We are using Table 1, 2, 3 and 4 of the Publication.
"""

# get galtool access
empty_access = analysis_tools.AnalysisTools(object_type='empty')

# first table with coordinates redshift stellar mass and srf
data_table_1 = np.genfromtxt(empty_access.project_path / 'data/co_data/phibss2/freundlich_2019_table_1.txt',
                             dtype=object)

# second table with beam size and observation meta data
data_table_2 = np.genfromtxt(empty_access.project_path / 'data/co_data/phibss2/freundlich_2019_table_2.txt',
                             dtype=object)

# third table with CO estimation and molecular gas mass
data_table_3 = np.genfromtxt(empty_access.project_path / 'data/co_data/phibss2/freundlich_2019_table_3.txt',
                             dtype=object)
# fourth table consists of sersic fit to HST I-band image
data_table_4 = np.genfromtxt(empty_access.project_path / 'data/co_data/phibss2/freundlich_2019_table_4.txt',
                             dtype=object)

print('length table 1 ', len(data_table_1))
print('length table 2 ', len(data_table_2))
print('length table 3 ', len(data_table_3))
print('length table 4 ', len(data_table_4))


# get names for data table
names = np.array(['ra', 'dec', 'ra_co_peak', 'dec_co_peak', 'z_ned', 'major_beam_axis', 'minor_beam_axis',

                  'flux_co21', 'flux_co21_err', 'vel_co21',
                  'FWHM_co21', 'FWHM_co21_err', 'L_co21', 'limit_flag_co21',

                  'literature_m_h2', 'literature_mu_gas', 'literature_f_gas',
                  'literature_t_depl',

                  'literature_sersic_fit_flag', 'literature_r_sersic', 'literature_n_sersic', 'literature_q_sersic',
                  'literature_r_12_bulge', 'literature_r_12_disk', 'literature_bt'])

# space holder for final table
final_table = None

for index in range(len(data_table_1)):

    # get galaxy literature identifier
    identifier = data_table_1[index, 1]
    field = data_table_1[index, 2]
    source_number = data_table_1[index, 3]
    # create columns for the identifier
    identifier_column = Column(np.array([identifier]), name='literature_identifier', dtype='S12')
    field_column = Column(np.array([field]), name='literature_field', dtype='S12')
    source_number_column = Column(np.array([source_number]), name='literature_source_number', dtype='S12')


    ra_raw = str(data_table_1[index, 4], encoding='utf-8')
    dec_raw = str(data_table_1[index, 5], encoding='utf-8')
    coordinates = ra_raw + ' ' + dec_raw
    coord_table = SkyCoord(coordinates,  unit=(u.hourangle, u.deg))
    ra = coord_table.ra.deg
    dec = coord_table.dec.deg

    if data_table_2[index, 10] != b'nan':
        co_ra_raw_offset = float(data_table_2[index, 10])
        co_dec_raw_offset = float(data_table_2[index, 11])

        co_ra = ra + co_ra_raw_offset / 3600
        co_dec = dec + co_dec_raw_offset / 3600
    else:
        co_ra = np.nan
        co_dec = np.nan

    print(ra, dec)
    print(co_ra, co_dec)

    # create empty CO table
    co_data = np.zeros(len(names))
    co_data[:] = np.nan  # fill with NaN values

    # coordinates
    co_data[0] = ra
    co_data[1] = dec
    co_data[2] = co_ra
    co_data[3] = co_dec
    # redshift
    co_data[4] = float(data_table_1[index, 6])

    # beam axis
    co_data[5] = float(str(data_table_1[index, 6], encoding='utf-8').replace('\"', ''))
    co_data[6] = float(str(data_table_1[index, 8], encoding='utf-8').replace('\"', ''))

    if float(data_table_3[index, 4]) > 0:
        # co21 flux
        co_data[7] = float(data_table_3[index, 4])
        co_data[8] = float(data_table_3[index, 5])
        # vel_co21
        co_data[9] = float(data_table_2[index, 9]) * constants.c * 1e-3
        # FWHM_co21
        co_data[10] = float(data_table_2[index, 14])
        co_data[11] = float(data_table_2[index, 15])
        # luminosity
        co_data[12] = float(data_table_3[index, 7]) * 1e-9
        # limit flag
        co_data[13] = 0

        # molecular gas mass
        co_data[14] = float(data_table_3[index, 8]) * 1e-9
        # mu gas
        co_data[15] = float(data_table_3[index, 9])
        # gas fraction
        co_data[16] = float(data_table_3[index, 10])
        # depletion_time
        co_data[17] = float(data_table_3[index, 11])

    else:
        # co21 flux
        co_data[7] = float(data_table_3[index, 4]) * -1

        # luminosity
        co_data[12] = float(data_table_3[index, 7]) * -1 * 1e-9
        # limit flag
        co_data[13] = 1

        # molecular gas mass
        co_data[14] = float(data_table_3[index, 8]) * -1 * 1e-9
        # mu gas
        co_data[15] = float(data_table_3[index, 9]) * -1
        # gas fraction
        co_data[16] = float(data_table_3[index, 10]) * -1
        # depletion_time
        co_data[17] = float(data_table_3[index, 11]) * -1

    # r_sersic
    co_data[19] = float(data_table_4[index, 4])

    # n_sersic
    if str(data_table_4[index, 5], encoding='utf-8')[-1] == '+':
        co_data[20] = float(str(data_table_4[index, 5], encoding='utf-8')[:-1])
        # sersic_fit_flag
        co_data[18] = 1
    else:
        co_data[20] = float(data_table_4[index, 5])
        # sersic_fit_flag
        co_data[18] = 0

    # q_sersic
    co_data[21] = float(data_table_4[index, 6])
    # r_12_bulge
    co_data[22] = float(data_table_4[index, 7])
    # r_12_disk
    co_data[23] = float(data_table_4[index, 8])
    # bt
    co_data[24] = float(data_table_4[index, 9])


    co_data_table = QTable(data=co_data, names=names)

    co_data_table = hstack([identifier_column, field_column, source_number_column, co_data_table])

    if final_table is None:
        final_table = co_data_table
    else:
        final_table = vstack([final_table, co_data_table])

print(final_table)

# add units to table
final_table['ra'].unit = u.deg
final_table['dec'].unit = u.deg
final_table['ra_co_peak'].unit = u.deg
final_table['dec_co_peak'].unit = u.deg

final_table['major_beam_axis'].unit = u.arcsec
final_table['minor_beam_axis'].unit = u.arcsec


final_table['flux_co21'].unit = u.Jy * u.km / u.s
final_table['flux_co21_err'].unit = u.Jy * u.km / u.s
final_table['vel_co21'].unit = u.km / u.s
final_table['FWHM_co21'].unit = u.km / u.s
final_table['FWHM_co21_err'].unit = u.km / u.s
final_table['L_co21'].unit = 1e9 * u.K * u.km / u.s

final_table['literature_m_h2'].unit = 1e9 * u.M_sun
final_table['literature_t_depl'].unit = u.Gyr

final_table['literature_r_sersic'].unit = u.kpc
final_table['literature_r_12_bulge'].unit = u.kpc
final_table['literature_r_12_disk'].unit = u.kpc

# print final table
print(final_table)

# create galtool object
final_table_access = analysis_tools.AnalysisTools(table=final_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'freundlich_2019'):
    os.makedirs(final_table_access.data_path / 'tables' / 'freundlich_2019')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'freundlich_2019' / 'freundlich_2019.fits',
                               overwrite=True)

