import numpy as np
import os

from xgaltool import analysis_tools, table_utils
from astropy.table import Table, Column, hstack, vstack
from astropy.coordinates import SkyCoord
import astropy.units as u

"""
This macro will create an object table of the observation sample published by Luo et al. (2020)
doi:10.1093/mnrasl/slaa099
Table 1
"""

rcsed_access = analysis_tools.AnalysisTools(object_type='rcsed', writable_table=True)

luo_table_1 = np.genfromtxt(rcsed_access.project_path / 'data/co_data/luo_2020/luo_2020_table_1.txt', dtype=object)


names = np.array(['flux_co10', 'flux_co10_err', 'L_co10', 'L_co10_err', 'limit_flag_co10', 'literature_m_h2', 'literature_m_h2_err'])

# galaxies without rcsed counter part will use dummy table
nan_table = Table(data=np.ones(len(rcsed_access.col_names)) * np.nan, names=rcsed_access.col_names)

final_table = None

for index in range(0, len(luo_table_1)):

    designation = str(luo_table_1[index, 0], 'utf-8')[1:]
    # print(designation)
    coodinates = (designation[0:2] + ':' + designation[2:4] + ':' + designation[4:9] + ' ' +
                  designation[9:12] + ':' + designation[12:14] + ':' + designation[14:])
    # print(coodinates)

    coord_table = SkyCoord(coodinates,  unit=(u.hourangle, u.deg))
    ra = coord_table.ra.deg
    dec = coord_table.dec.deg

    rcsed_object = table_utils.get_closest_object(rcsed_access.table, ra, dec, search_radius=5)

    object_access = analysis_tools.AnalysisTools(object_type='single_object', ra=ra, dec=dec)
    object_access.table.add_column(np.array([1]) * rcsed_object['z'], name='z_ned', index=-1)
    general_data_table = object_access.table

    # create empty CO table
    co_data = np.zeros(len(names))
    co_data[:] = np.nan

    # column with name
    name_column = Column(np.array(['SDSS ' + designation]), name='SDSS designation', dtype='S25')

    if float(luo_table_1[index, 11]) > 0:
        # co10 flux (K km s^{-1})
        co_data[0] = float(luo_table_1[index, 11])
        co_data[1] = float(luo_table_1[index, 13])

        # luminosity in 1e9 K km s^-1 pc^2
        co_data[2] = object_access.compute_co_luminosity(s_co_vel=co_data[0], redshift=rcsed_object['z'], conversion_factor=5)
        co_data[3] = object_access.compute_co_luminosity(s_co_vel=co_data[1], redshift=rcsed_object['z'], conversion_factor=5)
        co_data[4] = 0

        # log Molecular Gas Mass M_{gas} (M_{sun})
        co_data[5] = 10 ** float(luo_table_1[index, 17])
        co_data[6] = 10 ** float(luo_table_1[index, 19])
    else:
        # co10 flux (K km s^{-1})
        co_data[0] = float(luo_table_1[index, 11]) * -1

        # luminosity in 1e9 K km s^-1 pc^2
        co_data[2] = object_access.compute_co_luminosity(s_co_vel=co_data[0], redshift=rcsed_object['z'], conversion_factor=5)
        co_data[4] = 1

        # log Molecular Gas Mass M_{gas} (M_{sun})
        co_data[5] = 10 ** (float(luo_table_1[index, 17]) * -1)

    co_data_table = Table(data=co_data, names=names)
    co_data_table = hstack([general_data_table, name_column, co_data_table])

    if final_table is None:
        final_table = co_data_table
    else:
        final_table = vstack([final_table, co_data_table])

# add units
final_table['flux_co10'].unit = u.K * u.km / u.s
final_table['flux_co10_err'].unit = u.K * u.km / u.s

final_table['L_co10'].unit = 1e9 * u.K * u.km / u.s * u.pc**2
final_table['L_co10_err'].unit = 1e9 * u.K * u.km / u.s * u.pc**2

final_table['literature_m_h2'].unit = u.M_sun
final_table['literature_m_h2_err'].unit = u.M_sun

# create galtool object
final_table_access = analysis_tools.AnalysisTools(table=final_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'luo_2020'):
    os.makedirs(final_table_access.data_path / 'tables' / 'luo_2020')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'luo_2020' / 'luo_2020.fits',
                               overwrite=True)
