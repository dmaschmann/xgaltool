import numpy as np
import os

from galtool import analysis_tools, coordinate_utils
from astropy.table import QTable, hstack, vstack, Column
import astropy.units as u
"""
This macro will create an galaxy table for the galtool package of the CO observation published in 
Bauermeister et al. (2013) doi:10.1088/0004-637X/768/2/132

We are using Table 2 and 3 of the Publication.
"""

# get empty galtool object to access data structure
empty_access = analysis_tools.AnalysisTools(object_type='empty')
# load publication tables
bauermeister_table_2 = np.genfromtxt(empty_access.project_path /
                                     'data/co_data/bauermeister_2013/bauermeister_2013_table_2.txt', dtype=object)
bauermeister_table_3 = np.genfromtxt(empty_access.project_path /
                                     'data/co_data/bauermeister_2013/bauermeister_2013_table_3.txt', dtype=object)

print('length of table 2 = ', len(bauermeister_table_2))
print('length of table 3 = ', len(bauermeister_table_3))

# space holder for final table
final_table = None

# get all data names
names = np.array(['flux_co10', 'flux_co10_err', 'vel_co10', 'vel_co10_err', 'delta_v_co10', 'delta_v_co10_err',
                  'L_co10', 'L_co10_err', 'limit_flag_co10',
                  'flux_co32', 'flux_co32_err', 'vel_co32', 'vel_co32_err', 'delta_v_co32', 'delta_v_co32_err',
                  'L_co32', 'L_co32_err', 'limit_flag_co32',
                  'literature_m_h2', 'literature_m_h2_err', 'literature_limit_flag_m_h2'])

# loop over all galaxies and create Astropy tables
for index, galaxy_identifier in zip(range(0, len(bauermeister_table_2)), bauermeister_table_2[:, 0]):
    # get name
    galaxy_identifier = str(galaxy_identifier, 'utf-8')
    # get rid of footnotes
    if (galaxy_identifier[-1] == 'a') | (galaxy_identifier[-1] == 'b'):
        galaxy_identifier = galaxy_identifier[:-2]

    # get SDSS name
    sdss_galaxy_name = str(bauermeister_table_2[index, 1], 'utf-8') + ' ' + str(bauermeister_table_2[index, 2], 'utf-8')
    # print all identifying data
    print('index: ', index, 'Bauermeister 2013 identifier: ', galaxy_identifier, "SDSS identifier: ", sdss_galaxy_name)

    # get ra, dec and redshift from the NED database
    ra, dec, ned_redshift = coordinate_utils.get_galaxy_ned_coords_and_z(galaxy_name=sdss_galaxy_name)

    # column with name and tables with coordinates and redshift
    name_column = Column(np.array([galaxy_identifier]), name='literature name', dtype='S8')
    identifier_column = QTable(data=np.array([ra, dec, ned_redshift]), names=['ra', 'dec', 'z_ned'], dtype=(float, float, float))

    # create empty CO table
    co_data = np.zeros(len(names))
    co_data[:] = np.nan  # fill with NaN values

    # get mask for galaxies observed in CO10
    data_table_co10_index = ((bauermeister_table_3[:, 0] == str.encode(galaxy_identifier)) &
                             (bauermeister_table_3[:, 1] == b'1'))
    # get mask for galaxies observed in CO31
    data_table_co32_index = ((bauermeister_table_3[:, 0] == str.encode(galaxy_identifier)) &
                             (bauermeister_table_3[:, 1] == b'3'))

    # fill data into co data _table
    # CO10 data
    if sum(data_table_co10_index) > 0:
        # flux
        co_data[0] = float(bauermeister_table_3[data_table_co10_index, 2])
        co_data[1] = float(bauermeister_table_3[data_table_co10_index, 4])
        # velocity
        co_data[2] = float(bauermeister_table_3[data_table_co10_index, 5])
        co_data[3] = float(bauermeister_table_3[data_table_co10_index, 7])
        # delta v of emission line
        co_data[4] = float(bauermeister_table_3[data_table_co10_index, 8])
        co_data[5] = float(bauermeister_table_3[data_table_co10_index, 10])
        # line luminosity
        co_data[6] = float(bauermeister_table_3[data_table_co10_index, 11])
        co_data[7] = float(bauermeister_table_3[data_table_co10_index, 13])
        # limit flag
        co_data[8] = int(bauermeister_table_3[data_table_co10_index, 18])
    # CO32
    if sum(data_table_co32_index) > 0:
        # flux
        co_data[9] = float(bauermeister_table_3[data_table_co32_index, 2])
        co_data[10] = float(bauermeister_table_3[data_table_co32_index, 4])
        # velocity
        co_data[11] = float(bauermeister_table_3[data_table_co32_index, 5])
        co_data[12] = float(bauermeister_table_3[data_table_co32_index, 7])
        # delta v of emission line
        co_data[13] = float(bauermeister_table_3[data_table_co32_index, 8])
        co_data[14] = float(bauermeister_table_3[data_table_co32_index, 10])
        # line luminosity
        co_data[15] = float(bauermeister_table_3[data_table_co32_index, 11])
        co_data[16] = float(bauermeister_table_3[data_table_co32_index, 13])
        # limit flag
        co_data[17] = int(bauermeister_table_3[data_table_co32_index, 18])

    # also save the literature M-H2 value. here we prefer the MH2 value computed by CO10
    if sum(data_table_co10_index) > 0:
        co_data[18] = float(bauermeister_table_3[data_table_co10_index, 15])
        co_data[19] = float(bauermeister_table_3[data_table_co10_index, 17])
        co_data[20] = int(bauermeister_table_3[data_table_co10_index, 18])

    else:
        co_data[18] = float(bauermeister_table_3[data_table_co32_index, 15])
        co_data[19] = float(bauermeister_table_3[data_table_co32_index, 17])
        co_data[20] = int(bauermeister_table_3[data_table_co32_index, 18])

    # create data table and put everything together
    co_data_table = QTable(data=co_data, names=names)
    co_data_table = hstack([name_column, identifier_column, co_data_table])

    if final_table is None:
        final_table = co_data_table
    else:
        final_table = vstack([final_table, co_data_table])

# add units to table
final_table['ra'].unit = u.deg
final_table['dec'].unit = u.deg

final_table['flux_co10'].unit = u.Jy * u.km / u.s
final_table['flux_co10_err'].unit = u.Jy * u.km / u.s
final_table['vel_co10'].unit = u.km / u.s
final_table['vel_co10_err'].unit = u.km / u.s
final_table['delta_v_co10'].unit = u.km / u.s
final_table['delta_v_co10_err'].unit = u.km / u.s
final_table['L_co10'].unit = 1e9 * u.K * u.km / u.s * u.pc**2
final_table['L_co10_err'].unit = 1e9 * u.K * u.km / u.s * u.pc**2

final_table['flux_co32'].unit = u.Jy * u.km / u.s
final_table['flux_co32_err'].unit = u.Jy * u.km / u.s
final_table['vel_co32'].unit = u.km / u.s
final_table['vel_co32_err'].unit = u.km / u.s
final_table['delta_v_co32'].unit = u.km / u.s
final_table['delta_v_co32_err'].unit = u.km / u.s
final_table['L_co32'].unit = 1e9 * u.K * u.km / u.s * u.pc**2
final_table['L_co32_err'].unit = 1e9 * u.K * u.km / u.s * u.pc**2

final_table['literature_m_h2'].unit = 1e9 * u.M_sun
final_table['literature_m_h2_err'].unit = 1e9 * u.M_sun

print(final_table.colnames)
print(final_table)

# create galtool object
final_table_access = analysis_tools.AnalysisTools(table=final_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'bauermeister_2013'):
    os.makedirs(final_table_access.data_path / 'tables' / 'bauermeister_2013')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'bauermeister_2013' / 'bauermeister_2013.fits',
                               overwrite=True)

