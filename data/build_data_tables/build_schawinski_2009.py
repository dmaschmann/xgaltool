import numpy as np
import os

from galtool import analysis_tools, coordinate_utils, table_utils
from astropy.table import Table, Column, hstack, vstack
import astropy.units as u


"""
This macro will create an object table of the observation sample published by Schawinski et al. (2009)
doi:10.1088/0004-637X/690/2/1672
Table 1
"""

rcsed_access = analysis_tools.AnalysisTools(object_type='rcsed', writable_table=True)

schawinski_table_1 = np.genfromtxt(rcsed_access.project_path /
                                   'data/co_data/schawinski_2009/schawinski_2009_table_1.txt', dtype=object)

names = np.array(['flux_co10', 'flux_co10_err', 'L_co10', 'L_co10_err', 'limit_flag_co10',
                  'literature_m_h2', 'literature_m_h2_err', 'literature_limit_flag_m_h2'])

# galaxies without rcsed counter part will use dummy table
nan_table = Table(data=np.ones(len(rcsed_access.col_names)) * np.nan, names=rcsed_access.col_names)

final_table = None

for index in range(0, len(schawinski_table_1)):
    # get the SDSS designation to find access in NED
    sdss_designation = 'SDSS ' + str(schawinski_table_1[index, 1], 'utf-8')
    ra, dec, ned_redshift = coordinate_utils.get_galaxy_ned_coords_and_z(galaxy_name=sdss_designation)
    print(sdss_designation)

    #
    object_access = analysis_tools.AnalysisTools(object_type='single_object', ra=ra, dec=dec)
    rcsed_cross_match = table_utils.get_closest_object(rcsed_access.table, ra, dec, search_radius=5)
    object_access.table.add_column(rcsed_cross_match['z'], name='z_ned', index=-1)

    # create empty CO table
    co_data = np.zeros(len(names))
    co_data[:] = np.nan

    if float(schawinski_table_1[index, 12]) > 0:
        # co10 flux (K km s^{-1})
        co_data[0] = float(schawinski_table_1[index, 12])
        co_data[1] = float(schawinski_table_1[index, 14])

        # CO10 line luminosity in 1e9 K km s^-1 pc^2
        co_data[2] = object_access.compute_co_luminosity(s_co_vel=co_data[0], redshift=rcsed_cross_match['z'],
                                                         conversion_factor=5)
        co_data[3] = object_access.compute_co_luminosity(s_co_vel=co_data[1], redshift=rcsed_cross_match['z'],
                                                         conversion_factor=5)
        # flag
        co_data[4] = 0
        # M H2 in units of 1e9
        co_data[5] = float(schawinski_table_1[index, 15]) * 0.1
        co_data[6] = float(schawinski_table_1[index, 17]) * 0.1
        co_data[7] = 0

    else:
        # co10 flux limit (K km s^{-1}) this is 3 sigma of the noise
        co_data[0] = float(schawinski_table_1[index, 12]) * -3

        # CO10 line luminosity in 1e9 K km s^-1 pc^2
        co_data[2] = object_access.compute_co_luminosity(s_co_vel=co_data[0], redshift=rcsed_cross_match['z'],
                                                     conversion_factor=5)
        # flag
        co_data[4] = 1
        # M H2 in units of 1e9
        co_data[5] = float(schawinski_table_1[index, 15]) * -0.1
        co_data[7] = 1

    co_data_table = Table(data=co_data, names=names)
    co_data_table = hstack([object_access.table, co_data_table])

    if final_table is None:
        final_table = co_data_table
    else:
        final_table = vstack([final_table, co_data_table])

# add units
final_table['flux_co10'].unit = u.K * u.km / u.s
final_table['flux_co10_err'].unit = u.K * u.km / u.s

final_table['L_co10'].unit = 1e9 * u.K * u.km / u.s * u.pc**2
final_table['L_co10_err'].unit = 1e9 * u.K * u.km / u.s * u.pc**2

final_table['literature_m_h2'].unit = 1e9 * u.M_sun
final_table['literature_m_h2_err'].unit = 1e9 * u.M_sun

# create galtool object
final_table_access = analysis_tools.AnalysisTools(table=final_table)

# check if folder exists
if not os.path.isdir(final_table_access.data_path / 'tables' / 'schawinski_2009'):
    os.makedirs(final_table_access.data_path / 'tables' / 'schawinski_2009')

# save table
final_table_access.table.write(final_table_access.data_path / 'tables' / 'schawinski_2009' / 'schawinski_2009.fits',
                               overwrite=True)
