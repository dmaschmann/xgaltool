# Table 2
# IRAM 30 m CO(1-0) Observations

# Target^a	t_obs	I_CO^b	L'_CO ^c	M(H_2) ^d	FWHM^e
# (hr)	(K km s^-1)	(10^7 K km s^-1 pc^2)	(10^7 M_sun)	(km s^-1)
EAH01   0.30    2.58    +or-    0.33  128.6    +or- 16.4 514.5 +or- 65.7 309.8 +or- 18.4
EAH02   0.31    1.32    +or-    0.27  84.3 +or- 17.4  337.0    +or- 69.7 172.9 +or- 29.8
EAH03   0.20    3.60    +or-    0.39  158.0    +or- 17.2 632.1 +or- 68.9 158.7 +or- 8.6
EAH04   0.51    0.59    +or-    0.13  9.2  +or- 2.0    36.7    +or- 7.9   106.7    +or- 20.3
EAH05   0.60    0.97    +or-    0.21  90.7 +or- 19.5  362.7    +or- 77.9 278.7 +or- 44.8
EAH06	0.71	-0.59   +or-    nan -24.84  +or-    nan -99.34  +or-    nan nan +or-    nan
EAH07	1.02	-0.33   +or-    nan	-10.31  +or-    nan	-41.25  +or-    nan	 nan +or-    nan
EAH08	0.83	0.45    +or-    0.15	35.6    +or-    12.0	142.5   +or-    47.8	233.7   +or-    56.9
EAH09	0.61	0.43    +or-    0.13	7.8 +or-    2.4	31.4    +or-    9.7	119.5   +or-    28.5
EAH10	0.43	0.73    +or-    0.20	181.5   +or-    48.6	726.1   +or-    194.3	275.1   +or-    58.8
EAH11	0.40	-0.60   +or-    nan	-38.53  +or-    nan	-154.12 +or-    nan	    nan +or-    nan
EAH12	0.70	-0.44   +or-    nan	-65.87  +or-    nan	-263.47 +or-    nan	    nan +or-    nan
EAH13	0.94	0.68    +or-    0.13	193.8   +or-    37.6	775.2   +or-    150.3	190.1   +or-    27.8
EAH14	0.80	-0.53   +or-    nan	-44.96  +or-    nan	-179.85 +or-    nan	    nan +or-    nan
EAH15	0.60	-0.75  +or-    nan	-27.57  +or-    nan	-110.27  +or-    nan	 nan +or-    nan
EAH16	0.30	-0.56  +or-    nan	-155.85  +or-    nan	-623.40  +or-    nan	 nan +or-    nan
EAH17	0.43	-0.51  +or-    nan	-25.67  +or-    nan	-102.66  +or-    nan	 nan +or-    nan
EAS01	0.33	-0.76  +or-    nan	-6.23  +or-    nan	-24.93  +or-    nan	 nan +or-    nan
EAS02	0.40	1.11 +or- 0.34	12.8 +or- 3.9	51.2 +or- 15.5	162.7 +or- 44.0
EAS03	0.47	1.66 +or- 0.24	143.6 +or- 21.1	574.4 +or- 84.5	270.5 +or- 30.3
EAS04	1.56	-0.28  +or-    nan	-1.38  +or-    nan	-5.51  +or-    nan	 nan +or-    nan
EAS05	0.74	0.64 +or- 0.20	30.5 +or- 9.5	121.8 +or- 38.2	346.9 +or- 82.0
EAS06	0.31	3.83 +or- 0.39	42.6 +or- 4.3	170.6 +or- 17.2	112.9 +or- 4.0
EAS07	0.61	-0.47  +or-    nan	-10.82  +or-    nan	-43.27  +or-    nan	 nan +or-    nan
EAS08	1.14	-0.28  +or-    nan	-10.06  +or-    nan	-40.24  +or-    nan	 nan +or-    nan
EAS09	0.54	2.12 +or- 0.27	33.3 +or- 4.3	133.3 +or- 17.2	267.8 +or- 20.5
EAS10	0.80	-0.49  +or-    nan	-15.53  +or-    nan	-62.12  +or-    nan	 nan +or-    nan
EAS11	0.40	-0.51  +or-    nan	-17.20  +or-    nan	-68.80  +or-    nan	 nan +or-    nan
EAS12	1.10	0.36 +or- 0.12	8.7 +or- 2.8	34.9 +or- 11.3	141.7 +or- 29.1
EAS13	0.39	-0.70  +or-    nan	-32.75  +or-    nan	-131.01  +or-    nan	 nan +or-    nan
EAS14	1.61	0.83 +or- 0.17	124.6 +or- 25.3	498.5 +or- 101.1	428.7 +or- 73.4
EAS15	0.74	0.48 +or- 0.15	29.9 +or- 9.5	119.8 +or- 38.0	166.2 +or- 57.2

#Notes.
# a Lines in bold represent >3sigma detections in IRAM 30 m CO(1-0) observations.
# b Upper limits are shown at the 3sigma level.
# c Calculated using .
# d Masses calculated assuming alphaCO =4 Msun (K km s-1 pc2)-1, .
# e FWHM from Gaussian fit to data..