#Table 1
#SDSS & IRAM Data
#
#SDSS Object Name	R.A. (J2000)	Decl. (J2000)	Emission Line Class^a	Stellar Mass M_{stellar} (x10^{10} M_{sun})	Total CO Flux^b S_{CO(1 rarr 0)} (K km s^{-1})	Molecular Gas Mass^c M_{gas} (x10^8 M_{sun})	Age of Starburst^d t_y (Myr)
#
SDSS J075109.77+342636.5	07 51 09.8	+34 26 36.6	SF	5.7 +or- 1.03	-0.07 +or- nan  -1.88 +or- nan  180^{70}_{-70}
SDSS J012039.73+142824.2	01 20 39.7	+14 28 24.2	SF	1.1 +or- 0.07	0.63 +or- 0.19	6.0 +or- 1.89	70^{40}_{-40}
SDSS J002227.39+135430.6	00 22 27.4	+13 54 30.6	SF	4.1 +or- 0.31	-0.07 +or- nan  -2.03 +or- nan  340^{70}_{-60}
SDSS J011458.35+142726.3	01 14 58.4	+14 27 26.4	AGN+SF	7.0 +or- 1.12	-0.14 +or- nan  -2.90 +or- nan  540^{80}_{-80}
SDSS J000336.67+141433.9	00 03 36.7	+14 14 34.0	AGN+SF	19.3 +or- 1.67	-0.14 +or- nan  -5.26 +or- nan  1990^{1480}_{-1720}
SDSS J100124.35+375046.8	10 01 24.4	+37 50 46.9	AGN+SF	1.9 +or- 0.28	0.28 +or- 0.055	2.5 +or- 0.49	460^{200}_{-200}
SDSS J083539.01+362923.2	08 35 39.0	+36 29 23.2	SF	4.2 +or- 0.43	0.73 +or- 0.10	7.5 +or- 1.04	200^{70}_{-70}	
SDSS J110234.97+422309.2	11 02 35.0	+42 23 09.2	AGN+SF	1.5 +or- 0.27	0.56 +or- 0.11	6.5 +or- 1.35	150^{80}_{-60}
SDSS J090125.61+442536.0	09 01 25.6	+44 25 36.0	SF	1.4 +or- 0.25	0.39 +or- 0.070	3.8 +or- 0.68	190^{40}_{-40}	
SDSS J082729.49+360022.1	08 27 29.5	+36 00 22.1	AGN+SF	3.5 +or- 0.59	0.97 +or- 0.16	9.6 +or- 1.66	150^{100}_{-90}
SDSS J082104.65+412108.5	08 21 04.7	+41 21 08.6	AGN+SF	3.5 +or- 0.63	-0.07 +or- nan  -1.65 +or- nan  310^{190}_{-130}
SDSS J102847.41+404231.3	10 28 47.4	+40 42 31.4	SF	3.8 +or- 1.11	0.53 +or- 0.1	5.3 +or- 1.00	190^{40}_{-40}	
SDSS J124450.01+422123.3	12 44 50.0	+42 21 23.4	SF	8.3 +or- 1.69	1.38 +or- 0.190	13.4 +or- 1.84	140^{100}_{-90}	
SDSS J125716.71+424625.9	12 57 16.7	+42 46 26.0	SF	3.1 +or- 0.19	-0.21 +or- nan  -4.09 +or- nan  310^{120}_{-110}
SDSS J075025.78+444621.3	07 50 25.8	+44 46 21.3	Seyfert	2.8 +or- 0.31	-0.07 +or- nan  -1.18 +or- nan  470^{150}_{-140}
SDSS J111850.04+422541.8	11 18 50.0	+42 25 41.8	SF	2.6 +or- 0.47	0.55 +or- 0.10	6.2 +or- 1.13	170^{50}_{-50}	
SDSS J084014.81+364803.7	08 40 14.8	+36 48 03.7	AGN+SF	3.1 +or- 0.58	-0.07 +or- nan  -1.45 +or- nan  900^{460}_{-500}
SDSS J103855.94+392157.5	10 38 55.9	+39 21 57.6	Seyfert	1.7 +or- 0.24	-0.07 +or- nan  -1.40 +or- nan  960^{330}_{-380}
SDSS J093014.24+360909.5	09 30 14.2	+36 09 09.6	AGN+SF	2.6 +or- 0.53	0.17 +or- 0.065	2.1 +or- 0.79	100^{70}_{-80}
SDSS J082115.75+355924.6	08 21 15.8	+35 59 24.7	Seyfert	4.0 +or- 0.41	-0.07 +or- nan  -1.68 +or- nan  1080^{660}_{-430}
SDSS J082507.10+410319.4	08 25 07.1	+41 03 19.5	AGN+SF	3.8 +or- 0.60	-0.07 +or- nan  -1.51 +or- nan  220^{170}_{-120}
SDSS J084216.23+360141.4	08 42 16.2	+36 01 41.5	Seyfert	3.8 +or- 0.35	-0.07 +or- nan  -1.37 +or- nan  1740^{1020}_{-1150}
SDSS J104705.19+411514.5	10 47 05.2	+41 15 14.5	SF	1.7 +or- 0.31	0.34 +or- 0.090	3.4 +or- 0.89	190^{40}_{-40}	
SDSS J103843.56+404221.0	10 38 43.6	+40 42 21.0	AGN+SF	2.8 +or- 0.49	-0.07 +or- nan  -1.39 +or- nan  890^{380}_{-440}

#Notes.
#a
#BPT emission line classification, described in detail in Schawinski et al. (2007a).
#b
#Upper limits for the total CO flux are based on the rms noise of the smoothed spectrum and assuming a line of width 70 km s^{-1}, they are 1sigma limits.
#c
#Molecular gas masses are derived from the total CO luminosity and assuming a conversion from CO to molecular gas mass of alpha = 1.5 K km s^{-1} pc^2 (Evans et al. 2005). The gas mass upper limits are 2sigma as in the Figures.
#d
#The age of the last episode of star formation t_y is taken from Schawinski et al. (2007a) and is based on fitting the UV/optical/NIR spectral energy distribution and the stellar (Lick) absorption indices.