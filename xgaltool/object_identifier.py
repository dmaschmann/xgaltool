#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
This part provides table and single object identification.
"""

import numpy as np
import os
from pathlib import Path

from xgaltool import basic_attributes, table_utils


def combine_tables(initial_table, cross_match, cross_match_table_data_path='data', cross_match_algorithm=None, search_radius=1, objid_flag=False):
    r"""
    This Function will merge two tables based on three possibilities
     1) SDSS MJD PLATE FIBERID Which is exact and thus recommended
     2) Ra and DEC which needs to be specified with the search radius
     3) SDSS objid, which is not recommended unless you know what you do.
    :param initial_table: Initial table you want to cross match. This must be initialised using object_identifier
    :type initial_table: Astropy Table
    :param cross_match: name of table you want to cross match.
    :type cross_match: str
    :param cross_match_algorithm: defines which keywords for cross-match is used. this can be 'objid',
    'mjd_plate_fiberid' or 'ra_dec'. The default is None. In this case the first working keyword match will be used.
    :type cross_match_algorithm: str
    :param search_radius: search radius in arcsec
    :type search_radius: int or float
    :param objid_flag: Additional flag to ensure you correctly use the possibility of table cross match using objid
    :type objid_flag: bool
    """
    from astropy.table import Table, Column, hstack
    import copy

    # get table access
    # cross_match_access = ObjectIdentifier(data_path='data', object_type=cross_match, writable_table=True)
    if cross_match_table_data_path == 'data':  # use short cut
        cross_match_table_data_path = Path.home() / 'data'
    if os.path.isdir(Path(cross_match_table_data_path)):
        cross_match_table_data_path = Path(cross_match_table_data_path)
    else:
        raise OSError('The data path %s is not correct' % cross_match_table_data_path)
    cross_match_access = ObjectIdentifier(data_path=cross_match_table_data_path, object_type=cross_match, writable_table=True)

    # select keywords to perform cross-match
    if cross_match_algorithm is None:
        if (('mjd' in cross_match_access.col_names) & ('plate' in cross_match_access.col_names) &
                ('fiberid' in cross_match_access.col_names) & ('mjd' in initial_table.colnames) &
                ('plate' in initial_table.colnames) & ('fiberid' in initial_table.colnames)):
            cross_match_algorithm = 'mjd_plate_fiberid'
        elif (('ra' in cross_match_access.col_names) & ('dec' in cross_match_access.col_names) &
              ('ra' in initial_table.colnames) & ('dec' in initial_table.colnames)):
            cross_match_algorithm = 'ra_dec'
        elif ('objid' in cross_match_access.col_names) & ('objid' in initial_table.colnames) & objid_flag:
            cross_match_algorithm = 'objid'
        else:
            raise KeyError('The two tables do not have matching identifiers')

    # get identifier
    col_names_cross_match = cross_match_access.col_names
    # all possible identifier
    identifier_list = ['objid', 'mjd', 'plate', 'fiberid', 'ra', 'dec']

    # get all col_names of data which should be in the new table
    data_col_names_cross_match = copy.deepcopy(col_names_cross_match)
    for identifier in identifier_list:
        if identifier in data_col_names_cross_match:
            data_col_names_cross_match.remove(identifier)

    # create an empty table of the size of the initial table to fill it with data afterwards.
    cross_match_data_table = Table()
    for data_identifier in data_col_names_cross_match:
        cross_match_data_table[data_identifier] = Column(np.ones(len(initial_table)) * np.nan,
                                                         dtype=cross_match_access.table[data_identifier].dtype)

    # merge table
    if cross_match_algorithm == 'mjd_plate_fiberid':

        # initialize hashmap/dict
        hashmap_cross_match = {}
        for index, mjd, plate, fiberid in zip(range(0, len(cross_match_access.table)),
                                              np.array(cross_match_access.table['mjd'], dtype=int),
                                              np.array(cross_match_access.table['plate'], dtype=int),
                                              np.array(cross_match_access.table['fiberid'], dtype=int)):
            hashmap_cross_match.update({str(mjd) + '-' + str(plate) + '-' + str(fiberid): index})

        # index list to map which entries will be filled in the initial table
        index_list = np.zeros(len(cross_match_data_table), dtype=int)

        # for loop over second table
        for index, mjd, plate, fiberid in zip(range(0, len(initial_table)),
                                              np.array(initial_table['mjd'], dtype=int),
                                              np.array(initial_table['plate'], dtype=int),
                                              np.array(initial_table['fiberid'], dtype=int)):
            current_hashmap = str(mjd) + '-' + str(plate) + '-' + str(fiberid)

            # use only entries which are in in the cross_match table
            if hashmap_cross_match.get(current_hashmap) is None:
                index_list[index] = -1
            else:
                index_list[index] = int(hashmap_cross_match.get(current_hashmap))

        # fill empty table
        cross_match_data_table[index_list != -1] = \
            cross_match_access.table[data_col_names_cross_match][index_list[index_list != -1]]

    elif cross_match_algorithm == 'ra_dec':

        # convert ra and dec tables into SkyCoord objects
        from astropy.coordinates import SkyCoord

        if initial_table['ra'].unit == 'deg':
            coord_initial_table = SkyCoord(initial_table['ra'], initial_table['dec'])
        else:
            import astropy.units as u
            coord_initial_table = SkyCoord(initial_table['ra']*u.deg, initial_table['dec']*u.deg)

        if cross_match_access.table['ra'].unit == 'deg':
            coord_new_table = SkyCoord(cross_match_access.table['ra'], cross_match_access.table['dec'])
        else:
            import astropy.units as u
            # print(cross_match_access.table['ra'])
            coord_new_table = SkyCoord(cross_match_access.table['ra']*u.deg, cross_match_access.table['dec']*u.deg)

        # coord_new_table = SkyCoord(cross_match_access.table['ra'], cross_match_access.table['dec'])
        # find the smallest distance
        idx_initial_table, d2d_initial_table, d3d_initial_table = coord_new_table.match_to_catalog_sky(
            coord_initial_table)

        # identify counterparts which are inside the search radius
        identified_counter_parts = d2d_initial_table.arcsec < search_radius

        # fill empty table
        cross_match_data_table[idx_initial_table[identified_counter_parts]] = \
            cross_match_access.table[data_col_names_cross_match][identified_counter_parts]

    elif cross_match_algorithm == 'objid':
        # !!! cross match using only object id is not safe !!!!
        import warnings
        warnings.warn("You are trying to cross match tables only by using objid. "
                      "You have to make sure that the objid type must be long int."
                      "Do not use this unless you know what You are doing!")

        # initialize hashmap/dict
        hashmap_cross_match = {}
        for index, objid in zip(range(0, len(cross_match_access.table)),
                                np.array(cross_match_access.table['objid'], dtype=int)):
            hashmap_cross_match.update({str(objid): index})

        # index list to map which entries will be filled in the initial table
        index_list = np.zeros(len(cross_match_data_table), dtype=int)

        # for loop over second table
        for index, objid in zip(range(0, len(initial_table)),
                                np.array(initial_table['objid'], dtype=int)):
            current_hashmap = str(objid)

            # use only entries which are in in the cross_match table
            if hashmap_cross_match.get(current_hashmap) is None:
                index_list[index] = -1
            else:
                index_list[index] = int(hashmap_cross_match.get(current_hashmap))
        # fill empty table
        cross_match_data_table[index_list != -1] = \
            cross_match_access.table[data_col_names_cross_match][index_list[index_list != -1]]

    elif cross_match_algorithm == 'plateifu':
        # initialize hashmap/dict
        hashmap_cross_match = {}
        for index, plateifu in zip(range(0, len(cross_match_access.table)),
                                              np.array(cross_match_access.table['plateifu'], dtype=str)):
            hashmap_cross_match.update({str(plateifu).replace(" ", ""): index})
            #print(hashmap_cross_match)
        # index list to map which entries will be filled in the initial table
        index_list = np.zeros(len(cross_match_data_table), dtype=int)

        # for loop over second table
        for index, plateifu in zip(range(0, len(initial_table)),
                                              np.array(initial_table['plateifu'], dtype=str)):
            current_hashmap = str(plateifu).replace(" ", "")

            #print(hashmap_cross_match.get(current_hashmap))
            # use only entries which are in in the cross_match table
            if hashmap_cross_match.get(current_hashmap) is None:
                index_list[index] = -1
            else:
                index_list[index] = int(hashmap_cross_match.get(current_hashmap))
        # fill empty table
        cross_match_data_table[index_list != -1] = \
            cross_match_access.table[data_col_names_cross_match][index_list[index_list != -1]]           

    elif cross_match_algorithm == 'mangaid':
        # initialize hashmap/dict
        hashmap_cross_match = {}
        for index, mangaid in zip(range(0, len(cross_match_access.table)),
                                              np.array(cross_match_access.table['mangaid'], dtype=str)):
            hashmap_cross_match.update({str(mangaid).replace(" ", ""): index})
            #print(hashmap_cross_match)
        # index list to map which entries will be filled in the initial table
        index_list = np.zeros(len(cross_match_data_table), dtype=int)

        # for loop over second table
        for index, mangaid in zip(range(0, len(initial_table)),
                                              np.array(initial_table['mangaid'], dtype=str)):
            current_hashmap = str(mangaid).replace(" ", "")

            #print(hashmap_cross_match.get(current_hashmap))
            # use only entries which are in in the cross_match table
            if hashmap_cross_match.get(current_hashmap) is None:
                index_list[index] = -1
            else:
                index_list[index] = int(hashmap_cross_match.get(current_hashmap))
        # fill empty table
        cross_match_data_table[index_list != -1] = \
            cross_match_access.table[data_col_names_cross_match][index_list[index_list != -1]]

    else:
        raise ReferenceError('The table you are trying to cross match does not have the needed identifier. '
                             'Make sure that the astro tables has one of the following identifiers: '
                             'objid or mjd + plate + fiberid or ra + dec')

    # combine initial and new data table and return
    initial_table = hstack([initial_table, cross_match_data_table])
    return initial_table


class ObjectIdentifier(basic_attributes.DataStructure, basic_attributes.PhysicalConstants):

    def __init__(self, data_path='data', file_name=None, object_type=None, **kwargs):
        r"""
        Class to identify Objects or fits tables of objects.
        """
        super().__init__()

        # get project path. This path is the location where the galtool software is located
        self.project_path = Path(__file__).parent.parent

        # check if data_path is correct and add to attributes
        if data_path == 'data':  # use short cut
            data_path = Path.home() / 'data'
        if os.path.isdir(Path(data_path)):
            self.data_path = Path(data_path)
        else:
            raise OSError('The data path %s is not correct' % data_path)

        # check if the table should be writable this will distinguish between table format
        # astropy.tables and astropy.oi fits.
        if 'writable_table' in kwargs:
            self.writable_table = kwargs.get('writable_table')
        else:
            self.writable_table = False

        # check if single object or table
        if file_name:  # by providing a file name we load directly a specific table
            self.object_type = None  # no object type declared
            self.file_name = file_name
            # filename must be without suffix
            if 'suffix' in kwargs:
                self.suffix = kwargs.get('suffix')
            else:
                self.suffix = '.fits'
        elif object_type in self.table_identifier:  # if we call an object of the table list
            self.object_type = object_type
            self.file_name = 'tables/%s/%s' % (self.table_identifier[object_type]['folder_name'],
                                               self.table_identifier[object_type]['file_name'])
            self.suffix = self.table_identifier[object_type]['suffix']
        elif (object_type == 'rcsed_object') | (object_type == 'single_object'):  # if we only call a single SDSS object by using an identifier
            self.object_type = 'rcsed_object'
            # create object table of single rcsed object
            self.table = table_utils.get_rcsed_object_table(data_path=self.data_path, **kwargs)
            self.writable_table = True  # using astropy.table thus table is rewritable

        elif object_type == 'coordinate_object':  # if we introduce corrdinates as an object
            self.object_type = 'coordinate_object'
            # create coordinate table of single object
            if ('ra' in kwargs) & ('dec' in kwargs):
                self.table = table_utils.object_table(ra=kwargs.get('ra'), dec=kwargs.get('dec'))
                self.writable_table = True  # using astropy.table thus table is rewritable
            else:
                raise KeyError('coordinates in form of ra and dec are needed for single coordinate object')
        elif object_type == 'empty':  # empty table. With this option you can only access basic functions
            # set only a dummy table attribute to access this class without data
            self.object_type = None
            self.table = None
        elif 'table' in kwargs:
            self.object_type = object_type
            self.table = kwargs.get('table')
        else:
            raise LookupError('You have not provided a specific reference to an object. '
                              'If you want to use galtool without any object reference use object_type=`empty´')

        # check if a cross-matched table is specified and add the cross-match to the file_name
        if 'table_cross_match' in kwargs:
            self.file_name += '_match'
            for cross_match in self.table_identifier.keys():
                if cross_match in kwargs.get('table_cross_match'):
                    if cross_match == 'cs':
                        if 'rcsed' in kwargs.get('table_cross_match'):
                            continue
                    self.file_name += '_' + cross_match

        # load the table
        if not hasattr(self, 'table'):
            # change file name to a path format
            self.file_name = Path(self.file_name)
            # distinguish between astropy.tables and astropy.oi fits.
            # the latter is much faster in access but is unhandy to write to
            if self.writable_table:
                from astropy.table import Table
                self.table = Table.read((self.data_path / self.file_name).with_suffix(self.suffix), memmap=True)
                self.col_names = self.table.colnames
            else:
                from astropy.io import fits
                data_table = fits.open((self.data_path / self.file_name).with_suffix(self.suffix))
                self.table = data_table[1].data
                self.col_names = data_table[1].columns.names
                data_table.close()

    def get_objid(self):
        r"""
        Access SDSS objid
        """
        return self.table['objid']

    def get_mjd(self):
        r"""
        Access SDSS mjd
        """
        return self.table['mjd']

    def get_plate(self):
        r"""
        Access SDSS plate
        """
        return self.table['plate']

    def get_fiberid(self):
        r"""
        Access SDSS fiberid
        """
        return self.table['fiberid']

    def get_ra(self):
        r"""
        Access SDSS ra
        """
        return self.table['ra']

    def get_dec(self):
        r"""
        Access SDSS dec
        """
        return self.table['dec']

    def get_redshift(self, manga_dr15=False, manga_dr17=False):
        r"""
        Access to SDSS redshift
        """
        if manga_dr15:
            return self.table['nsa_z']
        elif manga_dr17:
            return self.table['z_manga']
        else:
            return self.table['z']

    def get_redshift_err(self):
        r"""
        Access to SDSS redshift error
        """
        return self.table['zerr']

    def get_redshift_conf(self):
        r"""
        Access to SDSS redshift confidence
        """
        return self.table['zconf']

    def cross_match_table(self, cross_match='rcsed',  cross_match_table_data_path='data', cross_match_algorithm=None, search_radius=1, objid_flag=False):

        # check if cross_match_name is in table list
        if cross_match not in self.table_identifier.keys():
            raise KeyError('The table %s is not listed in table_identifier' % cross_match)

        # check if table is writable
        if not self.writable_table:
            raise KeyError('The main table is not writable. Use keyword writable_table=True')

        self.table = combine_tables(self.table, cross_match=cross_match, cross_match_table_data_path=cross_match_table_data_path, cross_match_algorithm=cross_match_algorithm,
                                    search_radius=search_radius, objid_flag=objid_flag)

        # update colnames
        self.col_names = self.table.colnames

    def cross_match_project_table(self, project_name, **kwargs):

        if not os.path.isdir(self.data_path / 'tables' / project_name):
            os.makedirs(self.data_path / 'tables' / project_name)
        import copy

        if (kwargs.get('match_rcsed_mpa_gswlc') is None) | (kwargs.get('match_rcsed_mpa_gswlc') is True):
            rcsed_mpa_gswlc_table = copy.deepcopy(self.table)
            rcsed_mpa_gswlc_table = combine_tables(rcsed_mpa_gswlc_table, cross_match='rcsed', search_radius=5)
            rcsed_mpa_gswlc_table = combine_tables(rcsed_mpa_gswlc_table, cross_match='mpa', search_radius=5)
            rcsed_mpa_gswlc_table = combine_tables(rcsed_mpa_gswlc_table, cross_match='gswlc_a_v1', search_radius=5)
            rcsed_mpa_gswlc_table.write(Path(self.data_path / 'tables' / project_name /
                                             str(project_name +
                                                 '_match_rcsed_mpa_gswlc_a_v1')).with_suffix(self.suffix),
                                        overwrite=True)

        if (kwargs.get('match_ir') is None) | (kwargs.get('match_ir') is True):
            wise_iras_herschel_table = copy.deepcopy(self.table)
            wise_iras_herschel_table = combine_tables(wise_iras_herschel_table, cross_match='wise', search_radius=1)
            wise_iras_herschel_table = combine_tables(wise_iras_herschel_table, cross_match='iras', search_radius=1)
            wise_iras_herschel_table = combine_tables(wise_iras_herschel_table, cross_match='herschel_combined',
                                                      search_radius=1)
            wise_iras_herschel_table.write(Path(self.data_path / 'tables' / project_name /
                                                str(project_name +
                                                    '_match_wise_iras_herschel')).with_suffix(self.suffix),
                                           overwrite=True)

        if (kwargs.get('match_meert_morph') is None) | (kwargs.get('match_meert_morph') is True):
            meert_morph_table = copy.deepcopy(self.table)
            meert_morph_table = combine_tables(meert_morph_table, cross_match='meert_2014', search_radius=1)
            meert_morph_table = combine_tables(meert_morph_table, cross_match='dominguez_sanchez_2018', search_radius=1)
            meert_morph_table.write(Path(self.data_path / 'tables' / project_name /
                                         str(project_name +
                                             '_match_meert_2014_dominguez_sanchez_2018')).with_suffix(self.suffix),
                                    overwrite=True)

        if ((kwargs.get('match_radio') is None) |
                (kwargs.get('match_radio') is True)):
            vlass_nvss_first_lotss_dr2_table = copy.deepcopy(self.table)
            vlass_nvss_first_lotss_dr2_table = combine_tables(vlass_nvss_first_lotss_dr2_table, cross_match='rcsed',
                                                             search_radius=5)
            vlass_nvss_first_lotss_dr2_table = combine_tables(vlass_nvss_first_lotss_dr2_table, cross_match='first',
                                                              search_radius=5)
            vlass_nvss_first_lotss_dr2_table = combine_tables(vlass_nvss_first_lotss_dr2_table, cross_match='nvss',
                                                              search_radius=5)
            vlass_nvss_first_lotss_dr2_table = combine_tables(vlass_nvss_first_lotss_dr2_table, cross_match='lotss_dr2',
                                                              search_radius=5)
            vlass_nvss_first_lotss_dr2_table = combine_tables(vlass_nvss_first_lotss_dr2_table, cross_match='vlass',
                                                              search_radius=5)
            vlass_nvss_first_lotss_dr2_table.write(Path(self.data_path / 'tables' / project_name /
                                                   str(project_name +
                                                       '_match_rcsed_vlass_nvss_first_lotss_dr2')
                                                        ).with_suffix(self.suffix), overwrite=True)

        if (kwargs.get('match_environment') is None) | (kwargs.get('match_environment') is True):
            saulder_yang_table = copy.deepcopy(self.table)
            saulder_yang_table = combine_tables(saulder_yang_table, cross_match='saulder_2016', search_radius=3)
            saulder_yang_table = combine_tables(saulder_yang_table, cross_match='yang_2007', search_radius=3)
            saulder_yang_table.write(Path(self.data_path / 'tables' / project_name /
                                          str(project_name + '_match_saulder_2016_yang_2007')).with_suffix(self.suffix),
                                     overwrite=True)

        if (kwargs.get('match_hi') is None) | (kwargs.get('match_hi') is True):
            hi_table = copy.deepcopy(self.table)
            hi_table = combine_tables(hi_table, cross_match='alfalfa', search_radius=5)
            hi_table = combine_tables(hi_table, cross_match='springob', search_radius=5)
            hi_table = combine_tables(hi_table, cross_match='xgass', search_radius=5)
            hi_table.write(Path(self.data_path / 'tables' / project_name /
                                str(project_name + '_match_alfalfa_springob_xgass')).with_suffix(self.suffix),
                           overwrite=True)

        if (kwargs.get('match_rcsed_dp') is None) | (kwargs.get('match_rcsed_dp') is True):
            rcsed_dp_table = copy.deepcopy(self.table)
            rcsed_dp_table = combine_tables(rcsed_dp_table, cross_match='rcsed', search_radius=5)
            rcsed_dp_table = combine_tables(rcsed_dp_table, cross_match='dp', search_radius=5)
            rcsed_dp_table.write(Path(self.data_path / 'tables' / project_name /
                                      str(project_name + '_match_rcsed_dp')).with_suffix(self.suffix),
                                 overwrite=True)

        if (kwargs.get('match_xray') is None) | (kwargs.get('match_xray') is True):
            chandra_xmm_newton_table = copy.deepcopy(self.table)
            chandra_xmm_newton_table = combine_tables(chandra_xmm_newton_table, cross_match='chandra',
                                                          search_radius=1)
            chandra_xmm_newton_table = combine_tables(chandra_xmm_newton_table, cross_match='xmm_newton',
                                                          search_radius=1)
            chandra_xmm_newton_table.write(Path(self.data_path / 'tables' / project_name /
                                                str(project_name + '_match_chandra_xmm_newton')
                                                ).with_suffix(self.suffix), overwrite=True)

        if (kwargs.get('match_manga_dr15')) is True:
            manga_dr15_table = copy.deepcopy(self.table)
            manga_dr15_table = combine_tables(manga_dr15_table, cross_match='manga', search_radius=1)
            manga_dr15_table.write(Path(self.data_path / 'tables' / project_name / str(project_name + '_match_dr15')).with_suffix(self.suffix), overwrite=True)

        if (kwargs.get('match_pipe3d')) is True:
            pipe3d_table = copy.deepcopy(self.table)
            pipe3d_table = combine_tables(pipe3d_table, cross_match='pipe3d', cross_match_algorithm='mangaid')
            pipe3d_table.write(Path(self.data_path / 'tables'/ project_name / str(project_name + '_match_pipe3D')).with_suffix(self.suffix), overwrite=True)

        if (kwargs.get('match_manga_dr17')) is True:
            manga_dr17_table = copy.deepcopy(self.table)
            manga_dr17_table = combine_tables(manga_dr17_table, cross_match='manga', search_radius=1)
            manga_dr17_table.write(Path(self.data_path / 'tables' / project_name / str(project_name + '_match_dr17')).with_suffix(self.suffix), overwrite=True)

        if (kwargs.get('match_pipe3d_dr17')) is True:
            pipe3d_dr17_table = copy.deepcopy(self.table)
            pipe3d_dr17_table = combine_tables(pipe3d_dr17_table, cross_match='pipe3d-dr17', cross_match_algorithm='mangaid')
            pipe3d_dr17_table.write(Path(self.data_path / 'tables'/ project_name / str(project_name + '_match_pipe3D_dr17')).with_suffix(self.suffix), overwrite=True)



    def get_sdss_object_file_path(self, index=0):
        r"""
        function to get file_path of sdss spectral objects
        """
        # get identifier
        mjd = int(self.get_mjd()[index])
        plate = int(self.get_plate()[index])
        fiberid = int(self.get_fiberid()[index])

        # merge filepath
        file_path = self.data_path / 'galaxies' / 'sdss_spectra' / str(mjd) / str(plate) / str(fiberid)

        # check if filepath exists and create it if necessary
        if not os.path.isdir(file_path):
            os.makedirs(file_path)

        return file_path

    def get_hi_object_file_path(self, index=0):
        r"""
        function to get file_path of sdss spectral objects
        """
        # get identifier
        mjd = int(self.get_mjd()[index])
        plate = int(self.get_plate()[index])
        fiberid = int(self.get_fiberid()[index])

        # merge filepath
        file_path = self.data_path / 'galaxies' / 'hi_21cm_line' / str(mjd) / str(plate) / str(fiberid)

        # check if filepath exists and create it if necessary
        if not os.path.isdir(file_path):
            os.makedirs(file_path)

        return file_path

    def get_manga_object_data_path(self, index=0):
        # get sdss filepath
        sdss_file_path = self.get_sdss_object_file_path(index=index)
        manga_file_path = sdss_file_path / 'manga'

        # check if filepath exists and create it if necessary
        if not os.path.isdir(manga_file_path):
            os.makedirs(manga_file_path)

        return manga_file_path

    def get_manga_file_identifier(self, cube_format='reduced', unpack=True, index=0):
        r"""
        Function to get data cubes described in https://www.sdss.org/dr16/manga/manga-data/data-access/
        :param cube_format: Kew word to specify which cube format to download. "reduced" for the reduced data cube,
        "hybrid" for derived quantities with a hybrid model and  "voronoy" for derived quantities with a pure Voronoy
        binning model
        :type cube_format: str
        :param unpack:
        :param index: index in case of galaxy table
        :type index: int
        :return:
        """

        manga_object = table_utils.identify_manga_object(self.data_path, self.get_ra()[index], self.get_dec()[index])

        if manga_object is None:
            return None, None

        manga_plate = manga_object['manga_plate']
        manga_ifudsgn = manga_object['manga_ifudsgn']

        if cube_format == 'reduced':
            # reduced spectra
            cube_file_name = 'manga_%i_%i_reduced_spectra' % (int(manga_plate), int(manga_ifudsgn))

            # DRP 3D Output Files (Reduced spectra)
            url = 'https://data.sdss.org/sas/dr16/manga/spectro/redux/' \
                  'v2_4_3/%i/stack/manga-%i-%i-LOGCUBE.fits.gz' % (int(manga_plate), int(manga_plate), int(manga_ifudsgn))

        elif cube_format == 'hybrid_maps':
            # hybrid derived quantities
            cube_file_name = 'manga_%i_%i_derived_hybrid_maps' % (int(manga_plate), int(manga_ifudsgn))

            # DAP Output Files (Derived Quantities) with an hybrid model
            url = 'https://data.sdss.org/sas/dr16/manga/spectro/analysis/v2_4_3/2.2.1/HYB10-GAU-MILESHC/' \
                  '%i/%i/manga-%i-%i-MAPS-HYB10-GAU-MILESHC.fits.gz' % (int(manga_plate), int(manga_ifudsgn),
                                                                        int(manga_plate), int(manga_ifudsgn))
        elif cube_format == 'hybrid_logcube':
            # hybrid derived quantities
            cube_file_name = 'manga_%i_%i_derived_hybrid_logcube' % (int(manga_plate), int(manga_ifudsgn))

            # DAP Output Files (Derived Quantities) with an hybrid model
            url = 'https://data.sdss.org/sas/dr16/manga/spectro/analysis/v2_4_3/2.2.1/HYB10-GAU-MILESHC/' \
                  '%i/%i/manga-%i-%i-LOGCUBE-HYB10-GAU-MILESHC.fits.gz' % (int(manga_plate), int(manga_ifudsgn),
                                                                           int(manga_plate), int(manga_ifudsgn))
        elif cube_format == 'voronoy_maps':
            # pure Voronoy binned spaxels providing derived quantities
            cube_file_name = 'manga_%i_%i_derived_voronoy_maps' % (int(manga_plate), int(manga_ifudsgn))

            # DAP Output Files (Derived Quantities) for pure voronoy binned model
            url = 'https://data.sdss.org/sas/dr16/manga/spectro/analysis/v2_4_3/2.2.1/VOR10-GAU-MILESHC/' \
                  '%i/%i/manga-%i-%i-MAPS-VOR10-GAU-MILESHC.fits.gz' % (int(manga_plate), int(manga_ifudsgn),
                                                                        int(manga_plate), int(manga_ifudsgn))
        elif cube_format == 'voronoy_logcube':
            # pure Voronoy binned spaxels providing derived quantities
            cube_file_name = 'manga_%i_%i_derived_voronoy_logcube' % (int(manga_plate), int(manga_ifudsgn))

            # DAP Output Files (Derived Quantities) for pure voronoy binned model
            url = 'https://data.sdss.org/sas/dr16/manga/spectro/analysis/v2_4_3/2.2.1/VOR10-GAU-MILESHC/' \
                  '%i/%i/manga-%i-%i-LOGCUBE-VOR10-GAU-MILESHC.fits.gz' % (int(manga_plate), int(manga_ifudsgn),
                                                                           int(manga_plate), int(manga_ifudsgn))
        else:
            raise KeyError('cube format not understand')

        if unpack:
            cube_file_name = Path(cube_file_name).with_suffix('.fits')
        else:
            cube_file_name = Path(cube_file_name).with_suffix('.fits.gz')

        return cube_file_name, url

    def get_sdss_file_identifier(self, index=0):
        r"""
        get file_name of sdss spectra
        """
        # get identifier as strings (plate with 4 digits, mjd 5 and fiberid with 3)
        mjd = int(self.get_mjd()[index])
        plate = int(self.get_plate()[index])
        if len(str(int(plate))) == 3:
            plate = "0%s" % str(plate)
        fiberid = int(self.get_fiberid()[index])
        if len(str(int(fiberid))) == 2:
            fiberid = "0%s" % str(fiberid)
        if len(str(int(fiberid))) == 1:
            fiberid = "00%s" % str(fiberid)
        # compose file name
        file_name = 'sdss_spec_contfit_nburst_%i_%i_%i.fits' % (int(mjd), int(plate), int(fiberid))
        # compose url
        url = 'http://gal-02.sai.msu.ru/data1/fit_MILES_x/%s/spSpec%s-%s-%s_results.fits' % (plate, mjd, plate, fiberid)

        return file_name, url

    def get_radio_hi_source_identifier(self, only_file_name=False, hi_catalog='alfalfa', suffix='fits', index=0):
        r"""
        get file_name of HI observation
        """

        mjd = int(self.get_mjd()[index])
        plate = int(self.get_plate()[index])
        fiberid = int(self.get_fiberid()[index])

        if only_file_name:
            if suffix == 'fits':
                file_name = 'HI_line_observation_%i_%i_%i.fits' % (int(mjd), int(plate), int(fiberid))
                return file_name, None
            else:
                file_name = 'HI_line_observation_%i_%i_%i.txt' % (int(mjd), int(plate), int(fiberid))
                return file_name, None
        # compose file name

        # compose url
        if hi_catalog == 'alfalfa':
            agc = self.table['AGC'][index]
        elif hi_catalog == 'springob_2005':
            agc = str(self.table['UGC_AGC'][index])
        elif hi_catalog == 'xgass':
            agc = str(self.table['AGC_xgass'][index])
        else:
            raise KeyError('HI catalog not understand')
        # if self.table['AGC'][index] != 'nan':
        #     agc = self.table['AGC'][index]
        # elif self.table['AGC_xgass'][index] != 'nan':
        #     agc = str(self.table['AGC_xgass'][index])
        # else:
        #     agc = str(self.table['UGC_AGC'][index])

        print('agc', agc)
        if agc[0] == '0':
            agc = agc[1:]
        print('agc', agc)

        from urllib import request
        for i in range(0, 101):
            print(i)
            try:
                url = 'https://ned.ipac.caltech.edu/spc1/2018/2018ApJ...861...49H/%02i/AGC_%s:S:HI:hgk2018.fits.gz' % (i, agc)
                respond = request.urlopen(url)
                print(respond.code)

                if respond.code == 200:
                    file_name = 'HI_line_observation_%i_%i_%i.fits' % (int(mjd), int(plate), int(fiberid))
                    return file_name, url
            except:
                print('deneid')

        for i in range(0, 101):
            print(i)
            try:
                url = 'https://ned.ipac.caltech.edu/spc1/2018/2018ApJ...861...49H/%02i/UGC_%s:S:HI:hgk2018.fits.gz' % (i, agc)
                respond = request.urlopen(url)
                print(respond.code)

                if respond.code == 200:
                    file_name = 'HI_line_observation_%i_%i_%i.fits' % (int(mjd), int(plate), int(fiberid))
                    return file_name, url
            except:
                print('deneid')

        if(len(agc) <5):
            print('!')

            while(len(agc) < 5):
                print('!')
                agc = '0' + agc
        print('agc', agc)

        try:
            url = 'https://ned.ipac.caltech.edu/spc1/2005/2005ApJS..160..149S/AGC_%s:S:HI:shg2005_NED.txt' % agc
            respond = request.urlopen(url)
            print(respond.code)

            if respond.code == 200:
                file_name = 'HI_line_observation_%i_%i_%i.txt' % (int(mjd), int(plate), int(fiberid))
                return file_name, url
        except:
            print('deneid')

        try:
            url = 'https://ned.ipac.caltech.edu/spc1/2005/2005ApJS..160..149S/UGC_%s:S:HI:shg2005_NED.txt' % agc
            respond = request.urlopen(url)
            print(respond.code)

            if respond.code == 200:
                file_name = 'HI_line_observation_%i_%i_%i.txt' % (int(mjd), int(plate), int(fiberid))
                return file_name, url
        except:
            print('deneid')

        for i in range(0, 101):
            print(i)
            try:
                url = 'https://ned.ipac.caltech.edu/spc1/2018/2018ApJ...861...49H/%02i/UGC_%s:S:HI:hgk2018.fits.gz' % (i, agc)
                respond = request.urlopen(url)
                print(respond.code)

                if respond.code == 200:
                    file_name = 'HI_line_observation_%i_%i_%i.fits' % (int(mjd), int(plate), int(fiberid))
                    return file_name, url
            except:
                print('deneid')

        if(len(agc) < 6):
            print('!')

            while(len(agc) < 6):
                print('!')
                agc = '0' + agc
        print('agc', agc)

        for i in range(0, 101):
            print(i)
            try:
                url = 'https://ned.ipac.caltech.edu/spc1/2018/2018ApJ...861...49H/%02i/AGC_%s:S:HI:hgk2018.fits.gz' % (i, agc)
                respond = request.urlopen(url)
                print(respond.code)

                if respond.code == 200:
                    file_name = 'HI_line_observation_%i_%i_%i.fits' % (int(mjd), int(plate), int(fiberid))
                    return file_name, url
            except:
                print('deneid')

        # return None, None
        raise LookupError('We cannot find a spectrum on the NED webpage')

    @staticmethod
    def download_file(file_path, file_name, url, unpack=False, authentification=None):
        r"""
        method to download file and save to filepath
        :param file_path: the data_path directly to the data file
        :param file_name: file name
        :param url: the url where to download
        :param unpack: flag to unpack file.
        :param authentification: Should be a dictionary in care of needed authentification
        :return
        """

        # check if spectrum already exists
        if os.path.isfile(file_path / file_name):
            print(file_path / file_name, 'already exists')
            return True
        else:
            import urllib3
            # download file
            http = urllib3.PoolManager()
            if authentification is None:
                r = http.request('GET', url, preload_content=False)
            else:
                print('try authentification')
                myheaders = urllib3.util.make_headers(basic_auth=authentification)
                r = http.request('GET', url, headers=myheaders)
            print(r)
            if unpack:
                with open((file_path / file_name).with_suffix(".gz"), 'wb') as out:
                    while True:
                        data = r.read()
                        if not data:
                            break
                        out.write(data)
                r.release_conn()
                # uncompress file
                import gzip
                # read compressed file
                compressed_file = gzip.GzipFile((file_path / file_name).with_suffix(".gz"), 'rb')
                s = compressed_file.read()
                compressed_file.close()
                # save compressed file
                uncompressed_file = open(file_path / file_name, 'wb')
                uncompressed_file.write(s)
                uncompressed_file.close()
                # delete compressed file
                os.remove((file_path / file_name).with_suffix(".gz"))

            else:
                with open(file_path / file_name, 'wb') as out:
                    while True:
                        data = r.read()
                        if not data:
                            break
                        out.write(data)
                r.release_conn()

    def get_image_path(self, index=0):
        image_path = self.data_path / Path('images/%i/%i/%i/' % (int(self.get_mjd()[index]),
                                                                 int(self.get_plate()[index]),
                                                                 int(self.get_fiberid()[index])))
        # make sure the path exists
        if not os.path.isdir(image_path):
            os.makedirs(image_path)

        return image_path

    def get_table_path(self, table_name):
        return self.data_path / 'tables' / self.table_identifier[table_name]['folder_name']

    def get_subset_path(self, table_name=None):
        if table_name is None:
            table_name = self.object_type
        return self.get_table_path(table_name) / 'subsets'

    def download_galaxy_table(self, table_name, table_specifier=None, unpack=False, authentification=None):

        table_path = self.get_table_path(table_name=table_name)
        # if table path is not existing create it
        if not os.path.isdir(table_path):
            os.makedirs(table_path)

        if table_specifier is None:
            table_file_name = Path(self.table_identifier[table_name]['file_name']
                                   ).with_suffix(self.table_identifier[table_name]['suffix'])
            url = self.__getattribute__('%s_table_url' % table_name)

        else:
            table_file_name = Path(self.table_identifier[table_name + '_' + table_specifier]['file_name']
                                   ).with_suffix(self.table_identifier[table_name]['suffix'])
            url = self.__getattribute__('%s_table_url' % table_name)[table_specifier]

        self.download_file(file_path=table_path, file_name=table_file_name, url=url, unpack=unpack,
                           authentification=authentification)

    def download_sdss_spectrum_fits_file(self, index=0):
        r"""
        download an sdss object spectrum processed by rcsed
        """

        # get filepath where to store data
        file_path = self.get_sdss_object_file_path(index)
        # get file name of sdss spectrum
        spec_file_name, url = self.get_sdss_file_identifier(index=index)

        self.download_file(file_path=file_path, file_name=spec_file_name, url=url)

    def download_hi_line_spectrum_file(self, index=0):
        r"""
        download HI-21cm line observation
        """

        # get filepath where to store data
        file_path = self.get_hi_object_file_path(index)
        # get file name of sdss spectrum
        spec_file_name, url = self.get_radio_hi_source_identifier(index=index)
        if spec_file_name is not None:
            self.download_file(file_path=file_path, file_name=spec_file_name, url=url)

    def download_manga_cube(self, cube_format='reduced', index=0, unpack=True):
        r"""
        Function to download Manga data cubes described in https://www.sdss.org/dr16/manga/manga-data/data-access/
        :param cube_format: Kew word to specify which cube format to download. "reduced" for the reduced data cube,
        "hybrid" for derived quantities with a hybrid model and  "voronoy" for derived quantities with a pure Voronoy
        binning model
        :type cube_format: str
        :param index: index in case of galaxy table
        :type index: int
        :return:
        """
        file_path = self.get_manga_object_data_path(index=index)
        cube_file_name, url = self.get_manga_file_identifier(cube_format=cube_format, index=index, unpack=unpack)

        self.download_file(file_path=file_path, file_name=cube_file_name, url=url, unpack=unpack)

    def open_hdu(self, file_path, file_name, data_type='sdss', index=0):

        if not os.path.isfile(file_path / file_name):
            if data_type == 'sdss':
                self.download_sdss_spectrum_fits_file(index=index)
            elif data_type == 'manga':
                self.download_manga_cube(index=index)
            elif data_type == 'hi':
                self.download_hi_line_spectrum_file(index=index)

        from astropy.io import fits
        try:
            # try to open. This can cause trouble due to corrupt files
            hdu = fits.open(file_path / file_name)
        except OSError:
            # delete spec file and reload
            print('cannot open', file_path / file_name)
            print('--->reload file')
            # delete file and download again
            os.remove(file_path / file_name)
            if data_type == 'sdss':
                self.download_sdss_spectrum_fits_file(index=index)
            elif data_type == 'manga':
                self.download_manga_cube(index=index)
            elif data_type == 'hi':
                self.download_hi_line_spectrum_file(index=index)

            hdu = fits.open(file_path / file_name)
        return hdu

    def get_snap_shot_file_path(self, index, mode='sdss'):

        mjd = int(self.get_mjd()[index])
        plate = int(self.get_plate()[index])

        ra = self.get_ra()[index]
        dec = self.get_dec()[index]

        if mode == 'sdss':
            file_path = self.data_path / 'galaxies' / 'images' / 'sdss' / str(mjd) / str(plate)
            url_string = ('http://skyservice.pha.jhu.edu/DR12/ImgCutout/getjpeg.aspx?'
                          'ra=%f&dec=%f&scale=0.36&width=150&height=150&opt=&query=&' % (ra, dec))
        elif mode == 'legacy':
            file_path = self.data_path / 'galaxies' / 'images' / 'legacy' / str(mjd) / str(plate)
            # url_string = ('http://legacysurvey.org//viewer/jpeg-cutout?ra=%.4f&dec=%.4f&size150&layer=decals-dr7' %
            #               (ra, dec))
            url_string = ('http://legacysurvey.org/viewer/cutout.jpg?ra=%.4f&dec=%.4f&layer=dr8&pixscale=0.25' %
                          (ra, dec))
        elif mode == 'galex':
            file_path = self.data_path / 'galaxies' / 'images' / 'galex' / str(mjd) / str(plate)
            url_string = ('http://legacysurvey.org//viewer/jpeg-cutout?ra=%.4f&dec=%.4f&size150&layer=galex' %
                          (ra, dec))
        elif mode == 'unwise':
            file_path = self.data_path / 'galaxies' / 'images' / 'unwise' / str(mjd) / str(plate)
            url_string = ('http://legacysurvey.org//viewer/jpeg-cutout?ra=%.4f&dec=%.4f&size150&layer=unwise-neo4' %
                          (ra, dec))
        else:
            raise ValueError('mode not understand')

        return url_string, file_path

    @staticmethod
    def get_observation_image_url(ra, dec, size, pixel_scale, layer='dr8', bands=None, file_format='fits'):
        pixel_scale /= 100
        url = 'http://legacysurvey.org/viewer/%s-cutout?' %file_format
        url += 'ra=%.4f&dec=%.4f&size=%i' % (ra, dec, size)
        url += '&layer=%s' % layer
        url += '&pixscale=%.2f' % pixel_scale
        if bands:
            url += '&bands=%s' % bands
        return url

    def get_first_image_url(self, size=2 * 60, index=0):
        r"""

        :param size: image size in arc seconds
        :param index: index
        :return:
        """

        from astropy import units as u
        from astropy.coordinates import SkyCoord

        c = SkyCoord(ra=self.get_ra()[index] * u.degree, dec=self.get_dec() [index]* u.degree)
        ra_coords = c.ra.hms
        dec_coords = c.dec.dms
        d = dec_coords.d
        if d > 0:
            d_string = '+%02i' % d
        else:
            if d == 0:
                if dec_coords.m > 0:
                    d_string = '+%02i' % d
                else:
                    if dec_coords.m == 0:
                        if dec_coords.s >= 0:
                            d_string = '+%02i' % d
                        else:
                            d_string = '-%02i' % abs(d)
                    else:
                        d_string = '-%02i' % abs(d)
            else:
                d_string = '-%02i' % abs(d)

        url_beginning = 'https://third.ucllnl.org/cgi-bin/firstimage?'
        url_coord_part = 'RA=%02i%%20%02i%%20%06.3f%%20%%2B%s%%20%02i%%20%05.2f&Dec=&Equinox=J2000&' % \
                         (ra_coords.h, abs(ra_coords.m), abs(ra_coords.s), d_string, abs(dec_coords.m),
                          abs(dec_coords.s))
        url_finish = 'ImageSize=%.2f&MaxInt=10&FITS=1&Download=1' % (size / 60)  # convert size into arcmin

        return url_beginning + url_coord_part + url_finish

    def get_lotss_dr1_image_url(self, size=2 * 60, index=0):
        r"""

        :param size: image size in arcsec
        :param index: index
        :return:
        """
        mosaic_id = str(self.table['Mosaic_ID'][index])
        # change mosaic id to url convention
        mosaic_id = mosaic_id.replace('+', '_')

        ra = self.get_ra()[index]
        dec = self.get_dec()[index]

        # resize the right ascension with the declination to get squared image
        dec_size = size / 3600
        ra_size = size / 3600 / abs(np.cos(dec * np.pi / 180))

        url_beginning = 'https://vo.astron.nl/getproduct/hetdex/data/mosaics/%s-mosaic.fits?' % mosaic_id
        url_coord_part = 'sdec=%.3f&dec=%.5f&ra=%.5f&sra=%.3f' % (dec_size, dec, ra, ra_size)

        return url_beginning + url_coord_part
