#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Table utils
"""

import numpy as np
import os
from pathlib import Path


def object_table(objid=None, mjd=None, plate=None, fiberid=None, ra=None, dec=None):
    r"""
    function to produce a one row table with the SDSS identifier mjd, plate and fiberid
    :param objid: SDSS objid
    :type objid: int or array like
    :param mjd: SDSS mjd
    :type mjd: int or array like
    :param plate: SDSS plate
    :type plate: int or array like
    :param fiberid: SDSS fiberid
    :type fiberid: int or array like
    :param ra: ra
    :type ra: float or array like
    :param dec: dec
    :type dec: float or array like
    :return table with identifier for object (1 row)
    """
    from astropy.table import Table, Column

    identifier_table = Table()
    if objid is not None:
        identifier_table['objid'] = Column(np.array([1]) * objid, dtype='i8')
    if (mjd is not None) & (plate is not None) & (fiberid is not None):
        identifier_table['mjd'] = Column(np.array([1]) * mjd, dtype='i4')
        identifier_table['plate'] = Column(np.array([1]) * plate, dtype='i4')
        identifier_table['fiberid'] = Column(np.array([1]) * fiberid, dtype='i4')
    if (ra is not None) & (dec is not None):
        identifier_table['ra'] = Column(np.array([1]) * ra, dtype='f8')
        identifier_table['dec'] = Column(np.array([1]) * dec, dtype='f8')

    return identifier_table


def load_rcsed_coordinate_table(data_path):
    r"""
    load rcsed table with only coordinates
    this is obtained from rcsed.sai.msu.ru/media/files/rcsed.fits
    :param data_path: path where to find rcsed table
    :return:
    """
    from astropy.io import fits
    table = fits.open((data_path / 'tables' / 'rcsed' / 'rcsed_coords').with_suffix(".fits"))
    return table[1].data


def load_manga_coordinate_table(data_path):
    r"""
    load manga DRPALL table with coordinates
    this is obtained from https://data.sdss.org/sas/dr16/manga/spectro/redux/v2_4_3/drpall-v2_4_3.fits
    :param data_path: path where to find rcsed table
    :return:
    """
    from astropy.io import fits
    table = fits.open((data_path / 'tables' / 'manga' / 'drpall-v2_4_3').with_suffix(".fits"))
    return table[1].data


def isolate_data_table(table, identifier_list=None):
    r"""
    This function removes all the identifier to get a pure data table which can be appended to another table
    :param table: must be object of the astropy.tables.Table class
    :param identifier_list: can be a list of str you want to remove
    :return: object of the astropy.tables.Table class
    """
    if identifier_list is None:
        identifier_list = ['objid', 'mjd', 'plate', 'fiberid', 'ra', 'dec']
    for identifier in identifier_list:
        if identifier in table.colnames:
            print(identifier)
            table.remove_column(identifier)


def identify_single_rcsed_object(data_path, **kwargs):
    r"""
    identify a single RCSED object by using keywords
    :param data_path: local data path
    :param kwargs: objid, mjd + plate + fiberid or ra + dec to correctly identify
    :return:
    """
    rcsed_coord_table = load_rcsed_coordinate_table(data_path)
    if 'objid' in kwargs:
        return rcsed_coord_table[rcsed_coord_table['objid'] == kwargs.get('objid')]
    elif ('mjd' in kwargs) & ('plate' in kwargs) & ('fiberid' in kwargs):
        return rcsed_coord_table[((rcsed_coord_table['mjd'] == kwargs.get('mjd')) &
                                  (rcsed_coord_table['plate'] == kwargs.get('plate')) &
                                  (rcsed_coord_table['fiberid'] == kwargs.get('fiberid')))]
    elif ('ra' in kwargs) & ('dec' in kwargs):
        return get_closest_object(rcsed_coord_table, **kwargs)
    else:
        raise ReferenceError('The table you are trying to cross match does not have the needed identifier. '
                             'Make sure that the astro tables has one of the following identifiers: '
                             'objid or mjd + plate + fiberid or ra + dec')


def identify_rcsed_objects(data_path, **kwargs):
    r"""
    identify SDSS objects by using keywords
    :param data_path: local data path
    :param kwargs: objid, mjd + plate + fiberid or ra + dec to correctly identify
    :return:
    """
    rcsed_coord_table = load_rcsed_coordinate_table(data_path)
    if 'objid' in kwargs:
        objid_list = kwargs.get('objid')

        hashmap_cross_match = {}
        for index, current_objid in zip(range(0, len(rcsed_coord_table)),
                                        np.array(rcsed_coord_table['objid'], dtype=int)):
            hashmap_cross_match.update({str(current_objid): index})

        # index list to map which entries will be filled in the initial table
        index_list = np.zeros(len(objid_list), dtype=int)

        # for loop over second table
        for index, current_objid in zip(range(0, len(objid_list)), np.array(objid_list, dtype=int)):
            current_hashmap = str(current_objid)

            # use only entries which are in in the cross_match table
            if hashmap_cross_match.get(current_hashmap) is None:
                raise LookupError('The Object with the index ', index,  ' and objid ', current_objid,
                                  ' is not in the RCSED catalog. The list of objids must be consistent')
            else:
                index_list[index] = int(hashmap_cross_match.get(current_hashmap))

        # return the RCSED coodinate table
        return rcsed_coord_table[index_list]

    elif ('mjd' in kwargs) & ('plate' in kwargs) & ('fiberid' in kwargs):

        mjd_list = kwargs.get('mjd')
        plate_list = kwargs.get('plate')
        fiberid_list = kwargs.get('fiberid')

        # initialize hashmap/dict
        hashmap_cross_match = {}
        for index, current_mjd, current_plate, current_fiberid in zip(range(0, len(rcsed_coord_table)),
                                                                      np.array(rcsed_coord_table['mjd'], dtype=int),
                                                                      np.array(rcsed_coord_table['plate'], dtype=int),
                                                                      np.array(rcsed_coord_table['fiberid'],
                                                                               dtype=int)):
            hashmap_cross_match.update({str(current_mjd) + '-' + str(current_plate) + '-' +
                                        str(current_fiberid): index})

        # index list to map which entries will be filled in the initial table
        index_list = np.zeros(len(mjd_list), dtype=int)

        # for loop over second table
        for index, current_mjd, current_plate, current_fiberid in zip(range(0, len(mjd_list)),
                                                                      np.array(mjd_list, dtype=int),
                                                                      np.array(plate_list, dtype=int),
                                                                      np.array(fiberid_list, dtype=int)):
            current_hashmap = str(current_mjd) + '-' + str(current_plate) + '-' + str(current_fiberid)

            # use only entries which are in in the cross_match table
            if hashmap_cross_match.get(current_hashmap) is None:
                raise LookupError('The Object with the index ', index,  ' and mjd-plate-fiberid ', current_hashmap,
                                  ' is not in the RCSED catalog. The list of objids must be consistent')
            else:
                index_list[index] = int(hashmap_cross_match.get(current_hashmap))

        # retuen the RCSED coodinate table
        return rcsed_coord_table[index_list]

    elif ('ra' in kwargs) & ('dec' in kwargs):
        return get_closest_object(rcsed_coord_table, **kwargs)
    else:
        raise ReferenceError('The table you are trying to cross match does not have the needed identifier. '
                             'Make sure that the astro tables has one of the following identifiers: '
                             'objid or mjd + plate + fiberid or ra + dec')


def get_rcsed_object_table(data_path, **kwargs):
    r"""
    function to construct a table only with identifiers based on a few identifiers
    :param data_path: local data path
    :param kwargs: objid, mjd + plate + fiberid or ra + dec to correctly identify
    :return: table
    """

    if ('mjd' in kwargs) & ('plate' in kwargs) & ('fiberid' in kwargs):
        mjd = kwargs.get('mjd')
        plate = kwargs.get('plate')
        fiberid = kwargs.get('fiberid')

        # load remaining identifier
        if isinstance(mjd, int) & isinstance(plate, int) & isinstance(fiberid, int):
            coord_table = identify_single_rcsed_object(data_path, mjd=mjd, plate=plate, fiberid=fiberid)
        elif isinstance(mjd, float) | isinstance(plate, float) | isinstance(fiberid, float):
            raise KeyError('mjd, plate and fiberid cannot be float. They must be integer')
        elif len(mjd) == len(plate) == len(fiberid):
            coord_table = identify_rcsed_objects(data_path, mjd=mjd, plate=plate, fiberid=fiberid)
        else:
            raise KeyError('mjd, plate and fiberid must be integer or lists of the same length')
        ra = coord_table['ra']
        dec = coord_table['dec']
        objid = coord_table['objid']
    elif ('ra' in kwargs) & ('dec' in kwargs):
        ra = kwargs.get('ra')
        dec = kwargs.get('dec')
        # load remaining identifier
        if isinstance(ra, float) & isinstance(dec, float):
            coord_table = identify_single_rcsed_object(data_path, **kwargs)
            if coord_table is None:
                raise LookupError('The Object at ra ', ra, ' and dec ', dec,
                                  ' is not in the RCSED catalog. The list of objids must be consistent')
        elif len(ra) == len(dec):
            coord_table = identify_rcsed_objects(data_path, **kwargs)
            if coord_table.size < len(ra):
                raise LookupError('Not all objects are in the RSCED catalog. Make sure the given coordinates are part '
                                  'of the RCSED catalog or adjust the search radius')
        else:
            raise KeyError('ra and dec must be integer or lists of the same length')
        objid = coord_table['objid']
        mjd = coord_table['mjd']
        plate = coord_table['plate']
        fiberid = coord_table['fiberid']
    elif 'objid' in kwargs:  # check if objid is the first identifier
        objid = kwargs.get('objid')
        if isinstance(objid, int):
            coord_table = identify_single_rcsed_object(data_path, objid=objid)
        else:
            coord_table = identify_rcsed_objects(data_path, objid=objid)
        mjd = coord_table['mjd']
        plate = coord_table['plate']
        fiberid = coord_table['fiberid']
        ra = coord_table['ra']
        dec = coord_table['dec']
    else:
        raise KeyError('No SDSS identifier for single object was found; '
                       'please use objid, '
                       'mjd + plate + fiberid or '
                       'ra + dec')
    # create table of single object
    return object_table(objid, mjd, plate, fiberid, ra, dec)


def identify_manga_object(data_path, ra, dec, search_radius=10):
    r"""
    function to find manga data using ra and dec of a point in a sky with a search radius of 10"
    :param data_path: local data path
    :param ra: ra coordinate where to search
    :param dec: dec coordinate where to search
    :param search_radius: search radius to identify object, should be around 10"
    :return: table with with one or multiple objects. In case of no match inside the radius it returns None
    """
    manga_coord_table = load_manga_coordinate_table(data_path)
    return get_closest_object(manga_coord_table, ra, dec, search_radius=search_radius)


def get_closest_object(table, ra, dec, search_radius=1):
    r"""
    Function to find neighbors
    :param table: can be rcsed or manga table (.fits) It needs ra and dec
    :param ra: ra coordinate
    :param dec: dec coordinate
    :param search_radius: search radius in arcsec
    :return: table with with one or multiple objects. In case of no match inside the radius it returns None
    """

    # convert ra and dec tables into SkyCoord objects
    from astropy.coordinates import SkyCoord
    import astropy.units as u

    if hasattr(table['ra'], 'unit'):
        if table['ra'].unit == 'deg':
            coord_table = SkyCoord(table['ra'], table['dec'])
        else:
            raise KeyError('the objects unit was not understand')
    else:
        coord_table = SkyCoord(table['ra']*u.deg, table['dec']*u.deg)

    if hasattr(ra, 'unit'):
        if ra.unit == 'deg':
            coord_object = SkyCoord(ra*u.deg, dec*u.deg)
        else:
            raise KeyError('the objects unit was not understand')
    else:
        coord_object = SkyCoord(ra*u.deg, dec*u.deg)

    # find the smallest distance
    idx_table, d2d_table, d3d_table = coord_object.match_to_catalog_sky(coord_table)

    if idx_table.size == 1:
        if d2d_table.arcsec < search_radius:
            return table[idx_table]
        else:
            return None
    else:
        identified_counter_parts = idx_table[d2d_table.arcsec < search_radius]
        return table[identified_counter_parts]


def initialise_project(project_name, data_path='data'):
    # use short cut
    if data_path == 'data':
        data_path = Path.home() / 'data'

    # create folder for project table
    project_file_path = data_path / 'tables' / 'project_tables' / project_name
    if not os.path.isdir(project_file_path):
        os.makedirs(project_file_path)

    # paths for masks
    if not os.path.isdir(project_file_path / 'subsets'):
        os.makedirs(project_file_path / 'subsets')
    if not os.path.isdir(project_file_path / 'subsets' / 'bpt'):
        os.makedirs(project_file_path / 'subsets' / 'bpt')
    if not os.path.isdir(project_file_path / 'subsets' / 'morphology'):
        os.makedirs(project_file_path / 'subsets' / 'morphology')
    if not os.path.isdir(project_file_path / 'subsets' / 'other'):
        os.makedirs(project_file_path / 'subsets' / 'other')
    return project_file_path
