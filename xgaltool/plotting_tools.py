#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import numpy.ma as ma
import os

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from scipy import stats

from xgaltool import basic_attributes, data_access


class StatisticFunctions:

    def __init__(self):
        r"""
        Class of different statistical tools
        """

    @staticmethod
    def get_y_axis_binned(bins, x_values, y_values, mode='mean'):
        if mode == 'mean':
            mean_y = np.zeros(len(bins) - 1)
            std_y = np.zeros(len(bins) - 1)
            for i in range(0, len(bins) - 1):
                x_range = (x_values.data > bins[i]) & (x_values.data < bins[i + 1])
                if sum(x_range) > 0:
                    mean_y[i] = ma.mean(y_values[x_range])
                    std_y[i] = ma.std(y_values[x_range])
                else:
                    mean_y[i] = np.nan
                    std_y[i] = np.nan
            return mean_y, std_y
        elif mode == 'median':
            med_y = np.zeros(len(bins) - 1)
            p16_y = np.zeros(len(bins) - 1)
            p84_y = np.zeros(len(bins) - 1)
            for i in range(0, len(bins) - 1):
                x_range = (x_values > bins[i]) & (x_values < bins[i + 1]) & ~np.isnan(x_values)
                if sum(x_range) > 0:
                    med_y[i] = np.nanmedian(y_values[x_range])
                    p16_y[i] = np.nanpercentile(y_values[x_range], 16)
                    p84_y[i] = np.nanpercentile(y_values[x_range], 84)
                else:
                    med_y[i] = np.nan
                    p16_y[i] = np.nan
                    p84_y[i] = np.nan
            return med_y, p16_y, p84_y
        else:
            raise ValueError('mode not understand')

    @staticmethod
    def get_centre_of_bin(bins):
        return (bins[:-1] + bins[1:]) / 2


class DensityContours:

    def __init__(self):
        r"""
        Class to produce contour lines

        """

    @staticmethod
    def compute_contour_counts(x_data, y_data):

        good = np.invert(((np.isnan(x_data) | np.isnan(y_data)) | (np.isinf(x_data) | np.isinf(y_data))))

        k = stats.gaussian_kde(np.vstack([x_data[good], y_data[good]]))
        xi, yi = np.mgrid[x_data[good].min():x_data[good].max():x_data[good].size**0.5*1j,
                              y_data[good].min():y_data[good].max():y_data[good].size**0.5*1j]

        zi = k(np.vstack([xi.flatten(), yi.flatten()]))

        # set zi_1 to 0-1 scale
        zi = (zi-zi.min())/(zi.max() - zi.min())
        zi = zi.reshape(xi.shape)
        return xi, yi, zi

    @staticmethod
    def plot_contours_percentage(ax, xi, yi, zi, color='black', percent=True, **kwargs):

        if 'linewidth' in kwargs:
            linewidth = kwargs.get('linewidth')
        else:
            linewidth = 1.5

        if 'fontsize' in kwargs:
            fontsize = kwargs.get('fontsize')
        else:
            fontsize = 8

        if 'alpha' in kwargs:
            alpha = kwargs.get('alpha')
        else:
            alpha = 1

        if 'contour_levels' in kwargs:
            contour_levels = kwargs.get('contour_levels')
        else:
            contour_levels = [0, 0.1, 0.2, 0.5, 0.68, 0.95, 0.99]

        cs = ax.contour(xi, yi, zi, levels=contour_levels, colors=color, linewidths=linewidth, origin='lower',
                        alpha=alpha)
        if percent:
            ax.clabel(cs, fmt='%.2f', colors=color, fontsize=fontsize)

    @staticmethod
    def get_contours_percentage(ax, x_data, y_data,
                                color='black', percent=True, **kwargs):

        if 'linewidth' in kwargs:
            linewidth = kwargs.get('linewidth')
        else:
            linewidth = 1.5

        if 'fontsize' in kwargs:
            fontsize = kwargs.get('fontsize')
        else:
            fontsize = 8    

        if 'alpha' in kwargs:
            alpha = kwargs.get('alpha')
        else:
            alpha = 1

        if 'contour_levels' in kwargs:
            contour_levels = kwargs.get('contour_levels')
        else:
            contour_levels = [0, 0.1, 0.2, 0.5, 0.68, 0.95, 0.99]
            # contour_levels = [0.68, 0.95]

        good = np.invert(((np.isnan(x_data) | np.isnan(y_data)) | (np.isinf(x_data) | np.isinf(y_data))))

        k = stats.gaussian_kde(np.vstack([x_data[good], y_data[good]]))
        xi, yi = np.mgrid[x_data[good].min():x_data[good].max():x_data[good].size**0.5*1j,
                              y_data[good].min():y_data[good].max():y_data[good].size**0.5*1j]

        zi = k(np.vstack([xi.flatten(), yi.flatten()]))

        # set zi_1 to 0-1 scale
        zi = (zi-zi.min())/(zi.max() - zi.min())
        zi = zi.reshape(xi.shape)

        cs = ax.contour(xi, yi, zi, levels=contour_levels, colors=color, linewidths=linewidth, origin='lower',
                        alpha=alpha)
        if percent:
            ax.clabel(cs, fmt='%.2f', colors=color, fontsize=fontsize)

    @staticmethod
    def get_two_contours_percentage(ax, x_data_1, y_data_1, x_data_2, y_data_2,
                                    color_1='black', color_2='blue', percent_1=True, percent_2=True, **kwargs):

        if 'linewidth' in kwargs:
            linewidth = kwargs.get('linewidth')
        else:
            linewidth = 1.5

        if 'fontsize' in kwargs:
            fontsize = kwargs.get('fontsize')
        else:
            fontsize = 8

        if 'alpha' in kwargs:
            alpha = kwargs.get('alpha')
        else:
            alpha = 1

        if 'contour_levels' in kwargs:
            contour_levels = kwargs.get('contour_levels')
        else:
            # contour_levels = [0, 0.1, 0.2, 0.5, 0.68, 0.95, 0.99]
            contour_levels = [0, 0.2, 0.5, 0.68, 0.95, 0.99]
            # contour_levels = [0.68, 0.95]

        good_1 = np.invert(np.isnan(x_data_1) | np.isnan(y_data_1))
        good_2 = np.invert(np.isnan(x_data_2) | np.isnan(y_data_2))

        k_1 = stats.gaussian_kde(np.vstack([x_data_1[good_1], y_data_1[good_1]]))

        if ('x_lim_1' in kwargs) & ('y_lim_1' in kwargs):
            x_lim_1 = kwargs.get('x_lim_1')
            y_lim_1 = kwargs.get('y_lim_1')
        else:
            x_lim_1 = (x_data_1[good_1].min(), x_data_1[good_1].max())
            y_lim_1 = (y_data_1[good_1].min(), y_data_1[good_1].max())
        if 'size_1' in kwargs:
            size_1 = kwargs.get('size_1')
        else:
            size_1 = x_data_1[good_1].size
        xi_1, yi_1 = np.mgrid[x_lim_1[0]:x_lim_1[1]:size_1**0.5*1j,
                              y_lim_1[0]:y_lim_1[1]:size_1**0.5*1j]

        # xi_1, yi_1 = np.mgrid[x_data_1[good_1].min():x_data_1[good_1].max():x_data_1[good_1].size**0.5*1j,
        #                       y_data_1[good_1].min():y_data_1[good_1].max():y_data_1[good_1].size**0.5*1j]

        zi_1 = k_1(np.vstack([xi_1.flatten(), yi_1.flatten()]))

        # set zi_1 to 0-1 scale
        zi_1 = (zi_1-zi_1.min())/(zi_1.max() - zi_1.min())
        zi_1 = zi_1.reshape(xi_1.shape)

        cs_1 = ax.contour(xi_1, yi_1, zi_1, levels=contour_levels, colors=color_1, linewidths=linewidth, origin='lower',
                          alpha=alpha)

        if percent_1:
            ax.clabel(cs_1, fmt='%.2f', colors=color_1, fontsize=fontsize)

        k_2 = stats.gaussian_kde(np.vstack([x_data_2[good_2], y_data_2[good_2]]))

        if ('x_lim_2' in kwargs) & ('y_lim_2' in kwargs):
            x_lim_2 = kwargs.get('x_lim_2')
            y_lim_2 = kwargs.get('y_lim_2')
        else:
            x_lim_2 = (x_data_2[good_2].min(), x_data_2[good_2].max())
            y_lim_2 = (y_data_2[good_2].min(), y_data_2[good_2].max())
        if 'size_2' in kwargs:
            size_2 = kwargs.get('size_2')
        else:
            size_2 = x_data_2[good_2].size
        xi_2, yi_2 = np.mgrid[x_lim_2[0]:x_lim_2[1]:size_2**0.5*1j,
                              y_lim_2[0]:y_lim_2[1]:size_2**0.5*1j]
        # xi_2, yi_2 = np.mgrid[x_data_2[good_2].min():x_data_2[good_2].max():x_data_2[good_2].size**0.5*1j,
        #                       y_data_2[good_2].min():y_data_2[good_2].max():y_data_2[good_2].size**0.5*1j]

        zi_2 = k_2(np.vstack([xi_2.flatten(), yi_2.flatten()]))

        # set zi_2 to 0-1 scale
        zi_2 = (zi_2-zi_2.min())/(zi_2.max() - zi_2.min())
        zi_2 =zi_2.reshape(xi_2.shape)

        cs_2 = ax.contour(xi_2, yi_2, zi_2, levels=contour_levels, colors=color_2, linewidths=linewidth, origin='lower',
                          alpha=alpha)

        if percent_2:
            ax.clabel(cs_2, fmt='%.2f', colors=color_2, fontsize=fontsize)

    @staticmethod
    def get_three_contours_percentage(ax, x_data_1, y_data_1, x_data_2, y_data_2, x_data_3, y_data_3,
                                      color_1='black', color_2='blue', color_3='red',
                                      percent_1=True, percent_2=True, percent_3=True, **kwargs):

        if 'linewidth' in kwargs:
            linewidth = kwargs.get('linewidth')
        else:
            linewidth = 1.5

        if 'fontsize' in kwargs:
            fontsize = kwargs.get('fontsize')
        else:
            fontsize = 8

        if 'alpha_1' in kwargs:
            alpha_1 = kwargs.get('alpha_1')
        else:
            alpha_1 = 1

        if 'alpha_2' in kwargs:
            alpha_2 = kwargs.get('alpha_2')
        else:
            alpha_2 = 1

        if 'alpha_3' in kwargs:
            alpha_3 = kwargs.get('alpha_3')
        else:
            alpha_3 = 1

        if 'contour_levels_1' in kwargs:
            contour_levels_1 = kwargs.get('contour_levels_1')
        else:
            contour_levels_1 = [0, 0.1, 0.2, 0.5, 0.68, 0.95, 0.99]

        if 'contour_levels_2' in kwargs:
            contour_levels_2 = kwargs.get('contour_levels_2')
        else:
            contour_levels_2 = [0, 0.1, 0.2, 0.5, 0.68, 0.95, 0.99]

        if 'contour_levels_3' in kwargs:
            contour_levels_3 = kwargs.get('contour_levels_3')
        else:
            contour_levels_3 = [0, 0.1, 0.2, 0.5, 0.68, 0.95, 0.99]


        k_1 = stats.gaussian_kde(np.vstack([x_data_1, y_data_1]))
        xi_1, yi_1 = np.mgrid[x_data_1.min():x_data_1.max():x_data_1.size**0.5*1j,
                              y_data_1.min():y_data_1.max():y_data_1.size**0.5*1j]

        zi_1 = k_1(np.vstack([xi_1.flatten(), yi_1.flatten()]))

        # set zi_1 to 0-1 scale
        zi_1 = (zi_1-zi_1.min())/(zi_1.max() - zi_1.min())
        zi_1 = zi_1.reshape(xi_1.shape)

        cs_1 = ax.contour(xi_1, yi_1, zi_1, levels=contour_levels_1, colors=color_1, linewidths=linewidth, origin='lower',
                          alpha=alpha_1)

        if percent_1:
            ax.clabel(cs_1, fmt='%.2f', colors=color_1, fontsize=fontsize)

        k_2 = stats.gaussian_kde(np.vstack([x_data_2, y_data_2]))
        xi_2, yi_2 = np.mgrid[x_data_2.min():x_data_2.max():x_data_2.size**0.5*1j,
                              y_data_2.min():y_data_2.max():y_data_2.size**0.5*1j]

        zi_2 = k_2(np.vstack([xi_2.flatten(), yi_2.flatten()]))

        # set zi_2 to 0-1 scale
        zi_2 = (zi_2-zi_2.min())/(zi_2.max() - zi_2.min())
        zi_2 = zi_2.reshape(xi_2.shape)

        cs_2 = ax.contour(xi_2, yi_2, zi_2, levels=contour_levels_2, colors=color_2, linewidths=linewidth, origin='lower',
                          alpha=alpha_2)

        if percent_2:
            ax.clabel(cs_2, fmt='%.2f', colors=color_2, fontsize=fontsize)

        k_3 = stats.gaussian_kde(np.vstack([x_data_3, y_data_3]))
        xi_3, yi_3 = np.mgrid[x_data_3.min():x_data_3.max():x_data_3.size**0.5*1j,
                              y_data_3.min():y_data_3.max():y_data_3.size**0.5*1j]

        zi_3 = k_3(np.vstack([xi_3.flatten(), yi_3.flatten()]))

        # set zi_3 to 0-1 scale
        zi_3 = (zi_3-zi_3.min())/(zi_3.max() - zi_3.min())
        zi_3 = zi_3.reshape(xi_3.shape)

        cs_3 = ax.contour(xi_3, yi_3, zi_3, levels=contour_levels_3, colors=color_3, linewidths=linewidth, origin='lower',
                          alpha=alpha_3)

        if percent_3:
            ax.clabel(cs_3, fmt='%.2f', colors=color_3, fontsize=fontsize)

    @staticmethod
    def get_four_contours_percentage(ax, x_data_1, y_data_1, x_data_2, y_data_2, x_data_3, y_data_3, x_data_4, y_data_4,
                                      color_1='black', color_2='blue', color_3='m', color_4='c',
                                      percent_1=True, percent_2=True, percent_3=True, percent_4=True, **kwargs):

        if 'linewidth' in kwargs:
            linewidth = kwargs.get('linewidth')
        else:
            linewidth = 1.5

        if 'fontsize' in kwargs:
            fontsize = kwargs.get('fontsize')
        else:
            fontsize = 8

        if 'alpha_1' in kwargs:
            alpha_1 = kwargs.get('alpha_1')
        else:
            alpha_1 = 1

        if 'alpha_2' in kwargs:
            alpha_2 = kwargs.get('alpha_2')
        else:
            alpha_2 = 1

        if 'alpha_3' in kwargs:
            alpha_3 = kwargs.get('alpha_3')
        else:
            alpha_3 = 1

        if 'alpha_4' in kwargs:
            alpha_4 = kwargs.get('alpha_4')
        else:
            alpha_4 = 1

        if 'contour_levels_1' in kwargs:
            contour_levels_1 = kwargs.get('contour_levels_1')
        else:
            contour_levels_1 = [0, 0.1, 0.2, 0.5, 0.68, 0.95, 0.99]

        if 'contour_levels_2' in kwargs:
            contour_levels_2 = kwargs.get('contour_levels_2')
        else:
            contour_levels_2 = [0, 0.1, 0.2, 0.5, 0.68, 0.95, 0.99]

        if 'contour_levels_3' in kwargs:
            contour_levels_3 = kwargs.get('contour_levels_3')
        else:
            contour_levels_3 = [0, 0.1, 0.2, 0.5, 0.68, 0.95, 0.99]


        if 'contour_levels_4' in kwargs:
            contour_levels_4 = kwargs.get('contour_levels_4')
        else:
            contour_levels_4 = [0, 0.1, 0.2, 0.5, 0.68, 0.95, 0.99]


        k_1 = stats.gaussian_kde(np.vstack([x_data_1, y_data_1]))
        xi_1, yi_1 = np.mgrid[x_data_1.min():x_data_1.max():x_data_1.size**0.5*1j,
                              y_data_1.min():y_data_1.max():y_data_1.size**0.5*1j]

        zi_1 = k_1(np.vstack([xi_1.flatten(), yi_1.flatten()]))

        # set zi_1 to 0-1 scale
        zi_1 = (zi_1-zi_1.min())/(zi_1.max() - zi_1.min())
        zi_1 = zi_1.reshape(xi_1.shape)

        cs_1 = ax.contour(xi_1, yi_1, zi_1, levels=contour_levels_1, colors=color_1, linewidths=linewidth, origin='lower',
                          alpha=alpha_1)

        if percent_1:
            ax.clabel(cs_1, fmt='%.2f', colors=color_1, fontsize=fontsize)

        k_2 = stats.gaussian_kde(np.vstack([x_data_2, y_data_2]))
        xi_2, yi_2 = np.mgrid[x_data_2.min():x_data_2.max():x_data_2.size**0.5*1j,
                              y_data_2.min():y_data_2.max():y_data_2.size**0.5*1j]

        zi_2 = k_2(np.vstack([xi_2.flatten(), yi_2.flatten()]))

        # set zi_2 to 0-1 scale
        zi_2 = (zi_2-zi_2.min())/(zi_2.max() - zi_2.min())
        zi_2 = zi_2.reshape(xi_2.shape)

        cs_2 = ax.contour(xi_2, yi_2, zi_2, levels=contour_levels_2, colors=color_2, linewidths=linewidth, origin='lower',
                          alpha=alpha_2)

        if percent_2:
            ax.clabel(cs_2, fmt='%.2f', colors=color_2, fontsize=fontsize)

        k_3 = stats.gaussian_kde(np.vstack([x_data_3, y_data_3]))
        xi_3, yi_3 = np.mgrid[x_data_3.min():x_data_3.max():x_data_3.size**0.5*1j,
                              y_data_3.min():y_data_3.max():y_data_3.size**0.5*1j]

        zi_3 = k_3(np.vstack([xi_3.flatten(), yi_3.flatten()]))

        # set zi_3 to 0-1 scale
        zi_3 = (zi_3-zi_3.min())/(zi_3.max() - zi_3.min())
        zi_3 = zi_3.reshape(xi_3.shape)

        cs_3 = ax.contour(xi_3, yi_3, zi_3, levels=contour_levels_3, colors=color_3, linewidths=linewidth, origin='lower',
                          alpha=alpha_3)

        if percent_3:
            ax.clabel(cs_3, fmt='%.2f', colors=color_3, fontsize=fontsize)


        k_4 = stats.gaussian_kde(np.vstack([x_data_4, y_data_4]))
        xi_4, yi_4 = np.mgrid[x_data_4.min():x_data_4.max():x_data_4.size**0.5*1j,
                              y_data_4.min():y_data_4.max():y_data_4.size**0.5*1j]

        zi_4 = k_4(np.vstack([xi_4.flatten(), yi_4.flatten()]))

        # set zi_4 to 0-1 scale
        zi_4 = (zi_4-zi_4.min())/(zi_4.max() - zi_4.min())
        zi_4 = zi_4.reshape(xi_4.shape)

        cs_4 = ax.contour(xi_4, yi_4, zi_4, levels=contour_levels_4, colors=color_4, linewidths=linewidth, origin='lower',
                          alpha=alpha_4)

        if percent_4:
            ax.clabel(cs_4, fmt='%.2f', colors=color_4, fontsize=fontsize)


class ColorColorMag(basic_attributes.PublishingQualityParameters):

    def __init__(self):
        r"""
        Class to produce different plots of color-mag and color-color-magnitude diagrams
        function to plot color-color-magnitude diagram following Chilingarian et al. 2011
        doi:10.1111/j.1365-2966.2011.19837.
        """

        super().__init__(self)

        self.contour_levels = np.logspace(0.3, 2, 13)
        self.nuv_r_range = (0.5, 6.5)
        self.nuv_z_range = (0.5, 6.5)
        self.g_r_range = (0.05, 1.05)
        self.g_z_range = (0.05, 2)
        self.delta_g_r_range = (-0.15, 0.15)
        self.delta_g_z_range = (-0.25, 0.25)

        # load best fit model
        # attention: in both files mag_z_grid is the same check for future and for more general version
        self.nuv_r_grid, self.mag_z_grid, self.g_r_grid = ColorColorMag.get_nuv_g_r_z_fit()
        self.nuv_z_grid, self.mag_z_grid, self.g_z_grid = ColorColorMag.get_nuv_z_g_z_fit()

    def nuv_gr_z_compare(self, color_nuv_r_reference, color_g_r_reference, morphology_reference,
                         color_nuv_r_1, color_g_r_1, color_nuv_r_2, color_g_r_2, **kwargs):
        r"""
        Function to execute color-color-magnitude diagram as a 2D projection following Fig. 1 in
        Chilingarian et al. 2011 doi:10.1111/j.1365-2966.2011.19837.
        :param color_nuv_r_reference: Difference between the NUV- and r-band magnitude of a reference catalog.
        :type color_nuv_r_reference: array_like
        :param color_g_r_reference: Difference between the g- and r-band magnitude of a reference catalog.
        :type color_g_r_reference: array_like
        :param morphology_reference: Masks for morphological classifications of the reference catalog
        :type morphology_reference: dictionary
        :param color_nuv_r_1: Difference between the NUV- and r-band magnitude for the first sample.
        :type color_nuv_r_1: array_like
        :param color_g_r_1: Difference between the g- and r-band magnitude for the first sample.
        :type color_g_r_1: array_like
        :param color_nuv_r_2: Difference between the NUV- and r-band magnitude for the second sample.
        :type color_nuv_r_2: array_like
        :param color_g_r_2: Difference between the g- and r-band magnitude for the second sample.
        :type color_g_r_2: array_like
        :return Figure
        """

        if 'label_1' in kwargs:
            label_1 = kwargs.get('label_1')
        else:
            label_1 = ''

        if 'label_2' in kwargs:
            label_2 = kwargs.get('label_2')
        else:
            label_2 = ''

        # create figure
        fig, ax = plt.subplots(ncols=1, nrows=2, sharex='col', figsize=(self.fig_size_x_axis_half_page,
                                                                        self.fig_size_y_axis_half_page))

        DensityContours.get_two_contours(ax[0], color_nuv_r_reference[morphology_reference['elliptical']],
                                         color_g_r_reference[morphology_reference['elliptical']],
                                         color_nuv_r_reference[morphology_reference['ltg']],
                                         color_g_r_reference[morphology_reference['ltg']],
                                         x_borders=(0, 7), y_borders=(0, 1),
                                         contour_size=(4, 4), bins=(100, 100), contour_levels_1=self.contour_levels,
                                         color_1='r', color_2='b', label_1='RCSED Elliptical', label_2='RCSED LTG')

        DensityContours.get_two_contours(ax[1], color_nuv_r_reference[morphology_reference['elliptical']],
                                         color_g_r_reference[morphology_reference['elliptical']],
                                         color_nuv_r_reference[morphology_reference['ltg']],
                                         color_g_r_reference[morphology_reference['ltg']],
                                         x_borders=(0, 7), y_borders=(0, 1),
                                         contour_size=(4, 4), bins=(100, 100), contour_levels_1=self.contour_levels,
                                         color_1='r', color_2='b', label_1='RCSED Elliptical', label_2='RCSED LTG')


        ax[0].scatter(color_nuv_r_1, color_g_r_1, color='k', s=self.marker_size_half_page, label=label_1, alpha=0.5)

        ax[0].plot([], [], '', color='r', label='RCSED Elliptical')
        ax[0].plot([], [], '', color='b', label='RCSED LTG')

        g_r_curve_24 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_r_grid, mag=-24)
        g_r_curve_23 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_r_grid, mag=-23)
        g_r_curve_22 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_r_grid, mag=-22)
        g_r_curve_21 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_r_grid, mag=-21)

        reference, = ax[0].plot([], [], '', color='white')
        curve_24, = ax[0].plot(self.nuv_r_grid, g_r_curve_24, linewidth=self.line_width_half_page, color='c')
        curve_23, = ax[0].plot(self.nuv_r_grid, g_r_curve_23, linewidth=self.line_width_half_page, color='darkorange')
        curve_22, = ax[0].plot(self.nuv_r_grid, g_r_curve_22, linewidth=self.line_width_half_page, color='g')
        curve_21, = ax[0].plot(self.nuv_r_grid, g_r_curve_21, linewidth=self.line_width_half_page, color='m')

        leg1 = ax[0].legend(loc='lower right', frameon=False, fontsize=self.fontsize_half_page)
        # Add second legend for the maxes and mins.
        # leg1 will be removed from figure
        ax[0].legend([reference, curve_24, curve_23, curve_22, curve_21], ['Chilingarian and Zolotukhin 2012',
                                                                 r'M$_{\rm z}$ = -24 mag',
                                                                 r'M$_{\rm z}$ = -23 mag',
                                                                 r'M$_{\rm z}$ = -22 mag',
                                                                 r'M$_{\rm z}$ = -21 mag'],
                     loc='upper left', frameon=False, fontsize=self.fontsize_half_page)

        # Manually add the first legend back
        ax[0].add_artist(leg1)

        ax[0].set_xlim(self.nuv_r_range)
        ax[0].set_ylim(self.g_r_range)


        ax[1].scatter(color_nuv_r_2, color_g_r_2, color='k', s=self.marker_size_half_page, label=label_2, alpha=0.5)

        # ax[1].plot([], [], '', color='r', label='RCSED Elliptical')
        # ax[1].plot([], [], '', color='b', label='RCSED LTG')

        g_r_curve_24 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_r_grid, mag=-24)
        g_r_curve_23 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_r_grid, mag=-23)
        g_r_curve_22 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_r_grid, mag=-22)
        g_r_curve_21 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_r_grid, mag=-21)

        reference, = ax[1].plot([], [], '', color='white')
        curve_24, = ax[1].plot(self.nuv_r_grid, g_r_curve_24, linewidth=self.line_width_half_page, color='c')
        curve_23, = ax[1].plot(self.nuv_r_grid, g_r_curve_23, linewidth=self.line_width_half_page, color='darkorange')
        curve_22, = ax[1].plot(self.nuv_r_grid, g_r_curve_22, linewidth=self.line_width_half_page, color='g')
        curve_21, = ax[1].plot(self.nuv_r_grid, g_r_curve_21, linewidth=self.line_width_half_page, color='m')

        # plt.draw()
        #
        # p = leg1.get_window_extent()
        # print(p)
        ax[1].legend(bbox_to_anchor=(0.84, 0.13), frameon=False, fontsize=self.fontsize_half_page)
        # Add second legend for the maxes and mins.
        # leg1 will be removed from figure
        # ax[1].legend([reference, curve_24, curve_23, curve_22, curve_21], ['Chilingarian and Zolotukhin 2012',
        #                                                          r'M$_{\rm z}$ = -24 mag',
        #                                                          r'M$_{\rm z}$ = -23 mag',
        #                                                          r'M$_{\rm z}$ = -22 mag',
        #                                                          r'M$_{\rm z}$ = -21 mag'],
        #              loc='upper left', frameon=False, fontsize=self.fontsize_half_page)

        # Manually add the first legend back
        # ax[1].add_artist(leg1)

        ax[1].set_xlim(self.nuv_r_range)
        ax[1].set_ylim(self.g_r_range)

        # layout
        ax[0].xaxis.set_major_locator(plt.MultipleLocator(1))
        ax[0].xaxis.set_minor_locator(plt.MultipleLocator(1 / 4))
        ax[0].yaxis.set_major_locator(plt.MultipleLocator(0.1))
        ax[0].yaxis.set_minor_locator(plt.MultipleLocator(0.1 / 4))

        ax[1].xaxis.set_major_locator(plt.MultipleLocator(1))
        ax[1].xaxis.set_minor_locator(plt.MultipleLocator(1 / 4))
        ax[1].yaxis.set_major_locator(plt.MultipleLocator(0.1))
        ax[1].yaxis.set_minor_locator(plt.MultipleLocator(0.1 / 4))

        ax[0].set_ylabel(r'g-r, mag', fontsize=self.fontsize_half_page)
        ax[1].set_ylabel(r'g-r, mag', fontsize=self.fontsize_half_page)
        ax[1].set_xlabel(r'NUV-r, mag', fontsize=self.fontsize_half_page)

        ax[0].tick_params(axis='both', which='both', width=2, direction='in', labelsize=self.fontsize_half_page)
        ax[0].xaxis.set_ticks_position('both')
        ax[0].yaxis.set_ticks_position('both')

        ax[1].tick_params(axis='both', which='both', width=2, direction='in', labelsize=self.fontsize_half_page)
        ax[1].xaxis.set_ticks_position('both')
        ax[1].yaxis.set_ticks_position('both')

        plt.tight_layout()
        plt.subplots_adjust(hspace=0)

        return fig

    def nuv_gr_z_diagram(self, color_nuv_r_reference, color_g_r_reference,
                         color_nuv_r, color_g_r, abs_mag_z, morphology_reference, label='', ssfr=None):
        r"""
        Function to execute color-color-magnitude diagram as a 2D projection following Fig. 1 in
        Chilingarian et al. 2011 doi:10.1111/j.1365-2966.2011.19837.
        :param color_nuv_r_reference: Difference between the NUV- and r-band magnitude of a reference catalog.
        :type color_nuv_r_reference: array_like
        :param color_g_r_reference: Difference between the g- and r-band magnitude of a reference catalog.
        :type color_g_r_reference: array_like
        :param color_nuv_r: Difference between the NUV- and r-band magnitude.
        :type color_nuv_r: array_like
        :param color_g_r: Difference between the g- and r-band magnitude.
        :type color_g_r: array_like
        :param abs_mag_z: Absolute magnitude in the z-band
        :type abs_mag_z: array_like
        :param morphology_reference: Masks for morphological classifications of the reference catalog
        :type morphology_reference: dictionary
        :param label: label of presented catalog
        :type label: str
        :return Figure
        """

        # get residuals
        residuals_g_r = ColorColorMag.get_color_color_residuals(self.nuv_r_grid, self.mag_z_grid, self.g_r_grid,
                                                                color_nuv_r, abs_mag_z, color_g_r)

        # get magnitude masks
        mag_24 = (abs_mag_z.data > -24.5) & (abs_mag_z.data < -23.5)
        mag_23 = (abs_mag_z.data > -23.5) & (abs_mag_z.data < -22.5)
        mag_22 = (abs_mag_z.data > -22.5) & (abs_mag_z.data < -21.5)
        mag_21 = (abs_mag_z.data > -21.5) & (abs_mag_z.data < -20.5)

        # create figure
        fig, ax = plt.subplots(ncols=1, nrows=5, sharex='col', figsize=(self.fig_size_x_axis_half_page,
                                                                        self.fig_size_y_axis_half_page),
                               gridspec_kw={'height_ratios': [3, 1, 1, 1, 1]})

        DensityContours.get_two_contours(ax[0], color_nuv_r_reference[morphology_reference['elliptical']],
                                            color_g_r_reference[morphology_reference['elliptical']],
                                            color_nuv_r_reference[morphology_reference['ltg']],
                                            color_g_r_reference[morphology_reference['ltg']],
                                            x_borders=(0, 7), y_borders=(0, 1),
                                            contour_size=(4, 4), bins=(100, 100), contour_levels_1=self.contour_levels,
                                            color_1='r', color_2='b', label_1='RCSED Elliptical', label_2='RCSED LTG')

        # plot all different morphologies
        # self._plot_morphologies(ax[0], color_nuv_r, color_g_r, morphology=morphology, label=label)

        mask_1 = (ssfr > -11.8) & (ssfr < -10.8)
        ax[0].scatter(color_nuv_r[~mask_1], color_g_r[~mask_1], color='k', s=self.marker_size_half_page, label=label, alpha=0.5)
        ax[0].scatter(color_nuv_r[mask_1], color_g_r[mask_1], color='y', s=self.marker_size_half_page + 5, marker='*', label=label + ' intermediate SSFR')

        ax[0].plot([], [], '', color='r', label='RCSED Elliptical')
        ax[0].plot([], [], '', color='b', label='RCSED LTG')

        g_r_curve_24 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_r_grid, mag=-24)
        g_r_curve_23 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_r_grid, mag=-23)
        g_r_curve_22 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_r_grid, mag=-22)
        g_r_curve_21 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_r_grid, mag=-21)

        reference, = ax[0].plot([], [], '', color='white')
        curve_24, = ax[0].plot(self.nuv_r_grid, g_r_curve_24, linewidth=self.line_width_half_page, color='c')
        curve_23, = ax[0].plot(self.nuv_r_grid, g_r_curve_23, linewidth=self.line_width_half_page, color='darkorange')
        curve_22, = ax[0].plot(self.nuv_r_grid, g_r_curve_22, linewidth=self.line_width_half_page, color='g')
        curve_21, = ax[0].plot(self.nuv_r_grid, g_r_curve_21, linewidth=self.line_width_half_page, color='m')

        leg1 = ax[0].legend(loc='lower right', frameon=False, fontsize=self.fontsize_half_page)
        # Add second legend for the maxes and mins.
        # leg1 will be removed from figure
        ax[0].legend([reference, curve_23, curve_22, curve_21], ['Chilingarian and Zolotukhin 2012',
                                                                 r'M$_{\rm z}$ = -23 mag',
                                                                 r'M$_{\rm z}$ = -22 mag',
                                                                 r'M$_{\rm z}$ = -21 mag'],
                     loc='upper left', frameon=False, fontsize=self.fontsize_half_page)

        # Manually add the first legend back
        ax[0].add_artist(leg1)

        ax[0].set_xlim(self.nuv_r_range)
        ax[0].set_ylim(self.g_r_range)

        self._plot_residuals_nuv_g_r_z(ax[1], color_nuv_r[mag_24], residuals_g_r[mag_24], mag=-24, color='c')
        self._plot_residuals_nuv_g_r_z(ax[2], color_nuv_r[mag_23], residuals_g_r[mag_23], mag=-23, color='darkorange')
        self._plot_residuals_nuv_g_r_z(ax[3], color_nuv_r[mag_22], residuals_g_r[mag_22], mag=-22, color='g')
        self._plot_residuals_nuv_g_r_z(ax[4], color_nuv_r[mag_21], residuals_g_r[mag_21], mag=-21, color='m')

        ax[0].xaxis.set_major_locator(plt.MultipleLocator(1))
        ax[0].xaxis.set_minor_locator(plt.MultipleLocator(1 / 4))
        ax[0].yaxis.set_major_locator(plt.MultipleLocator(0.1))
        ax[0].yaxis.set_minor_locator(plt.MultipleLocator(0.1 / 4))

        ax[1].xaxis.set_major_locator(plt.MultipleLocator(1))
        ax[1].xaxis.set_minor_locator(plt.MultipleLocator(1 / 4))
        ax[1].yaxis.set_major_locator(plt.MultipleLocator(0.1))
        ax[1].yaxis.set_minor_locator(plt.MultipleLocator(0.1 / 4))

        ax[2].xaxis.set_major_locator(plt.MultipleLocator(1))
        ax[2].xaxis.set_minor_locator(plt.MultipleLocator(1 / 4))
        ax[2].yaxis.set_major_locator(plt.MultipleLocator(0.1))
        ax[2].yaxis.set_minor_locator(plt.MultipleLocator(0.1 / 4))

        ax[3].xaxis.set_major_locator(plt.MultipleLocator(1))
        ax[3].xaxis.set_minor_locator(plt.MultipleLocator(1 / 4))
        ax[3].yaxis.set_major_locator(plt.MultipleLocator(0.1))
        ax[3].yaxis.set_minor_locator(plt.MultipleLocator(0.1 / 4))

        ax[0].set_ylabel(r'g-r, mag', fontsize=self.fontsize_half_page)
        ax[1].set_ylabel(r'$\Delta$ g-r, mag', fontsize=self.fontsize_half_page)
        ax[2].set_ylabel(r'$\Delta$ g-r, mag', fontsize=self.fontsize_half_page)
        ax[3].set_ylabel(r'$\Delta$ g-r, mag', fontsize=self.fontsize_half_page)
        ax[4].set_ylabel(r'$\Delta$ g-r, mag', fontsize=self.fontsize_half_page)
        ax[4].set_xlabel(r'NUV-r, mag', fontsize=self.fontsize_half_page)

        ax[0].tick_params(axis='both', which='both', width=2, direction='in', labelsize=self.fontsize_half_page)
        ax[0].xaxis.set_ticks_position('both')
        ax[0].yaxis.set_ticks_position('both')

        ax[1].tick_params(axis='both', which='both', width=2, direction='in', labelsize=self.fontsize_half_page)
        ax[1].xaxis.set_ticks_position('both')
        ax[1].yaxis.set_ticks_position('both')

        ax[2].tick_params(axis='both', which='both', width=2, direction='in', labelsize=self.fontsize_half_page)
        ax[2].xaxis.set_ticks_position('both')
        ax[2].yaxis.set_ticks_position('both')

        ax[3].tick_params(axis='both', which='both', width=2, direction='in', labelsize=self.fontsize_half_page)
        ax[3].xaxis.set_ticks_position('both')
        ax[3].yaxis.set_ticks_position('both')

        ax[4].tick_params(axis='both', which='both', width=2, direction='in', labelsize=self.fontsize_half_page)
        ax[4].xaxis.set_ticks_position('both')
        ax[4].yaxis.set_ticks_position('both')

        plt.tight_layout()
        plt.subplots_adjust(hspace=0)

        return fig

    def nuv_gz_z_diagram(self, color_nuv_z_reference, color_g_z_reference,
                          color_nuv_z, color_g_z, abs_mag_z, morphology_reference, label='', ssfr=None):
        r"""
        Function to execute color-color-magnitude diagram as a 2D projection following Fig. 1 in
        Chilingarian et al. 2011 doi:10.1111/j.1365-2966.2011.19837.
        :param color_nuv_z_reference: Difference between the NUV- and r-band magnitude of a reference catalog.
        :type color_nuv_z_reference: array_like
        :param color_g_z_reference: Difference between the g- and r-band magnitude of a reference catalog.
        :type color_g_z_reference: array_like
        :param color_nuv_z: Difference between the NUV- and r-band magnitude.
        :type color_nuv_z: array_like
        :param color_g_z: Difference between the g- and r-band magnitude.
        :type color_g_z: array_like
        :param abs_mag_z: Absolute magnitude in the z-band
        :type abs_mag_z: array_like
        :param morphology_reference: Masks for morphological classifications of the reference catalog
        :type morphology_reference: dictionary
        :param label: label of presented catalog
        :type label: str
        :return Figure
        """

        # get residuals
        residuals_g_z = ColorColorMag.get_color_color_residuals(self.nuv_z_grid, self.mag_z_grid, self.g_z_grid,
                                                   color_nuv_z, abs_mag_z, color_g_z)

        # get magnitude masks
        mag_23 = (abs_mag_z.data > -23.5) & (abs_mag_z.data < -22.5)
        mag_22 = (abs_mag_z.data > -22.5) & (abs_mag_z.data < -21.5)
        mag_21 = (abs_mag_z.data > -21.5) & (abs_mag_z.data < -20.5)

        # create figure
        fig, ax = plt.subplots(ncols=1, nrows=4, sharex='col', figsize=(self.fig_size_x_axis_half_page,
                                                                        self.fig_size_y_axis_half_page),
                               gridspec_kw={'height_ratios': [3, 1, 1, 1]})

        DensityContours.get_two_contours(ax[0], color_nuv_z_reference[morphology_reference['elliptical']],
                                            color_g_z_reference[morphology_reference['elliptical']],
                                            color_nuv_z_reference[morphology_reference['ltg']],
                                            color_g_z_reference[morphology_reference['ltg']],
                                            x_borders=(0, 7), y_borders=(0, 2),
                                            contour_size=(4, 4), bins=(100, 100), contour_levels_1=self.contour_levels,
                                            color_1='r', color_2='b', label_1='RCSED Elliptical', label_2='RCSED LTG')

        # plot all different morphologies
        # self._plot_morphologies(ax[0], color_nuv_z, color_g_z, morphology=morphology, label=label)
        ax[0].scatter(color_nuv_z, color_g_z, color='k', s=self.marker_size_half_page, label=label, alpha=0.5)


        mask_1 = (ssfr > -11.8) & (ssfr < -10.8)
        ax[0].scatter(color_nuv_z[mask_1], color_g_z[mask_1], color='y', s=self.marker_size_half_page + 5, marker='*', label=label + ' in transition', alpha=0.5)

        ax[0].plot([], [], '', color='r', label='RCSED Elliptical')
        ax[0].plot([], [], '', color='b', label='RCSED LTG')

        g_z_curve_23 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_z_grid, mag=-23)
        g_z_curve_22 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_z_grid, mag=-22)
        g_z_curve_21 = ColorColorMag.get_color_color_curve(self.mag_z_grid, self.g_z_grid, mag=-21)

        reference, = ax[0].plot([], [], '', color='white')
        curve_23, = ax[0].plot(self.nuv_z_grid, g_z_curve_23, linewidth=self.line_width_half_page, color='darkorange')
        curve_22, = ax[0].plot(self.nuv_z_grid, g_z_curve_22, linewidth=self.line_width_half_page, color='g')
        curve_21, = ax[0].plot(self.nuv_z_grid, g_z_curve_21, linewidth=self.line_width_half_page, color='m')

        leg1 = ax[0].legend(loc='lower right', frameon=False, fontsize=self.fontsize_half_page)
        # Add second legend for the maxes and mins.
        # leg1 will be removed from figure
        ax[0].legend([reference, curve_23, curve_22, curve_21], ['Chilingarian and Zolotukhin 2012',
                                                                 r'M$_{\rm z}$ = -23 mag',
                                                                 r'M$_{\rm z}$ = -22 mag',
                                                                 r'M$_{\rm z}$ = -21 mag'],
                     loc='upper left', frameon=False, fontsize=self.fontsize_half_page)

        # Manually add the first legend back
        ax[0].add_artist(leg1)

        ax[0].set_xlim(self.nuv_z_range)
        ax[0].set_ylim(self.g_z_range)

        self._plot_residuals_nuv_z_g_z(ax[1], color_nuv_z[mag_23], residuals_g_z[mag_23], mag=-23, color='darkorange')
        self._plot_residuals_nuv_z_g_z(ax[2], color_nuv_z[mag_22], residuals_g_z[mag_22], mag=-22, color='g')
        self._plot_residuals_nuv_z_g_z(ax[3], color_nuv_z[mag_21], residuals_g_z[mag_21], mag=-21, color='m')

        ax[0].xaxis.set_major_locator(plt.MultipleLocator(1))
        ax[0].xaxis.set_minor_locator(plt.MultipleLocator(1 / 4))
        ax[0].yaxis.set_major_locator(plt.MultipleLocator(0.2))
        ax[0].yaxis.set_minor_locator(plt.MultipleLocator(0.2 / 4))

        ax[1].xaxis.set_major_locator(plt.MultipleLocator(1))
        ax[1].xaxis.set_minor_locator(plt.MultipleLocator(1 / 4))
        ax[1].yaxis.set_major_locator(plt.MultipleLocator(0.1))
        ax[1].yaxis.set_minor_locator(plt.MultipleLocator(0.1 / 4))

        ax[2].xaxis.set_major_locator(plt.MultipleLocator(1))
        ax[2].xaxis.set_minor_locator(plt.MultipleLocator(1 / 4))
        ax[2].yaxis.set_major_locator(plt.MultipleLocator(0.1))
        ax[2].yaxis.set_minor_locator(plt.MultipleLocator(0.1 / 4))

        ax[3].xaxis.set_major_locator(plt.MultipleLocator(1))
        ax[3].xaxis.set_minor_locator(plt.MultipleLocator(1 / 4))
        ax[3].yaxis.set_major_locator(plt.MultipleLocator(0.1))
        ax[3].yaxis.set_minor_locator(plt.MultipleLocator(0.1 / 4))

        ax[0].set_ylabel(r'g-z, mag', fontsize=self.fontsize_half_page)
        ax[1].set_ylabel(r'$\Delta$ g-z, mag', fontsize=self.fontsize_half_page)
        ax[2].set_ylabel(r'$\Delta$ g-z, mag', fontsize=self.fontsize_half_page)
        ax[3].set_ylabel(r'$\Delta$ g-z, mag', fontsize=self.fontsize_half_page)
        ax[3].set_xlabel(r'NUV-z, mag', fontsize=self.fontsize_half_page)

        ax[0].tick_params(axis='both', which='both', width=2, direction='in', labelsize=self.fontsize_half_page)
        ax[0].xaxis.set_ticks_position('both')
        ax[0].yaxis.set_ticks_position('both')

        ax[1].tick_params(axis='both', which='both', width=2, direction='in', labelsize=self.fontsize_half_page)
        ax[1].xaxis.set_ticks_position('both')
        ax[1].yaxis.set_ticks_position('both')

        ax[2].tick_params(axis='both', which='both', width=2, direction='in', labelsize=self.fontsize_half_page)
        ax[2].xaxis.set_ticks_position('both')
        ax[2].yaxis.set_ticks_position('both')

        ax[3].tick_params(axis='both', which='both', width=2, direction='in', labelsize=self.fontsize_half_page)
        ax[3].xaxis.set_ticks_position('both')
        ax[3].yaxis.set_ticks_position('both')

        plt.tight_layout()
        plt.subplots_adjust(hspace=0)

        return fig

    def _plot_residuals_nuv_g_r_z(self, ax, color_nuv_r, residuals_g_r, mag=-22, color=None):

        ax.scatter(color_nuv_r, residuals_g_r, s=self.marker_size_half_page, color=color)

        # print mean residuals with std
        mean_residuals, std_residuals = StatisticFunctions.get_y_axis_binned(self.nuv_r_grid, color_nuv_r,
                                                                             residuals_g_r, mode='mean')

        center_of_nuv_r_bins = StatisticFunctions.get_centre_of_bin(self.nuv_r_grid)

        mask_nuv_r = (center_of_nuv_r_bins > 2) & (center_of_nuv_r_bins < 5.7)

        ax.plot(center_of_nuv_r_bins[mask_nuv_r], mean_residuals[mask_nuv_r], linewidth=self.line_width_half_page,
                linestyle=':', color='k')
        ax.plot(center_of_nuv_r_bins[mask_nuv_r], mean_residuals[mask_nuv_r] + std_residuals[mask_nuv_r],
                linewidth=self.line_width_half_page, linestyle='--', color='k')
        ax.plot(center_of_nuv_r_bins[mask_nuv_r], mean_residuals[mask_nuv_r] - std_residuals[mask_nuv_r],
                linewidth=self.line_width_half_page, linestyle='--', color='k')

        ax.text(4.5, 0.1, r'%.1f < M$_{\rm z}$ < %.1f' % (mag - 0.5, mag + 0.5), fontsize=self.fontsize_half_page)

        ax.set_xlim(self.nuv_r_range)
        ax.set_ylim(self.delta_g_r_range)

    def _plot_residuals_nuv_z_g_z(self, ax, color_nuv_z, residuals_g_z, mag=-22, color=None):

        ax.scatter(color_nuv_z, residuals_g_z, s=self.marker_size_half_page, color=color)

        # print mean residuals with std
        mean_residuals, std_residuals = StatisticFunctions.get_y_axis_binned(self.nuv_z_grid, color_nuv_z,
                                                                             residuals_g_z, mode='mean')

        center_of_nuv_z_bins = StatisticFunctions.get_centre_of_bin(self.nuv_z_grid)

        mask_nuv_z = (center_of_nuv_z_bins > 2) & (center_of_nuv_z_bins < 5.7)

        ax.plot(center_of_nuv_z_bins[mask_nuv_z], mean_residuals[mask_nuv_z], linewidth=self.line_width_half_page,
                linestyle=':', color='k')
        ax.plot(center_of_nuv_z_bins[mask_nuv_z], mean_residuals[mask_nuv_z] + std_residuals[mask_nuv_z],
                linewidth=self.line_width_half_page, linestyle='--', color='k')
        ax.plot(center_of_nuv_z_bins[mask_nuv_z], mean_residuals[mask_nuv_z] - std_residuals[mask_nuv_z],
                linewidth=self.line_width_half_page, linestyle='--', color='k')

        ax.text(4.5, 0.1, r'%.1f < M$_{\rm z}$ < %.1f' % (mag - 0.5, mag + 0.5), fontsize=self.fontsize_half_page)

        ax.set_xlim(self.nuv_z_range)
        ax.set_ylim(self.delta_g_z_range)

    @staticmethod
    def get_nuv_g_r_z_fit():

        from astropy.io import fits
        table = fits.open('/home/benutzer/data/double_peak_catalog/' +
                          'parameters/color_color_mag/coeff_surf_NUVgr_z.fits')
        data = table[1].data

        mag_z_grid = data[0]['MAGGRID']
        nuv_r_grid = data[0]['COL1GRID']
        g_r_grid = data[0]['C2FIT']
        # std = data[0]['C2STDEV']
        return nuv_r_grid, mag_z_grid, g_r_grid

    @staticmethod
    def get_nuv_z_g_z_fit():

        from astropy.io import fits
        table = fits.open('/home/benutzer/data/double_peak_catalog/' +
                          'parameters/color_color_mag/coeff_surf_NUVgz_z.fits')
        data = table[1].data

        mag_z_grid = data[0]['MAGGRID']
        nuv_g_grid = data[0]['COL1GRID']
        g_z_grid = data[0]['C2FIT']
        # std = data[0]['C2STDEV']
        return nuv_g_grid, mag_z_grid, g_z_grid

    @staticmethod
    def get_color_color_curve(abs_mag_grid, color_grid, mag=-22):

        index_lower = abs_mag_grid == mag - 0.125
        index_higher = abs_mag_grid == mag + 0.125

        return np.mean([color_grid[:, index_lower], color_grid[:, index_higher]], axis=0)

    @staticmethod
    def get_color_color_residuals(color_x_grid, abs_mag_grid, color_y_grid, measured_color_x, measured_abs_mag, measured_color_y):

        from scipy.interpolate import RegularGridInterpolator
        my_interpolating_function = RegularGridInterpolator((color_x_grid, abs_mag_grid), color_y_grid, bounds_error=False)

        return measured_color_y - my_interpolating_function(np.transpose([measured_color_x, measured_abs_mag]))


class ImagePlot:
    def __init__(self):
        r"""
        Class to produce different plots for SDSS images

        """
    @staticmethod
    def image_pattern(table, data_path, annex='', figsize=None, max_pictures=20,
                      mask_1=None, mask_2=None, mask_3=None, mode='sdss_legacy', number_in_line=10):

        number_of_rows = int(max_pictures / number_in_line)
        if figsize is None:
            figsize = (number_in_line * 4, number_of_rows * 4.8)

        if len(table) > max_pictures:
            length = max_pictures
        else:
            length = len(table)

        fig = plt.figure(figsize=figsize)

        for index in range(0, length):
            mjd = int(table[index]['mjd%s' % annex])
            plate = int(table[index]['plate%s' % annex])
            fiberid = int(table[index]['fiberid%s' % annex])

            print(mjd, plate, fiberid)

            ax = fig.add_subplot(int(length / number_in_line) + 1, number_in_line, index + 1)
            if mode == 'sdss':
                file_path = data_path / 'galaxies' / 'images' / 'sdss' / str(mjd) / str(plate)
            elif mode == 'sdss_unwise':
                if not os.path.isfile((data_path / 'galaxies' / 'images' / 'unwise' / str(mjd) / str(plate) / str(fiberid)).with_suffix('.jpg')):
                    file_path = data_path / 'galaxies' / 'images' / 'sdss' / str(mjd) / str(plate)
                else:
                    file_path = data_path / 'galaxies' / 'images' / 'unwise' / str(mjd) / str(plate)

            elif mode == 'sdss_galex':
                if not os.path.isfile((data_path / 'galaxies' / 'images' / 'galex' / str(mjd) / str(plate) / str(fiberid)).with_suffix('.jpg')):
                    file_path = data_path / 'galaxies' / 'images' / 'sdss' / str(mjd) / str(plate)
                else:
                    file_path = data_path / 'galaxies' / 'images' / 'galex' / str(mjd) / str(plate)
            elif mode == 'legacy':
                file_path = data_path / 'galaxies' / 'images' / 'legacy' / str(mjd) / str(plate)
            else:
                raise ValueError('mode not understand')

            img = mpimg.imread((file_path / str(fiberid)).with_suffix('.jpg'))
            plt.imshow(img)
            if mask_1 is not None:
                if mask_1[index]:
                    ax.set_xticks([])
                    ax.set_yticks([])
                    ax.spines['bottom'].set_color('orange')
                    ax.spines['top'].set_color('orange')
                    ax.spines['right'].set_color('orange')
                    ax.spines['left'].set_color('orange')
                    [i.set_linewidth(5) for i in ax.spines.itervalues()]
                elif mask_2[index]:
                    ax.set_xticks([])
                    ax.set_yticks([])
                    ax.spines['bottom'].set_color('lime')
                    ax.spines['top'].set_color('lime')
                    ax.spines['right'].set_color('lime')
                    ax.spines['left'].set_color('lime')
                    [i.set_linewidth(5) for i in ax.spines.itervalues()]
                elif mask_3[index]:
                    ax.set_xticks([])
                    ax.set_yticks([])
                    ax.spines['bottom'].set_color('darkblue')
                    ax.spines['top'].set_color('darkblue')
                    ax.spines['right'].set_color('darkblue')
                    ax.spines['left'].set_color('darkblue')
                    [i.set_linewidth(5) for i in ax.spines.itervalues()]
                else:
                    ax.axis('off')
            else:
                ax.axis('off')

        return fig


class PlotBPT(data_access.DataAccess, basic_attributes.PublishingQualityParameters):

    def __init__(self, **kwargs):
        r"""

        """
        super().__init__(**kwargs)

        # limits for all three plots
        self.x_range_first = np.log10([0.04, 3.0])
        self.x_range_second = np.log10([0.01, 3.0])
        self.x_range_third = np.log10([0.005, 0.5])
        self.y_range = np.log10([0.09, 15])

        # custom ticklabels
        self.xticks_first_bpt = np.log10([0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1,
                                          0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 2.0, 3.0, 4.0])
        self.xticklabels_first_bpt = ['', '0.02', '', '', '0.05', '', '', '', '', '0.1', '0.2',
                                      '', '', '0.5', '', '', '', '', '1.0', '2.0', '', '']

        self.xticks_second_bpt = np.log10([0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09,
                                           0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 2.0, 3.0])
        self.xticklabels_second_bpt = ['', '0.02', '', '', '0.05', '', '', '', '', '0.1',
                                       '0.2', '', '', '0.5', '', '', '', '', '1.0', '', '']

        self.xticks_third_bpt = np.log10([0.005, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06,
                                          0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5])
        self.xticklabels_third_bpt = ['', '0.01', '0.02', '', '', '0.05', '', '',
                                      '', '', '0.1', '0.2', '', '', '0.5']

        self.yticks = np.log10([0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7,
                                0.8, 0.9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30])
        self.yticklables = ['', '', '', '', '0.1', '0.2', '0.3', '', '0.5', '', '', '',
                            '', '1', '2', '3', '', '5', '', '', '', '', '10', '20', '']

        # colorbar range
        self.ssfr_colorbar_range = (-11.5, - 8.5)

        # contours
        self.contour_levels = np.logspace(-0.1, 3, 15)

        self.contour_counts_first_bpt = None
        self.extent_first_bpt = None
        self.contour_counts_second_bpt = None
        self.extent_second_bpt = None
        self.contour_counts_third_bpt = None
        self.extent_third_bpt = None

    def get_bpt_coordinates(self, bpt='first', line_shape='gauss'):
        r"""
        This function gets the coordinates for the BPt diagram
        """
        if bpt == 'first':
            x_val = self.get_log10_ratio_nii_h_alpha(line_shape=line_shape)
        elif bpt == 'second':
            x_val = self.get_log10_ratio_sii_h_alpha(line_shape=line_shape)
        elif bpt == 'third':
            x_val = self.get_log10_ratio_oi_h_alpha(line_shape=line_shape)
        else:
            raise ValueError('BPT diagram not understood')

        y_val = self.get_log10_ratio_oiii_h_beta(line_shape=line_shape)

        return x_val, y_val

    @staticmethod
    def create_contours(bpt='first'):

        tiny_rcsed_access = data_access.DataAccess(object_type='tiny_rcsed')

        if bpt == 'first':
            contour_x_val = tiny_rcsed_access.get_log10_ratio_nii_h_alpha(line_shape='gauss')
            contour_snr_mask_x_val = ((tiny_rcsed_access.get_emission_line_snr(line_wavelength=6565, line_shape='gauss')
                                       > 3) &
                                      (tiny_rcsed_access.get_emission_line_snr(line_wavelength=6585, line_shape='gauss')
                                       > 3))
        elif bpt == 'second':
            contour_x_val = tiny_rcsed_access.get_log10_ratio_sii_h_alpha(line_shape='gauss')
            contour_snr_mask_x_val = ((tiny_rcsed_access.get_emission_line_snr(line_wavelength=6565, line_shape='gauss')
                                       > 3) &
                                      (tiny_rcsed_access.get_emission_line_snr(line_wavelength=6718, line_shape='gauss')
                                       > 3))
        elif bpt == 'third':
            contour_x_val = tiny_rcsed_access.get_log10_ratio_oi_h_alpha(line_shape='gauss')
            contour_snr_mask_x_val = ((tiny_rcsed_access.get_emission_line_snr(line_wavelength=6565, line_shape='gauss')
                                       > 3) &
                                      (tiny_rcsed_access.get_emission_line_snr(line_wavelength=6302, line_shape='gauss')
                                       > 3))
        else:
            raise ValueError('BPT diagram not understood')

        contour_y_val = tiny_rcsed_access.get_log10_ratio_oiii_h_beta(line_shape='gauss')
        contour_snr_mask_y_val = ((tiny_rcsed_access.get_emission_line_snr(line_wavelength=5008, line_shape='gauss')
                                   > 3) &
                                  (tiny_rcsed_access.get_emission_line_snr(line_wavelength=4863, line_shape='gauss')
                                   > 3))

        xi, yi, zi = DensityContours.compute_contour_counts(contour_x_val[contour_snr_mask_x_val *
                                                                          contour_snr_mask_y_val],
                                                            contour_y_val[[contour_snr_mask_x_val *
                                                                           contour_snr_mask_y_val]])
        return xi, yi, zi

    def load_contours(self, bpt='first'):
        r"""
        This function loads contour lines. or calculates them with the tiny version of the RCSED catalog
        """

        bpt_contour_path = self.project_path / 'data' / 'bpt_contours'
        bpt_contour_file_name = (bpt_contour_path / ('%s_bpt_contours' % bpt)).with_suffix('.npy')
        if os.path.isfile(bpt_contour_path / bpt_contour_file_name):
            bpt_contours = np.load(bpt_contour_path / bpt_contour_file_name, allow_pickle=True).item()
            xi = bpt_contours['xi']
            yi = bpt_contours['yi']
            zi = bpt_contours['zi']
        else:
            xi, yi, zi = self.create_contours(bpt=bpt)
            bpt_contours = {'xi': xi, 'yi': yi, 'zi': zi}
            if not os.path.isdir(bpt_contour_path):
                os.makedirs(bpt_contour_path)
            np.save(bpt_contour_path / bpt_contour_file_name, bpt_contours)

        return xi, yi, zi

    def get_contours(self, ax, bpt='first', color='black', alpha=1.0):

        r"""
        This function draws contour lines.
        It can also use saved contour lines to have a quick access to a reference sample
        """

        xi, yi, zi = self.load_contours(bpt=bpt)

        contour_levels = [0.02, 0.05, 0.1, 0.2, 0.5, 0.68, 0.95, 0.99]

        DensityContours.plot_contours_percentage(ax, xi, yi, zi, contour_levels=contour_levels, color=color,
                                                 percent=False, alpha=alpha)

    @staticmethod
    def lines_first_bpt(ax, schawinsky=False, fmt_kew1='b--', fmt_kew2='r-', fmt_schaw='g-', linewidth=2.):
        r"""
        This function plots the separation lines on the first BPT diagram to separate star forming (SF) and
         composite (COMP) (blue dashed line) and COMP and active galactic nuclei (AGN) (red line).
         This was found by Kewley et al. (2006) doi:10.1111/j.1365-2966.2006.10859.x
         We further show a line to further separate AGN and low intensity narrow emission line regions (LINER) from AGN
         This was found by Schawinski t al. (2007) doi:10.1111/j.1365-2966.2007.12487.x and is shown with a green line
        :param ax: plt axis
        :return: None
        """
        # Kewley et al. (2006) EQ. (1, 4)
        tt_1 = np.arange(0.01, 1.0, 0.001)
        ax.plot(np.log10(tt_1), 0.61 / (np.log10(tt_1) - 0.05) + 1.3, fmt_kew1, linewidth=linewidth)

        # Kewley et al. (2006) EQ. (5, 6)
        tt_2 = np.arange(0.0521, 2.0, 0.01)
        ax.plot(np.log10(tt_2), 0.61 / (np.log10(tt_2) - 0.47) + 1.19, fmt_kew2, linewidth=linewidth)

        if schawinsky:
            # Schawinski et al. 2007 EQ. (1)
            tt_3 = np.arange(0.655, 9.0, 0.01)
            ax.plot(np.log10(tt_3), 1.05 * np.log10(tt_3) + 0.45, fmt_schaw, linewidth=2)

    @staticmethod
    def lines_second_bpt(ax, fmt_kew1='r-', fmt_kew2='b-', linewidth=2.):
        r"""
        This function plots the separation lines on the second BPT diagram to separate star forming (SF) and
         and active galactic nuclei (AGN) (red line). It also separates AGN and
         low intensity narrow emission line regions (LINER) with a blue line
         This was found by Kewley et al. (2006) doi:10.1111/j.1365-2966.2006.10859.x
        :param ax: plt axis
        :return: None
        """
        # Kewley et al. (2006) EQ. (2, 7, 12)
        tt = np.arange(0.006, 2.0, 0.001)
        ax.plot(np.log10(tt), 0.72 / (np.log10(tt) - 0.32) + 1.30, fmt_kew1, linewidth=linewidth)

        # Kewley et al. (2006) EQ. (13)
        tt = np.arange(0.485, 3.0, 0.001)
        ax.plot(np.log10(tt), 1.89 * (np.log10(tt)) + 0.76, fmt_kew2, linewidth=linewidth)

        return ax

    @staticmethod
    def lines_third_bpt(ax, fmt_kew1='r-', fmt_kew2='b-', linewidth=2.):
        r"""
        This function plots the separation lines on the third BPT diagram to separate star forming (SF) and
         and active galactic nuclei (AGN) (red line). It also separates AGN and
         low intensity narrow emission line regions (LINER) with a blue line
         This was found by Kewley et al. (2006) doi:10.1111/j.1365-2966.2006.10859.x
        :param ax: plt axis
        :return: None
        """
        # Kewley et al. (2006) EQ. (3, 8, 14)
        tt = np.arange(0.005, 0.2, 0.001)
        ax.plot(np.log10(tt), 0.73 / (np.log10(tt) + 0.59) + 1.33, fmt_kew1, linewidth=linewidth)

        # Kewley et al. (2006) EQ. (15)
        tt = np.arange(0.075, 0.5, 0.01)
        ax.plot(np.log10(tt), 1.18 * (np.log10(tt)) + 1.3, fmt_kew2, linewidth=linewidth)

        return ax

    def plot_scatter_bpt_points(self, ax, bpt='first', line_shape='gauss', color_data=None, data_point_mask=None,
                                **kwargs):
        r"""
        plots scatter points for bpt diagram

        :param ax: matplotlib axis
        :param bpt: specify if first, second or third bpt diagram will be plotted
        :type bpt: str
        :param line_shape: line shape of emission line to compute line flux ratios. Default is Gaussian shape
        :type line_shape: str
        :param color_data: optional. array with the color map for the data points.
        :type color_data: array like
        :param data_point_mask: array with masking which data points to draw and which not
        :param kwargs: all matplotlib scatter kwargs allowed
        :return: None
        """

        # get data points
        x_data, y_data = self.get_bpt_coordinates(bpt=bpt, line_shape=line_shape)

        if data_point_mask is None:
            data_point_mask = np.ones(len(x_data), dtype=bool)

        # get color coding for plot
        if color_data is not None:
            color_data = color_data[data_point_mask]

        # plot
        ax.scatter(x_data[data_point_mask], y_data[data_point_mask], c=color_data, **kwargs)

    def get_axis_labels(self, ax, bpt='first', fontsize=13,
                        x_label=True, x_ticks=True, x_tick_label=True, x_labelpad=1, xlim=None,
                        y_label=True, y_ticks=True, y_tick_label=True, y_labelpad=2, ylim=None):

        # get labels and numbers to the y axis
        if y_label:
            ax.set_ylabel(r'${\rm [OIII]/H}_\beta \, (5008\AA/4863\AA)$', labelpad=y_labelpad, fontsize=fontsize)
        if y_ticks:
            ax.set_yticks(self.yticks)
        else:
            ax.set_yticks([])
        if y_tick_label:
            ax.set_yticklabels(self.yticklables, fontsize=fontsize)
        else:
            ax.set_yticklabels([])

        # get labels and numbers to the x axis
        if x_label:
            if bpt == 'first':
                ax.set_xlabel(r'${\rm [NII]/H}_\alpha \, (6585\AA/6565\AA)$', labelpad=x_labelpad, fontsize=fontsize)
            elif bpt == 'second':
                ax.set_xlabel(r'${\rm [SII]/H}_\alpha \, (6718\AA+6733\AA/6565\AA)$', labelpad=x_labelpad, fontsize=fontsize)
            elif bpt == 'third':
                ax.set_xlabel(r'${\rm [OI]/H}_\alpha \, (6302\AA/6565\AA)$', labelpad=x_labelpad, fontsize=fontsize)

        if x_ticks:
            ax.set_xticks(getattr(self, 'xticks_%s_bpt' % bpt))
        else:
            ax.set_xticks([])
        if x_tick_label:
            ax.set_xticklabels(getattr(self, 'xticklabels_%s_bpt' % bpt), fontsize=fontsize)
        else:
            ax.set_xticklabels([])

        # get limits
        if ylim is None:
            ax.set_ylim(self.y_range)
        else:
            ax.set_ylim(ylim)
        if xlim is None:
            ax.set_xlim(getattr(self, 'x_range_%s' % bpt))
        else:
            ax.set_xlim(xlim)

    def create_bpt_plot(self, bpt='first', line_shape='gauss'):

        # create figure
        fig, ax = plt.subplots(ncols=1, nrows=1, figsize=(self.fig_size_x_axis_half_page,
                                                          self.fig_size_y_axis_small_scatter))

        # get contours
        self.get_contours(ax=ax, bpt=bpt, color='k')

        # get the bpt line(s)
        line_function_name = 'lines_%s_bpt' % bpt
        getattr(self, line_function_name)(ax)

        # get data_point_mask
        # snr_mask = self.get_snr_mask_bpt(bpt=bpt, line_shape=line_shape)
        snr_mask = None


        # plot data points
        if line_shape == 'dp':
            self.plot_scatter_bpt_points(ax, bpt=bpt, line_shape='peak_1', color='b', data_point_mask=snr_mask,
                                         s=self.marker_size_small_scatter + 50)
            self.plot_scatter_bpt_points(ax, bpt=bpt, line_shape='peak_2', color='r', data_point_mask=snr_mask,
                                         s=self.marker_size_small_scatter + 50)


        else:
            # get color map
            ssfr = self.get_log_mpa_ssfr()
            self.plot_scatter_bpt_points(ax, bpt=bpt, line_shape=line_shape, color_data=ssfr, data_point_mask=snr_mask,
                                         s=self.marker_size_small_scatter,
                                         vmin=self.ssfr_colorbar_range[0], vmax=self.ssfr_colorbar_range[1],
                                         cmap="gist_rainbow")

        # get axis labels
        self.get_axis_labels(ax, bpt=bpt, fontsize=self.fontsize_small_scatter)

        # add numbers or legends
        # to be done ...

        # cosmetics
        ax.tick_params(axis='both', which='both', width=2, labelsize=self.fontsize_small_scatter, direction='in',
                       top=True, right=True)

        # plt.show()
        # plt.savefig('plot_output/bpt.pdf')
        return fig

