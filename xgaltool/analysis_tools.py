# -*- coding: utf-8 -*-
"""
Class to analyse galaxy data of different sources
"""

import numpy as np
from xgaltool import data_access, plotting_tools
from astropy import units as u
import os
from pathlib import Path


class EmissionLineTools(data_access.DataAccess):

    def __init__(self, **kwargs):
        r"""

        """
        super().__init__(**kwargs)

    def get_gas_met_o2o3hb(self, line_shape='nonpar'):
        r"""
        Gas metalicity using the OII, OIII and H_beta emission line
        following Tremonti et al. (2004) doi:10.1086/423264

        :param line_shape: Specified the emission-line fit. For the RCSED catalogue (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
        This function is not suited for Maschmann et al. (2020) since the OII doublet is not fitted.
        :type line_shape: str

        :return metallicity in [Z/H] dex
        """

        # get emission line fluxe
        flux_oii = self.get_emission_line_flux(line_wavelength=3727, line_shape=line_shape)
        flux_oiii_1 = self.get_emission_line_flux(line_wavelength=4960, line_shape=line_shape)
        flux_oiii_2 = self.get_emission_line_flux(line_wavelength=5008, line_shape=line_shape)
        flux_h_beta = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

        # Tremonti et al. (2004) EQ. (1)
        log_r_23 = np.log10((flux_oii + flux_oiii_1 + flux_oiii_2) / flux_h_beta)
        return 9.185 - 0.313 * log_r_23 - 0.264 * (log_r_23 ** 2) - 0.321 * (log_r_23 ** 3)

    def get_gas_met_n2(self, line_shape='gauss'):
        r"""
        Calculate gas metallicity using emission lines [NII] 6585 and Halpha 6565
        We follow Pettini and Pagel (2004) doi:10.1111/j.1365-2966.2004.07591.x

        :param line_shape: Specified the emission-line fit. For the RCSED catalogue (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
        For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
        ('peak_1') or red-shifted component ('peak_2').
        :type line_shape: str

        :return metallicity in [Z/H] dex
        """
        # get emission line fluxes
        flux_nii = self.get_emission_line_flux(line_wavelength=6585, line_shape=line_shape)
        flux_h_alpha = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)

        # Pettini and Pagel (2004) Eq. (2)
        log_n2 = np.log10(flux_nii / flux_h_alpha )
        return 9.37 + 2.03 * log_n2 + 1.26 * (log_n2**2) + 0.32 * (log_n2**3)

    def get_gas_met_o3n2(self, flux_oiii=None, flux_h_beta=None, flux_nii=None, flux_h_alpha=None, line_shape='gauss'):
        r"""
        Calculate gas metallicity following using emission lines [OIII] 5008, Hbeta 4863, [NII] 6585 and Halpha 6565
        We follow Pettini and Pagel (2004) doi:10.1111/j.1365-2966.2004.07591.x

        :param line_shape: Specified the emission-line fit. For the RCSED catalogue (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
        For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
        ('peak_1') or red-shifted component ('peak_2').
        :type line_shape: str

        :return metallicity in [Z/H] dex
        """

        # get emission line fluxes
        if (flux_oiii is None) & (flux_h_beta is None) & (flux_nii is None) & (flux_h_alpha is None):
            flux_oiii = self.get_emission_line_flux(line_wavelength=5008, line_shape=line_shape)
            flux_h_beta = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)
            flux_nii = self.get_emission_line_flux(line_wavelength=6585, line_shape=line_shape)
            flux_h_alpha = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)

        # Pettini and Pagel (2004) EQ. (3)
        log_o3n2 = np.log10((flux_oiii / flux_h_beta) * (flux_h_alpha / flux_nii))
        return 8.73 - 0.32 * log_o3n2

    def get_gas_met_o3n2_err(self, flux_oiii=None, flux_h_beta=None, flux_nii=None, flux_h_alpha=None, line_shape='gauss'):
        r"""
        Calculate gas metallicity error from gas metallicity using emission lines [OIII] 5008, Hbeta 4863, [NII] 6585 and Halpha 6565
        We follow Pettini and Pagel (2004) doi:10.1111/j.1365-2966.2004.07591.x

        :param line_shape: Specified the emission-line fit. For the RCSED catalogue (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
        For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
        ('peak_1') or red-shifted component ('peak_2').
        :type line_shape: str

        :return metallicity error in [Z/H] dex
        """

        # get emission line fluxes
        if (flux_oiii is None) & (flux_h_beta is None) & (flux_nii is None) & (flux_h_alpha is None):
            flux_oiii = self.get_emission_line_flux(line_wavelength=5008, line_shape=line_shape)
            flux_h_beta = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)
            flux_nii = self.get_emission_line_flux(line_wavelength=6585, line_shape=line_shape)
            flux_h_alpha = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)

            flux_oiii_err = self.get_emission_line_flux_err(line_wavelength=5008, line_shape=line_shape)
            flux_h_beta_err = self.get_emission_line_flux_err(line_wavelength=4863, line_shape=line_shape)
            flux_nii_err = self.get_emission_line_flux_err(line_wavelength=6585, line_shape=line_shape)
            flux_h_alpha_err = self.get_emission_line_flux_err(line_wavelength=6565, line_shape=line_shape)

        flux_err_over_flux_oiii = flux_oiii_err / flux_oiii
        flux_err_over_flux_h_beta = flux_h_beta_err / flux_oiii
        flux_err_over_flux_nii = flux_nii_err / flux_nii
        flux_err_over_flux_h_alpha = flux_h_alpha_err / flux_h_alpha


        sigma_o3n2 = ((1 / np.log(10) ** 2) * ( (flux_err_over_flux_oiii) ** 2 + (flux_err_over_flux_h_beta) ** 2 +
                                              (flux_err_over_flux_nii) ** 2 + (flux_err_over_flux_h_alpha) ** 2 ))

        return 0.32 * np.sqrt(sigma_o3n2)

    def get_gas_met_n2s2ha(self, flux_nii=None, flux_sii=None, flux_h_alpha=None, line_shape='gauss', fifth_order_correction=False):
        r"""
        Calculate gas metallicity following using emission lines [NII]6585, [SII]6718,6732 and Halpha 6565
        We follow Dopita et al. (2016) doi:10.1007/s10509-016-2657-8

        :param line_shape: Specified the emission-line fit. For the RCSED catalogue (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
        For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
        ('peak_1') or red-shifted component ('peak_2').
        :type line_shape: str

        :return metallicity in [Z/H] dex
        """

        # get emission line fluxes
        if (flux_nii is None) & (flux_sii is None) & (flux_h_alpha is None):
            flux_nii = self.get_emission_line_flux(line_wavelength=6585, line_shape=line_shape)
            flux_sii = self.get_emission_line_flux(line_wavelength=6718, line_shape=line_shape) + self.get_emission_line_flux(line_wavelength=6733, line_shape=line_shape)
            flux_h_alpha = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)

        # Dopita et al. (2016) EQ. (1)
        log_n2s2ha = np.log10( (flux_nii / flux_sii) ) + 0.264*np.log10( (flux_nii / flux_h_alpha) )

        if fifth_order_correction:
            return 8.77 + log_n2s2ha + 0.45*(log_n2s2ha+0.3)**5
        else:
            return 8.77 + log_n2s2ha

    def get_gas_met_n2s2ha_err(self, flux_nii=None, flux_sii=None, flux_h_alpha=None, line_shape='gauss'):
        r"""
        Calculate gas metallicity error following using emission lines [NII]6585, [SII]6718,6732 and Halpha 6565
        We follow Dopita et al. (2016) doi:10.1007/s10509-016-2657-8

        :param line_shape: Specified the emission-line fit. For the RCSED catalogue (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
        For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
        ('peak_1') or red-shifted component ('peak_2').
        :type line_shape: str

        :return metallicity in [Z/H] dex
        """

        # get emission line fluxes
        if (flux_nii is None) & (flux_sii is None) & (flux_h_alpha is None):
            flux_nii = self.get_emission_line_flux(line_wavelength=6585, line_shape=line_shape)
            flux_sii = self.get_emission_line_flux(line_wavelength=6718, line_shape=line_shape) + self.get_emission_line_flux(line_wavelength=6733, line_shape=line_shape)
            flux_h_alpha = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)

            flux_nii_err = self.get_emission_line_flux_err(line_wavelength=6585, line_shape=line_shape)
            flux_sii_err = np.sqrt( self.get_emission_line_flux_err(line_wavelength=6718, line_shape=line_shape)**2 + self.get_emission_line_flux(_errline_wavelength=6733, line_shape=line_shape)**2 )
            flux_h_alpha_err = self.get_emission_line_flux_err(line_wavelength=6565, line_shape=line_shape)

        flux_err_over_flux_nii = flux_nii_err / flux_nii
        flux_err_over_flux_sii = flux_sii_err / flux_sii
        flux_err_over_flux_h_alpha = flux_h_alpha_err / flux_h_alpha

        sigma_n2s2ha = ( (1 / np.log(10))** 2 * ( (flux_err_over_flux_nii)**2 + (flux_err_over_flux_sii)**2 + (flux_err_over_flux_h_alpha)**2 * 0.264**2 + (flux_err_over_flux_h_alpha)**2 * 0.264**2 ))

        return np.sqrt(sigma_n2s2ha)

    def get_sfr_curti2020(self, log_m_star, flux_oiii=None, flux_h_beta=None, flux_nii=None, flux_h_alpha=None, line_shape='gauss',
                          estimat='fiber'):
        """
        following Curti et al. (2020) DOI:10.1093/mnras/stz2910
          equation 5
        :param flux_oiii:
        :param flux_h_beta:
        :param flux_nii:
        :param flux_h_alpha:
        :param line_shape:
        :param estimat:
        :return:
        """
        # get emission line fluxes
        if (flux_oiii is None) & (flux_h_beta is None) & (flux_nii is None) & (flux_h_alpha is None):
            flux_oiii = self.get_emission_line_flux(line_wavelength=5008, line_shape=line_shape)
            flux_h_beta = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)
            flux_nii = self.get_emission_line_flux(line_wavelength=6585, line_shape=line_shape)
            flux_h_alpha = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)

        met = self.get_gas_met_o3n2(flux_oiii=flux_oiii, flux_h_beta=flux_h_beta, flux_nii=flux_nii,
                                    flux_h_alpha=flux_h_alpha, line_shape=line_shape)

        # parameters from curti et al. (2020) table 6
        if estimat == 'fiber':
            z0 = 8.782  # uncertainty 0.004
            m0 = 10.39  # uncertainty 0.03
            m1 = 0.454  # uncertainty 0.008
            gamma = 0.299  # uncertainty 0.008
            beta = 2.6  # uncertainty 0.5
        elif estimat == 'total':
            z0 = 8.779  # uncertainty 0.005
            m0 = 10.11  # uncertainty 0.03
            m1 = 0.56  # uncertainty 0.01
            gamma = 0.31  # uncertainty 0.01
            beta = 2.1  # uncertainty 0.4

        # Transformation of Eq 5
        log_sfr = np.log10(10 ** ((z0- met) * beta / gamma) - 1)/(beta * m1) + log_m_star/m1 - m0/m1

        return log_sfr

    def get_extinction(self, flux_h_alpha_6565=None, flux_h_beta_4863=None, line_shape='gauss'):
        r"""
        calculate gas colour excess using the Balmer decrement
         following dominguez+13 doi:10.1088/0004-637X/763/2/145
         assuming an intrinsic ratio H\alpha/H\beta=2.87 (Osterbrock 1989)
         and the reddening curve from Calzetti et al. (2000) doi:10.1086/308692

        :param line_shape: Specified the emission-line fit. For the RCSED catalogue (Chilingarian et al.2017) this
         can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
         For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
         ('peak_1') or red-shifted component ('peak_2').
        :type line_shape: str
        :return colour excess E(B - V) in mag
        :rtype: array_like
        """
        if (flux_h_alpha_6565 is None) & (flux_h_beta_4863 is None):
            flux_h_alpha_6565 = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863 = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

        # dominguez et al 2013 doi:10.1088/0004-637X/763/2/145
        # eq. 4
        # e_b_v = 1.97 * np.log10(flux_h_alpha_6565 / flux_h_beta_4863) - 1.97 * np.log10(2.86)
        e_b_v = 1.97 * np.log10((flux_h_alpha_6565 / flux_h_beta_4863) / 2.87 )
        # e_b_v = 1.97 * np.log10((flux_h_alpha_6565 / flux_h_beta_4863) / 2.83 )

        return e_b_v

    def get_extinction_err(self, flux_h_alpha_6565=None, flux_h_beta_4863=None, flux_h_alpha_6565_err=None,
                           flux_h_beta_4863_err=None, line_shape='gauss'):
        r"""
        calculate error of colour excess using the Balmer decrement following
         Dominguez et al 2013 doi:10.1088/0004-637X/763/2/145

        :param line_shape: Specified the emission-line fit. For the RCSED catalogue (Chilingarian et al.2017) this
         can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
         For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
         ('peak_1') or red-shifted component ('peak_2').
        :type line_shape: str
        :return error of colour excess E(B - V) in mag
        :rtype: array_like
        """
        if (flux_h_alpha_6565 is None) & (flux_h_beta_4863 is None):
            flux_h_alpha_6565 = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863 = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

            flux_h_alpha_6565_err = self.get_emission_line_flux_err(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863_err = self.get_emission_line_flux_err(line_wavelength=4863, line_shape=line_shape)

        # Using error propagation on Dominguez et al 2013 doi:10.1088/0004-637X/763/2/145 eq. 4
        e_b_v_err = np.sqrt((1.97 * flux_h_alpha_6565_err / (flux_h_alpha_6565 * np.log(10))) ** 2 +
                            (1.97 * flux_h_beta_4863_err / (flux_h_beta_4863 * np.log(10))) ** 2)
        return e_b_v_err

    @staticmethod
    def compute_reddening_curve(wavelength=6565, r_v=3.1):
        r"""
        calculate reddening courve
         following  Calzetti et al. (2000) doi:10.1086/308692
         using eq. 4

        :param wavelength: restframe wavelength in angstrom of spectral part of which to compute the reddening curve
        :type wavelength: float or int
        :param r_v: default 3.1  total extinction at V
        :type r_v: float

        :return extinction E(B - V) in mag
        :rtype: array_like
        """

        # change wavelength from Angstrom to microns
        wavelength *= 1e-4

        # eq. 4
        if (wavelength > 0.63) & (wavelength < 2.20):
            # sutable for 0.63 micron < wavelength < 2.20 micron
            k_lambda = 2.659 * ( -1.857 + 1.040/wavelength ) + r_v
        elif (wavelength > 0.12) & (wavelength < 0.63):
            # sutable for 0.12 micron < wavelength < 0.63 micron
            k_lambda = 2.659 * (- 2.156 + 1.509 / wavelength - 0.198/ wavelength**2 + 0.011/wavelength**3) + r_v
        else:
            raise KeyError('wavelength must be > 1200 Angstrom and < 22000 Angstrom')

        return k_lambda

    def get_corr_h_alpha_flux(self, flux_h_alpha_6565=None, flux_h_beta_4863=None, line_shape='gauss'):
        """
        Get extinction corrected h_alpha flux err following Calzetti et al. (2000) doi:10.1086/308692
         using eq. 2 and eq.3
        """

        # get eimission line flux
        if (flux_h_alpha_6565 is None) & (flux_h_beta_4863 is None):
            flux_h_alpha_6565 = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863 = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

        # get extinction
        e_b_v = self.get_extinction(flux_h_alpha_6565=flux_h_alpha_6565, flux_h_beta_4863=flux_h_beta_4863,
                                    line_shape=line_shape)

        #  the color excess of the stellar continuum is linked to the color excess e_s_b_v derived from the nebular
        #  gas emission lines e_b_v
        # correcting using eq. 3
        # e_s_b_v = 0.44 * e_b_v

        # get reddening curve
        k_h_alpha = self.compute_reddening_curve(wavelength=6565, r_v=3.1)

        # flux crrection eq. 2
        # corr_flux_h_alpha_6565 = flux_h_alpha_6565 * 10 ** (0.4 * e_s_b_v * k_h_alpha)
        corr_flux_h_alpha_6565 = flux_h_alpha_6565 * 10 ** (0.4 * e_b_v * k_h_alpha)

        return corr_flux_h_alpha_6565

    def get_corr_h_alpha_flux_err(self, flux_h_alpha_6565=None, flux_h_beta_4863=None, flux_h_alpha_6565_err=None,
                                       flux_h_beta_4863_err=None, line_shape='gauss'):
        """
        Get extinction corrected h_alpha flux following Calzetti et al. (2000) doi:10.1086/308692
         using eq. 2 and eq.3
        """

        # get eimission line flux
        if (flux_h_alpha_6565 is None) & (flux_h_beta_4863 is None):
            flux_h_alpha_6565 = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863 = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

            flux_h_alpha_6565_err = self.get_emission_line_flux_err(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863_err = self.get_emission_line_flux_err(line_wavelength=4863, line_shape=line_shape)

        # get extinction
        e_b_v = self.get_extinction(flux_h_alpha_6565=flux_h_alpha_6565, flux_h_beta_4863=flux_h_beta_4863,
                                    line_shape=line_shape)
        # get extinction err
        e_b_v_err = self.get_extinction_err(flux_h_alpha_6565=flux_h_alpha_6565, flux_h_beta_4863=flux_h_beta_4863,
                                            flux_h_alpha_6565_err=flux_h_alpha_6565_err,
                                            flux_h_beta_4863_err=flux_h_beta_4863_err, line_shape=line_shape)

        # the color excess of the stellar continuum is linked to the color excess e_s_b_v derived from the nebular
        # gas emission lines e_b_v
        # correcting using eq. 3
        # e_s_b_v = 0.44 * e_b_v
        # err
        # e_s_b_v_err = 0.44 * e_b_v_err

        # get reddening curve
        k_h_alpha = self.compute_reddening_curve(wavelength=6565, r_v=3.1)

        # errorpropagation of eq. 2
        corr_flux_h_alpha_6565_err = np.sqrt((flux_h_alpha_6565_err * 10 ** (0.4 * e_b_v * k_h_alpha)) ** 2 +
                                             (e_b_v_err * flux_h_alpha_6565 * 10 ** (0.4 * e_b_v * k_h_alpha) *
                                              np.log(10) * 0.4 * k_h_alpha) ** 2)

        return corr_flux_h_alpha_6565_err

    def get_corr_h_alpha_lum(self, flux_h_alpha_6565=None, flux_h_beta_4863=None, redshift=None, line_shape='gauss'):
        """
         as described in Kewley et al. 2002 doi:10.1086/344487
        :param line_shape: line shape as described in get_emission_line_flux()
        :return: float or array
        """
        # get eimission line flux
        if (flux_h_alpha_6565 is None) & (flux_h_beta_4863 is None):
            flux_h_alpha_6565 = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863 = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

        corrected_flux = np.array(self.get_corr_h_alpha_flux(flux_h_alpha_6565=flux_h_alpha_6565,
                                                             flux_h_beta_4863=flux_h_beta_4863, line_shape=line_shape),
                                  dtype=np.float64)

        if redshift is None:
            redshift = self.get_redshift()

        luminosity_dist = np.array(self.cosmology.luminosity_distance(redshift).to(u.cm).value, dtype=np.float64)

        corr_h_alpha_lum = corrected_flux * (1e-17 * 4 * np.pi) * (luminosity_dist**2)

        return corr_h_alpha_lum

    def get_corr_h_alpha_lum_err(self, flux_h_alpha_6565=None, flux_h_beta_4863=None, flux_h_alpha_6565_err=None,
                                 flux_h_beta_4863_err=None, redshift=None, line_shape='gauss'):
        """
         as described in Kewley et al. 2002 doi:10.1086/344487
        :param line_shape: line shape as described in get_emission_line_flux()
        :return: float or array
        """
        # get eimission line flux
        if (flux_h_alpha_6565 is None) & (flux_h_beta_4863 is None):
            flux_h_alpha_6565 = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863 = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

            flux_h_alpha_6565_err = self.get_emission_line_flux_err(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863_err = self.get_emission_line_flux_err(line_wavelength=4863, line_shape=line_shape)

        corrected_flux_err = np.array(self.get_corr_h_alpha_flux_err(flux_h_alpha_6565=flux_h_alpha_6565,
                                                                     flux_h_beta_4863=flux_h_beta_4863,
                                                                     flux_h_alpha_6565_err=flux_h_alpha_6565_err,
                                                                     flux_h_beta_4863_err=flux_h_beta_4863_err,
                                                                     line_shape=line_shape), dtype=np.float64)
        if redshift is None:
            redshift = self.get_redshift()

        luminosity_dist = np.array(self.cosmology.luminosity_distance(redshift).to(u.cm).value, dtype=np.float64)

        corr_h_alpha_lum_err = corrected_flux_err * (1e-17 * 4 * np.pi) * luminosity_dist * luminosity_dist

        return corr_h_alpha_lum_err

    def get_log_corr_h_alpha_lum(self, flux_h_alpha_6565=None, flux_h_beta_4863=None, redshift=None,
                                 line_shape='gauss'):
        """
         as described in Kewley et al. 2002 doi:10.1086/344487
        :param line_shape: line shape as described in get_emission_line_flux()
        :return: float or array
        """
        # get eimission line flux
        if (flux_h_alpha_6565 is None) & (flux_h_beta_4863 is None):
            flux_h_alpha_6565 = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863 = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

        log_corrected_flux = np.log10(self.get_corr_h_alpha_flux(flux_h_alpha_6565=flux_h_alpha_6565,
                                                                      flux_h_beta_4863=flux_h_beta_4863,
                                                                      line_shape=line_shape))

        if redshift is None:
            redshift = self.get_redshift()

        log_lum_dist = np.log10(self.cosmology.luminosity_distance(redshift).to(u.cm).value)

        log_corr_h_alpha_lum = log_corrected_flux + np.log10(1e-17 * 4 * np.pi) + 2 * log_lum_dist

        return log_corr_h_alpha_lum

    def get_log_corr_h_alpha_lum_err(self, flux_h_alpha_6565=None, flux_h_beta_4863=None, flux_h_alpha_6565_err=None,
                                     flux_h_beta_4863_err=None, line_shape='gauss'):
        """
         as described in Kewley et al. 2002 doi:10.1086/344487
        :param line_shape: line shape as described in get_emission_line_flux()
        :return: float or array
        """
        # get eimission line flux
        if (flux_h_alpha_6565 is None) & (flux_h_beta_4863 is None):
            flux_h_alpha_6565 = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863 = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

            flux_h_alpha_6565_err = self.get_emission_line_flux_err(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863_err = self.get_emission_line_flux_err(line_wavelength=4863, line_shape=line_shape)

        corrected_flux = self.get_corr_h_alpha_flux(flux_h_alpha_6565=flux_h_alpha_6565,
                                                         flux_h_beta_4863=flux_h_beta_4863, line_shape=line_shape)

        corrected_flux_err = self.get_corr_h_alpha_flux_err(flux_h_alpha_6565=flux_h_alpha_6565,
                                                            flux_h_beta_4863=flux_h_beta_4863,
                                                            flux_h_alpha_6565_err=flux_h_alpha_6565_err,
                                                            flux_h_beta_4863_err=flux_h_beta_4863_err,
                                                            line_shape=line_shape)

        log_corr_h_alpha_lum_err = corrected_flux_err / (corrected_flux * np.log(10))

        return log_corr_h_alpha_lum_err

    def get_h_alpha_corr_sfr(self, flux_h_alpha_6565=None, flux_h_beta_4863=None, redshift=None,
                             line_shape='gauss', IMF='Salpeter'):
        """
        to calculate the SFR from the H\alpha emission line
         we need to correct the luminosity for  extinction-corrected
         as described in Kewley et al. 2002 doi:10.1086/344487
        :param line_shape: line shape as described in get_emission_line_flux()
        :return: float or array
        """
        if (flux_h_alpha_6565 is None) & (flux_h_beta_4863 is None):
            flux_h_alpha_6565 = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863 = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

        if redshift is None:
            redshift=self.get_redshift()

        corr_h_alpha_lum = self.get_corr_h_alpha_lum(flux_h_alpha_6565=flux_h_alpha_6565,
                                                     flux_h_beta_4863=flux_h_beta_4863, redshift=redshift,
                                                     line_shape=line_shape)

        # Kewley et al. 2002 doi:10.1086/344487
        # equation 6
        # Salpeter IMF
        if IMF == 'Salpeter':
            sfr = 7.9 * 10 **(-42) * corr_h_alpha_lum
        elif IMF == 'Chabrier':
            sfr = 1.2 * 10 **(-41) * corr_h_alpha_lum
            
        return sfr

    def get_h_alpha_corr_sfr_err(self, flux_h_alpha_6565=None, flux_h_beta_4863=None,
                                     flux_h_alpha_6565_err=None, flux_h_beta_4863_err=None, line_shape='gauss'):
        """
        to calculate the SFR from the H\alpha emission line
         we need to correct the luminosity for  extinction-corrected
         as described in Kewley et al. 2002 doi:10.1086/344487
        :param line_shape: line shape as described in get_emission_line_flux()
        :return: float or array
        """
        # get emission line flux
        if (flux_h_alpha_6565 is None) & (flux_h_beta_4863 is None):
            flux_h_alpha_6565 = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863 = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

            flux_h_alpha_6565_err = self.get_emission_line_flux_err(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863_err = self.get_emission_line_flux_err(line_wavelength=4863, line_shape=line_shape)

        corr_h_alpha_lum_err = self.get_corr_h_alpha_lum_err(flux_h_alpha_6565=flux_h_alpha_6565,
                                                             flux_h_beta_4863=flux_h_beta_4863,
                                                             flux_h_alpha_6565_err=flux_h_alpha_6565_err,
                                                             flux_h_beta_4863_err=flux_h_beta_4863_err,
                                                             line_shape=line_shape)

        # Kewley et al. 2002 doi:10.1086/344487
        # error propagation on equation 6
        sfr_err = 7.9 *1e-42 * corr_h_alpha_lum_err

        return sfr_err

    def get_log_h_alpha_corr_sfr(self, flux_h_alpha_6565=None, flux_h_beta_4863=None, redshift=None,
                                 line_shape='gauss', IMF='Salpeter'):
        """
        to calculate the SFR from the H\alpha emission line
         we need to correct the luminosity for  extinction-corrected
         as described in Kewley et al. 2002 doi:10.1086/344487
        :param line_shape: line shape as described in get_emission_line_flux()
        :return: float or array
        """
        if (flux_h_alpha_6565 is None) & (flux_h_beta_4863 is None):
            flux_h_alpha_6565 = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863 = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

        if redshift is None:
            redshift=self.get_redshift()

        log_corr_h_alpha_lum = self.get_log_corr_h_alpha_lum(flux_h_alpha_6565=flux_h_alpha_6565,
                                                         flux_h_beta_4863=flux_h_beta_4863, redshift=redshift,
                                                             line_shape=line_shape)

        # Kewley et al. 2002 doi:10.1086/344487
        # equation 6
        # using a Salpeter IMF
        if IMF == 'Salpeter':
            log_sfr = np.log10(7.9) - 42 + log_corr_h_alpha_lum
        elif IMF == 'Chabrier':
            log_sfr = np.log10(1.2) - 41 + log_corr_h_alpha_lum
        elif IMF == 'Kroupa':
            log_sfr = np.log10(5.5) - 42 + log_corr_h_alpha_lum

        return log_sfr

    def get_log_h_alpha_corr_sfr_err(self, flux_h_alpha_6565=None, flux_h_beta_4863=None,
                                 flux_h_alpha_6565_err=None, flux_h_beta_4863_err=None, line_shape='gauss', IMF='Salpeter'):
        """
        to calculate the SFR from the H\alpha emission line
         we need to correct the luminosity for  extinction-corrected
         as described in Kewley et al. 2002 doi:10.1086/344487
        :param line_shape: line shape as described in get_emission_line_flux()
        :return: float or array
        """

        if (flux_h_alpha_6565 is None) & (flux_h_beta_4863 is None):
            flux_h_alpha_6565 = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863 = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)

            flux_h_alpha_6565_err = self.get_emission_line_flux_err(line_wavelength=6565, line_shape=line_shape)
            flux_h_beta_4863_err = self.get_emission_line_flux_err(line_wavelength=4863, line_shape=line_shape)

        log_corr_h_alpha_l_err = self.get_log_corr_h_alpha_lum_err(flux_h_alpha_6565=flux_h_alpha_6565,
                                                               flux_h_beta_4863=flux_h_beta_4863,
                                                               flux_h_alpha_6565_err=flux_h_alpha_6565_err,
                                                               flux_h_beta_4863_err=flux_h_beta_4863_err,
                                                               line_shape=line_shape)

        # Kewley et al. 2002 doi:10.1086/344487
        # equation 6 (using error propagation)
        # using a Salpeter IMF
        if IMF == 'Salpeter':
            log_sfr_err = np.log10(7.9) - 42 + log_corr_h_alpha_l_err
        elif IMF == 'Chabrier':
            log_sfr_err = np.log10(1.2) - 41 + log_corr_h_alpha_l_err

        return log_sfr_err


class SFRTools(data_access.DataAccess):

    def __init__(self, **kwargs):
        r"""

        """
        super().__init__(**kwargs)

    def main_sequence_sf(self, redshift=None, log_stellar_mass=None, ref='Speagle+14', output='sfr'):
        r"""
        Estimation of the star formation rate of the main sequence (MS) using
         Speagle+14 doi:10.1088/0067-0049/214/2/15 or Whiteker+12 doi:10.1088/2041-8205/754/2/L29
         The relation of Speagle+14 is valid for 0 < z < 6
         The relation of Whiteker+12 is valid for 0 < z < 2.5

        :param redshift: redshift z. If None it will be accessed from table keyword ``z``
        :param log_stellar_mass: logarithmic stellar mass. If None it will be accessed from mpa
        :param ref: ``Speagle+14`` for Speagle+14 or ``Whiteker+12`` for  Whiteker+12
        :param output: set ``sfr`` for star formation rate in log M_sun yr^-1
                       or ``ssfr`` for specific star formation rate in log yr^-1 and
        :return: estimated log SFR or sSFR of the main sequence at given redshift and stellar mass
        """

        # redshift
        if redshift is None:
            redshift = self.get_redshift()

        # log_stellar_mass
        if log_stellar_mass is None:
            log_stellar_mass = self.get_log_mpa_stellar_mass()

        if ref == 'Speagle+14':
            # get Age of the universe in Gyr at redshift ``z``
            t = self.cosmology.age(z=redshift).value
            # Speagle+14 EQ. (28)
            log_sfr_ms = (0.84 - 0.026 * t) * log_stellar_mass - (6.51 - 0.11 * t)
        elif ref == 'Whiteker+12':
            # Whiteker+12 EQ. 1,2,3
            log_sfr_ms = ((0.70 - 0.13 * redshift) * (log_stellar_mass - 10.5) +
                          0.38 + 1.14 * redshift - 0.19 * redshift ** 2)
        else:
            raise KeyError('Reference not understand. You can only use ref = Speagle+14 or Whiteker+12')

        if output == 'sfr':
            return log_sfr_ms
        elif output == 'ssfr':
            return log_sfr_ms - log_stellar_mass
        else:
            raise KeyError('Output format not understand. You can only use output = sfr or ssfr')

    @staticmethod
    def fir2sfr(fir_luminosity):
        r"""
        This function follows Kennicutt (1998) DOI:10.1086/305588 EQ. (3) to estimate the star formation ratio (SFR)
        using the far infrared luminosity
        :param fir_luminosity: far infra-red luminosity
        :return: sfr in M_sun / yr
        """

        # Kennicutt 1998 EQ. (3)
        return fir_luminosity / (5.8 * 1e9)


class AnalyseGas(EmissionLineTools, SFRTools):

    def __init__(self, **kwargs):
        r"""

        """
        super().__init__(**kwargs)

    def compute_co_luminosity(self, s_co_vel, luminosity_dist=None, redshift=None, observed_co_frequency=None,
                              conversion_factor=1, index=None):
        r"""
        Compute the CO emission line luminosity. We use as default the CO(J1->0) line.
         following Solomon et al. 1997 APJ 478 : 144-161, doi:10.1086/303765
          Using EQ. (3) in Sect. 3.1 for the luminosity
          (see also Saintonge et al 2011  doi:10.1111/j.1365-2966.2011.18677.x EG. (4))

        :param s_co_vel: the velocity-integrated flux in K km/s or Jy km/s this must be specified through the
        conversion_factor.
        :type s_co_vel: float or array
        :param redshift: redshift z. If None it will use the galtool access function get_redshift().
         For local galaxies this should be set to 0
        :type redshift: float or array
        :param luminosity_dist: luminosity distance in Mpc. If None it will be calculated with RCSED redshift and
         galtool cosmology
        :type luminosity_dist: float or array
        :param observed_co_frequency: observed CO line frequency in GHz. If None the CO(J 1->0) line is taken
        :type observed_co_frequency: float
        :param conversion_factor: conversion factor. If the flux is in K km/s and not Jy km/s it needs to converted with
        a telescope specific conversion factor. For the IRAM 30m telescope this for example 5 Jy / K
        :type conversion_factor: float
        :param index: index to specify if a complete array will be computed or a single value

        :returns: CO line luminosity in 1e9 K km s^-1 pc^2. float or Array like
        """

        # redshift
        if redshift is None:
            redshift = self.get_redshift()

        # luminosity distance in Mpc
        if luminosity_dist is None:
            luminosity_dist = self.cosmology.luminosity_distance(redshift).value

        if observed_co_frequency is None:
            # frequency of CO(J 1->0) line in Ghz
            observed_co_frequency = self.co10_line_frequency / (1 + redshift)

        # Kelvin to Jy transformation factor if needed
        s_co_vel *= conversion_factor

        # CO line luminosity in 1e9 K km s^-1 pc^2
        # see Solomon et al. 1997 EQ. (3)
        co_luminosity = (3.25 *
                         10 ** -2 *
                         s_co_vel *
                         luminosity_dist ** 2 *
                         observed_co_frequency ** -2 *
                         (1 + redshift) ** -3)

        # if no index is set this function will return the whole table, otherwise only one element will be returned.
        if index is None:
            return co_luminosity
        else:
            return co_luminosity[index]

    def old_co2molecular_gas_conversion_factor(self, redshift=None, log_stellar_mass=None, log_ssfr=None,
                                               gas_met=None):
        r"""
        This function calculates CO-to-H2 conversion factor following Accurso+17 doi:10.1093/mnras/stx1556
         This is valid for redshift 0 < z < 2.5,
         gas phase metallicity 7.9 < 12 + log (O/H) < 8.8
         and difference to espected main sequence sSFR -0.8 < log_delta_ssfr_ms < 1.3

        :param redshift: redshift z. If None it will be accessed over attribute
        :param log_stellar_mass: logarithmic stellar mass. If None it will be accessed over attribute
        :param log_ssfr: logarithmic specific star formation rate. If None it will be accessed over attribute
        :param gas_met: gas phase metalicity. If None it will be accessed over attribute using O3n2 function

        :return: The logarithmic CO-to-H2 conversion factor
        """

        # redshift
        if gas_met is None:
            gas_met = self.get_gas_met_o3n2(line_shape='nonpar')

        # log_stellar_mass
        if log_stellar_mass is None:
            log_stellar_mass = self.get_log_mpa_stellar_mass()

        log_delta_ssfr_ms = self.log_delta_ssfr_ms(redshift=redshift, log_stellar_mass=log_stellar_mass,
                                                   log_ssfr=log_ssfr)

        # Accurso+17 EQ. 26
        # systematic uncertainty is +/- 0.165 dex
        return 15.623 - 1.732 * gas_met + 0.051 * log_delta_ssfr_ms

    def t_depl_tacconi_2018(self, redshift, log_sfr=None, log_stellar_mass=None, r_e=None):
        r"""
        This function calculates the expected depletion time for given redshift, star formation rate, stellar mass and
        effective radius. To calculate this we follow Tacconi+18 doi.org/10.3847/1538-4357/aaa4b4 Using EQ. (5)
        t_dep = M_H2 / SFR

        :param redshift: redshift
        :type redshift: array-like
        :param log_sfr: log star formation ratio (sfr). If None this term will be drop to 0
        :type log_sfr: array-like
        :param log_stellar_mass: log stellar mass. If None this term will be drop to 0
        :type log_stellar_mass: array-like
        :param r_e: effective radius in kpc. If None this term will drop to 0. (e_t * log_delta_r)
        :type r_e: array-like

        :return: expected log depletion time in yr
        """

        # Fit parameters for Tacconie+18 EQ. (5) taken from Table 3
        # using Best (with bootstrap errors) t_depl (Gyr)
        # with parametrization of Speagle+14 doi:10.1088/0067-0049/214/2/15
        a_t = 0.09  # uncertainty 0.05
        b_t = -0.62  # uncertainty 0.13
        c_t = -0.44  # uncertainty 0.04
        d_t = 0.09  # uncertainty 0.05
        e_t = 0.11  # uncertainty 0.12

        # get difference of ssfr to the expected ssfr of the main sequence
        if log_sfr is None:
            log_delta_ssfr = 0
        else:
            log_delta_ssfr = log_sfr - log_stellar_mass - self.main_sequence_sf(redshift=redshift,
                                                                                log_stellar_mass=log_stellar_mass,
                                                                                    ref='Speagle+14', output='ssfr')

        # corrected to fiducial stellar mass of 5*10^10 M_sun
        if log_stellar_mass is None:
            log_delta_stellar_mass = 0
        else:
            log_delta_stellar_mass = log_stellar_mass - 10.7

        # difference to the average radius of the star forming population
        if r_e is None:
            log_delta_r = 0
        else:
            log_delta_r = (np.log10(r_e) -
                           (np.log10(8.9) - 0.75 * np.log10(1 + redshift) + 0.23 * log_delta_stellar_mass))

        # Tacconie+18 EQ. (5)
        log_t_depl = (a_t +
                      b_t * np.log10(1 + redshift) +
                      c_t * log_delta_ssfr +
                      d_t * log_delta_stellar_mass +
                      e_t * log_delta_r)

        return log_t_depl

    def mu_tacconi_2018(self, redshift, log_sfr=None, log_stellar_mass=None, r_e=None):
        r"""
        This function calculates the expected ratio of molecular to stellar mass for given redshift,
        star formation rate, stellar mass and effective radius.
        To calculate this we follow Tacconi+18 doi.org/10.3847/1538-4357/aaa4b4 Using EQ. (6)

        mu_H2 = M_H2 / M_star

        :param redshift: redshift
        :type redshift: array-like
        :param log_sfr: log star formation ratio (sfr). If None this term will be drop to 0
        :type log_sfr: array-like
        :param log_stellar_mass: log stellar mass. If None this term will be drop to 0
        :type log_stellar_mass: array-like
        :param r_e: effective radius in kpc. If None this term will drop to 0. (e_t * log_delta_r)
        :type r_e: array-like

        :return: expected log ratio of molecular to stellar mass
        """

        # Fit parameters for Tacconie+18 EQ. (5) taken from Table 3
        # using Best fit with Zero-point offset for beta=2.
        # with parametrization of Speagle+14 doi:10.1088/0067-0049/214/2/15
        a_mu = 0.12  # uncertainty 0.15
        b_mu = -3.62  # uncertainty 0.4
        c_mu = 0.53  # uncertainty 0.03
        d_mu = -0.35  # uncertainty 0.03
        e_mu = 0.11  # uncertainty 0.1
        f_mu = 0.66  # uncertainty 0.1
        beta_mu = 2

        # get difference of ssfr to the expected ssfr of the main sequence
        if log_sfr is None:
            log_delta_ssfr = 0
        else:
            log_delta_ssfr = log_sfr - log_stellar_mass - self.main_sequence_sf(redshift=redshift,
                                                                                log_stellar_mass=log_stellar_mass,
                                                                                ref='Speagle+14', output='ssfr')

        # corrected to fiducial stellar mass of 5*10^10 M_sun
        if log_stellar_mass is None:
            log_delta_stellar_mass = 0
        else:
            log_delta_stellar_mass = log_stellar_mass - 10.7

        # difference to the average radius of the star forming population
        if r_e is None:
            log_delta_r = 0
        else:
            log_delta_r = (np.log10(r_e) -
                           (np.log10(8.9) - 0.75 * np.log10(1 + redshift) + 0.23 * log_delta_stellar_mass))

        # Tacconie+18 EQ. (6)
        log_mu = (a_mu +
                  b_mu * (np.log10(1 + redshift) - f_mu) ** beta_mu +
                  c_mu * log_delta_ssfr +
                  d_mu * log_delta_stellar_mass +
                  e_mu * log_delta_r)

        return log_mu

    @staticmethod
    def co2molecular_gas_conversion_factor(redshift=None, log_stellar_mass=None):
        r"""
        This function calculates CO-to-H2 conversion factor

        following Freundlich+19 A&A 622, A105 (2019) https://doi.org/10.1051/0004-6361/201732223
        see EQ. (3), (4) and (5)
        This was adapted from Genzel+15 doi:10.1088/0004-637X/800/1/20 EQ. (12a)
        This formula is a combination in Genzel+15 it is explained as:
         ``Following Maiolino+08 we combined the mass–metallicity relations at different redshifts
         presented by Erb+06, Maiolino+08, Zahid+14, and Wuyts+14
         in the following fitting function:``

        :param redshift: redshift z. If None it will be accessed over attribute
        :param log_stellar_mass: logarithmic stellar mass. If None it will be accessed over attribute

        :return: CO-to-H2 conversion factor
        """

        # The fundamental relation between L_co and M_H2 we are using is:
        # M_H2 = alpha_co * L_co(j->j-1) * r_j1
        # with r_j1 = L_co(j->j-1) / L_co(1->0)

        # log(z) = 12 + log(O/H)

        # for the Milkyway we use alpha_g = 4.36 +/- 0.9M/(K km s^-1 pc^2), including helium
        # see (Strong & Mattox 1996; Dame et al. 2001; Grenier et al. 2005; Bolatto et al.  2008,  2013;
        # Abdo et al. 2010; Schinnerer et al.2010; Leroy et al. 2011).
        alpha_g = 4.36

        b = 10.4 + 4.46 * np.log10(1 + redshift) - 1.78 * (np.log10(1 + redshift)) ** 2

        log_z = 8.74 - 0.087 * (log_stellar_mass - b) ** 2

        alpha_co = alpha_g * np.sqrt(0.67 * np.exp(0.36 * 10 ** (8.67 - log_z)) * 10 ** (-1.27 * (log_z - 8.67)))

        return alpha_co

    @staticmethod
    def compute_h2_mass(co_luminosity, alpha=4.36):
        r"""
        CO luminosity to molecular gas mass M_H2 = alpha * L'_CO

        Solomon et al. (1987) doi:10.1086/165493
         alpha = 4.6 M_sun (K km s^-1 pc^2)^-1

        Combes+13 DOI: 10.1051/0004-6361/201220392 Section 4.2
         for ULIRGs: alpha = 0.8 M_sun (K km s^-1 pc^2)^-1

        Tacconi et al. 2010 doi:10.1038/nature08773 Introduction EQ. (1)
         Total H_2 + He mass in cold gas (=1.36 times the H_2 mass),
         assuming X = N(H_2)/I(CO) = 2* 10^20 cm^-2 K^-1 km^-1 s, or alpha = M(H_2)/L_CO(1-0) = 3.2,
         where X and alpha are the CO-H_2 conversion factor in terms of gas surface density (X) and mass (alpha),
         determined from CO 3–2 luminosity and a correction I(CO 1–0)/I(CO 3–2) = 2.
         The 1sigma r.m.s. uncertainties are in parentheses, and upper limits are 3sigma r.m.s.

        Accurso+17 doi:10.1093/mnras/stx1556
         the CO-to-H2conversion factor, can be consid-ered a mass-to-light ratio.
         Across observations of the Milky Wayand nearby star-forming galaxies with approximately solar metal-licities,
         the empirical CO (1-0) conversion factors are consistent with a typically value of 4.36 M_sun (K km s^-1)^-1,
         which includes a 36 per cent correction for helium gas see Strong and Mattox (1996) 1996A&A...308L..21S and
         Abdo+10 doi:10.1088/0004-637X/710/1/133

        :param co_luminosity: CO luminosity in K km s^-1 pc^2
        :type co_luminosity: float or array-like
        :param alpha: factor to calculate the molecular gas mass using CO luminosity.
        :type alpha: float
        :returns: molecular gas (H2) mass in M_sun
        """

        return co_luminosity * alpha

    @staticmethod
    def get_iram30m_beam_eff(observed_line_frequency):
        r"""
        Function to return the beam efficiency for different frequencies of the IRAM 30m telescope
        :param observed_line_frequency: The observed frequency of the CO line
        :return: the IRAM 30m beam efficiency
        """
        from scipy import interpolate
        freq = [86, 110, 145, 170, 210, 235, 260, 279]
        eff = [0.78, 0.75, 0.69, 0.65, 0.57, 0.51, 0.46, 0.42]
        interpolate_beam_eff = interpolate.interp1d(freq, eff, kind='quadratic')
        return interpolate_beam_eff(observed_line_frequency)

    def calculate_iram_co10_temperature(self):
        r"""
        We use here Tacconi et al. 2013 (2013ApJ...768...74T)
        See their Figure 8 on the right side: SFR/LCO_3_2 = 1.1x10^(-8)

        In Section 2.4 of Tacconi et al. 2015 they specify the conversion of the CO 3–2 luminosity to an
        equivalent CO 1–0 luminosity. They apply a correction factor of R_1_3 = L′CO_1–0 / L′CO_3–2 ∼ 2
        Thus LCO_3_2 = SFR / (1.1x10^(-8))
        LCO_1_0 = 2 * LCO_3_2 = 2/1.1 * 10^8 * SFR
        This would lead us to MH2 = 4.36 * 2/1.1 * 10^8 * SFR
        The conversion factor is 4.36 is the CO–H2 conversion factor
        (includingthe factor 1.36 correction for helium) see end of Section 1 in Tacconi et al. 2013
        But his is for z~1
        We know that the depletion time is smaller, so it goes very well with MH2 = 2 * 10^9 * SFR at z=0.
        see (Bigiel et al. 2008 (2008AJ....136.2846B)) Section 4.3
        using the conversion factor of 4.36 we get
        LCO10 = 4.6 * 10^8 * SFR

        We can now estimate the CO temperature using Solomon et al. 1997 (1997ApJ...478..144S)
        EQ. (3)
        L_CO10 = 3.25 * 10^7 * S_CO * delta_v * nu_obs^(-2) * D_L^2 * (1 + z)^(-3)

        For the IRAM 30m telescope: Tmb= SCO/5  (5 Jy/K for the 30m)

        Putting everything together:
        5 * T_mb = L_CO10 * 1/3.25 * 10^(-7) * 1/delta_v * nu_obs^2 * D_L^(-2) * (1 + z)^3
        5 * T_mb = 4.6 * 10^8 * SFR * 1/3.25 * 10^(-7) * 1/delta_v * nu_obs^2 * D_L^(-2) * (1 + z)^3
        T_mb = 2.83 * SFR * 1/delta_v * nu_obs^2 * D_L^(-2) * (1 + z)^3
        """

        # get the sfr
        sfr_mpa = 10 ** self.get_log_mpa_sfr()  # from MPA
        sfr = 10 ** self.get_log_sed_sfr()  # from Salim+16
        # we use in principal Salim+16. If there is no value we use the estimation of MPA
        sfr[(np.isnan(sfr) | (sfr == -99))] = sfr_mpa[(np.isnan(sfr) | (sfr == -99))]

        # get redshift
        redshift = self.get_redshift()

        # get luminosity distance
        d_l_mpc = self.cosmology.luminosity_distance(redshift).value  # in Mpc

        # get an approximation of emission line width in km/s
        delta_v = 300  # in km/s

        # get the CO1-0 observed frequency
        nu_obs = self.co10_line_frequency / (1 + redshift)  # in GHz

        # calculate the CO temperature in K:
        t_mb = 2.83 * sfr * 1 / delta_v * nu_obs ** 2 * d_l_mpc ** -2 * (1 + redshift) ** 3

        # return CO temperature in mK
        return t_mb * 1e3

    def __not_working_get_co_temperature(self):

        # note Meyer et al. 2017 doi:10.1017/pasa.2017.31
        # this is on HI but we can try to understand how this is working for CO

        # get the sfr
        sfr_mpa = 10 ** self.get_log_mpa_sfr()  # from MPA
        sfr = 10 ** self.get_log_sed_sfr()  # from Salim+16
        # we use in principal Salim+16. If there is no value we use the estimation of MPA
        sfr[(np.isnan(sfr) | (sfr == -99))] = sfr_mpa[(np.isnan(sfr) | (sfr == -99))]

        # get redshift
        redshift = self.get_redshift()

        # Relation between SFR and molecular taken from (Bigiel et al. 2008 (2008AJ....136.2846B)) Section 4.3
        # This takes also a depletion time into account
        # (I)
        # M_h2[M$_{\odot}$] = 2 * 10^9 [yr ^{-1}] SFR [M$_{\odot}$  yr $^{-1}$]
        m_h2 = 2 * 10 ** 9 * sfr

        # Strong et al. 1988 (1988A&A...207....1S)
        # The average ratio of H2 column density to integrated CO temperature is determined , the best estimate bing
        # in a formula this is
        # (II)
        # X_{CO} = N_{H2} / I_{CO} = 2.3 10^{20} cm^{-2} (K km s^{-1})^{-1}
        x_co = 2.3 * 1e20
        # here N_{H2} is the number of H2 molecules on the entire beam surface.
        # I_{CO} is the velocity-integrated source brightness temperature, thus I_{CO} = \DeltaV T_{CO}
        # for double peak galaxies e use the emission line delta_v
        delta_v = self.get_delta_v()  # in km/s
        # We calculate the number of H2 molecules as follows N_{H2} = M_{H2} / (2 * M_p * S)
        # M_{H2} is the total H2 mass, M_p is the proton mass (H2 = 2 protons) and S the surface of the Galaxy
        # Proton mass = 1.6726219 *1e-27 kg  ( in Astropy units = u.M_p)
        # 1 solar mass is 1.9884099 * 1e30 kg (in Astropy units = u.solMass)
        # thus in Solar masses 2 * M_p = 2 * u.M_p / u.solMass
        # (III)
        # Therefore M_H2_molecule in solar masses is 1.682371325952461e-57 M_{\odot}
        m_h2_molecule = (2 * u.M_p).to_value(u.kg) / (1 * u.solMass).to_value(u.kg)  # in M_{\odot}

        # Now we need to calculate the surface S of the galaxy
        # to calculate the surface we orientate from solomon et al. 1997 (1997ApJ...478..144S)
        # see Section 3.1.1 above EQ. (2)
        # From trigonometry: tan(alpha) = alpha for small sources
        # source_radius = radius_in_degree x Distance (here we have to take the angular distance)
        # surface is  r^2 x pi
        # S = radius_in_degree ^ 2 * D_A ^ 2 * np.pi
        # (IV)
        # D_A is in Mpc and we want now to transform this into cm ^ 2
        # 1 pc = 3.0856776 * 1e16 m =  3.0856776 * 1e18 cm
        # thus 1 Mpc = 3.0856776 * 1e22 m 3.0856776 * 1e24 cm
        iram_beam_radius = 11 / 3600  # in degree (note IRAM beam is 22" in diameter)
        d_a_mpc = self.cosmology.angular_diameter_distance(redshift).value  # in Mpc
        d_a_cm = self.cosmology.angular_diameter_distance(redshift).to_value(u.cm)  # in cm

        # using Meyer et al. 2017 (2017PASA...34...52M) EQ. (26)
        # get solid angle of the surface omega_beam = pi * a * b / 4 * ln(2)
        # a and b are the two elliptical radii of the radio beam
        omega_beam = np.pi * iram_beam_radius ** 2 / (4 * np.log(2))
        # (V)
        # omega beam is 1.0579004189064245e-05 Sr
        # Thus the surface S = omega_beam * d_a ** 2

        # now we can put all formula together
        # N_H2 = M_H2  / (2 * M_p * S) = I_CO * X_CO = T_CO * delta_v * X_CO
        # thus we get
        # T_co = M_H2 / (2 * M_p * S * delta_v * X_CO)
        # now we know that S = omega_beam * d_a ** 2
        # thus
        # T_co = M_H2 / (2 * M_p * omega_beam * d_a ** 2 * delta_v * X_CO)

        # now lets go through the different units
        # T_co[K] = M_H2[M_{\odot}] / (2 * M_p[M_{\odot}] * omega_beam[SR] * d_a[Mpc] ** 2 *
        #                              delta_v[km s^{-1}] * X_CO[cm^{-2} (K km s^{-1})^{-1}])
        # Here we see that we need an exchange factor from Mpc to cm:  1Mpc = 3.0856776 * 1e18 cm

        # using (I)
        # T_co = M_H2 / (2 * M_p * omega_beam * d_a ** 2 * delta_v * X_CO)
        # putting all conversion factors together:
        # T_co = 2 * 10 ** 9 SFR /
        #       (1.682371325952461e-57 *              <-- using (III)
        #       1.0579004189064245e-05 *              <-- using (V)
        #       d_a ** 2 * 9.521406251141759e+48 *    <-- using (IV)
        #       delta_v * 300 / 300                   <-- just to have the factor 300
        #       2.3 * 1e20)                           <-- using (II)

        # the new factor should be
        # 2 * 10 ** 9 / (1.682371325952461e-57 * 1.0579004189064245e-05 * 9.521406251141759e+48 * 300 * 2.3 * 1e20)
        # = 0.1710460945361374

        # compressing all conversion factors into one
        # T_co = 0.1710460945361374 SFR / (d_a ** 2 * delta_v / 300 )
        # specifying the units of each variable
        # T_co = 0.1710460945361374 * SFR[M_{\odot} yr^{-1}] * 1 / (d_a[Mpc] ** 2) * 300 / (delta_v[km s^{-1}])
        # we now add a factor of 1e3 to convert t_co into mk
        # furthermore we take the redshift into account and add a factor (1 + z)
        t_co_formula = 171.0460945361374 * sfr * 1 / (d_a_mpc ** 2) * 300 / delta_v * (1 + redshift)

        # using log10 values because the exponents can get too large and become inf/nan values
        log_m_h2 = np.log10(m_h2)
        log_m_h2_molecule = np.log10(m_h2_molecule)
        log_omega_beam = np.log10(omega_beam)
        log_d_a_cm = np.log10(d_a_cm)
        log_delta_v = np.log10(delta_v)
        log_x_co = np.log10(x_co)
        log_1_plus_z = np.log10(1 + redshift)

        t_co_direct = 10 ** (log_m_h2 - (log_m_h2_molecule +
                                         log_omega_beam + 2 * log_d_a_cm +
                                         log_delta_v +
                                         log_x_co)
                             + 3 + log_1_plus_z)

        # return t_co_direct
        raise ReferenceError('Do not use this function!!! The order of magnitude is not working and we do not know why')

    def get_hi_mass(self, s_hi, redshift=None, luminosity_dist=None, flux_density_frame='observed', index=None):
        r"""
        following Meyer et al. 2017 doi:10.1017/pasa.2017.31 EQ 49 and 50

        :param s_hi: integrated line profile in Jy km s^-1. (integrated over over the Doppler velocity)
         This can be approximated as S_peak * W_50 (better is a direct surface measurement of a line-fit)
        :type s_hi: float or array
        :param redshift: redshift z. If None it will use the galtool access function get_redshift().
         For local galaxies this should be set to 0
        :type redshift: float or array
        :param luminosity_dist: luminosity distance in Mpc. If None it will be calculated with RCSED redshift and
         galtool cosmology
        :type luminosity_dist: float or array
        :param flux_density_frame: keyword to specify if the flux density is measured in the observed or rest-frame
        :type flux_density_frame: str
        :param index: index to specify if a complete array will be computed or a single value

        :returns: HI-mass in units of 10^5 M_sun
        """

        # redshift
        if redshift is None:
            redshift = self.get_redshift()

        # luminosity distance in Mpc
        if luminosity_dist is None:
            luminosity_dist = self.cosmology.luminosity_distance(redshift).value

        # check if observed or rest- frame
        if flux_density_frame == 'observed':
            # Meyer et al. 2017 Eq. 49
            hi_mass = 2.356 * luminosity_dist ** 2 * s_hi * ((1 + redshift) ** -2)
        elif flux_density_frame == 'rest':
            # Meyer et al. 2017 Eq. 50
            hi_mass = 2.356 * luminosity_dist ** 2 * s_hi / (1 + redshift)
        else:
            raise KeyError('you must specify if the flux density of the HI line profile measured in rest or observed '
                           'frame')

        if index is None:
            return hi_mass
        else:
            return hi_mass[index]


class LuminosityTools(data_access.DataAccess):

    def __init__(self, **kwargs):
        r"""

        """
        super().__init__(**kwargs)

    def get_wise_luminosity(self, band='W1'):

        # the unit of the flux is in erg s-1 cm-2 Hz-1
        log_flux = np.log10(self.get_wise_band_flux(band=band, return_format='cgs'))
        # the unit of luminosity distance is in cm now
        log_lum_dist = np.log10(self.cosmology.luminosity_distance(self.get_redshift()).to(u.cm).value)
        # get log10 WISE band width in HZ
        wise_band_width = self.wise_bandwidth[band]['frequency']

        log_wise_bw = np.log10(wise_band_width)

        log_luminosity = np.log10(4 * np.pi) + 2 * log_lum_dist + log_flux + log_wise_bw

        return log_luminosity

    def get_band_luminosity(self, redshift=None, observation='wise', band='W1'):

        if redshift is None:
            redshift = self.get_redshift()

        try:
            log_band_flux = np.log10(getattr(self, 'get_%s_flux' % observation)(band=band, return_format='Jy'))
        except AttributeError:
            try:
                log_band_flux = np.log10(getattr(self, 'get_%s_band_flux' % observation)(band=band, return_format='Jy'))
            except AttributeError:
                raise AttributeError('This band is not available')
        except AttributeError:
            raise AttributeError('This band is not available')


        # the flux is units of Jy this transfers to
        # with the transfer 1 Jy = 10^-26 W m^-2 Hz^-1
        # we calculate the log luminosity distance in m:
        log_lum_dist = np.log10(self.cosmology.luminosity_distance(redshift).to(u.m).value)

        # calculate the radio luminosity
        band_luminosity = (log_band_flux +
                           np.log10(1e-26 * 4 * np.pi) +
                           2 * log_lum_dist)

        return band_luminosity

    def get_radio_spec_index(self, observation_1='lofar', observation_2='first', flux_estimation='int'):
        r"""
        We use Zajacek et al. (2019) DOI:10.1051/0004-6361/201833388 EQ. (1) to calculate the spectral index
        this is the negative value of EQ (1) in Calistro Rivera et al. (2017) DOI:10.1093/mnras/stx1040
        :param observation_1:
        :param observation_2:
        :param flux_estimation:
        :return:
        """
        # get observation flux
        flux_observation_1 = self.get_radio_continuum_flux(observation=observation_1, flux_estimation=flux_estimation,
                                                           unit_system='Jy')
        flux_observation_2 = self.get_radio_continuum_flux(observation=observation_2, flux_estimation=flux_estimation,
                                                           unit_system='Jy')

        # get observation frequency in MHz
        if observation_1 == 'lofar':
            observation_1_freq = 150
        elif (observation_1 == 'first') | (observation_1 == 'nvss'):
            observation_1_freq = 1400
        else:
            raise KeyError('first observation must be 150MHz or 1.4GHz')

        if (observation_2 == 'first') | (observation_2 == 'nvss'):
            observation_2_freq = 1400
        elif observation_2 == 'vlass':
            observation_2_freq = 3000
        else:
            raise KeyError('first observation must be lofar or first')

        # Zajacek et al. (2019) EQ. (1)
        return (np.log10(flux_observation_1 / flux_observation_2)) / (np.log10(observation_1_freq / observation_2_freq))

    def compute_best_radio_spec_index(self, default_index=-0.7, observation_hireachy=None, flux_estimation='int'):

        if observation_hireachy is None:
            observation_hireachy = [['lofar', 'first'], ['lofar', 'nvss'], ['lofar', 'vlass'], ['first', 'vlass'],
                                    ['nvss', 'vlass']]

        radio_index = np.ones(len(self.table)) * default_index
        computed_radio_values = np.zeros(len(self.table), dtype=bool)
        for observation_tuple in observation_hireachy:

            computed_radio_index = self.get_radio_spec_index(observation_1=observation_tuple[0],
                                                             observation_2=observation_tuple[1],
                                                             flux_estimation=flux_estimation)
            # mask all objects which will get the computed index
            new_radio_index = np.invert(np.isnan(computed_radio_index)) * np.invert(computed_radio_values)

            radio_index[new_radio_index] = computed_radio_index[new_radio_index]

            computed_radio_values[new_radio_index] = True

        return radio_index

    def get_radio_luminosity(self, observation, redshift=None, luminosity_distance=None, flux_estimation='int',
                             k_correction=False, spectral_index=-0.7):
        r"""
        Function to estimate the radio luminosity. This is usually done by using k correction.
        To estimate the spectral index for k-correction see get_radio_spec_index

        :param observation: vlass, first, nvss or lofar
        :param redshift: redshift. otherwise accessed through attribute
        :param luminosity_distance: luminosity_distance in Mpc. Otherwise calculated with redshift.
        :param flux_estimation: peak for peak flux or int for integrated flux
        :param k_correction: flag if k-correction should be applied
        :param spectral_index: spectral index for k-correction
        :return: log radio luminosity in W Hz-1
        """

        # get redshift
        if (redshift is None) & ((luminosity_distance is None) | (k_correction is False)):
            redshift = self.get_redshift()

        if luminosity_distance is None:
            # we calculate the log luminosity distance in m:
            log_lum_dist = np.log10(self.cosmology.luminosity_distance(redshift).to(u.m).value)
        else:
            log_lum_dist = np.log10((luminosity_distance * u.Mpc).to(u.m).value)

        # the flux is units of Jy this transfers to
        # with the transfer 1 Jy = 10^-26 W m^-2 Hz^-1
        log_band_flux = np.log10(self.get_radio_continuum_flux(observation=observation, flux_estimation=flux_estimation,
                                                               unit_system='Jy'))

        if k_correction:
            # calculate the radio luminosity
            # see Condon et al. 2019 eq. 23 doi.org/10.3847/1538-4357/ab0301
            # The k-corection is also described in https://en.wikipedia.org/wiki/Luminosity
            radio_luminosity = (log_band_flux +
                                np.log10(1e-26 * 4 * np.pi) +
                                2 * log_lum_dist -
                                (1 + spectral_index) * np.log10(1 + redshift))
        else:
            # calculate the radio luminosity
            radio_luminosity = (log_band_flux +
                                np.log10(1e-26 * 4 * np.pi) +
                                2 * log_lum_dist)

        return radio_luminosity

    def get_radio_luminosity_err(self, observation, redshift=None, luminosity_distance=None, flux_estimation='int',
                                 k_correction=False, spectral_index=-0.7):
        r"""
        Function to estimate the radio luminosity error. 

        :param observation: vlass, first, nvss or lofar
        :param redshift: redshift. otherwise accessed through attribute
        :param luminosity_distance: luminosity_distance in Mpc. Otherwise calculated with redshift.
        :param flux_estimation: peak for peak flux or int for integrated flux
        :param k_correction: flag if k-correction should be applied
        :param spectral_index: spectral index for k-correction
        :return: log radio luminosity error in W Hz-1
        """

        # get redshift
        if (redshift is None) & ((luminosity_distance is None) | (k_correction is False)):
            redshift = self.get_redshift()

        if luminosity_distance is None:
            # we calculate the log luminosity distance in m:
            log_lum_dist = np.log10(self.cosmology.luminosity_distance(redshift).to(u.m).value)
        else:
            log_lum_dist = np.log10((luminosity_distance * u.Mpc).to(u.m).value)

        # the flux is units of Jy this transfers to
        # with the transfer 1 Jy = 10^-26 W m^-2 Hz^-1
        band_flux = self.get_radio_continuum_flux(observation=observation, flux_estimation=flux_estimation,
                                                               unit_system='Jy')
        band_flux_err = self.get_radio_continuum_flux_err(observation=observation, flux_estimation=flux_estimation,
                                                               unit_system='Jy')

        radio_luminosity_err = np.log10(np.e) * band_flux_err / band_flux

        return radio_luminosity_err

    def get_vla_lum(self, redshift=None, luminosity_distance=None, flux_estimation='int', k_correction=False,
                    spectral_index=-0.7):
        r"""
        this function uses the FIRST and NVSS catalog to estimate the flux at 1.4 GHz.
        We prefer the FIRST catalog since it is deeper.
        parameters like get_radio_luminosity()
        """
        radio_vla_lum = np.zeros(len(self.table)) * np.nan
        radio_vla_lum_first = self.get_radio_luminosity(observation='first', redshift=redshift,
                                                        luminosity_distance=luminosity_distance,
                                                        flux_estimation=flux_estimation, k_correction=k_correction,
                                                        spectral_index=spectral_index)
        radio_vla_lum_nvss = self.get_radio_luminosity(observation='nvss', redshift=redshift,
                                                       luminosity_distance=luminosity_distance,
                                                       flux_estimation=flux_estimation, k_correction=k_correction,
                                                       spectral_index=spectral_index)

        reasonable_first_values = np.invert(np.isnan(radio_vla_lum_first))
        reasonable_nvss_values = np.invert(np.isnan(radio_vla_lum_nvss))

        radio_vla_lum[reasonable_first_values] = radio_vla_lum_first[reasonable_first_values]

        radio_vla_lum[reasonable_nvss_values * ~reasonable_first_values] = radio_vla_lum_nvss[reasonable_nvss_values *
                                                                                              ~reasonable_first_values]

        return radio_vla_lum

    def get_vla_lum_err(self, redshift=None, luminosity_distance=None, flux_estimation='int', k_correction=False,
                    spectral_index=-0.7):
        r"""
        this function uses the FIRST and NVSS catalog to estimate the flux at 1.4 GHz.
        We prefer the FIRST catalog since it is deeper.
        parameters like get_radio_luminosity()
        """
        radio_vla_lum_err = np.zeros(len(self.table)) * np.nan
        radio_vla_lum_err_first = self.get_radio_luminosity_err(observation='first', redshift=redshift,
                                                        luminosity_distance=luminosity_distance,
                                                        flux_estimation=flux_estimation, k_correction=k_correction,
                                                        spectral_index=spectral_index)
        radio_vla_lum_err_nvss = self.get_radio_luminosity_err(observation='nvss', redshift=redshift,
                                                       luminosity_distance=luminosity_distance,
                                                       flux_estimation=flux_estimation, k_correction=k_correction,
                                                       spectral_index=spectral_index)

        reasonable_first_values = np.invert(np.isnan(radio_vla_lum_err_first))
        reasonable_nvss_values = np.invert(np.isnan(radio_vla_lum_err_nvss))

        radio_vla_lum_err[reasonable_first_values] = radio_vla_lum_err_first[reasonable_first_values]

        radio_vla_lum_err[reasonable_nvss_values * ~reasonable_first_values] = radio_vla_lum_err_nvss[reasonable_nvss_values *
                                                                                              ~reasonable_first_values]

        return radio_vla_lum_err

    def get_line_luminosity(self, line_wavelength, line_shape='gauss'):
        r"""
        Access emission-line luminosities

        :param line_wavelength: line of specific line in angstrom
        :type line_wavelength: int
        :param line_shape: Specified the emission-line fit. For the RCSED catalogue (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
        For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
        ('peak_1') or red-shifted component ('peak_2').
        For MaNGA data, this refers to the result given by the evolution fit procedure:
        peak 1 (resp. 2) for the blueshifted (redshifted) component.
        :type line_shape: str

        """
        log_flux = np.log10(self.get_emission_line_flux(line_wavelength=line_wavelength, line_shape=line_shape))

        # calculate the luminosity distance and convert it into cm since the unity of flux is 10^-17erg/cm^2/s
        log_lum_dist = np.log10(self.cosmology.luminosity_distance(self.get_redshift()).to(u.cm).value)
        return log_flux + np.log10(1e-17 * 4 * np.pi) + 2 * log_lum_dist

    def get_line_luminosity_err(self, line_wavelength, line_shape='gauss'):
        """
        Access uncertainty of emission-line luminosities
        :param line_wavelength:
        :param line_shape:
        :return:
        """
        log_flux_err = np.log10(self.get_emission_line_flux_err(line_wavelength=line_wavelength, line_shape=line_shape))

        log_lum_dist = np.log10(self.cosmology.luminosity_distance(self.get_redshift()).value * 3.0857 * 1e24)

        log_lineluminosity_err = log_flux_err + np.log10(1e-17 * 4 * np.pi) + 2 * log_lum_dist

        return log_lineluminosity_err

    def get_B_V_colour_from_g_r(self, region=None):
        r"""
        Compute B-V colour from g-r given in RCSED
        Following http://classic.sdss.org/dr7/algorithms/sdssUBVRITransform.html 
        Those transformation equations are "for stars [but] should also provide reasonable results for normal galaxies (i.e., galaxies without strong emission lines)."
        B = g + 0.3130*(g - r) + 0.2271;
        V = g - 0.5784*(g - r) - 0.0038; so: B-V = 0.8914*(g - r) + 0.2309
        :param region: either 'total' or 'fibre', to compute the colour for the whole galaxy or within the fibre, respectively
        :type region: str
        """
        if region == 'total':
            g_r_colour_total = self.get_rcsed_band_mag(band='g', k_correction=True) - self.get_rcsed_band_mag(band='r', k_correction=True)
            B_V_colour_total = 0.8914 * g_r_colour_total + 0.2309
            return B_V_colour_total
        elif region == 'fibre':
            g_r_colour_fibre = self.get_rcsed_fib_band_mag(band='g') - self.get_rcsed_fib_band_mag(band='r')
            B_V_colour_fibre = 0.8914 * g_r_colour_fibre + 0.2309
            return B_V_colour_fibre

    def get_B_V_colour_err_from_g_r(self, region=None):
        r"""
        Compute error on B-V colour, from (g-r)err given in RCSED
        :param region: either 'total' or 'fibre', to compute the colour error for the whole galaxy or within the fibre, respectively
        :type region: str
        """
        if region == 'total':
            g_r_colour_err = np.sqrt( self.get_rcsed_band_mag_err(band='g')**2 + self.get_rcsed_band_mag_err(band='r')**2 )
            B_V_colour_total_err = 0.8914 * g_r_colour_err
            return B_V_colour_total_err

        elif region == 'fibre':
            g_r_fibre_err = np.sqrt( self.get_rcsed_fib_band_mag_err(band='g')**2 + self.get_rcsed_fib_band_mag_err(band='r')**2 )
            B_V_colour_fibre_err = 0.8914 * g_r_fibre_err
            return B_V_colour_fibre_err

class EnvironmentTools(data_access.DataAccess):

    def __init__(self, **kwargs):
        r"""

        """
        super().__init__(**kwargs)

    def get_environment_saulder(self, manga_dr15=False, manga_dr17=False):

        # access to galaxy table of saulder+16
        saulder_access = data_access.DataAccess(data_path='data', object_type='saulder_2016')

        ra_saulder = saulder_access.get_ra()
        dec_saulder = saulder_access.get_dec()

        ra = self.get_ra()
        dec = self.get_dec()

        redshift = self.get_redshift(manga_dr15, manga_dr17)

        group_id = self.table['iGrID']
        group_id_saulder = saulder_access.table['iGrID']

        galaxy_id = self.table['iGalID']
        galaxy_id_saulder = saulder_access.table['iGalID']

        number_of_neighbours = np.zeros(len(self.table))
        distance_closest_neighbour = np.zeros(len(self.table))
        most_massive_flag = np.zeros(len(self.table))

        for index in range(0, len(self.table)):
            # detect galaxies with the same group id
            # if the galaxy is not covered by Saulder+16 the galaxy id is -2147483648
            if group_id[index] not in group_id_saulder:
                # this galaxy is not covered by Saulder et al. 2016 (probably the galaxy has a redshift > 0.11)
                number_of_neighbours[index] = -1
                distance_closest_neighbour[index] = -1
                most_massive_flag[index] = -1
            elif np.sum(group_id_saulder == group_id[index]) == 1:
                # the galaxy is isolated
                number_of_neighbours[index] = 0
                distance_closest_neighbour[index] = -1
                most_massive_flag[index] = 1
            else:
                # get only galaxies which have the same group id but a different galaxy id
                galaxy_indices = np.where((group_id_saulder == group_id[index]) &
                                          (galaxy_id_saulder != galaxy_id[index]))[0]

                # calculate number of neighbors
                number_of_neighbours[index] = len(galaxy_indices)

                # get distance to all galaxies
                separation_degree = np.sqrt((ra[index] - ra_saulder[galaxy_indices]) ** 2 +
                                            (dec[index] - dec_saulder[galaxy_indices]) ** 2)
                separation_kpc = self.degree_to_comoving_distance(degree=separation_degree, z=redshift[index])
                distance_closest_neighbour[index] = min(separation_kpc)

                # check if the the galaxy is the most massive
                if (max(saulder_access.table['logMstar_saulder'][galaxy_indices]) >
                        self.table['logMstar_saulder'][index]):
                    most_massive_flag[index] = 0
                else:
                    most_massive_flag[index] = 1

        return number_of_neighbours, distance_closest_neighbour, most_massive_flag

    def get_environment_yang(self, manga_dr15=False, manga_dr17=False):

        # We used the galaxy group catalogue compiled on the SDSS DR 7  available
        # under: https://gax.sjtu.edu.cn/data/Group.html
        # this is relatied to Yang et al. 2007

        # access to galaxy table of saulder+16
        yang_access = data_access.DataAccess(data_path='data', object_type='yang_2007')

        ra_yang = yang_access.get_ra()
        dec_yang = yang_access.get_dec()

        ra = self.get_ra()
        dec = self.get_dec()

        redshift = self.get_redshift(manga_dr15, manga_dr17)

        group_id = self.table['group_id']
        group_id_yang = yang_access.table['group_id']

        galaxy_id = self.table['galaxy_id']
        galaxy_id_yang = yang_access.table['galaxy_id']

        number_of_neighbours = np.zeros(len(self.table))
        distance_closest_neighbour = np.zeros(len(self.table))
        most_massive_flag = np.zeros(len(self.table))

        for index in range(0, len(self.table)):
            # detect galaxies with the same group id
            # if the galaxy is not covered by Saulder+16 the galaxy id is -2147483648
            if group_id[index] not in group_id_yang:

                # this galaxy is not covered by Saulder et al. 2016 (probably the galaxy has a redshift > 0.11)
                number_of_neighbours[index] = -1
                distance_closest_neighbour[index] = -1
                most_massive_flag[index] = -1
            elif np.sum(group_id_yang == group_id[index]) == 1:
                # the galaxy is isolated
                number_of_neighbours[index] = 0
                distance_closest_neighbour[index] = -1
                most_massive_flag[index] = 1
            else:

                # get only galaxies which have the same group id but a different galaxy id
                galaxy_indices = np.where((group_id_yang == group_id[index]) &
                                          (galaxy_id_yang != galaxy_id[index]))[0]

                # calculate number of neighbors
                number_of_neighbours[index] = len(galaxy_indices)

                # get distance to all galaxies
                separation_degree = np.sqrt((ra[index] - ra_yang[galaxy_indices]) ** 2 +
                                            (dec[index] - dec_yang[galaxy_indices]) ** 2)
                separation_kpc = self.degree_to_comoving_distance(degree=separation_degree, z=redshift[index])
                distance_closest_neighbour[index] = min(separation_kpc)

                # check if the the galaxy is the most massive
                if self.table['most_massive'][index] == 2:
                    most_massive_flag[index] = 0
                else:
                    most_massive_flag[index] = 1

        return number_of_neighbours, distance_closest_neighbour, most_massive_flag

    def get_environment(self, manga_dr17=False):
        r"""
        We use Group size definitions taken from  Blanton & Moustakas (2009):
        poor group holds 2 to 4 members,
        rich group 5 to 9
        cluster more than 10.

        :return: dict with all environment information and group size masks
        """
        if (self.object_type == 'single_object') | (self.object_type is None):
            number_of_neighbours_saulder, distance_closest_neighbour_saulder, most_massive_flag_saulder = \
                self.get_environment_saulder()
            not_in_catalog_saulder = number_of_neighbours_saulder == -1
            isolated_saulder = number_of_neighbours_saulder == 0
            poor_group_saulder = (number_of_neighbours_saulder > 0) & (number_of_neighbours_saulder < 4)
            rich_group_saulder = (number_of_neighbours_saulder >= 4) & (number_of_neighbours_saulder < 9)
            cluster_saulder = number_of_neighbours_saulder >= 9

            number_of_neighbours_yang, distance_closest_neighbour_yang, most_massive_flag_yang = \
                self.get_environment_yang()
            not_in_catalog_yang = number_of_neighbours_yang == -1
            isolated_yang = number_of_neighbours_yang == 0
            poor_group_yang = (number_of_neighbours_yang > 0) & (number_of_neighbours_yang < 4)
            rich_group_yang = (number_of_neighbours_yang >= 4) & (number_of_neighbours_yang < 9)
            cluster_yang = number_of_neighbours_yang >= 9

            environment_dict = {'number_of_neighbours_saulder': number_of_neighbours_saulder,
                                'distance_closest_neighbour_saulder': distance_closest_neighbour_saulder,
                                'most_massive_flag_saulder': most_massive_flag_saulder,
                                'not_in_catalog_saulder': not_in_catalog_saulder,
                                'isolated_saulder': isolated_saulder,
                                'poor_group_saulder': poor_group_saulder,
                                'rich_group_saulder': rich_group_saulder,
                                'cluster_saulder': cluster_saulder,
                                'number_of_neighbours_yang': number_of_neighbours_yang,
                                'distance_closest_neighbour_yang': distance_closest_neighbour_yang,
                                'most_massive_flag_yang': most_massive_flag_yang,
                                'not_in_catalog_yang': not_in_catalog_yang,
                                'isolated_yang': isolated_yang,
                                'poor_group_yang': poor_group_yang,
                                'rich_group_yang': rich_group_yang,
                                'cluster_yang': cluster_yang}
            return environment_dict
        else:
            subset_path = self.get_subset_path(table_name=self.object_type)
            if os.path.isfile(subset_path / 'environment' / 'environment_dict.npy'):
                environment_dict = np.load(subset_path / 'environment' / 'environment_dict.npy',
                                           allow_pickle=True).item()
                return environment_dict
            else:
                if not os.path.isdir(subset_path / 'environment'):
                    os.makedirs(subset_path / 'environment')

                number_of_neighbours_saulder, distance_closest_neighbour_saulder, most_massive_flag_saulder = \
                    self.get_environment_saulder(manga_dr17)
                not_in_catalog_saulder = number_of_neighbours_saulder == -1
                isolated_saulder = number_of_neighbours_saulder == 0
                poor_group_saulder = (number_of_neighbours_saulder > 0) & (number_of_neighbours_saulder < 4)
                rich_group_saulder = (number_of_neighbours_saulder >= 4) & (number_of_neighbours_saulder < 9)
                cluster_saulder = number_of_neighbours_saulder >= 9

                number_of_neighbours_yang, distance_closest_neighbour_yang, most_massive_flag_yang = \
                    self.get_environment_yang(manga_dr17)
                not_in_catalog_yang = number_of_neighbours_yang == -1
                isolated_yang = number_of_neighbours_yang == 0
                poor_group_yang = (number_of_neighbours_yang > 0) & (number_of_neighbours_yang < 4)
                rich_group_yang = (number_of_neighbours_yang >= 4) & (number_of_neighbours_yang < 9)
                cluster_yang = number_of_neighbours_yang >= 9
                environment_dict = {'number_of_neighbours_saulder': number_of_neighbours_saulder,
                                    'distance_closest_neighbour_saulder': distance_closest_neighbour_saulder,
                                    'most_massive_flag_saulder': most_massive_flag_saulder,
                                    'not_in_catalog_saulder': not_in_catalog_saulder,
                                    'isolated_saulder': isolated_saulder,
                                    'poor_group_saulder': poor_group_saulder,
                                    'rich_group_saulder': rich_group_saulder,
                                    'cluster_saulder': cluster_saulder,
                                    'number_of_neighbours_yang': number_of_neighbours_yang,
                                    'distance_closest_neighbour_yang': distance_closest_neighbour_yang,
                                    'most_massive_flag_yang': most_massive_flag_yang,
                                    'not_in_catalog_yang': not_in_catalog_yang,
                                    'isolated_yang': isolated_yang,
                                    'poor_group_yang': poor_group_yang,
                                    'rich_group_yang': rich_group_yang,
                                    'cluster_yang': cluster_yang}
                np.save(subset_path / 'environment' / 'environment_dict.npy', environment_dict)
                return environment_dict


class BPTClassification(data_access.DataAccess):

    def __init__(self, **kwargs):
        r"""

        """
        super().__init__(**kwargs)

    def get_simple_bpt_classification(self, line_shape='gauss'):
        from xgaltool import bpt_classify

        bpt_classification = np.zeros((len(self.table), 3), dtype='S20')
        bpt_classification[:, 0] = bpt_classify.BPTCategorisation.classify_first_bpt(self.table, dp=False,
                                                                                     line_shape=line_shape)
        bpt_classification[:, 1] = bpt_classify.BPTCategorisation.classify_second_bpt(self.table, dp=False,
                                                                                      line_shape=line_shape)
        bpt_classification[:, 2] = bpt_classify.BPTCategorisation.classify_third_bpt(self.table, dp=False,
                                                                                     line_shape=line_shape)

        # classification with uncertainty consideration
        bpt_classification_uncertainty = np.zeros((len(self.table), 4), dtype='S20')
        # for index in range(0, len(self.table)):
        bpt_classification_uncertainty = bpt_classify.BPTCategorisation.classify_uncertainty_first_bpt(self.table,
                                                                                                       dp=False,
                                                                                                       line_shape=line_shape)

        # print(bpt_classification_uncertainty.shape)
        #
        # exit()

        snr_first_bpt = bpt_classify.BPTFilter.snr_first_bpt(self.table, dp=False, line_shape='gauss')
        snr_second_bpt = bpt_classify.BPTFilter.snr_second_bpt(self.table, dp=False, line_shape='gauss')
        snr_third_bpt = bpt_classify.BPTFilter.snr_third_bpt(self.table, dp=False, line_shape='gauss')

        sf_four_lines = ((bpt_classification[:, 0] == b'sf') * snr_first_bpt +
                         (bpt_classification[:, 1] == b'sf') * snr_second_bpt * ~snr_first_bpt +
                         (bpt_classification[:, 2] == b'sf') * snr_third_bpt * ~snr_second_bpt * ~snr_first_bpt)

        comp_four_lines = ((bpt_classification[:, 0] == b'comp') * snr_first_bpt)

        agn_four_lines = ((bpt_classification[:, 0] == b'agn') * snr_first_bpt +
                          (bpt_classification[:, 1] == b'agn') * snr_second_bpt * ~snr_first_bpt +
                          (bpt_classification[:, 2] == b'agn') * snr_third_bpt * ~snr_second_bpt * ~snr_first_bpt)

        liner_four_lines = ((bpt_classification[:, 0] == b'liner') * snr_first_bpt +
                            (bpt_classification[:, 1] == b'liner') * snr_second_bpt * ~snr_first_bpt +
                            (bpt_classification[:, 2] == b'liner') * snr_third_bpt * ~snr_second_bpt * ~snr_first_bpt)

        classified_sample_four_lines = snr_first_bpt + snr_second_bpt + snr_third_bpt
        not_classifiable_four_lines = np.invert(classified_sample_four_lines)

        # classify with 3 lines
        snr_flag_h_beta = bpt_classify.BPTFilter.snr_flag_h_beta(self.table, dp=False)
        snr_flag_oiii = bpt_classify.BPTFilter.snr_flag_oiii(self.table, dp=False)
        # snr_flag_oi = bpt_classify.BPTFilter.snr_flag_oi(self.table, dp=False)
        snr_flag_h_alpha = bpt_classify.BPTFilter.snr_flag_h_alpha(self.table, dp=False)
        # snr_flag_sii = bpt_classify.BPTFilter.snr_flag_sii(self.table, dp=False)
        snr_flag_nii = bpt_classify.BPTFilter.snr_flag_nii(self.table, dp=False)

        classify_no_oiii = ~snr_flag_oiii * snr_flag_h_beta * snr_flag_h_alpha * snr_flag_nii
        classify_no_h_beta = snr_flag_oiii * ~snr_flag_h_beta * snr_flag_h_alpha * snr_flag_nii
        classify_no_h_alpha = snr_flag_oiii * snr_flag_h_beta * ~snr_flag_h_alpha * snr_flag_nii
        classify_no_nii = snr_flag_oiii * snr_flag_h_beta * snr_flag_h_alpha * ~snr_flag_nii

        sf_three_lines = (((bpt_classification_uncertainty[0] == b'sf') *
                           classify_no_oiii * not_classifiable_four_lines) +
                          ((bpt_classification_uncertainty[2] == b'sf') *
                           classify_no_nii * not_classifiable_four_lines))

        # sf_comp_three_lines = (((bpt_classification_uncertainty[:, 0] == b'comp') *
        #                           classify_no_oiii * not_classifiable_four_lines) +
        #                          ((bpt_classification_uncertainty[:, 2] == b'comp') *
        #                           classify_no_nii * not_classifiable_four_lines))

        agn_three_lines = (((bpt_classification_uncertainty[1] == b'agn') *
                            classify_no_h_beta * not_classifiable_four_lines))

        # agn_comp_three_lines = (((bpt_classification_uncertainty[:, 1] == b'comp') *
        #                            classify_no_h_beta * not_classifiable_four_lines))
        #
        # agn_liner_three_lines = (((bpt_classification_uncertainty[:, 1] == b'liner') *
        #                            classify_no_h_beta * not_classifiable_four_lines))

        liner_three_lines = ((bpt_classification_uncertainty[3] == b'liner') *
                             classify_no_h_alpha * not_classifiable_four_lines)
        #
        # liner_comp_three_lines = ((bpt_classification_uncertainty[:, 3] == b'comp') *
        #                            classify_no_h_alpha * not_classifiable_four_lines)
        # liner_agn_three_lines = ((bpt_classification_uncertainty[:, 3] == b'agn') *
        #                            classify_no_h_alpha * not_classifiable_four_lines)

        sf = sf_four_lines + sf_three_lines
        comp = comp_four_lines
        agn = agn_four_lines + agn_three_lines
        liner = liner_four_lines + liner_three_lines

        classified_sample = (sf + comp + agn + liner)

        not_bpt_classified = np.invert(classified_sample)

        print('SF & %i & (%.1f \%%)   \\\\' % (sum(sf), sum(sf) / len(self.table) * 100))
        print('COMP & %i  & (%.1f \%%)   \\\\' % (sum(comp), sum(comp) / len(self.table) * 100))
        print('AGN & %i  & (%.1f \%%)   \\\\' % (sum(agn), sum(agn) / len(self.table) * 100))
        print('LINER & %i  & (%.1f \%%)  \\\\' % (sum(liner), sum(liner) / len(self.table) * 100))
        print('Classified Galaxies & %i  & (%.1f \%%)  \\\\' % (
            sum(classified_sample), sum(classified_sample) / len(self.table) * 100))
        print('Not classifiable Galaxies & %i  & (%.1f \%%)  \\\\' % (
            sum(not_bpt_classified), sum(not_bpt_classified) / len(self.table) * 100))

        bpt_classification_dict = {'bpt_classification_%s' % line_shape: bpt_classification,
                                   'bpt_classification_uncertainty_%s' % line_shape: bpt_classification_uncertainty,
                                   'sf_%s' % line_shape: sf,
                                   'comp_%s' % line_shape: comp,
                                   'agn_%s' % line_shape: agn,
                                   'liner_%s' % line_shape: liner,
                                   'classified_sample_%s' % line_shape: classified_sample,
                                   'not_bpt_classified_%s' % line_shape: not_bpt_classified}

        return bpt_classification_dict

    def get_double_peak_bpt_classification(self):
        from xgaltool import bpt_classify

        # general classification
        classification = np.zeros((3, 2, len(self.table)), dtype='S20')
        # for index in range(0, len(self.table)):

        classification[0] = bpt_classify.BPTCategorisation.classify_first_bpt(self.table)
        classification[1] = bpt_classify.BPTCategorisation.classify_second_bpt(self.table)
        classification[2] = bpt_classify.BPTCategorisation.classify_third_bpt(self.table)

        print(classification.shape)

        # classification with uncertainty consideration
        classification_uncertainty = np.zeros((3, 4, 2, len(self.table)), dtype='S20')

        classification_uncertainty[0] = bpt_classify.BPTCategorisation.classify_uncertainty_first_bpt(self.table)

        print(classification_uncertainty.shape)

        # snr filter
        snr_first_bpt_peak_1, snr_first_bpt_peak_2 = bpt_classify.BPTFilter.snr_first_bpt(self.table)
        snr_second_bpt_peak_1, snr_second_bpt_peak_2 = bpt_classify.BPTFilter.snr_second_bpt(self.table)
        snr_third_bpt_peak_1, snr_third_bpt_peak_2 = bpt_classify.BPTFilter.snr_third_bpt(self.table)

        not_classifiable_four_lines_peak_1 = np.invert(
            snr_first_bpt_peak_1 + snr_second_bpt_peak_1 + snr_third_bpt_peak_1)
        not_classifiable_four_lines_peak_2 = np.invert(
            snr_first_bpt_peak_2 + snr_second_bpt_peak_2 + snr_third_bpt_peak_2)

        sf_four_lines_peak_1 = (((classification[0, 0] == b'sf') * snr_first_bpt_peak_1) +
                                ((classification[1, 0] == b'sf') * snr_second_bpt_peak_1 * ~snr_first_bpt_peak_1) +
                                ((classification[
                                      2, 0] == b'sf') * snr_third_bpt_peak_1 * ~snr_first_bpt_peak_1 * ~snr_second_bpt_peak_1))

        sf_four_lines_peak_2 = (((classification[0, 1] == b'sf') * snr_first_bpt_peak_2) +
                                ((classification[1, 1] == b'sf') * snr_second_bpt_peak_2 * ~snr_first_bpt_peak_2) +
                                ((classification[
                                      2, 1] == b'sf') * snr_third_bpt_peak_2 * ~snr_first_bpt_peak_2 * ~snr_second_bpt_peak_2))

        comp_four_lines_peak_1 = ((classification[0, 0] == b'comp') * snr_first_bpt_peak_1)
        comp_four_lines_peak_2 = ((classification[0, 1] == b'comp') * snr_first_bpt_peak_2)

        agn_four_lines_peak_1 = (((classification[0, 0] == b'agn') * snr_first_bpt_peak_1) +
                                 ((classification[1, 0] == b'agn') * snr_second_bpt_peak_1 * ~snr_first_bpt_peak_1) +
                                 ((classification[
                                       2, 0] == b'agn') * snr_third_bpt_peak_1 * ~snr_first_bpt_peak_1 * ~snr_second_bpt_peak_1))

        agn_four_lines_peak_2 = (((classification[0, 1] == b'agn') * snr_first_bpt_peak_2) +
                                 ((classification[1, 1] == b'agn') * snr_second_bpt_peak_2 * ~snr_first_bpt_peak_2) +
                                 ((classification[
                                       2, 1] == b'agn') * snr_third_bpt_peak_2 * ~snr_first_bpt_peak_2 * ~snr_second_bpt_peak_2))

        liner_four_lines_peak_1 = (((classification[0, 0] == b'liner') * snr_first_bpt_peak_1) +
                                   ((classification[
                                         1, 0] == b'liner') * snr_second_bpt_peak_1 * ~snr_first_bpt_peak_1) +
                                   ((classification[
                                         2, 0] == b'liner') * snr_third_bpt_peak_1 * ~snr_first_bpt_peak_1 * ~snr_second_bpt_peak_1))

        liner_four_lines_peak_2 = (((classification[0, 1] == b'liner') * snr_first_bpt_peak_2) +
                                   ((classification[
                                         1, 1] == b'liner') * snr_second_bpt_peak_2 * ~snr_first_bpt_peak_2) +
                                   ((classification[
                                         2, 1] == b'liner') * snr_third_bpt_peak_2 * ~snr_first_bpt_peak_2 * ~snr_second_bpt_peak_2))

        # classified_four_lines_second_bpt_1 = snr_second_bpt_peak_1 * ~snr_first_bpt_peak_1
        # classified_four_lines_second_bpt_2 = snr_second_bpt_peak_2 * ~snr_first_bpt_peak_2
        #
        # classified_four_lines_third_bpt_1 = snr_third_bpt_peak_1 * ~snr_first_bpt_peak_1 * ~snr_second_bpt_peak_1
        # classified_four_lines_third_bpt_2 = snr_third_bpt_peak_2 * ~snr_first_bpt_peak_2 * ~snr_second_bpt_peak_2

        # classify with correction
        # snr flags for individual lines
        snr_flag_h_beta_peak_1 = bpt_classify.BPTFilter.snr_flag_h_beta(self.table, peak=1)
        snr_flag_oiii_peak_1 = bpt_classify.BPTFilter.snr_flag_oiii(self.table, peak=1)
        # snr_flag_oi_peak_1 = bpt_classify.BPTFilter.snr_flag_oi(self.table, peak=1)
        snr_flag_h_alpha_peak_1 = bpt_classify.BPTFilter.snr_flag_h_alpha(self.table, peak=1)
        # snr_flag_sii_peak_1 = bpt_classify.BPTFilter.snr_flag_sii(self.table, peak=1)
        snr_flag_nii_peak_1 = bpt_classify.BPTFilter.snr_flag_nii(self.table, peak=1)

        snr_flag_h_beta_peak_2 = bpt_classify.BPTFilter.snr_flag_h_beta(self.table, peak=2)
        snr_flag_oiii_peak_2 = bpt_classify.BPTFilter.snr_flag_oiii(self.table, peak=2)
        # snr_flag_oi_peak_2 = bpt_classify.BPTFilter.snr_flag_oi(self.table, peak=2)
        snr_flag_h_alpha_peak_2 = bpt_classify.BPTFilter.snr_flag_h_alpha(self.table, peak=2)
        # snr_flag_sii_peak_2 = bpt_classify.BPTFilter.snr_flag_sii(self.table, peak=2)
        snr_flag_nii_peak_2 = bpt_classify.BPTFilter.snr_flag_nii(self.table, peak=2)

        classify_no_oiii_1 = ~snr_flag_oiii_peak_1 * snr_flag_h_beta_peak_1 * snr_flag_h_alpha_peak_1 * snr_flag_nii_peak_1
        classify_no_h_beta_1 = snr_flag_oiii_peak_1 * ~snr_flag_h_beta_peak_1 * snr_flag_h_alpha_peak_1 * snr_flag_nii_peak_1
        classify_no_h_alpha_1 = snr_flag_oiii_peak_1 * snr_flag_h_beta_peak_1 * ~snr_flag_h_alpha_peak_1 * snr_flag_nii_peak_1
        classify_no_nii_1 = snr_flag_oiii_peak_1 * snr_flag_h_beta_peak_1 * snr_flag_h_alpha_peak_1 * ~snr_flag_nii_peak_1

        classify_no_oiii_2 = ~snr_flag_oiii_peak_2 * snr_flag_h_beta_peak_2 * snr_flag_h_alpha_peak_2 * snr_flag_nii_peak_2
        classify_no_h_beta_2 = snr_flag_oiii_peak_2 * ~snr_flag_h_beta_peak_2 * snr_flag_h_alpha_peak_2 * snr_flag_nii_peak_2
        classify_no_h_alpha_2 = snr_flag_oiii_peak_2 * snr_flag_h_beta_peak_2 * ~snr_flag_h_alpha_peak_2 * snr_flag_nii_peak_2
        classify_no_nii_2 = snr_flag_oiii_peak_2 * snr_flag_h_beta_peak_2 * snr_flag_h_alpha_peak_2 * ~snr_flag_nii_peak_2

        sf_three_lines_peak_1 = (((classification_uncertainty[0, 0, 0] == b'sf') *
                                  classify_no_oiii_1 * not_classifiable_four_lines_peak_1) +
                                 ((classification_uncertainty[0, 2, 0] == b'sf') *
                                  classify_no_nii_1 * not_classifiable_four_lines_peak_1))

        sf_three_lines_peak_2 = (((classification_uncertainty[0, 0, 1] == b'sf') *
                                  classify_no_oiii_2 * not_classifiable_four_lines_peak_2) +
                                 ((classification_uncertainty[0, 2, 1] == b'sf') *
                                  classify_no_nii_2 * not_classifiable_four_lines_peak_2))

        # comp_three_lines_peak_1 = (((classification_uncertainty[0, 0, 0] == b'comp') *
        #                           classify_no_oiii_1 * not_classifiable_four_lines_peak_1))
        #
        # comp_three_lines_peak_2 = (((classification_uncertainty[0, 0, 1] == b'comp') *
        #                               classify_no_oiii_2 * not_classifiable_four_lines_peak_2))
        #
        #
        # sf_comp_three_lines_peak_1 = (((classification_uncertainty[0, 0, 0] == b'comp') *
        #                           classify_no_oiii_1 * not_classifiable_four_lines_peak_1) +
        #                          ((classification_uncertainty[0, 2, 0] == b'comp') *
        #                           classify_no_nii_1 * not_classifiable_four_lines_peak_1))
        #
        # sf_comp_three_lines_peak_2 = (((classification_uncertainty[0, 0, 1] == b'comp') *
        #                               classify_no_oiii_2 * not_classifiable_four_lines_peak_2) +
        #                          ((classification_uncertainty[0, 2, 1] == b'comp') *
        #                           classify_no_nii_2 * not_classifiable_four_lines_peak_2))

        agn_three_lines_peak_1 = (((classification_uncertainty[0, 1, 0] == b'agn') *
                                   classify_no_h_beta_1 * not_classifiable_four_lines_peak_1))

        agn_three_lines_peak_2 = (((classification_uncertainty[0, 1, 1] == b'agn') *
                                   classify_no_h_beta_2 * not_classifiable_four_lines_peak_2))

        # agn_comp_three_lines_peak_1 = (((classification_uncertainty[0, 1, 0] == b'comp') *
        #                            classify_no_h_beta_1 * not_classifiable_four_lines_peak_1))
        #
        # agn_comp_three_lines_peak_2 = (((classification_uncertainty[0, 1, 1] == b'comp') *
        #                            classify_no_h_beta_2 * not_classifiable_four_lines_peak_2))
        #
        #
        # agn_liner_three_lines_peak_1 = (((classification_uncertainty[0, 1, 0] == b'liner') *
        #                            classify_no_h_beta_1 * not_classifiable_four_lines_peak_1))
        #
        # agn_liner_three_lines_peak_2 = (((classification_uncertainty[0, 1, 1] == b'liner') *
        #                            classify_no_h_beta_2 * not_classifiable_four_lines_peak_2))

        liner_three_lines_peak_1 = ((classification_uncertainty[0, 3, 0] == b'liner') *
                                    classify_no_h_alpha_1 * not_classifiable_four_lines_peak_1)

        liner_three_lines_peak_2 = ((classification_uncertainty[0, 3, 1] == b'liner') *
                                    classify_no_h_alpha_2 * not_classifiable_four_lines_peak_2)

        #
        # liner_comp_three_lines_peak_1 = ((classification_uncertainty[0, 3, 0] == b'comp') *
        #                            classify_no_h_alpha_1 * not_classifiable_four_lines_peak_1)
        #
        # liner_comp_three_lines_peak_2 = ((classification_uncertainty[0, 3, 1] == b'comp') *
        #                             classify_no_h_alpha_2 * not_classifiable_four_lines_peak_2)
        #
        #
        # liner_agn_three_lines_peak_1 = ((classification_uncertainty[0, 3, 0] == b'agn') *
        #                            classify_no_h_alpha_1 * not_classifiable_four_lines_peak_1)
        #
        # liner_agn_three_lines_peak_2 = ((classification_uncertainty[0, 3, 1] == b'agn') *
        #                             classify_no_h_alpha_2 * not_classifiable_four_lines_peak_2)

        sf_peak_1 = sf_four_lines_peak_1 + sf_three_lines_peak_1
        sf_peak_2 = sf_four_lines_peak_2 + sf_three_lines_peak_2

        comp_peak_1 = comp_four_lines_peak_1  # + comp_three_lines_peak_1
        comp_peak_2 = comp_four_lines_peak_2  # + comp_three_lines_peak_2

        agn_peak_1 = agn_four_lines_peak_1 + agn_three_lines_peak_1
        agn_peak_2 = agn_four_lines_peak_2 + agn_three_lines_peak_2

        liner_peak_1 = liner_four_lines_peak_1 + liner_three_lines_peak_1
        liner_peak_2 = liner_four_lines_peak_2 + liner_three_lines_peak_2

        classification_list = np.zeros((len(self.table), 2), dtype='S20')

        classification_list[sf_peak_1, 0] = 'sf'
        classification_list[sf_peak_2, 1] = 'sf'

        classification_list[comp_peak_1, 0] = 'comp'
        classification_list[comp_peak_2, 1] = 'comp'

        classification_list[agn_peak_1, 0] = 'agn'
        classification_list[agn_peak_2, 1] = 'agn'

        classification_list[liner_peak_1, 0] = 'liner'
        classification_list[liner_peak_2, 1] = 'liner'

        double_sf = sf_peak_1 * sf_peak_2
        double_comp = comp_peak_1 * comp_peak_2
        double_agn = agn_peak_1 * agn_peak_2
        double_liner = liner_peak_1 * liner_peak_2

        sf_comp = (sf_peak_1 * comp_peak_2) + (sf_peak_2 * comp_peak_1)
        sf_agn = (sf_peak_1 * agn_peak_2) + (sf_peak_2 * agn_peak_1)
        sf_liner = (sf_peak_1 * liner_peak_2) + (sf_peak_2 * liner_peak_1)
        comp_agn = (comp_peak_1 * agn_peak_2) + (comp_peak_2 * agn_peak_1)
        comp_liner = (comp_peak_1 * liner_peak_2) + (comp_peak_2 * liner_peak_1)
        agn_liner = (agn_peak_1 * liner_peak_2) + (agn_peak_2 * liner_peak_1)

        # classify with only one peak
        two_classification = (double_sf + double_comp + double_agn + double_liner +
                              sf_comp + sf_agn + sf_liner + comp_agn + comp_liner + agn_liner)

        sf_uncertain = (sf_peak_1 * ~two_classification) + (sf_peak_2 * ~two_classification)
        comp_uncertain = (comp_peak_1 * ~two_classification) + (comp_peak_2 * ~two_classification)
        agn_uncertain = (agn_peak_1 * ~two_classification) + (agn_peak_2 * ~two_classification)
        liner_uncertain = (liner_peak_1 * ~two_classification) + (liner_peak_2 * ~two_classification)

        bpt_classified = two_classification + sf_uncertain + comp_uncertain + agn_uncertain + liner_uncertain

        not_bpt_classified = np.invert(bpt_classified)

        print('double_sf', sum(double_sf), '%.1f' % (sum(double_sf) / len(self.table) * 100))
        print('double_comp', sum(double_comp), '%.1f' % (sum(double_comp) / len(self.table) * 100))
        print('double_agn', sum(double_agn), '%.1f' % (sum(double_agn) / len(self.table) * 100))
        print('double_liner', sum(double_liner), '%.1f' % (sum(double_liner) / len(self.table) * 100))

        print('sf_comp', sum(sf_comp), '%.1f' % (sum(sf_comp) / len(self.table) * 100))
        print('sf_agn', sum(sf_agn), '%.1f' % (sum(sf_agn) / len(self.table) * 100))
        print('sf_liner', sum(sf_liner), '%.1f' % (sum(sf_liner) / len(self.table) * 100))
        print('comp_agn', sum(comp_agn), '%.1f' % (sum(comp_agn) / len(self.table) * 100))
        print('comp_liner', sum(comp_liner), '%.1f' % (sum(comp_liner) / len(self.table) * 100))
        print('agn_liner', sum(agn_liner), '%.1f' % (sum(agn_liner) / len(self.table) * 100))

        print('sf_uncertain', sum(sf_uncertain), '%.1f' % (sum(sf_uncertain) / len(self.table) * 100))
        print('comp_uncertain', sum(comp_uncertain), '%.1f' % (sum(comp_uncertain) / len(self.table) * 100))
        print('agn_uncertain', sum(agn_uncertain), '%.1f' % (sum(agn_uncertain) / len(self.table) * 100))
        print('liner_uncertain', sum(liner_uncertain), '%.1f' % (sum(liner_uncertain) / len(self.table) * 100))

        print('bpt_classified', sum(bpt_classified), '%.1f' % (sum(bpt_classified) / len(self.table) * 100))
        print('not_bpt_classified', sum(not_bpt_classified), '%.1f' % (sum(not_bpt_classified) / len(self.table) * 100))

        bpt_classification_dict_dp = {'classification_list_dp': classification_list,
                                      'classification_dp': classification,
                                      'classification_uncertainty_dp': classification_uncertainty,
                                      'double_sf': double_sf,
                                      'double_comp': double_comp,
                                      'double_agn': double_agn,
                                      'double_liner': double_liner,
                                      'sf_comp': sf_comp,
                                      'sf_agn': sf_agn,
                                      'sf_liner': sf_liner,
                                      'comp_agn': comp_agn,
                                      'comp_liner': comp_liner,
                                      'agn_liner': agn_liner,
                                      'sf_uncertain': sf_uncertain,
                                      'comp_uncertain': comp_uncertain,
                                      'agn_uncertain': agn_uncertain,
                                      'liner_uncertain': liner_uncertain,
                                      'bpt_classified': bpt_classified,
                                      'not_bpt_classified': not_bpt_classified,
                                      'snr_first_bpt_peak_1': snr_first_bpt_peak_1,
                                      'snr_first_bpt_peak_2': snr_first_bpt_peak_2,
                                      'snr_second_bpt_peak_1': snr_second_bpt_peak_1,
                                      'snr_second_bpt_peak_2': snr_second_bpt_peak_2,
                                      'snr_third_bpt_peak_1': snr_third_bpt_peak_1,
                                      'snr_third_bpt_peak_2': snr_third_bpt_peak_2}

        return bpt_classification_dict_dp

    def get_manga_double_peak_bpt_classification_new(self, number_of_gaussians):
        from galtool import bpt_classify

        if number_of_gaussians == 'single':
            fit_parameter_dict = self.get_manga_single_fit_parameter_dict()
            dp = False
        elif number_of_gaussians == 'double':
            fit_parameter_dict = self.get_manga_double_fit_parameter_dict()
            dp = True
        else:
            print('Number of Gaussian(s) not understood. Please specify single or double.')

        # general classification
        classification = np.zeros((3, 2, *fit_parameter_dict['chi2'].shape), dtype='S20')
        if number_of_gaussians == 'single':
            classification = np.zeros((3, *fit_parameter_dict['chi2'].shape), dtype='S20')
        classification[0] = bpt_classify.BPTCategorisation.classify_first_bpt(fit_parameter_dict, dp=dp, line_shape='manga_dp')
        classification[1] = bpt_classify.BPTCategorisation.classify_second_bpt(fit_parameter_dict,
                                                                               dp=dp, line_shape='manga_dp')
        classification[2] = bpt_classify.BPTCategorisation.classify_third_bpt(fit_parameter_dict, dp=dp, line_shape='manga_dp')

        # classification with uncertainty consideration
        classification_uncertainty = np.zeros((3, 4, 2, *fit_parameter_dict['chi2'].shape), dtype='S20')
        if number_of_gaussians == 'single':
            classification_uncertainty = np.zeros((3, 4, *fit_parameter_dict['chi2'].shape), dtype='S20')
        classification_uncertainty[0] = bpt_classify.BPTCategorisation.classify_uncertainty_first_bpt(
            fit_parameter_dict, dp=dp, line_shape='manga_dp')

        # snr filter
        if number_of_gaussians == 'single':
            snr_first_bpt_peak = bpt_classify.BPTFilter.snr_first_bpt(fit_parameter_dict, dp=dp, line_shape='manga_dp')
            snr_second_bpt_peak = bpt_classify.BPTFilter.snr_second_bpt(fit_parameter_dict, dp=dp, line_shape='manga_dp')
            snr_third_bpt_peak = bpt_classify.BPTFilter.snr_third_bpt(fit_parameter_dict, dp=dp, line_shape='manga_dp')

            not_classifiable_four_lines_peak = np.invert(snr_first_bpt_peak + snr_second_bpt_peak + snr_third_bpt_peak)
            not_classifiable_four_lines_peak = np.invert(snr_first_bpt_peak + snr_second_bpt_peak + snr_third_bpt_peak)

            sf_four_lines_peak = (((classification[0] == b'sf') * snr_first_bpt_peak) +
                                ((classification[1] == b'sf') * snr_second_bpt_peak * ~snr_first_bpt_peak) +
                                ((classification[2] == b'sf') * snr_third_bpt_peak * ~snr_first_bpt_peak * ~snr_second_bpt_peak))

            comp_four_lines_peak = ((classification[0] == b'comp') * snr_first_bpt_peak)

            agn_four_lines_peak = (((classification[0] == b'agn') * snr_first_bpt_peak) +
                                 ((classification[1] == b'agn') * snr_second_bpt_peak * ~snr_first_bpt_peak) +
                                 ((classification[2] == b'agn') * snr_third_bpt_peak * ~snr_first_bpt_peak * ~snr_second_bpt_peak))

            liner_four_lines_peak = (((classification[0] == b'liner') * snr_first_bpt_peak) +
                                   ((classification[1] == b'liner') * snr_second_bpt_peak * ~snr_first_bpt_peak) +
                                   ((classification[2] == b'liner') * snr_third_bpt_peak * ~snr_first_bpt_peak * ~snr_second_bpt_peak))

            snr_flag_h_beta_peak = bpt_classify.BPTFilter.snr_flag_h_beta(fit_parameter_dict, dp=False,
                                                                        line_shape='manga_dp')
            snr_flag_oiii_peak = bpt_classify.BPTFilter.snr_flag_oiii(fit_parameter_dict, dp=False, line_shape='manga_dp')
            snr_flag_h_alpha_peak = bpt_classify.BPTFilter.snr_flag_h_alpha(fit_parameter_dict, dp=False,
                                                                          line_shape='manga_dp')
            snr_flag_nii_peak = bpt_classify.BPTFilter.snr_flag_nii(fit_parameter_dict, dp=False, line_shape='manga_dp')

            classify_no_oiii = ~snr_flag_oiii_peak * snr_flag_h_beta_peak * snr_flag_h_alpha_peak * snr_flag_nii_peak
            classify_no_h_beta = snr_flag_oiii_peak * ~snr_flag_h_beta_peak * snr_flag_h_alpha_peak * snr_flag_nii_peak
            classify_no_h_alpha = snr_flag_oiii_peak * snr_flag_h_beta_peak * ~snr_flag_h_alpha_peak * snr_flag_nii_peak
            classify_no_nii = snr_flag_oiii_peak * snr_flag_h_beta_peak * snr_flag_h_alpha_peak * ~snr_flag_nii_peak

            sf_three_lines_peak = (((classification_uncertainty[0, 0] == b'sf') *
                                  classify_no_oiii * not_classifiable_four_lines_peak) +
                                 ((classification_uncertainty[0, 2] == b'sf') *
                                  classify_no_nii * not_classifiable_four_lines_peak))

            liner_three_lines_peak = ((classification_uncertainty[0, 3] == b'liner') * classify_no_h_alpha * not_classifiable_four_lines_peak)

            sf_peak = sf_four_lines_peak

            comp_peak = comp_four_lines_peak

            agn_peak = agn_four_lines_peak

            liner_peak = liner_four_lines_peak

            classification_map = np.zeros(fit_parameter_dict['chi2'].shape, dtype='S20')

            classification_map[sf_peak] = 'sf'

            classification_map[comp_peak] = 'comp'

            classification_map[agn_peak] = 'agn'

            classification_map[liner_peak] = 'liner'

            bpt_classified = sf_peak + comp_peak + agn_peak + liner_peak

            not_bpt_classified = np.invert(bpt_classified)

            bpt_classification_dict_sp = {'classification_map': classification_map,
                                      'classification': classification,
                                      'classification_uncertainty': classification_uncertainty,
                                      'bpt_classified': bpt_classified,
                                      'not_bpt_classified': not_bpt_classified}

        else:
            snr_first_bpt_peak_1, snr_first_bpt_peak_2 = bpt_classify.BPTFilter.snr_first_bpt(fit_parameter_dict,
                                                                                          dp=dp, line_shape='manga_dp')
            snr_second_bpt_peak_1, snr_second_bpt_peak_2 = bpt_classify.BPTFilter.snr_second_bpt(fit_parameter_dict,
                                                                                             line_shape='manga_dp')
            snr_third_bpt_peak_1, snr_third_bpt_peak_2 = bpt_classify.BPTFilter.snr_third_bpt(fit_parameter_dict,
                                                                                          dp=dp, line_shape='manga_dp')

            not_classifiable_four_lines_peak_1 = np.invert(snr_first_bpt_peak_1 + snr_second_bpt_peak_1 + snr_third_bpt_peak_1)
            not_classifiable_four_lines_peak_2 = np.invert(snr_first_bpt_peak_2 + snr_second_bpt_peak_2 + snr_third_bpt_peak_2)

            sf_four_lines_peak_1 = (((classification[0, 0] == b'sf') * snr_first_bpt_peak_1) +
                                ((classification[1, 0] == b'sf') * snr_second_bpt_peak_1 * ~snr_first_bpt_peak_1) +
                                ((classification[
                                      2, 0] == b'sf') * snr_third_bpt_peak_1 * ~snr_first_bpt_peak_1 * ~snr_second_bpt_peak_1))

            sf_four_lines_peak_2 = (((classification[0, 1] == b'sf') * snr_first_bpt_peak_2) +
                                ((classification[1, 1] == b'sf') * snr_second_bpt_peak_2 * ~snr_first_bpt_peak_2) +
                                ((classification[
                                      2, 1] == b'sf') * snr_third_bpt_peak_2 * ~snr_first_bpt_peak_2 * ~snr_second_bpt_peak_2))

            comp_four_lines_peak_1 = ((classification[0, 0] == b'comp') * snr_first_bpt_peak_1)
            comp_four_lines_peak_2 = ((classification[0, 1] == b'comp') * snr_first_bpt_peak_2)

            agn_four_lines_peak_1 = (((classification[0, 0] == b'agn') * snr_first_bpt_peak_1) +
                                 ((classification[1, 0] == b'agn') * snr_second_bpt_peak_1 * ~snr_first_bpt_peak_1) +
                                 ((classification[
                                       2, 0] == b'agn') * snr_third_bpt_peak_1 * ~snr_first_bpt_peak_1 * ~snr_second_bpt_peak_1))

            agn_four_lines_peak_2 = (((classification[0, 1] == b'agn') * snr_first_bpt_peak_2) +
                                 ((classification[1, 1] == b'agn') * snr_second_bpt_peak_2 * ~snr_first_bpt_peak_2) +
                                 ((classification[
                                       2, 1] == b'agn') * snr_third_bpt_peak_2 * ~snr_first_bpt_peak_2 * ~snr_second_bpt_peak_2))

            liner_four_lines_peak_1 = (((classification[0, 0] == b'liner') * snr_first_bpt_peak_1) +
                                   ((classification[
                                         1, 0] == b'liner') * snr_second_bpt_peak_1 * ~snr_first_bpt_peak_1) +
                                   ((classification[
                                         2, 0] == b'liner') * snr_third_bpt_peak_1 * ~snr_first_bpt_peak_1 * ~snr_second_bpt_peak_1))

            liner_four_lines_peak_2 = (((classification[0, 1] == b'liner') * snr_first_bpt_peak_2) +
                                   ((classification[
                                         1, 1] == b'liner') * snr_second_bpt_peak_2 * ~snr_first_bpt_peak_2) +
                                   ((classification[
                                         2, 1] == b'liner') * snr_third_bpt_peak_2 * ~snr_first_bpt_peak_2 * ~snr_second_bpt_peak_2))

            # classified_four_lines_second_bpt_1 = snr_second_bpt_peak_1 * ~snr_first_bpt_peak_1
            # classified_four_lines_second_bpt_2 = snr_second_bpt_peak_2 * ~snr_first_bpt_peak_2
            #
            # classified_four_lines_third_bpt_1 = snr_third_bpt_peak_1 * ~snr_first_bpt_peak_1 * ~snr_second_bpt_peak_1
            # classified_four_lines_third_bpt_2 = snr_third_bpt_peak_2 * ~snr_first_bpt_peak_2 * ~snr_second_bpt_peak_2

            # classify with correction
            # snr flags for individual lines
            snr_flag_h_beta_peak_1 = bpt_classify.BPTFilter.snr_flag_h_beta(fit_parameter_dict, peak=1,
                                                                        line_shape='manga_dp')
            snr_flag_oiii_peak_1 = bpt_classify.BPTFilter.snr_flag_oiii(fit_parameter_dict, peak=1, line_shape='manga_dp')
            # snr_flag_oi_peak_1 = bpt_classify.BPTFilter.snr_flag_oi(fit_parameter_dict, peak=1, line_shape='manga_dp)
            snr_flag_h_alpha_peak_1 = bpt_classify.BPTFilter.snr_flag_h_alpha(fit_parameter_dict, peak=1,
                                                                          line_shape='manga_dp')
            # snr_flag_sii_peak_1 = bpt_classify.BPTFilter.snr_flag_sii(fit_parameter_dict, peak=1, line_shape='manga_dp)
            snr_flag_nii_peak_1 = bpt_classify.BPTFilter.snr_flag_nii(fit_parameter_dict, peak=1, line_shape='manga_dp')

            snr_flag_h_beta_peak_2 = bpt_classify.BPTFilter.snr_flag_h_beta(fit_parameter_dict, peak=2,
                                                                        line_shape='manga_dp')
            snr_flag_oiii_peak_2 = bpt_classify.BPTFilter.snr_flag_oiii(fit_parameter_dict, peak=2, line_shape='manga_dp')
            # snr_flag_oi_peak_2 = bpt_classify.BPTFilter.snr_flag_oi(fit_parameter_dict, peak=2, line_shape='manga_dp)
            snr_flag_h_alpha_peak_2 = bpt_classify.BPTFilter.snr_flag_h_alpha(fit_parameter_dict, peak=2,
                                                                          line_shape='manga_dp')
            # snr_flag_sii_peak_2 = bpt_classify.BPTFilter.snr_flag_sii(fit_parameter_dict, peak=2, line_shape='manga_dp)
            snr_flag_nii_peak_2 = bpt_classify.BPTFilter.snr_flag_nii(fit_parameter_dict, peak=2, line_shape='manga_dp')

            classify_no_oiii_1 = ~snr_flag_oiii_peak_1 * snr_flag_h_beta_peak_1 * snr_flag_h_alpha_peak_1 * snr_flag_nii_peak_1
            classify_no_h_beta_1 = snr_flag_oiii_peak_1 * ~snr_flag_h_beta_peak_1 * snr_flag_h_alpha_peak_1 * snr_flag_nii_peak_1
            classify_no_h_alpha_1 = snr_flag_oiii_peak_1 * snr_flag_h_beta_peak_1 * ~snr_flag_h_alpha_peak_1 * snr_flag_nii_peak_1
            classify_no_nii_1 = snr_flag_oiii_peak_1 * snr_flag_h_beta_peak_1 * snr_flag_h_alpha_peak_1 * ~snr_flag_nii_peak_1

            classify_no_oiii_2 = ~snr_flag_oiii_peak_2 * snr_flag_h_beta_peak_2 * snr_flag_h_alpha_peak_2 * snr_flag_nii_peak_2
            classify_no_h_beta_2 = snr_flag_oiii_peak_2 * ~snr_flag_h_beta_peak_2 * snr_flag_h_alpha_peak_2 * snr_flag_nii_peak_2
            classify_no_h_alpha_2 = snr_flag_oiii_peak_2 * snr_flag_h_beta_peak_2 * ~snr_flag_h_alpha_peak_2 * snr_flag_nii_peak_2
            classify_no_nii_2 = snr_flag_oiii_peak_2 * snr_flag_h_beta_peak_2 * snr_flag_h_alpha_peak_2 * ~snr_flag_nii_peak_2

            sf_three_lines_peak_1 = (((classification_uncertainty[0, 0, 0] == b'sf') *
                                  classify_no_oiii_1 * not_classifiable_four_lines_peak_1) +
                                 ((classification_uncertainty[0, 2, 0] == b'sf') *
                                  classify_no_nii_1 * not_classifiable_four_lines_peak_1))

            sf_three_lines_peak_2 = (((classification_uncertainty[0, 0, 1] == b'sf') *
                                  classify_no_oiii_2 * not_classifiable_four_lines_peak_2) +
                                 ((classification_uncertainty[0, 2, 1] == b'sf') *
                                  classify_no_nii_2 * not_classifiable_four_lines_peak_2))

            # comp_three_lines_peak_1 = (((classification_uncertainty[0, 0, 0] == b'comp') *
            #                           classify_no_oiii_1 * not_classifiable_four_lines_peak_1))
            #
            # comp_three_lines_peak_2 = (((classification_uncertainty[0, 0, 1] == b'comp') *
            #                               classify_no_oiii_2 * not_classifiable_four_lines_peak_2))
            #
            #
            # sf_comp_three_lines_peak_1 = (((classification_uncertainty[0, 0, 0] == b'comp') *
            #                           classify_no_oiii_1 * not_classifiable_four_lines_peak_1) +
            #                          ((classification_uncertainty[0, 2, 0] == b'comp') *
            #                           classify_no_nii_1 * not_classifiable_four_lines_peak_1))
            #
            # sf_comp_three_lines_peak_2 = (((classification_uncertainty[0, 0, 1] == b'comp') *
            #                               classify_no_oiii_2 * not_classifiable_four_lines_peak_2) +
            #                          ((classification_uncertainty[0, 2, 1] == b'comp') *
            #                           classify_no_nii_2 * not_classifiable_four_lines_peak_2))

            agn_three_lines_peak_1 = (((classification_uncertainty[0, 1, 0] == b'agn') *
                                   classify_no_h_beta_1 * not_classifiable_four_lines_peak_1))

            agn_three_lines_peak_2 = (((classification_uncertainty[0, 1, 1] == b'agn') *
                                   classify_no_h_beta_2 * not_classifiable_four_lines_peak_2))

            # agn_comp_three_lines_peak_1 = (((classification_uncertainty[0, 1, 0] == b'comp') *
            #                            classify_no_h_beta_1 * not_classifiable_four_lines_peak_1))
            #
            # agn_comp_three_lines_peak_2 = (((classification_uncertainty[0, 1, 1] == b'comp') *
            #                            classify_no_h_beta_2 * not_classifiable_four_lines_peak_2))
            #
            #
            # agn_liner_three_lines_peak_1 = (((classification_uncertainty[0, 1, 0] == b'liner') *
            #                            classify_no_h_beta_1 * not_classifiable_four_lines_peak_1))
            #
            # agn_liner_three_lines_peak_2 = (((classification_uncertainty[0, 1, 1] == b'liner') *
            #                            classify_no_h_beta_2 * not_classifiable_four_lines_peak_2))

            liner_three_lines_peak_1 = ((classification_uncertainty[0, 3, 0] == b'liner') *
                                    classify_no_h_alpha_1 * not_classifiable_four_lines_peak_1)

            liner_three_lines_peak_2 = ((classification_uncertainty[0, 3, 1] == b'liner') *
                                    classify_no_h_alpha_2 * not_classifiable_four_lines_peak_2)

            #
            # liner_comp_three_lines_peak_1 = ((classification_uncertainty[0, 3, 0] == b'comp') *
            #                            classify_no_h_alpha_1 * not_classifiable_four_lines_peak_1)
            #
            # liner_comp_three_lines_peak_2 = ((classification_uncertainty[0, 3, 1] == b'comp') *
            #                             classify_no_h_alpha_2 * not_classifiable_four_lines_peak_2)
            #
            #
            # liner_agn_three_lines_peak_1 = ((classification_uncertainty[0, 3, 0] == b'agn') *
            #                            classify_no_h_alpha_1 * not_classifiable_four_lines_peak_1)
            #
            # liner_agn_three_lines_peak_2 = ((classification_uncertainty[0, 3, 1] == b'agn') *
            #                             classify_no_h_alpha_2 * not_classifiable_four_lines_peak_2)

            sf_peak_1 = sf_four_lines_peak_1 + sf_three_lines_peak_1
            sf_peak_2 = sf_four_lines_peak_2 + sf_three_lines_peak_2

            comp_peak_1 = comp_four_lines_peak_1  # + comp_three_lines_peak_1
            comp_peak_2 = comp_four_lines_peak_2  # + comp_three_lines_peak_2

            agn_peak_1 = agn_four_lines_peak_1 + agn_three_lines_peak_1
            agn_peak_2 = agn_four_lines_peak_2 + agn_three_lines_peak_2

            liner_peak_1 = liner_four_lines_peak_1 + liner_three_lines_peak_1
            liner_peak_2 = liner_four_lines_peak_2 + liner_three_lines_peak_2

            classification_map = np.zeros((*fit_parameter_dict['chi2'].shape, 2), dtype='S20')

            classification_map[sf_peak_1, 0] = 'sf'
            classification_map[sf_peak_2, 1] = 'sf'

            classification_map[comp_peak_1, 0] = 'comp'
            classification_map[comp_peak_2, 1] = 'comp'

            classification_map[agn_peak_1, 0] = 'agn'
            classification_map[agn_peak_2, 1] = 'agn'

            classification_map[liner_peak_1, 0] = 'liner'
            classification_map[liner_peak_2, 1] = 'liner'

            bpt_classified_1 = sf_peak_1 + comp_peak_1 + agn_peak_1 + liner_peak_1
            bpt_classified_2 = sf_peak_2 + comp_peak_2 + agn_peak_2 + liner_peak_2

            not_bpt_classified_1 = np.invert(bpt_classified_1)
            not_bpt_classified_2 = np.invert(bpt_classified_2)

            bpt_classification_dict_dp = {'classification_map': classification_map,
                                      'classification': classification,
                                      'classification_uncertainty': classification_uncertainty,
                                      'bpt_classified_1': bpt_classified_1,
                                      'bpt_classified_2': bpt_classified_2,
                                      'not_bpt_classified_1': not_bpt_classified_1,
                                      'not_bpt_classified_2': not_bpt_classified_2}

        if number_of_gaussians == 'single':
            return bpt_classification_dict_sp
        else:
            return bpt_classification_dict_dp

    def get_manga_double_peak_bpt_classification(self):
        from galtool import bpt_classify

        fit_parameter_dict = self.fit_parameter_dict

        # general classification
        classification = np.zeros((3, 2, *fit_parameter_dict['chi2'].shape), dtype='S20')
        classification[0] = bpt_classify.BPTCategorisation.classify_first_bpt(fit_parameter_dict, line_shape='manga_dp')
        classification[1] = bpt_classify.BPTCategorisation.classify_second_bpt(fit_parameter_dict,
                                                                               line_shape='manga_dp')
        classification[2] = bpt_classify.BPTCategorisation.classify_third_bpt(fit_parameter_dict, line_shape='manga_dp')

        # classification with uncertainty consideration
        classification_uncertainty = np.zeros((3, 4, 2, *fit_parameter_dict['chi2'].shape), dtype='S20')
        classification_uncertainty[0] = bpt_classify.BPTCategorisation.classify_uncertainty_first_bpt(
            fit_parameter_dict, line_shape='manga_dp')

        # snr filter
        snr_first_bpt_peak_1, snr_first_bpt_peak_2 = bpt_classify.BPTFilter.snr_first_bpt(fit_parameter_dict,
                                                                                          line_shape='manga_dp')
        snr_second_bpt_peak_1, snr_second_bpt_peak_2 = bpt_classify.BPTFilter.snr_second_bpt(fit_parameter_dict,
                                                                                             line_shape='manga_dp')
        snr_third_bpt_peak_1, snr_third_bpt_peak_2 = bpt_classify.BPTFilter.snr_third_bpt(fit_parameter_dict,
                                                                                          line_shape='manga_dp')

        not_classifiable_four_lines_peak_1 = np.invert(
            snr_first_bpt_peak_1 + snr_second_bpt_peak_1 + snr_third_bpt_peak_1)
        not_classifiable_four_lines_peak_2 = np.invert(
            snr_first_bpt_peak_2 + snr_second_bpt_peak_2 + snr_third_bpt_peak_2)

        sf_four_lines_peak_1 = (((classification[0, 0] == b'sf') * snr_first_bpt_peak_1) +
                                ((classification[1, 0] == b'sf') * snr_second_bpt_peak_1 * ~snr_first_bpt_peak_1) +
                                ((classification[
                                      2, 0] == b'sf') * snr_third_bpt_peak_1 * ~snr_first_bpt_peak_1 * ~snr_second_bpt_peak_1))

        sf_four_lines_peak_2 = (((classification[0, 1] == b'sf') * snr_first_bpt_peak_2) +
                                ((classification[1, 1] == b'sf') * snr_second_bpt_peak_2 * ~snr_first_bpt_peak_2) +
                                ((classification[
                                      2, 1] == b'sf') * snr_third_bpt_peak_2 * ~snr_first_bpt_peak_2 * ~snr_second_bpt_peak_2))

        comp_four_lines_peak_1 = ((classification[0, 0] == b'comp') * snr_first_bpt_peak_1)
        comp_four_lines_peak_2 = ((classification[0, 1] == b'comp') * snr_first_bpt_peak_2)

        agn_four_lines_peak_1 = (((classification[0, 0] == b'agn') * snr_first_bpt_peak_1) +
                                 ((classification[1, 0] == b'agn') * snr_second_bpt_peak_1 * ~snr_first_bpt_peak_1) +
                                 ((classification[
                                       2, 0] == b'agn') * snr_third_bpt_peak_1 * ~snr_first_bpt_peak_1 * ~snr_second_bpt_peak_1))

        agn_four_lines_peak_2 = (((classification[0, 1] == b'agn') * snr_first_bpt_peak_2) +
                                 ((classification[1, 1] == b'agn') * snr_second_bpt_peak_2 * ~snr_first_bpt_peak_2) +
                                 ((classification[
                                       2, 1] == b'agn') * snr_third_bpt_peak_2 * ~snr_first_bpt_peak_2 * ~snr_second_bpt_peak_2))

        liner_four_lines_peak_1 = (((classification[0, 0] == b'liner') * snr_first_bpt_peak_1) +
                                   ((classification[
                                         1, 0] == b'liner') * snr_second_bpt_peak_1 * ~snr_first_bpt_peak_1) +
                                   ((classification[
                                         2, 0] == b'liner') * snr_third_bpt_peak_1 * ~snr_first_bpt_peak_1 * ~snr_second_bpt_peak_1))

        liner_four_lines_peak_2 = (((classification[0, 1] == b'liner') * snr_first_bpt_peak_2) +
                                   ((classification[
                                         1, 1] == b'liner') * snr_second_bpt_peak_2 * ~snr_first_bpt_peak_2) +
                                   ((classification[
                                         2, 1] == b'liner') * snr_third_bpt_peak_2 * ~snr_first_bpt_peak_2 * ~snr_second_bpt_peak_2))

        # classified_four_lines_second_bpt_1 = snr_second_bpt_peak_1 * ~snr_first_bpt_peak_1
        # classified_four_lines_second_bpt_2 = snr_second_bpt_peak_2 * ~snr_first_bpt_peak_2
        #
        # classified_four_lines_third_bpt_1 = snr_third_bpt_peak_1 * ~snr_first_bpt_peak_1 * ~snr_second_bpt_peak_1
        # classified_four_lines_third_bpt_2 = snr_third_bpt_peak_2 * ~snr_first_bpt_peak_2 * ~snr_second_bpt_peak_2

        # classify with correction
        # snr flags for individual lines
        snr_flag_h_beta_peak_1 = bpt_classify.BPTFilter.snr_flag_h_beta(fit_parameter_dict, peak=1,
                                                                        line_shape='manga_dp')
        snr_flag_oiii_peak_1 = bpt_classify.BPTFilter.snr_flag_oiii(fit_parameter_dict, peak=1, line_shape='manga_dp')
        # snr_flag_oi_peak_1 = bpt_classify.BPTFilter.snr_flag_oi(fit_parameter_dict, peak=1, line_shape='manga_dp)
        snr_flag_h_alpha_peak_1 = bpt_classify.BPTFilter.snr_flag_h_alpha(fit_parameter_dict, peak=1,
                                                                          line_shape='manga_dp')
        # snr_flag_sii_peak_1 = bpt_classify.BPTFilter.snr_flag_sii(fit_parameter_dict, peak=1, line_shape='manga_dp)
        snr_flag_nii_peak_1 = bpt_classify.BPTFilter.snr_flag_nii(fit_parameter_dict, peak=1, line_shape='manga_dp')

        snr_flag_h_beta_peak_2 = bpt_classify.BPTFilter.snr_flag_h_beta(fit_parameter_dict, peak=2,
                                                                        line_shape='manga_dp')
        snr_flag_oiii_peak_2 = bpt_classify.BPTFilter.snr_flag_oiii(fit_parameter_dict, peak=2, line_shape='manga_dp')
        # snr_flag_oi_peak_2 = bpt_classify.BPTFilter.snr_flag_oi(fit_parameter_dict, peak=2, line_shape='manga_dp)
        snr_flag_h_alpha_peak_2 = bpt_classify.BPTFilter.snr_flag_h_alpha(fit_parameter_dict, peak=2,
                                                                          line_shape='manga_dp')
        # snr_flag_sii_peak_2 = bpt_classify.BPTFilter.snr_flag_sii(fit_parameter_dict, peak=2, line_shape='manga_dp)
        snr_flag_nii_peak_2 = bpt_classify.BPTFilter.snr_flag_nii(fit_parameter_dict, peak=2, line_shape='manga_dp')

        classify_no_oiii_1 = ~snr_flag_oiii_peak_1 * snr_flag_h_beta_peak_1 * snr_flag_h_alpha_peak_1 * snr_flag_nii_peak_1
        classify_no_h_beta_1 = snr_flag_oiii_peak_1 * ~snr_flag_h_beta_peak_1 * snr_flag_h_alpha_peak_1 * snr_flag_nii_peak_1
        classify_no_h_alpha_1 = snr_flag_oiii_peak_1 * snr_flag_h_beta_peak_1 * ~snr_flag_h_alpha_peak_1 * snr_flag_nii_peak_1
        classify_no_nii_1 = snr_flag_oiii_peak_1 * snr_flag_h_beta_peak_1 * snr_flag_h_alpha_peak_1 * ~snr_flag_nii_peak_1

        classify_no_oiii_2 = ~snr_flag_oiii_peak_2 * snr_flag_h_beta_peak_2 * snr_flag_h_alpha_peak_2 * snr_flag_nii_peak_2
        classify_no_h_beta_2 = snr_flag_oiii_peak_2 * ~snr_flag_h_beta_peak_2 * snr_flag_h_alpha_peak_2 * snr_flag_nii_peak_2
        classify_no_h_alpha_2 = snr_flag_oiii_peak_2 * snr_flag_h_beta_peak_2 * ~snr_flag_h_alpha_peak_2 * snr_flag_nii_peak_2
        classify_no_nii_2 = snr_flag_oiii_peak_2 * snr_flag_h_beta_peak_2 * snr_flag_h_alpha_peak_2 * ~snr_flag_nii_peak_2

        sf_three_lines_peak_1 = (((classification_uncertainty[0, 0, 0] == b'sf') *
                                  classify_no_oiii_1 * not_classifiable_four_lines_peak_1) +
                                 ((classification_uncertainty[0, 2, 0] == b'sf') *
                                  classify_no_nii_1 * not_classifiable_four_lines_peak_1))

        sf_three_lines_peak_2 = (((classification_uncertainty[0, 0, 1] == b'sf') *
                                  classify_no_oiii_2 * not_classifiable_four_lines_peak_2) +
                                 ((classification_uncertainty[0, 2, 1] == b'sf') *
                                  classify_no_nii_2 * not_classifiable_four_lines_peak_2))

        # comp_three_lines_peak_1 = (((classification_uncertainty[0, 0, 0] == b'comp') *
        #                           classify_no_oiii_1 * not_classifiable_four_lines_peak_1))
        #
        # comp_three_lines_peak_2 = (((classification_uncertainty[0, 0, 1] == b'comp') *
        #                               classify_no_oiii_2 * not_classifiable_four_lines_peak_2))
        #
        #
        # sf_comp_three_lines_peak_1 = (((classification_uncertainty[0, 0, 0] == b'comp') *
        #                           classify_no_oiii_1 * not_classifiable_four_lines_peak_1) +
        #                          ((classification_uncertainty[0, 2, 0] == b'comp') *
        #                           classify_no_nii_1 * not_classifiable_four_lines_peak_1))
        #
        # sf_comp_three_lines_peak_2 = (((classification_uncertainty[0, 0, 1] == b'comp') *
        #                               classify_no_oiii_2 * not_classifiable_four_lines_peak_2) +
        #                          ((classification_uncertainty[0, 2, 1] == b'comp') *
        #                           classify_no_nii_2 * not_classifiable_four_lines_peak_2))

        agn_three_lines_peak_1 = (((classification_uncertainty[0, 1, 0] == b'agn') *
                                   classify_no_h_beta_1 * not_classifiable_four_lines_peak_1))

        agn_three_lines_peak_2 = (((classification_uncertainty[0, 1, 1] == b'agn') *
                                   classify_no_h_beta_2 * not_classifiable_four_lines_peak_2))

        # agn_comp_three_lines_peak_1 = (((classification_uncertainty[0, 1, 0] == b'comp') *
        #                            classify_no_h_beta_1 * not_classifiable_four_lines_peak_1))
        #
        # agn_comp_three_lines_peak_2 = (((classification_uncertainty[0, 1, 1] == b'comp') *
        #                            classify_no_h_beta_2 * not_classifiable_four_lines_peak_2))
        #
        #
        # agn_liner_three_lines_peak_1 = (((classification_uncertainty[0, 1, 0] == b'liner') *
        #                            classify_no_h_beta_1 * not_classifiable_four_lines_peak_1))
        #
        # agn_liner_three_lines_peak_2 = (((classification_uncertainty[0, 1, 1] == b'liner') *
        #                            classify_no_h_beta_2 * not_classifiable_four_lines_peak_2))

        liner_three_lines_peak_1 = ((classification_uncertainty[0, 3, 0] == b'liner') *
                                    classify_no_h_alpha_1 * not_classifiable_four_lines_peak_1)

        liner_three_lines_peak_2 = ((classification_uncertainty[0, 3, 1] == b'liner') *
                                    classify_no_h_alpha_2 * not_classifiable_four_lines_peak_2)

        #
        # liner_comp_three_lines_peak_1 = ((classification_uncertainty[0, 3, 0] == b'comp') *
        #                            classify_no_h_alpha_1 * not_classifiable_four_lines_peak_1)
        #
        # liner_comp_three_lines_peak_2 = ((classification_uncertainty[0, 3, 1] == b'comp') *
        #                             classify_no_h_alpha_2 * not_classifiable_four_lines_peak_2)
        #
        #
        # liner_agn_three_lines_peak_1 = ((classification_uncertainty[0, 3, 0] == b'agn') *
        #                            classify_no_h_alpha_1 * not_classifiable_four_lines_peak_1)
        #
        # liner_agn_three_lines_peak_2 = ((classification_uncertainty[0, 3, 1] == b'agn') *
        #                             classify_no_h_alpha_2 * not_classifiable_four_lines_peak_2)

        sf_peak_1 = sf_four_lines_peak_1 + sf_three_lines_peak_1
        sf_peak_2 = sf_four_lines_peak_2 + sf_three_lines_peak_2

        comp_peak_1 = comp_four_lines_peak_1  # + comp_three_lines_peak_1
        comp_peak_2 = comp_four_lines_peak_2  # + comp_three_lines_peak_2

        agn_peak_1 = agn_four_lines_peak_1 + agn_three_lines_peak_1
        agn_peak_2 = agn_four_lines_peak_2 + agn_three_lines_peak_2

        liner_peak_1 = liner_four_lines_peak_1 + liner_three_lines_peak_1
        liner_peak_2 = liner_four_lines_peak_2 + liner_three_lines_peak_2

        classification_map = np.zeros((*fit_parameter_dict['chi2'].shape, 2), dtype='S20')

        classification_map[sf_peak_1, 0] = 'sf'
        classification_map[sf_peak_2, 1] = 'sf'

        classification_map[comp_peak_1, 0] = 'comp'
        classification_map[comp_peak_2, 1] = 'comp'

        classification_map[agn_peak_1, 0] = 'agn'
        classification_map[agn_peak_2, 1] = 'agn'

        classification_map[liner_peak_1, 0] = 'liner'
        classification_map[liner_peak_2, 1] = 'liner'

        bpt_classified_1 = sf_peak_1 + comp_peak_1 + agn_peak_1 + liner_peak_1
        bpt_classified_2 = sf_peak_2 + comp_peak_2 + agn_peak_2 + liner_peak_2

        not_bpt_classified_1 = np.invert(bpt_classified_1)
        not_bpt_classified_2 = np.invert(bpt_classified_2)

        bpt_classification_dict_dp = {'classification_map': classification_map,
                                      'classification': classification,
                                      'classification_uncertainty': classification_uncertainty,
                                      'bpt_classified_1': bpt_classified_1,
                                      'bpt_classified_2': bpt_classified_2,
                                      'not_bpt_classified_1': not_bpt_classified_1,
                                      'not_bpt_classified_2': not_bpt_classified_2}

        return bpt_classification_dict_dp

    def get_bpt_classification(self, line_shape='gauss', number_of_gaussians=None):
        r"""
        function to return bpt classification

        :return: dict with all possible BPT classification available
        """

        # otherwise calculate it
        if (line_shape == 'gauss') | (line_shape == 'nonpar'):
            subset_path = self.get_subset_path(table_name=self.object_type)
            # check if dict already exists
            if os.path.isfile(subset_path / 'bpt' / 'bpt_dict.npy'):
                bpt_dict = np.load(subset_path / 'bpt' / 'bpt_dict.npy', allow_pickle=True).item()
                return bpt_dict
            else:
                bpt_dict = self.get_simple_bpt_classification(line_shape=line_shape)
                if not os.path.isdir(subset_path / 'bpt'):
                    os.makedirs(subset_path / 'bpt')
                np.save(subset_path / 'bpt' / 'bpt_dict.npy', bpt_dict)
                return bpt_dict
        elif line_shape == 'dp':
            subset_path = self.get_subset_path(table_name=self.object_type)
            # check if dict already exists
            if os.path.isfile(subset_path / 'bpt' / 'bpt_dict.npy'):
                bpt_dict = np.load(subset_path / 'bpt' / 'bpt_dict.npy', allow_pickle=True).item()
                return bpt_dict
            else:
                bpt_dict = self.get_simple_bpt_classification(line_shape='nonpar')
                bpt_dict_double_peak = self.get_double_peak_bpt_classification()
                bpt_dict.update(bpt_dict_double_peak)
                if not os.path.isdir(subset_path / 'bpt'):
                    os.makedirs(subset_path / 'bpt')
                np.save(subset_path / 'bpt' / 'bpt_dict.npy', bpt_dict)
                return bpt_dict
        elif line_shape == 'manga':
            file_path, file_name = self.get_manga_bpt_classification_file_path(number_of_gaussians=number_of_gaussians)
            if not os.path.isdir(file_path):
                os.makedirs(file_path)
            if os.path.isfile(file_path / file_name):
                bpt_dict = np.load(file_path / file_name, allow_pickle=True).item()
                return bpt_dict
            else:
                bpt_dict = self.get_manga_double_peak_bpt_classification_new(number_of_gaussians=number_of_gaussians)
                np.save(file_path / file_name, bpt_dict)
                return bpt_dict
        else:
            raise KeyError('keyword line_shape not understand. Must be gauss, nonpar, dp or manga_dp')




class MorphAnalysis(data_access.DataAccess):

    def __init__(self, **kwargs):
        r"""

        """
        super().__init__(**kwargs)

    def _get_fit_flag(self, flag_mode='final'):
        r"""
        Access to the flagging system of Meert et al. 2015 2015MNRAS.446.3943M.
        In the paper Table 1 provides 27 bits which are saved as integer.
        :param flag_mode: specifies the used flagging system: final, auto, py,
        pyfit. see more details Table 3
        :type flag_mode: str
        :return array dim(n, 27) containing the flag of each n galaxies
        :rtype: array_like
        """
        to_str_func = np.vectorize(lambda x: np.binary_repr(x).rjust(27, '0'))
        binary_flags = to_str_func(self.table['%sflag' % flag_mode])
        ret = np.zeros(list(self.table['%sflag' % flag_mode].shape) + [27], dtype=np.int8)
        for bit_ix in range(0, 27):
            fetch_bit_func = np.vectorize(lambda x: x[bit_ix] == '1')
            ret[..., bit_ix] = fetch_bit_func(binary_flags).astype("int8")

        return np.fliplr(ret)

    def get_inclination(self):
        r"""
        providing galaxy inclination of galaxies which have a ser and/or exp
        fit provided by Meert et al. 2015 2015MNRAS.446.3943M.
        The inclination is calculated using the ellipticity of the apparent
        profile following Aquino-Ortiz+18 MNRAS 479, 2133
        :return array of cos(i) where i is the inclination angular
        :rtype: array_like
        """
        # select trustable fits from Meert et al. 2015 MNRAS 446, 3943–3974
        final_flag = self._get_fit_flag(flag_mode='final')
        # parallel = final_flag[:, 9] == 1
        # flip_components = final_flag[:, 13] == 1
        bulge = (final_flag[:, 1] == 1)
        disk = (final_flag[:, 4] == 1)
        two_com = final_flag[:, 10] == 1
        good_ser_exp = final_flag[:, 12] == 1
        # bulge_sersic = self.table['n_bulge'] == 8
        # disk_sersic = self.table['n_disk'] == 8

        ba = np.zeros(len(self.table))
        cos_i = np.zeros((len(self.table)))

        # ellipticity
        ba[two_com * good_ser_exp] = self.table[two_com * good_ser_exp]['ba_tot']
        ba[disk] = self.table[disk]['ba_bulge']
        ba[bulge] = self.table[bulge]['ba_bulge']

        # mask for trustalble 2d decomposition fits
        measured_inclination = two_com * good_ser_exp + disk + bulge

        # eq. 3 taken from Aquino-Ortiz+18 MNRAS 479, 2133
        q0 = 0.2

        cos_i[(ba <= 0.2) * measured_inclination] = 0

        cos_i[ba > 0.2] = np.sqrt((ba[ba > 0.2] ** 2 - q0 ** 2) / (1 - q0 ** 2))

        # galaxies with no trustable surface fit can not provide inclination
        cos_i[~measured_inclination] = np.nan

        return cos_i

    def classify_morph(self, morph_type='ltg'):
        r"""
        Using Dominguez Sanchez et al. 2018 MNRAS 476, 3661–3676 this function
        provide a mask of the demanded morphological galaxy type
        :param morph_type: mophological type which to select. This can be
        ltg (late type galaxies or 'spirals'), face_on, edge_on, or bar (which
        are sub groups of ltgs), elliptical, s0 or merger.
        :type morph_type: str
        :return mask for galaxy table indicating morphological type.
        :rtype: array_like
        """
        merger = self.table['P_merg'] > 0.9
        no_merger = np.invert(merger)

        if morph_type == 'ltg':
            morphology_mask = ((self.table['TType'] > 0) &
                               (self.table['P_disk'] > 0.3) & no_merger)
        elif morph_type == 'face_on':
            inclination_angular = (np.arccos(self.get_inclination()) *
                                   180 / np.pi)
            morphology_mask = ((inclination_angular < 30) &
                               (self.table['TType'] > 0) &
                               (self.table['P_edge_on'] < 0.1) &
                               (self.table['P_disk'] > 0.3) & no_merger)
        elif morph_type == 'edge_on':
            inclination_angular = (np.arccos(self.get_inclination()) *
                                   180 / np.pi)
            morphology_mask = ((inclination_angular > 70) &
                               (self.table['TType'] > 0) &
                               (self.table['P_edge_on'] > 0.4) &
                               (self.table['P_disk'] > 0.3) & no_merger)
        elif morph_type == 'bar':
            morphology_mask = ((self.table['TType'] > 0) &
                               (self.table['P_disk'] > 0.3) &
                               (self.table['P_bar_GZ2'] > 0.9) & no_merger)
        elif morph_type == 'elliptical':
            morphology_mask = ((self.table['TType'] <= 0) &
                               (self.table['P_S0'] < 0.4) & no_merger)
        elif morph_type == 's0':
            morphology_mask = ((self.table['TType'] <= 0) &
                               (self.table['P_S0'] > 0.6) &
                               (self.table['P_edge_on'] < 0.4) &
                               (self.table['P_disk'] < 0.3) & no_merger)
        elif morph_type == 'merger':
            morphology_mask = merger
        else:
            raise ValueError('morphology type not understood')

        return morphology_mask

    def create_morphology_mask(self):
        ltg = self.classify_morph(morph_type='ltg')
        face_on = self.classify_morph(morph_type='face_on')
        edge_on = self.classify_morph(morph_type='edge_on')
        bar = self.classify_morph(morph_type='bar')
        elliptical = self.classify_morph(morph_type='elliptical')
        s0 = self.classify_morph(morph_type='s0')
        merger = self.classify_morph(morph_type='merger')

        morph_mask_dict = {
            'ltg': ltg,
            'face_on': face_on,
            'edge_on': edge_on,
            'bar': bar,
            'elliptical': elliptical,
            's0': s0,
            'merger': merger
        }

        np.save((Path(self.data_path / self.file_name).parents[0]
                 / 'subsets' / 'morphology' / 'morph').with_suffix('.npy'), morph_mask_dict)

        return morph_mask_dict

    def classify_morph_with_MaNGA_VAC(object_access, VAC_version='DR15', morph_type='ltg'):
        r"""
        Using MaNGA Deep Learning DR15 or DR17 Morphology catalogue, this function
        provides a mask of the demanded morphological galaxy type
        :param VAC_version: catalogue's version to consider. This can be either DR15 or DR17
        :param morph_type: morphological type which to select. This can be
        ltg (late type galaxies or 'spirals'), etg, or bar (which
        are sub-groups of ltgs), elliptical, s0 or merger.
        :type VAC_version: str
        :type morph_type: str
        :return mask for galaxy table indicating morphological type.
        :rtype: array_like
        """
        if VAC_version == 'DR15':
            merger = object_access.table['P_MERG'] > 0.9
            no_merger = np.invert(merger)

            if morph_type == 'ltg':
                morphology_mask = ((object_access.table['TTYPE'] > 0) & no_merger)

            elif morph_type == 'etg':
                morphology_mask = ((object_access.table['TTYPE'] <= 0) & no_merger)

            elif morph_type == 'bar':
                morphology_mask = ((object_access.table['TTYPE'] > 0) &
                                    (object_access.table['P_BAR_GZ2'] > 0.9) & no_merger)
            elif morph_type == 'elliptical':
                morphology_mask = ((object_access.table['TTYPE'] <= 0) &
                                    (object_access.table['P_S0'] < 0.5) & no_merger)
            elif morph_type == 's0':
                morphology_mask = ((object_access.table['TTYPE'] <= 0) &
                                    (object_access.table['P_S0'] > 0.5) &
                                    no_merger | object_access.table['FLAG_S0']==1)
            elif morph_type == 'merger':
                morphology_mask = merger
            else:
                raise ValueError('morphology type not understood')
        
        if VAC_version == 'DR17' :
            if morph_type == 'ltg':
                morphology_mask = ( (object_access.table['T-Type'] > 0) &
                               (object_access.table['P_LTG'] >= 0.5) & (object_access.table['Visual_Class']==3) )

            elif morph_type == 'etg':
                morphology_mask = (object_access.table['T-Type'] <= 0)

            elif morph_type == 'bar':
                morphology_mask = ( (object_access.table['T-Type'] > 0) &
                                (object_access.table['P_bar'] > 0.9) & (object_access.table['P_edge'] < 0.3) )

            elif morph_type == 'elliptical':
                morphology_mask = ( (object_access.table['T-Type'] <= 0) &
                                (object_access.table['P_S0'] <= 0.5) & 
                                (object_access.table['P_LTG'] < 0.5) & 
                                (object_access.table['Visual_Class']==1) )
            elif morph_type == 's0':
                morphology_mask = ( (object_access.table['T-Type'] <= 0) &
                                (object_access.table['P_S0'] > 0.5) & 
                                (object_access.table['P_LTG'] < 0.5) & 
                                (object_access.table['Visual_Class']==2) )

            elif morph_type == 'unclassifiable':
                morphology_mask = ( object_access.table['Visual_Class']==0 )

            else:
                raise ValueError('morphology type not understood')

        return morphology_mask
    
    def create_MaNGA_morphology_mask(self, VAC_version):
        ltg = self.classify_morph_with_MaNGA_VAC(VAC_version=VAC_version, morph_type='ltg')
        etg = self.classify_morph_with_MaNGA_VAC(VAC_version=VAC_version, morph_type='etg')
        bar = self.classify_morph_with_MaNGA_VAC(VAC_version=VAC_version, morph_type='bar')
        elliptical = self.classify_morph_with_MaNGA_VAC(VAC_version=VAC_version, morph_type='elliptical')
        s0 = self.classify_morph_with_MaNGA_VAC(VAC_version=VAC_version, morph_type='s0')  

        if VAC_version == 'DR15' :
            merger = self.classify_morph_with_MaNGA_VAC(VAC_version=VAC_version, morph_type='merger')
            morph_mask_dict = {
            'ltg': ltg,
            'etg': etg,
            'bar': bar,
            'elliptical': elliptical,
            's0': s0,
            'merger': merger
        }
            dictname = 'morph_dict_dr15'
        if VAC_version == 'DR17' :
            unclassifiable = self.classify_morph_with_MaNGA_VAC(VAC_version=VAC_version, morph_type='unclassifiable')
            morph_mask_dict = {
            'ltg': ltg,
            'etg': etg,
            'bar': bar,
            'elliptical': elliptical,
            's0': s0,
            'unclassifiable': unclassifiable
        }
            dictname = 'morph_dict_dr17'
        np.save((Path(self.data_path / self.file_name).parents[0]
                 / 'subsets' / 'morphology' / dictname).with_suffix('.npy'), morph_mask_dict)

        return morph_mask_dict
    

    
    def get_image_parameter(self, index=0):
        # get galaxy coordinates
        ra = self.get_ra()[index]
        dec = self.get_dec()[index]

        from astropy.coordinates import SkyCoord
        c = SkyCoord(ra=ra * u.degree, dec=dec * u.degree)
        ra_coords = c.ra.hms
        dec_coords = c.dec.dms
        d = dec_coords.d

        if d > 0:
            d_string = '+%02i' % d
        else:
            if d == 0:
                if dec_coords.m > 0:
                    d_string = '+%02i' % d
                else:
                    if dec_coords.m == 0:
                        if dec_coords.s >= 0:
                            d_string = '+%02i' % d
                        else:
                            d_string = '-%02i' % abs(d)
                    else:
                        d_string = '-%02i' % abs(d)
            else:
                d_string = '-%02i' % abs(d)

        # download legacy survey images
        size = 250
        pixel_scale = 29
        file_path = (self.data_path / 'images' /
                     str('h_%02i_m_%02i_d_%s_m_%02i' %
                         (ra_coords.h,  ra_coords.m,  d_string, dec_coords.m)))
        file_name_r = Path('large_field_legacy_ra_s_%i_dec_s_%i_pixel_scale_%i_size_%i_offset_r' %
                           (ra_coords.s * 100, dec_coords.s * 100, int(pixel_scale),
                            int(size))).with_suffix('.fits')
        print(file_name_r)
        url_r = self.get_observation_image_url(ra, dec, size, pixel_scale, layer='dr8', bands='r', file_format='fits')

        # get data
        if not os.path.isdir(file_path):
            os.makedirs(file_path)

        self.download_file(file_path, file_name_r, url_r, unpack=False)

        from astropy.io import fits
        import photutils
        import scipy.ndimage as ndi
        import statmorph
        # from statmorph.utils.image_diagnostics import make_figure
        r_hdu = fits.open(file_path / file_name_r)[0]
        r_band = r_hdu.data
        # following the tutorial at:
        # https://nbviewer.jupyter.org/github/vrodgom/statmorph/blob/master/notebooks/tutorial.ipynb

        threshold = photutils.detect_threshold(r_band, 2.5)
        npixels = 5  # minimum number of connected pixels
        segm = photutils.detect_sources(r_band, threshold, npixels)

        # Keep only the largest segment
        label = np.argmax(segm.areas) + 1
        segmap = segm.data == label

        segmap_float = ndi.uniform_filter(np.float64(segmap), size=10)
        segmap = segmap_float > 0.5

        source_morphs = statmorph.source_morphology(r_band, segmap, gain=4.43)  # , psf=psf)

        morph = source_morphs[0]

        parameter_dict = {
            'xc_centroid': morph.xc_centroid,
            'yc_centroid': morph.yc_centroid,
            'ellipticity_centroid': morph.ellipticity_centroid,
            'elongation_centroid': morph.elongation_centroid,
            'orientation_centroid': morph.orientation_centroid,
            'xc_asymmetry': morph.xc_asymmetry,
            'yc_asymmetry': morph.yc_asymmetry,
            'ellipticity_asymmetry': morph.ellipticity_asymmetry,
            'elongation_asymmetry': morph.elongation_asymmetry,
            'orientation_asymmetry': morph.orientation_asymmetry,
            'rpetro_circ': morph.rpetro_circ,
            'rpetro_ellip': morph.rpetro_ellip,
            'rhalf_circ': morph.rhalf_circ,
            'rhalf_ellip': morph.rhalf_ellip,
            'r20': morph.r20,
            'r80': morph.r80,
            'Gini': morph.gini,
            'M20': morph.m20,
            'F(G, M20)': morph.gini_m20_bulge,
            'S(G, M20)': morph.gini_m20_merger,
            'sn_per_pixel': morph.sn_per_pixel,
            'C': morph.concentration,
            'A': morph.asymmetry,
            'S': morph.smoothness,
            'shape_asymmetry': morph.shape_asymmetry,
            'sersic_amplitude': morph.sersic_amplitude,
            'sersic_rhalf': morph.sersic_rhalf,
            'sersic_n': morph.sersic_n,
            'sersic_xc': morph.sersic_xc,
            'sersic_yc': morph.sersic_yc,
            'sersic_ellip': morph.sersic_ellip,
            'sersic_theta': morph.sersic_theta,
            'sky_mean': morph.sky_mean,
            'sky_median': morph.sky_median,
            'sky_sigma': morph.sky_sigma,
            'flag': morph.flag,
            'flag_sersic': morph.flag_sersic}
        return parameter_dict

    def load_image_parameter_dicts(self, index=0):

        parameter_dict = self.get_image_parameter(index=index)
        return parameter_dict



class AnalysisTools(AnalyseGas, LuminosityTools, EnvironmentTools, BPTClassification, MorphAnalysis,
                    plotting_tools.PlotBPT):

    def __init__(self, **kwargs):
        r"""

        """
        super().__init__(**kwargs)
