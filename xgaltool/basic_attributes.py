"""
The Classes in this file build up basic concepts properties and Physical constants for the galtool project
"""

import os
from astropy.cosmology import LambdaCDM
from scipy.constants import c as speed_of_light
from scipy.constants import h as plank
from scipy.constants import e as electron_charge


class DataStructure:

    def __init__(self):
        r"""
        Class to specify data structure
        """
        super().__init__()
        """
        RCSED catalog is provided at http://rcsed.sai.msu.ru/data/ These tables are in detailed described in 
        Chilingarian et al.2017 
        """
        self.rcsed_table_url = {'main_table': 'http://rcsed.sai.msu.ru/media/files/rcsed.fits',
                                'fibermags': 'http://rcsed.sai.msu.ru/media/files/rcsed_fibermags.fits',
                                'gas_met': 'http://rcsed.sai.msu.ru/media/files/rcsed_gasmet.fits',
                                'rcsed_lines_gauss':  'http://rcsed.sai.msu.ru/media/files/rcsed_lines_gauss.fits',
                                'rcsed_lines_nonpar': 'http://rcsed.sai.msu.ru/media/files/rcsed_lines_nonpar.fits'}

        # MaNGA catalog of DR15 find description under https://www.sdss.org/dr15/manga/manga-data/catalogs/
        self.manga_table_url = 'https://data.sdss.org/sas/dr15/manga/spectro/redux/v2_4_3/drpall-v2_4_3.fits'

        # VLA FIRST survey catalog see http://sundog.stsci.edu/first/catalogs/readme.html
        self.first_table_url = 'http://sundog.stsci.edu/first/catalogs/first_14dec17.fits.gz'

        # Lofar collaboration tables
        self.lotss_dr1_table_url = 'https://lofar-surveys.org/public/LOFAR_HBA_T1_DR1_catalog_v1.0.srl.fits'
        self.lotss_dr2_table_url = 'https://lofar-surveys.org/downloads/DR2/catalogues/LoTSS_DR2_v100.srl.fits'

        # Yang et al. 2007
        self.yang_2007_table_url = 'https://gax.sjtu.edu.cn/data/Group.html'

        # all tables of different publication which are included here
        self.table_identifier = {
            # RCSED chilingarian et al 2017
            'rcsed': {'folder_name': 'rcsed', 'file_name': 'rcsed', 'suffix': '.fits'},
            'rcsed_coords': {'folder_name': 'rcsed', 'file_name': 'rcsed_coords', 'suffix': '.fits'},
            'rcsed_main_table': {'folder_name': 'rcsed', 'file_name': 'rcsed_main_table', 'suffix': '.fits'},
            'rcsed_fibermags': {'folder_name': 'rcsed/raw_tables', 'file_name': 'rcsed_fibermags', 'suffix': '.fits'},
            'rcsed_gasmet': {'folder_name': 'rcsed/raw_tables', 'file_name': 'rcsed_gasmet', 'suffix': '.fits'},
            'rcsed_lines_gauss': {'folder_name': 'rcsed', 'file_name': 'rcsed_lines_gauss', 'suffix': '.fits'},
            'rcsed_lines_nonpar': {'folder_name': 'rcsed', 'file_name': 'rcsed_lines_nonpar', 'suffix': '.fits'},
            'tiny_rcsed': {'folder_name': 'tiny_rcsed', 'file_name': 'tiny_rcsed', 'suffix': '.fits'},
            # Maschmann et al. 2020
            'dp': {'folder_name': 'dp', 'file_name': 'dp', 'suffix': '.fits'},
            'cs': {'folder_name': 'cs', 'file_name': 'cs', 'suffix': '.fits'},
            'nbcs': {'folder_name': 'nbcs', 'file_name': 'nbcs', 'suffix': '.fits'},
            # x-ray observation
            'chandra': {'folder_name': 'chandra', 'file_name': 'chandra', 'suffix': '.fit'},
            'xmm_newton': {'folder_name': 'xmm_newton', 'file_name': 'xmm_newton', 'suffix': '.fit'},
            # infrared observation
            'wise': {'folder_name': 'wise', 'file_name': 'wise', 'suffix': '.fits'},
            'iras': {'folder_name': 'IRAS', 'file_name': 'Iras_extra_galactic_catalog', 'suffix': '.fit'},
            'herschel_atlas_north_dr2': {'folder_name': 'herschel', 'file_name': 'herschel_atlas_dr2_north',
                                         'suffix': '.fit'},
            'herschel_atlas_south_dr2': {'folder_name': 'herschel', 'file_name': 'herschel_atlas_dr2_south',
                                         'suffix': '.fit'},
            'hermes_spire_dr4': {'folder_name': 'herschel', 'file_name': 'hermes_spire_dr4', 'suffix': '.fit'},
            'hermes_pacs_dr4': {'folder_name': 'herschel', 'file_name': 'hermes_pacs_dr4', 'suffix': '.fit'},
            'herschel_spire_stripe_82': {'folder_name': 'herschel', 'file_name': 'herschel_spire_stripe_82',
                                         'suffix': '.fit'},
            'hermes_spire_cosmos': {'folder_name': 'herschel', 'file_name': 'hermes_spire_cosmos', 'suffix': '.fit'},
            'herschel_combined': {'folder_name': 'herschel', 'file_name': 'herschel_combined', 'suffix': '.fit'},
            'herschel': {'folder_name': 'herschel', 'file_name': 'herschel', 'suffix': '.fit'},
            'lilly_2007': {'folder_name': 'lilly_2007', 'file_name': 'lilly_2007', 'suffix': '.fit'},
            'casey_2012': {'folder_name': 'casey_2012', 'file_name': 'casey_2012', 'suffix': '.fit'},
            # radio observation
            'vlass': {'folder_name': 'vlass', 'file_name': 'vlass', 'suffix': '.fits'},
            'nvss': {'folder_name': 'nvss', 'file_name': 'nvss', 'suffix': '.fit'},
            'first': {'folder_name': 'FIRST', 'file_name': 'first_14_dec_17', 'suffix': '.fit'},
            'lotss_dr1': {'folder_name': 'lotss', 'file_name': 'LoTSS_DR1', 'suffix': '.fit'},
            'lotss_dr2': {'folder_name': 'lotss', 'file_name': 'LoTSS_DR2_v100', 'suffix': '.gaus.fits'},
            'schinnerer_2007': {'folder_name': 'schinnerer_2007', 'file_name': 'schinnerer_2007', 'suffix': '.fits'},
            # measurements of stellar properties (SFR m_star etc)
            'mpa': {'folder_name': 'mpa', 'file_name': 'mpa', 'suffix': '.fits'},
            'gswlc_a_v1': {'folder_name': 'gswlc/v1', 'file_name': 'gswlc_a_v1', 'suffix': '.fits'},
            'gswlc_m_v1': {'folder_name': 'gswlc/v1', 'file_name': 'gswlc_m_v1', 'suffix': '.fits'},
            'gswlc_d_v1': {'folder_name': 'gswlc/v1', 'file_name': 'gswlc_d_v1', 'suffix': '.fits'},
            # Meert et al. 2014 (two dimensional decomposition profile fit)
            'meert_2014': {'folder_name': 'meert_2014', 'file_name': 'meert_2014_r_band_best_fit', 'suffix': '.fits'},
            # Dominguez-Sanchez et al. 2018 morphological classification with DNN
            'dominguez_sanchez_2018': {'folder_name': 'dominguez_sanchez_2018', 'file_name': 'morph_SDSS_DR7',
                                       'suffix': '.fit'},
            # MaNGA DR15 catalog
            'manga': {'folder_name': 'manga', 'file_name': 'drpall-v2_4_3_modified', 'suffix': '.fits'},
            'manga_original': {'folder_name': 'manga', 'file_name': 'drpall-v2_4_3', 'suffix': '.fits'},
            # MaNGA DR14 value-added catalogue
            'graham_2018': {'folder_name': 'graham_2018', 'file_name': 'graham_2018', 'suffix': '.fits'},
            # MaNGA DR16 value-added catalogues
            'firefly': {'folder_name':'firefly', 'file_name': 'manga_firefly-v2_4_3', 'suffix': '.fits'},
            'manga-morphology': {'folder_name': 'mdlm', 'file_name': 'manga-morphology-dl-DR15', 'suffix': '.fits'},
            'manga-pymorph-gband': {'folder_name': 'pymorph', 'file_name': 'manga-pymorph-DR15_gband', 'suffix': '.fits'},
            'manga-pymorph-rband': {'folder_name': 'pymorph', 'file_name': 'manga-pymorph-DR15_rband', 'suffix': '.fits'},
            'manga-pymorph-iband': {'folder_name': 'pymorph', 'file_name': 'manga-pymorph-DR15_iband', 'suffix': '.fits'},
            'pipe3d': {'folder_name': 'pipe3d', 'file_name': 'manga_Pipe3D-v2_4_3', 'suffix': '.fits'},
            'hi-manga': {'folder_name': 'hi_manga', 'file_name': 'mangaHIall', 'suffix': '.fits'},
            # MaNGA DR17 catalog
            'manga-dr17': {'folder_name': 'manga', 'file_name': 'drpall-v3_1_1_modified', 'suffix': '.fits'},
            'manga_original-dr17': {'folder_name': 'manga', 'file_name': 'drpall-v3_1_1', 'suffix': '.fits'},
            # MaNGA DR17 value-added catalogues
            'manga-morphology-dr17': {'folder_name': 'mdlm', 'file_name': 'manga-morphology-dl-DR17', 'suffix': '.fits'},
            'manga-pymorph-dr17-gband': {'folder_name': 'pymorph', 'file_name': 'manga-pymorph-DR17_gband', 'suffix': '.fits'},
            'manga-pymorph-dr17-rband': {'folder_name': 'pymorph', 'file_name': 'manga-pymorph-DR17_rband', 'suffix': '.fits'},
            'manga-pymorph-dr17-iband': {'folder_name': 'pymorph', 'file_name': 'manga-pymorph-DR17_iband', 'suffix': '.fits'},
            'pipe3d-dr17': {'folder_name': 'pipe3d', 'file_name': 'SDSS17Pipe3D_v3_1_1', 'suffix': '.fits'},
            'hi-manga-dr3': {'folder_name': 'hi_manga', 'file_name': 'mangaHIall', 'suffix':'.fits'},
            'manga-gema-dr17': {'folder_name': 'GEMA', 'file_name': 'GEMA_param_1Mpc_z015', 'suffix': '.fits'},
            # galaxy groups Yang et al. 2007 and Saulder et al. 2016
            'saulder_2016': {'folder_name': 'saulder_2016', 'file_name': 'galaxy_table', 'suffix': '.fit'},
            'saulder_2016_group': {'folder_name': 'saulder_2016', 'file_name': 'group_table', 'suffix': '.fit'},
            'yang_2007': {'folder_name': 'yang_2007', 'file_name': 'yang_2007_galaxy', 'suffix': '.fits'},
            'yang_2007_group': {'folder_name': 'yang_2007', 'file_name': 'yang_2007_groups', 'suffix': '.fits'},
            # filaments in the SDSS Tempel et al. 2014
            'tempel_filament': {'folder_name': 'tempel_2014', 'file_name': 'filament_properties', 'suffix': '.fit'},
            'tempel_filament_points': {'folder_name': 'tempel_2014', 'file_name': 'filament_point_properties',
                                       'suffix': '.fit'},
            'tempel_galaxies': {'folder_name': 'tempel_2014', 'file_name': 'filament_galaxies', 'suffix': '.fit'},
            # H2 and HI gass
            'xcoldgass': {'folder_name': 'xcoldgass', 'file_name': 'xcoldgass', 'suffix': '.fits'},
            'alfalfa': {'folder_name': 'alfalfa', 'file_name': 'alfalfa_in_rcsed', 'suffix': '.fit'},
            'springob': {'folder_name': 'springob_2005', 'file_name': 'springob_2005', 'suffix': '.fit'},
            'xgass': {'folder_name': 'xgass', 'file_name': 'xGASS_representative_sample', 'suffix': '.fits'},
            'mascot': {'folder_name': 'mascot', 'file_name': 'MASCOT', 'suffix': '.fits'},
            'almaquest': {'folder_name': 'almaquest', 'file_name':'J_ApJ_903_145_table3.dat', 'suffix':'.fits'},
            # HI gas estimation
            'teimoorinja': {'folder_name': 'teimoorinja_2017', 'file_name': 'teimoorinja', 'suffix': '.fit'},
            'legac_dr2': {'folder_name': 'legac', 'file_name': 'legac_dr2', 'suffix': '.fits'},
            # CS created for DP/MaNGA galaxies
            'cs_manga_radius': {'folder_name': 'cs_manga', 'file_name': 'cs_manga_radius', 'suffix': '.fits'},
            'cs_manga_topcat': {'folder_name': 'cs_manga', 'file_name': 'cs_manga_topcat', 'suffix': '.fits'},
            'cs1_manga_dr17' : {'folder_name': 'cs_manga_dr17', 'file_name': 'cs1_manga_dr17', 'suffix': '.fits'},
            'cs2_manga_dr17' : {'folder_name': 'cs_manga_dr17', 'file_name': 'cs2_manga_dr17', 'suffix': '.fits'},
            # proposal project
            'long_slit_prob': {'folder_name': 'long_slit_prob', 'file_name': 'long_slit_prob', 'suffix': '.fits'}



        }

        # CO archive tables
        co_archive_tables = {'young_2011': {'folder_name': 'young_2011',
                                            'file_name': 'young_2011_atlas3d_co_sample',
                                            'suffix': '.fits'},
                             'bauermeister_2013': {'folder_name': 'bauermeister_2013',
                                                   'file_name': 'bauermeister_2013',
                                                   'suffix': '.fits'},
                             'chung_2009': {'folder_name': 'chung_2009',
                                            'file_name': 'chung_2009',
                                            'suffix': '.fits'},
                             'solomon_1997': {'folder_name': 'solomon_1997',
                                              'file_name': 'solomon_1997',
                                              'suffix': '.fits'},
                             'combes_2011': {'folder_name': 'combes_2011',
                                             'file_name': 'combes_2011',
                                             'suffix': '.fits'},
                             'combes_2013': {'folder_name': 'combes_2013',
                                             'file_name': 'combes_2013',
                                             'suffix': '.fits'},
                             'davis_2015': {'folder_name': 'davis_2015',
                                            'file_name': 'davis_2015',
                                            'suffix': '.fits'},
                             'french_2015': {'folder_name': 'french_2015',
                                                    'file_name': 'french_2015',
                                                    'suffix': '.fits'},
                             'geach_2009': {'folder_name': 'geach_2009',
                                            'file_name': 'geach_2009',
                                            'suffix': '.fits'},
                             'luo_2020': {'folder_name': 'luo_2020',
                                          'file_name': 'luo_2020',
                                          'suffix': '.fits'},
                             'tacconi_2013': {'folder_name': 'tacconi_2013',
                                                 'file_name': 'tacconi_2013',
                                                 'suffix': '.fits'},
                             'tacconi_2018': {'folder_name': 'tacconi_2018',
                                              'file_name': 'tacconi_2018',
                                              'suffix': '.fits'},
                             'freundlich_2019': {'folder_name': 'freundlich_2019',
                                                 'file_name': 'freundlich_2019',
                                                 'suffix': '.fits'},
                             'schawinski_2009': {'folder_name': 'schawinski_2009',
                                                 'file_name': 'schawinski_2009',
                                                 'suffix': '.fits'}}

        # CO observation made by the authors
        co_observation = {'maschmann_198_19': {'folder_name': 'maschmann_198_19',
                                               'file_name': 'maschmann_198_19',
                                               'suffix': '.fits'},
                          'co_observation': {'folder_name': 'co_observation',
                                               'file_name': 'co_observation',
                                               'suffix': '.fits'},
                          'fit_co': {'folder_name': 'fit_co',
                                               'file_name': 'fit_co',
                                               'suffix': '.fits'},
                          'maschmann_166_20_dp': {'folder_name': 'maschmann_166_20_dp',
                                               'file_name': 'maschmann_166_20_dp',
                                               'suffix': '.fits'},
                          'maschmann_166_20_pathfinder_dp': {'folder_name': 'maschmann_166_20_pathfinder_dp',
                                               'file_name': 'maschmann_166_20_pathfinder_dp',
                                               'suffix': '.fits'},
                          'maschmann_166_20_cs': {'folder_name': 'maschmann_166_20_cs',
                                               'file_name': 'maschmann_166_20_cs',
                                               'suffix': '.fits'},
                          'iram_run_166_20': {'folder_name': 'iram_run_166_20',
                                               'file_name': 'iram_run_166_20',
                                               'suffix': '.fits'},
                          'dp_manga_co' : {'folder_name': 'dp_manga_co',
                                            'file_name': 'dp_manga_co',
                                            'suffix': '.fits'},
                          'ngc3221_central_co' : {'folder_name': 'ngc3221_central_co',
                                                  'file_name': 'ngc3221_central_co',
                                                  'suffix': '.fits'},
                          'ngc3221_disk_co' : {'folder_name': 'ngc3221_disk_co',
                                               'file_name': 'ngc3221_disk_co',
                                               'suffix': '.fits'}}

        # project tables which are used in some local projects. This might change
        project_tables = {
            'atomic_and_molecular_control_sample': {'folder_name': 'atomic_and_molecular_control_sample',
                                                    'file_name': 'atomic_and_molecular_control_sample',
                                                    'suffix': '.fits'},
            'molecular_control_sample': {'folder_name': 'molecular_control_sample',
                                         'file_name': 'molecular_control_sample', 'suffix': '.fits'},
            'dp_manga': {'folder_name': 'dp_manga', 'file_name': 'dp_manga', 'suffix': '.fits'},
            'dp_manga_dr17': {'folder_name': 'dp_manga', 'file_name': 'dp_manga_dr17', 'suffix': '.fits'},
            'nbcs_manga': {'folder_name': 'nbcs_manga', 'file_name': 'nbcs_manga', 'suffix': '.fits'},
            'sf_radio_co': {'folder_name': 'sf_radio_co', 'file_name': 'sf_radio_co', 'suffix': '.fits'}
        }

        # update project tables and archives into table_names
        self.table_identifier.update(project_tables)
        self.table_identifier.update(co_archive_tables)
        self.table_identifier.update(co_observation)

        # to doo list
        """
        HI surveys in Orellana et al. (2017) DOI: 10.1051/0004-6361/201629009
        Table 2. HI surveys used in this work.
        Sample
        HiPASS References
        Koribalski et al. (2004), Meyer et al. (2004),
        Wong et al. (2006), Doyle et al. (2005)
        HiJASS Lang et al. (2003)
        ALFALFA Haynes et al. (2011), Martin et al. (2009),
        Kent et al. (2008), Saintonge et al. (2008),
        Saintonge (2007), Giovanelli et al. (2005)
        Others
        Springob et al. (2005)
        """
        """
        Access to MEGACAM images from the french canadian telescope on Hawaii
        https://www.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/en/megapipe/access/cut.html
        """


class PhysicalConstants:

    def __init__(self):
        """
        This class provides fundamental physical constants and parameters.
        """
        super().__init__()
        '''
        we use a standard cosmology to relate measured redshift to the
        distances depending on model specifications.
        '''
        self.cosmology = LambdaCDM(H0=70.0, Om0=0.3, Ode0=0.7)

        '''
        ionized emission line wavelength
        the vacuum wavelength are taken from the nburst fitting procedure under spiker/client/lib/eml_list_gen.pro
        the air wavelength are taken from http://astronomy.nmsu.edu/drewski/tableofemissionlines.html
        further information:
        overview on most vacuum wavelength in SDSS spectra http://classic.sdss.org/dr6/algorithms/linestable.html
        see also from the pyneb package
        or look a more detailed data base at at https://physics.nist.gov/PhysRefData/ASD/lines_form.html
        the wavelength are in :math: `\\AA´
        the emission line names are the vacuum wavelength rounded to integer
        '''
        self.em_wavelength = {3347: {'line_name': 'nev',
                                     'transition': 'forbidden',
                                     'air_wave': 3345.821,
                                     'vac_wave': 3346.783,
                                     'nbursts_name': '[Ne V] 3346'},
                              3427: {'line_name': 'nev',
                                     'transition': 'forbidden',
                                     'air_wave': 3425.881,
                                     'vac_wave': 3426.863,
                                     'nbursts_name': '[Ne V] 3426'},
                              3727: {'line_name': 'oii',
                                     'transition': 'forbidden',
                                     'air_wave': 3726.032,
                                     'vac_wave': 3727.092,
                                     'nbursts_name': '[O II] 3727',
                                     'rcsed_name': 'OII3727.09'},
                              3730: {'line_name': 'oii',
                                     'transition': 'forbidden',
                                     'air_wave': 3728.815,
                                     'vac_wave': 3729.875,
                                     'nbursts_name': '[O II] 3729',
                                     'rcsed_name': 'OII3729.88'},
                              3751: {'line_name': 'h_kappa',
                                     'transition': 'balmer',
                                     'air_wave': 3750.158,
                                     'vac_wave': 3751.224,
                                     'nbursts_name': 'H12 3751',
                                     'rcsed_name': 'H_kappa3751.22'},
                              3772: {'line_name': 'h_iota',
                                     'transition': 'balmer',
                                     'air_wave': 3770.637,
                                     'vac_wave': 3771.708,
                                     'nbust_name': 'H11 3771',
                                     'rcsed_name': 'H_iota3771.70'},
                              3799: {'line_name': 'h_theta',
                                     'transition': 'forbidden',
                                     'air_wave': 3797.904,
                                     'vac_wave': 3798.982,
                                     'nbursts_name': 'H10 3798',
                                     'rcsed_name': 'H_theta3798.98'},
                              3836: {'line_name': 'h_eta',
                                     'transition': 'balmer',
                                     'air_wave': 3835.391,
                                     'vac_wave': 3836.479,
                                     'nbursts_name': 'H9 3836',
                                     'rcsed_name': 'H_eta3836.47'},
                              3870: {'line_name': 'neiii',
                                     'transition': 'forbidden',
                                     'air_wave': 3868.760,
                                     'vac_wave': 3869.857,
                                     'nbursts_name': '[Ne III] 3869',
                                     'rcsed_name': 'NeIII3869.86'},
                              3889: {'line_name': 'hei',
                                     'transition': 'forbidden',
                                     'air_wave': 3888.647,
                                     'vac_wave': 3889.749,
                                     'nbursts_name': 'He I 3889',
                                     'rcsed_name': 'HeI3889.0'},
                              3890: {'line_name': 'h_dzita',
                                     'transition': 'balmer',
                                     'air_wave': 3889.064,
                                     'vac_wave': 3890.166,
                                     'nbursts_name': 'H8 3890',
                                     'rcsed_name': 'H_dzita3890.17'},
                              3971: {'line_name': 'h_epsilon',
                                     'transition': 'balmer',
                                     'air_wave': 3970.079,
                                     'vac_wave': 3971.202,
                                     'nbursts_name': 'H epsilon',
                                     'rcsed_name': 'H_epsilon3971.20'},
                              4070: {'line_name': 'sii',
                                     'transition': 'forbidden',
                                     'air_wave': 4068.600,
                                     'vac_wave': 4069.749,
                                     'nbursts_name': '[S II] 4069',
                                     'rcsed_name': 'SII4069.75'},
                              4078: {'line_name': 'sii',
                                     'transition': 'forbidden',
                                     'air_wave': 4076.349,
                                     'vac_wave': 4077.500,
                                     'nbursts_name': '[S II] 4077',
                                     'rcsed_name': 'SII4077.50'},
                              4103: {'line_name': 'h_delta',
                                     'transition': 'balmer',
                                     'air_wave': 4101.742,
                                     'vac_wave': 4102.900,
                                     'nbursts_name': 'H delta',
                                     'rcsed_name': 'H_delta4102.89'},
                              4342: {'line_name': 'h_gamma',
                                     'transition': 'balmer',
                                     'air_wave': 4340.471,
                                     'vac_wave': 4341.692,
                                     'nbursts_name': 'H gamma',
                                     'rcsed_name': 'H_gamma4341.68'},
                              4364: {'line_name': 'oiii',
                                     'transition': 'forbidden',
                                     'air_wave': 4363.210,
                                     'vac_wave': 4364.437,
                                     'nbursts_name': '[O III] 4364',
                                     'rcsed_name': 'OIII4364.44'},
                              4687: {'line_name': 'heii',
                                     'transition': 'forbidden',
                                     'air_wave': 4685.710,
                                     'vac_wave': 4687.022,
                                     'nbursts_name': 'He II 4687',
                                     'rcsed_name': 'HeII4687.07'},
                              4713: {'line_name': 'ariv',
                                     'transition': 'forbidden',
                                     'air_wave': 4711.260,
                                     'vac_wave': 4712.579,
                                     'nbursts_name': '[Ar IV] 4712',
                                     'rcsed_name': 'ArIV4712.69'},
                              4742: {'line_name': 'ariv',
                                     'transition': 'forbidden',
                                     'air_wave': 4740.120,
                                     'vac_wave': 4741.447,
                                     'nbursts_name': '[Ar IV] 4741',
                                     'rcsed_name': 'ArIV4741.50'},
                              4863: {'line_name': 'h_beta',
                                     'transition': 'balmer',
                                     'air_wave': 4861.333,
                                     'vac_wave': 4862.692,
                                     'nbursts_name': 'H beta',
                                     'rcsed_name': 'H_beta4862.72',
                                     'manga_name': 'hb_4862',
                                     'plot_name': r'H$\beta$'},
                              4960: {'line_name': 'oiii',
                                     'transition': 'forbidden',
                                     'air_wave': 4958.911,
                                     'vac_wave': 4960.296,
                                     'nbursts_name': '[O III] 4960',
                                     'rcsed_name': 'OIII4960.30',
                                     'manga_name': 'oiii_4960',
                                     'plot_name': r'[OIII]4960'},
                              5008: {'line_name': 'oiii',
                                     'transition': 'forbidden',
                                     'air_wave': 5006.843,
                                     'vac_wave': 5008.241,
                                     'nbursts_name': '[O III] 5008',
                                     'rcsed_name': 'OIII5008.24',
                                     'manga_name': 'oiii_5007',
                                     'plot_name': r'[OIII]5008'},
                              5199: {'line_name': 'fe ii',
                                     'transition': 'forbidden',
                                     'air_wave': 5197.577,
                                     'vac_wave': 5199.026,
                                     'nbursts_name': 'Fe II 5199'},
                              5202: {'line_name': 'ni',
                                     'transition': 'forbidden',
                                     'air_wave': 5200.257,
                                     'vac_wave': 5201.707,
                                     'nbursts_name': '[N I] 5201',
                                     'rcsed_name': 'NI5199.35'},
                              5756: {'line_name': 'nii',
                                     'transition': 'forbidden',
                                     'air_wave': 5754.590,
                                     'vac_wave': 5756.188,
                                     'nbursts_name': '[N II] 5756',
                                     'rcsed_name': 'NII5756.19'},
                              5877: {'line_name': 'hei',
                                     'transition': 'forbidden',
                                     'air_wave': 5875.624,
                                     'vac_wave': 5877.255,
                                     'nbursts_name': 'He I 5877',
                                     'rcsed_name': 'HeI5877.25'},
                              6302: {'line_name': 'oi',
                                     'transition': 'forbidden',
                                     'air_wave': 6300.304,
                                     'vac_wave': 6302.049,
                                     'nbursts_name': '[O I] 6302',
                                     'rcsed_name': 'OI6302.05',
                                     'manga_name': 'oi_6302',
                                     'plot_name': r'[OI]6302'},
                              6314: {'line_name': 'siii',
                                     'transition': 'forbidden',
                                     'air_wave': 6312.060,
                                     'vac_wave': 6313.808,
                                     'nbursts_name': '[S III] 6313',
                                     'plot_name': r'[SIII]6312'},
                              6349: {'line_name': 'si ii',
                                     'transition': 'forbidden',
                                     'air_wave': 6347.100,
                                     'vac_wave': 6348.858,
                                     'nbursts_name': 'Si II 6348',
                                     'plot_name': r'Si II6312'},
                              6366: {'line_name': 'oi',
                                     'transition': 'forbidden',
                                     'air_wave': 6363.776,
                                     'vac_wave': 6365.538,
                                     'nbursts_name': '[O I] 6365',
                                     'rcsed_name': 'OI6365.54'},
                              6550: {'line_name': 'nii',
                                     'transition': 'forbidden',
                                     'air_wave': 6548.050,
                                     'vac_wave': 6549.862,
                                     'nbursts_name': '[N II] 6549',
                                     'rcsed_name': 'NII6549.86',
                                     'manga_name': 'nii_6549',
                                     'plot_name': r'[NII]6550'},
                              6565: {'line_name': 'h_alpha',
                                     'transition': 'balmer',
                                     'air_wave': 6562.819,
                                     'vac_wave': 6564.635,
                                     'nbursts_name': 'H alpha',
                                     'rcsed_name': 'H_alpha6564.61',
                                     'manga_name': 'ha_6564',
                                     'plot_name': r'H$\alpha$'},
                              6585: {'line_name': 'nii',
                                     'transition': 'forbidden',
                                     'air_wave': 6583.460,
                                     'vac_wave': 6585.282,
                                     'nbursts_name': '[N II] 6585',
                                     'rcsed_name': 'NII6585.27',
                                     'manga_name': 'nii_6585',
                                     'plot_name': r'[NII]6585'},
                              6718: {'line_name': 'sii',
                                     'transition': 'forbidden',
                                     'air_wave': 6716.440,
                                     'vac_wave': 6718.298,
                                     'nbursts_name': '[S II] 6718',
                                     'rcsed_name': 'SII6718.29',
                                     'manga_name': 'sii_6718',
                                     'plot_name': r'[SII]6718'},
                              6733: {'line_name': 'sii',
                                     'transition': 'forbidden',
                                     'air_wave': 6730.810,
                                     'vac_wave': 6732.671,
                                     'nbursts_name': '[S II] 6732',
                                     'rcsed_name': 'SII6732.67',
                                     'manga_name': 'sii_6732',
                                     'plot_name': r'[SII]6733'}}

        self.new_nbursts_line_names = {'Mg II] 2796': 2796,
                                       'Mg II] 2803': 2803,
                                       '[O II] 3727': 3727,
                                       '[O II] 3729': 3729,
                                       'H epsilon': 3971,
                                       'H delta': 4103,
                                       'H gamma': 4342,
                                       'H beta': 4863,
                                       '[O III] 4960': 4960,
                                       '[O III] 5008': 5008,
                                       '[O I] 6302': 6302,
                                       '[O I] 6365': 6365,
                                       '[N II] 6549': 6549,
                                       'H alpha': 6565,
                                       '[N II] 6585': 6585,
                                       '[S II] 6718': 6718,
                                       '[S II] 6732': 6733,
                                       '[S III] 9071': 9071,
                                       '[S III] 9533': 9533,
                                       'Pa epsilon': 9549,
                                       'Pa delta': 10052,
                                       'Pa gamma': 10941}


        # theoretical emission line ratios taken from the python package pyneb
        # print(pyneb.Atom('O', 3).getHighDensRatio(wave1=5007, wave2=4959))
        # print(pyneb.Atom('N', 2).getHighDensRatio(wave1=6583, wave2=6548))
        # print(pyneb.Atom('O', 1).getHighDensRatio(wave1=6300, wave2=6363))
        self.em_ratios = {'6585/6550': 2.958075322302304,
                          '5008/4960': 3.0128111622091343,
                          '6302/6366': 3.09983543609435}

        # taken from Table 1 in Jarrett et al. 2011 735:112 doi:10.1088/0004-637X/735/2/112
        # the bandwidth in wavelength are in mu m  and the frequency are in Hz
        self.wise_bandwidth = {'W1': {'wavelength': 6.6256e-01, 'wavelength_err': 1.2168e-03,
                                      'frequency': 1.7506e+13, 'frequency_err': 3.0407e+10},
                               'W2': {'wavelength': 1.0423e+00, 'wavelength_err': 1.0982e-03,
                                      'frequency': 1.4653e+13, 'frequency_err': 1.1759e+10},
                               'W3': {'wavelength': 5.5069e+00, 'wavelength_err': 1.6942e-02,
                                      'frequency': 1.1327e+13, 'frequency_err': 8.5791e+09},
                               'W4': {'wavelength': 4.1013e+00, 'wavelength_err': 4.4812e-02,
                                      'frequency': 2.4961e+12, 'frequency_err': 4.0207e+09}}

        # the band positions in wavelength are in nm m  and the frequency are in Hz
        self.wise_band_pass_filter_position = {'W1': {'wavelength': 3.3526 * 1e3, 'wavelength_err': 0.0132 * 1e3,
                                                      'frequency': 8.8560e+13, 'frequency_err': 7.2306e+11},
                                               'W2': {'wavelength': 4.6028 * 1e3, 'wavelength_err': 0.0168 * 1e3,
                                                      'frequency': 6.4451e+13, 'frequency_err': 5.0629e+11},
                                               'W3': {'wavelength': 11.5608 * 1e3, 'wavelength_err': 0.0446 * 1e3,
                                                      'frequency': 2.6753e+13, 'frequency_err': 1.9731e+11},
                                               'W4': {'wavelength': 22.0883 * 1e3, 'wavelength_err': 0.1184 * 1e3,
                                                      'frequency': 1.3456e+13, 'frequency_err': 1.0049e+11}}

        # the band pass filter positions in wavelength are in nm frequency in Hz
        self.iras_band_pass_filter_position = {'12mu': {'wavelength': 12 * 1e3,
                                                        'frequency': speed_of_light / (12 * 1e-6)},
                                               '25mu': {'wavelength': 25 * 1e3,
                                                        'frequency': speed_of_light / (25 * 1e-6)},
                                               '60mu': {'wavelength': 60 * 1e3,
                                                        'frequency': speed_of_light / (60 * 1e-6)},
                                               '100mu': {'wavelength': 100 * 1e3,
                                                         'frequency': speed_of_light / (100 * 1e-6)}}

        # the band pass filter positions in wavelength are in nm frequency in Hz
        self.herschel_band_pass_filter_position = {'100mu': {'wavelength': 100 * 1e3,
                                                             'frequency': speed_of_light / (100 * 1e-6)},
                                                   '160mu': {'wavelength': 160 * 1e3,
                                                             'frequency': speed_of_light / (160 * 1e-6)},
                                                   '250mu': {'wavelength': 250 * 1e3,
                                                             'frequency': speed_of_light / (250 * 1e-6)},
                                                   '350mu': {'wavelength': 350 * 1e3,
                                                             'frequency': speed_of_light / (350 * 1e-6)},
                                                   '500mu': {'wavelength': 500 * 1e3,
                                                             'frequency': speed_of_light / (500 * 1e-6)}}

        self.hi_vac_wave = 21.1061140542  # in cm taken from Hydrogen line Wikipedia page
        self.hi_vacuum_frequency = 1420405751.7667  # in Hz taken from Hydrogen line Wikipedia page

        # source: IRAM
        self.co10_line_frequency = 115.271203  # in GHz
        self.co10_vac_wave = speed_of_light / self.co10_line_frequency * 10  # in angstrom
        self.co21_line_frequency = 230.538001  # in GHz
        self.co21_vac_wave = speed_of_light / self.co21_line_frequency * 10  # in angstrom
        # source: Spliker et al 2014 doi:10.1088/0004-637X/785/2/149
        # table 2
        self.co32_line_frequency = 345.7960  # in GHz
        self.co43_line_frequency = 461.040  # in GHz

        # IRAM telescope location of the 30m telescope taken from Wikipedia
        self.iram_30m_logaction = {'lat': 37.066161, 'lon': -3.392719, 'height': 2850}
        self.iram_noema_logaction = {'lat': 44.63389, 'lon': 5.90792, 'height': 2552}

        # band pass position of the VLASS survey is around 3 GHz LOOK FOR REFERENCES
        self.vlass_band_pass_filter_position = {'wavelength': speed_of_light / (3.0 * 1e9) * 1e9,
                                                'frequency': 3.0 * 1e9}

        # band pass position of the FIRST survey is between 1365 and 1435 MHz see White et al. 1997
        self.first_band_pass_filter_position = {'wavelength': speed_of_light / (1.4 * 1e9) * 1e9,
                                                'frequency': 1.4 * 1e9}

        # band pass position of the LOFAR survey is between 120-168 MHz see Shimwell et al. 2019
        self.lofar_band_pass_filter_position = {'wavelength': speed_of_light / (144 * 1e6) * 1e9,
                                                'frequency': 144 * 1e6}

        # band pass position taken from
        # http://svo2.cab.inta-csic.es/svo/theory/fps3/index.php?id=SLOAN/SDSS.uprime_filter&&mode=browse&gname=SLOAN&gname2=SDSS#filter
        # GALEX
        self.galex_band_pass_filter_position = {'FUV': {'wavelength': 154.58,
                                                        'frequency': speed_of_light / (1545.8 * 1e-9)},
                                                'NUV': {'wavelength': 234.49,
                                                        'frequency': speed_of_light / (2344.9 * 1e-9)}}
        # SDSS in nm
        self.sdss_band_pass_filter_position = {'u': {'wavelength': 356.18,
                                                     'frequency': speed_of_light / (356.18 * 1e-9)},
                                               'g': {'wavelength': 471.89,
                                                     'frequency': speed_of_light / (471.89 * 1e-9)},
                                               'r': {'wavelength': 618.52,
                                                     'frequency': speed_of_light / (618.52 * 1e-9)},
                                               'i': {'wavelength': 749.97,
                                                     'frequency': speed_of_light / (749.97 * 1e-9)},
                                               'z': {'wavelength': 896.15,
                                                     'frequency': speed_of_light / (896.15 * 1e-9)}}

        # UKIDSS in nm
        self.ukidss_band_pass_filter_position = {'Z': {'wavelength': 883.95,
                                                       'frequency': speed_of_light / (883.95 * 1e-9)},
                                                 'Y': {'wavelength': 1032.79,
                                                       'frequency': speed_of_light / (1032.79 * 1e-9)},
                                                 'J': {'wavelength': 1252.81,
                                                       'frequency': speed_of_light / (1252.81 * 1e-9)},
                                                 'H': {'wavelength': 1642.28,
                                                       'frequency': speed_of_light / (1642.28 * 1e-9)},
                                                 'K': {'wavelength': 2213.18,
                                                       'frequency': speed_of_light / (2213.18 * 1e-9)}}

        # chandra band pas borders in eV
        self.chandra_band_pass_filter_borders = {'b': {'min': 0.5 * 1e3, 'max': 7.0 * 1e3},
                                                 'h': {'min': 2.0 * 1e3, 'max': 7.0 * 1e3},
                                                 'm': {'min': 1.2 * 1e3, 'max': 2.0 * 1e3},
                                                 's': {'min': 0.5 * 1e3, 'max': 1.2 * 1e3},
                                                 'u': {'min': 0.2 * 1e3, 'max': 0.5 * 1e3},
                                                 'w': {'min': 0.1 * 1e3, 'max': 10.0 * 1e3}}

        # XMM Newton band pas borders in eV
        self.xmm_newton_band_pass_filter_borders = {'band8': {'min': 0.2 * 1e3, 'max': 12.0 * 1e3}}

        # band pass position of Chandra observation
        # wavelength in nm and frequency in Hz
        self.chandra_band_pass_filter_position = {
            'b': {'wavelength': plank * speed_of_light / (3.75 * 1e3 * electron_charge) * 1e9,
                  'frequency': (3.75 * 1e3 * electron_charge) / plank},
            'h': {'wavelength': plank * speed_of_light / (4.5 * 1e3 * electron_charge) * 1e9,
                  'frequency': (4.5 * 1e3 * electron_charge) / plank},
            'm': {'wavelength': plank * speed_of_light / (1.6 * 1e3 * electron_charge) * 1e9,
                  'frequency': (1.6 * 1e3 * electron_charge) / plank},
            's': {'wavelength': plank * speed_of_light / (0.85 * 1e3 * electron_charge) * 1e9,
                  'frequency': (0.85 * 1e3 * electron_charge) / plank},
            'u': {'wavelength': plank * speed_of_light / (0.35 * 1e3 * electron_charge) * 1e9,
                  'frequency': (0.35 * 1e3 * electron_charge) / plank},
            'w': {'wavelength': plank * speed_of_light / (5.05 * 1e3 * electron_charge) * 1e9,
                  'frequency': (5.05 * 1e3 * electron_charge) / plank}
        }

        # band pass observation of XMM Newton observation
        # in nm
        self.xmm_newton_band_pass_filter_position = {
            'band8': {'wavelength': plank * speed_of_light / (6.1 * 1e3 * electron_charge) * 1e9,
                      'frequency': (6.1 * 1e3 * electron_charge) / plank}}


class PublishingQualityParameters:

    def __init__(self):

        super().__init__()

        self.fig_size_x_axis_spec_plot = 18
        self.fig_size_y_axis_spec_plot = 8
        self.fontsize_spec = 16
        self.fontsize_narrow_full_page_spec = 14
        self.line_width = 3

        # declare font sizes for plotting scripts in publishing quality
        # self.fig_size_x_axis_half_page = 10
        # self.fig_size_x_axis_full_page = 20
        #
        # self.fig_size_y_axis_small_hist = 4
        # self.fig_size_y_axis_spec = 6.5
        # self.fig_size_y_axis_small_scatter = 8
        # self.fig_size_y_axis_one_third_page = 11.5
        # self.fig_size_y_axis_half_page = 13
        #
        # self.fontsize_small_hist = 16
        # self.fontsize_spec = 19
        # self.fontsize_small_scatter = 20
        # self.fontsize_one_third_page = 20
        # self.fontsize_half_page = 20
        #
        # self.line_width_small_hist = 3
        # self.line_width_small_scatter = 3
        # self.line_width_half_page = 3
        # self.line_width = 3
        # self.marker_size = 5
        # self.marker_size_small_scatter = 20
        # self.marker_size_half_page = 40
        # self.marker_size_one_third_page = 40


def download_file(file_path, file_name, url, unpack=False, reload=False):
    r"""
    Function to download file and save to filepath.
    :param file_path: the data_path directly to the data file
    :param file_name: file name
    :param url: the url where to download
    :param unpack: flag to unpack file.
    :param reload: If the file is corrupt or should be downloaded again
    :return: None
    """

    if reload:
        # if reload file the file will be removed to re download it
        os.remove(file_path / file_name)
    # check if file already exists
    if os.path.isfile(file_path / file_name):
        print(file_path / file_name, 'already exists')
        return True
    else:
        from urllib3 import PoolManager
        # download file
        http = PoolManager()
        r = http.request('GET', url, preload_content=False)

        if unpack:
            with open((file_path / file_name).with_suffix(".gz"), 'wb') as out:
                while True:
                    data = r.read()
                    if not data:
                        break
                    out.write(data)
            r.release_conn()
            # uncompress file
            from gzip import GzipFile
            # read compressed file
            compressed_file = GzipFile((file_path / file_name).with_suffix(".gz"), 'rb')
            s = compressed_file.read()
            compressed_file.close()
            # save compressed file
            uncompressed_file = open(file_path / file_name, 'wb')
            uncompressed_file.write(s)
            uncompressed_file.close()
            # delete compressed file
            os.remove((file_path / file_name).with_suffix(".gz"))
        else:
            with open(file_path / file_name, 'wb') as out:
                while True:
                    data = r.read()
                    if not data:
                        break
                    out.write(data)
            r.release_conn()

