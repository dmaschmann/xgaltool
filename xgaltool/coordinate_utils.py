import numpy as np

from astroquery.ned import Ned
from astropy.coordinates import SkyCoord
from astropy import units as u


def get_galaxy_ned_coords_and_z(galaxy_name):
    r"""
    Function to return first best matching coordinates (in deg) and redshift of the NED database
    using only the object name.

    """

    # get coordinates and redshift table
    position_table = Ned.get_table(galaxy_name, table='positions')
    redshift_table = Ned.get_table(galaxy_name, table='redshifts')

    # get these objects with the smallest accuracy.
    position_uncertainty = np.sqrt(position_table['Uncertainty Ellipse Semi-Major Axis'] ** 2 +
                                   position_table['Uncertainty Ellipse Semi-Minor Axis'] ** 2)
    # if multiple objects are precise we take the first
    best_position = np.where(position_uncertainty == np.min(position_uncertainty))[0][0]
    # get coordinates
    coordinates = str(position_table['RA'][best_position]) + ' ' + str(position_table['DEC'][best_position])
    coord_table = SkyCoord(coordinates,  unit=(u.hourangle, u.deg))
    ra = coord_table.ra.deg
    dec = coord_table.dec.deg

    # exclude those where the redshift uncertainty is 0.
    redshift_uncertainties_not_zero = redshift_table['Published Redshift Uncertainty'] != 0
    # get redshift measurement with the smallest uncertainty. If multiple we take the first
    best_redshift = np.where(redshift_table['Published Redshift Uncertainty'] == np.min(
        redshift_table['Published Redshift Uncertainty'][redshift_uncertainties_not_zero]))[0][0]
    redshift = redshift_table['Published Redshift'][best_redshift]

    return ra, dec, redshift
