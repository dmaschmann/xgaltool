import numpy as np


class BPTCategorisation:

    def __init__(self):
        r"""
        categorize galaxies using BPT diagrams

        """
    # get ratios
    @staticmethod
    def get_log10_ratio_oiii_h_beta(table, dp=True, peak=1, line_shape='gauss'):
        if dp:
            # oiii_5008_flux_name = 'flux_5008_' + str(peak)
            # h_beta_4863_flux_name = 'flux_4863_' + str(peak)
            oiii_5008_flux_name = 'flux_' + str(peak) +'_oiii_5008'
            h_beta_4863_flux_name = 'flux_' + str(peak) +'_h_beta_4863'
            return np.log10(table[oiii_5008_flux_name] / table[h_beta_4863_flux_name])
        else:
            return np.log10(table['f5008_oiii_flx_%s' % line_shape] / table['f4863_h_beta_flx_%s' % line_shape])

    @staticmethod
    def get_log10_ratio_nii_h_alpha(table, dp=True, peak=1, line_shape='gauss'):
        if dp:
            # nii_6585_flux_name = 'flux_6585_' + str(peak)
            # h_alpha_6565_flux_name = 'flux_6565_' + str(peak)
            nii_6585_flux_name = 'flux_' + str(peak) +'_nii_6585'
            h_alpha_6565_flux_name = 'flux_' + str(peak) +'_h_alpha_6565'
            return np.log10(table[nii_6585_flux_name] / table[h_alpha_6565_flux_name])
        else:
            return np.log10(table['f6585_nii_flx_%s' % line_shape] / table['f6565_h_alpha_flx_%s' % line_shape])

    @staticmethod
    def get_log10_ratio_sii_h_alpha(table, dp=True, peak=1, line_shape='gauss'):
        if dp:
            # sii_6718_flux_name = 'flux_6718_' + str(peak)
            # sii_6733_flux_name = 'flux_6733_' + str(peak)
            # h_alpha_6565_flux_name = 'flux_6565_' + str(peak)
            sii_6718_flux_name = 'flux_' + str(peak) +'_sii_6718'
            sii_6733_flux_name = 'flux_' + str(peak) +'_sii_6733'
            h_alpha_6565_flux_name = 'flux_' + str(peak) +'_h_alpha_6565'
            return np.log10((table[sii_6718_flux_name] + table[sii_6733_flux_name]) / table[h_alpha_6565_flux_name])
        else:
            return np.log10((table['f6718_sii_flx_%s' % line_shape] + table['f6733_sii_flx_%s' % line_shape]) / table['f6565_h_alpha_flx_%s' % line_shape])

    @staticmethod
    def get_log10_ratio_oi_h_alpha(table, dp=True, peak=1, line_shape='gauss'):
        if dp:
            # oi_6302_flux_name = 'flux_6302_' + str(peak)
            # h_alpha_6565_flux_name = 'flux_6565_' + str(peak)
            oi_6302_flux_name = 'flux_' + str(peak) +'_oi_6302'
            h_alpha_6565_flux_name = 'flux_' + str(peak) +'_h_alpha_6565'
            return np.log10(table[oi_6302_flux_name] / table[h_alpha_6565_flux_name])
        else:
            return np.log10(table['f6302_oi_flx_%s' % line_shape] / table['f6565_h_alpha_flx_%s' % line_shape])

    # get ratio_errors with only one uncertainty component
    @staticmethod
    def get_log10_ratio_oiii_err(table, dp=True, peak=1, line_shape='gauss'):
        if dp:
            # oiii_5008_flux_err_name = 'flux_5008_' + str(peak) + '_err'
            # h_beta_4863_flux_name = 'flux_4863_' + str(peak)
            oiii_5008_flux_err_name = 'flux_' + str(peak) +'_err_oiii_5008'
            h_beta_4863_flux_name = 'flux_' + str(peak) +'_h_beta_4863'
            return np.log10(3 * table[oiii_5008_flux_err_name] / table[h_beta_4863_flux_name])
        else:
            return np.log10(3 * table['f5008_oiii_flx_err_%s' % line_shape] / table['f4863_h_beta_flx_%s' % line_shape])

    @staticmethod
    def get_log10_ratio_h_beta_err(table, dp=True, peak=1, line_shape='gauss'):
        if dp:
            # oiii_5008_flux_name = 'flux_5008_' + str(peak)
            # h_beta_4863_flux_err_name = 'flux_4863_' + str(peak) + '_err'
            oiii_5008_flux_name = 'flux_' + str(peak) +'_oiii_5008'
            h_beta_4863_flux_err_name = 'flux_' + str(peak) +'_err_h_beta_4863'
            return np.log10(table[oiii_5008_flux_name] / (3 * table[h_beta_4863_flux_err_name]))
        else:
            return np.log10(table['f5008_oiii_flx_%s' % line_shape] / (3 * table['f4863_h_beta_flx_err_%s' % line_shape]))

    @staticmethod
    def get_log10_ratio_nii_err(table, dp=True, peak=1, line_shape='gauss'):
        if dp:
            # nii_6585_flux_err_name = 'flux_6585_' + str(peak) + '_err'
            # h_alpha_6565_flux_name = 'flux_6565_' + str(peak)
            nii_6585_flux_err_name = 'flux_' + str(peak) +'_err_nii_6585'
            h_alpha_6565_flux_name = 'flux_' + str(peak) +'_h_alpha_6565'
            return np.log10(3 * table[nii_6585_flux_err_name] / table[h_alpha_6565_flux_name])
        else:
            return np.log10(3 * table['f6585_nii_flx_err_%s' % line_shape] / table['f6565_h_alpha_flx_%s' % line_shape])

    @staticmethod
    def get_log10_ratio_h_alpha_err(table, dp=True, peak=1, line_shape='gauss'):
        if dp:
            # nii_6585_flux_name = 'flux_6585_' + str(peak)
            # h_alpha_6565_flux_err_name = 'flux_6565_' + str(peak) + '_err'
            nii_6585_flux_name = 'flux_' + str(peak) +'_nii_6585'
            h_alpha_6565_flux_err_name = 'flux_' + str(peak) +'_err_h_alpha_6565'
            return np.log10(table[nii_6585_flux_name] / (3 * table[h_alpha_6565_flux_err_name]))
        else:
            return np.log10(table['f6585_nii_flx_%s' % line_shape] / (3 * table['f6565_h_alpha_flx_err_%s' % line_shape]))

    @staticmethod
    def get_log10_ratio_sii_err(table, dp=True, peak=1, line_shape='gauss'):
        if dp:
            # sii_6718_flux_err_name = 'flux_6718_' + str(peak) + '_err'
            # sii_6733_flux_err_name = 'flux_6733_' + str(peak) + '_err'
            # h_alpha_6565_flux_name = 'flux_6565_' + str(peak)
            sii_6718_flux_err_name = 'flux_' + str(peak) +'_err_sii_6718'
            sii_6733_flux_err_name = 'flux_' + str(peak) +'_err_sii_6733'
            h_alpha_6565_flux_name = 'flux_' + str(peak) +'_h_alpha_6565'
            return np.log10(3 * (table[sii_6718_flux_err_name] + table[sii_6733_flux_err_name]) /
                            table[h_alpha_6565_flux_name])
        else:
            return np.log10(3 * (table['f6718_sii_flx_err_%s' % line_shape] + table['f6733_sii_flx_err_%s' % line_shape]) /
                            table['f6565_h_alpha_flx_%s' % line_shape])

    @staticmethod
    def get_log10_ratio_oi_err(table, dp=True, peak=1, line_shape='gauss'):
        if dp:
            # oi_6302_flux_err_name = 'flux_6302_' + str(peak) + '_err'
            # h_alpha_6565_flux_name = 'flux_6565_' + str(peak)
            oi_6302_flux_err_name = 'flux_' + str(peak) +'_err_oi_6302'
            h_alpha_6565_flux_name = 'flux_' + str(peak) +'_h_alpha_6565'
            return np.log10(3 * table[oi_6302_flux_err_name] / table[h_alpha_6565_flux_name])
        else:
            return np.log10(3 * table['f6302_oi_flx_err_%s' % line_shape] / table['f6565_h_alpha_flx_%s' % line_shape])

    @staticmethod
    def classify_first_bpt(table, dp=True, line_shape='gauss'):
        if dp:
            oiii_h_beta_ratio_1 = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=True, peak=1, line_shape=line_shape)
            oiii_h_beta_ratio_2 = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=True, peak=2, line_shape=line_shape)
            nii_h_alpha_ratio_1 = BPTCategorisation.get_log10_ratio_nii_h_alpha(table, dp=True, peak=1, line_shape=line_shape)
            nii_h_alpha_ratio_2 = BPTCategorisation.get_log10_ratio_nii_h_alpha(table, dp=True, peak=2, line_shape=line_shape)
            return BPTCategorisation.classify_first_bpt_double_peak(oiii_h_beta_ratio_1, oiii_h_beta_ratio_2,
                                                                    nii_h_alpha_ratio_1, nii_h_alpha_ratio_2)
        if not dp and line_shape=='manga_dp':
            oiii_h_beta_ratio = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=False, line_shape=line_shape)
            nii_h_alpha_ratio = BPTCategorisation.get_log10_ratio_nii_h_alpha(table, dp=False, line_shape=line_shape)
            return BPTCategorisation.classify_first_bpt_single_peak(oiii_h_beta_ratio,
                                                                    nii_h_alpha_ratio)
        else:
            oiii_h_beta_ratio = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=False, line_shape=line_shape)
            nii_h_alpha_ratio = BPTCategorisation.get_log10_ratio_nii_h_alpha(table, dp=False, line_shape=line_shape)
            return BPTCategorisation.classify_first_bpt_single_peak(oiii_h_beta_ratio, nii_h_alpha_ratio)

    @staticmethod
    def classify_second_bpt(table, dp=True, line_shape='gauss'):
        if dp:
            oiii_h_beta_ratio_1 = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=True, peak=1, line_shape=line_shape)
            oiii_h_beta_ratio_2 = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=True, peak=2, line_shape=line_shape)
            sii_h_alpha_ratio_1 = BPTCategorisation.get_log10_ratio_sii_h_alpha(table, dp=True, peak=1, line_shape=line_shape)
            sii_h_alpha_ratio_2 = BPTCategorisation.get_log10_ratio_sii_h_alpha(table, dp=True, peak=2, line_shape=line_shape)
            return BPTCategorisation.classify_second_bpt_double_peak(oiii_h_beta_ratio_1, oiii_h_beta_ratio_2,
                                                                     sii_h_alpha_ratio_1, sii_h_alpha_ratio_2)
        else:
            oiii_h_beta_ratio = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=False, line_shape=line_shape)
            sii_h_alpha_ratio = BPTCategorisation.get_log10_ratio_sii_h_alpha(table, dp=False, line_shape=line_shape)
            return BPTCategorisation.classify_second_bpt_single_peak(oiii_h_beta_ratio, sii_h_alpha_ratio)

    @staticmethod
    def classify_third_bpt(table, dp=True, line_shape='gauss'):
        if dp:
            oiii_h_beta_ratio_1 = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=True, peak=1, line_shape=line_shape)
            oiii_h_beta_ratio_2 = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=True, peak=2, line_shape=line_shape)
            oi_h_alpha_ratio_1 = BPTCategorisation.get_log10_ratio_oi_h_alpha(table, dp=True, peak=1, line_shape=line_shape)
            oi_h_alpha_ratio_2 = BPTCategorisation.get_log10_ratio_oi_h_alpha(table, dp=True, peak=2, line_shape=line_shape)
            return BPTCategorisation.classify_third_bpt_double_peak(oiii_h_beta_ratio_1, oiii_h_beta_ratio_2,
                                                                    oi_h_alpha_ratio_1, oi_h_alpha_ratio_2)
        else:
            oiii_h_beta_ratio = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=False, line_shape=line_shape)
            oi_h_alpha_ratio = BPTCategorisation.get_log10_ratio_oi_h_alpha(table, dp=False, line_shape=line_shape)
            return BPTCategorisation.classify_third_bpt_single_peak(oiii_h_beta_ratio, oi_h_alpha_ratio)

    @staticmethod
    def classify_uncertainty_first_bpt(table, dp=True, line_shape='gauss'):

        if dp:
            oiii_h_beta_ratio_1 = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=True, peak=1, line_shape=line_shape)
            oiii_h_beta_ratio_2 = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=True, peak=2, line_shape=line_shape)
            nii_h_alpha_ratio_1 = BPTCategorisation.get_log10_ratio_nii_h_alpha(table, dp=True, peak=1, line_shape=line_shape)
            nii_h_alpha_ratio_2 = BPTCategorisation.get_log10_ratio_nii_h_alpha(table, dp=True, peak=2, line_shape=line_shape)

            oiii_ratio_err_1 = BPTCategorisation.get_log10_ratio_oiii_err(table, dp=True, peak=1, line_shape=line_shape)
            oiii_ratio_err_2 = BPTCategorisation.get_log10_ratio_oiii_err(table, dp=True, peak=2, line_shape=line_shape)
            h_beta_ratio_err_1 = BPTCategorisation.get_log10_ratio_h_beta_err(table, dp=True, peak=1, line_shape=line_shape)
            h_beta_ratio_err_2 = BPTCategorisation.get_log10_ratio_h_beta_err(table, dp=True, peak=2, line_shape=line_shape)
            nii_ratio_err_1 = BPTCategorisation.get_log10_ratio_nii_err(table, dp=True, peak=1, line_shape=line_shape)
            nii_ratio_err_2 = BPTCategorisation.get_log10_ratio_nii_err(table, dp=True, peak=2, line_shape=line_shape)
            h_alpha_ratio_err_1 = BPTCategorisation.get_log10_ratio_h_alpha_err(table, dp=True, peak=1, line_shape=line_shape)
            h_alpha_ratio_err_2 = BPTCategorisation.get_log10_ratio_h_alpha_err(table, dp=True, peak=2, line_shape=line_shape)

            no_oiii = BPTCategorisation.classify_first_bpt_double_peak(oiii_ratio_err_1, oiii_ratio_err_2,
                                                                       nii_h_alpha_ratio_1, nii_h_alpha_ratio_2)

            no_h_beta = BPTCategorisation.classify_first_bpt_double_peak(h_beta_ratio_err_1, h_beta_ratio_err_2,
                                                                         nii_h_alpha_ratio_1, nii_h_alpha_ratio_2)

            no_nii = BPTCategorisation.classify_first_bpt_double_peak(oiii_h_beta_ratio_1, oiii_h_beta_ratio_2,
                                                                      nii_ratio_err_1, nii_ratio_err_2)

            no_h_alpha = BPTCategorisation.classify_first_bpt_double_peak(oiii_h_beta_ratio_1, oiii_h_beta_ratio_2,
                                                                          h_alpha_ratio_err_1, h_alpha_ratio_err_2)

            return np.array([no_oiii, no_h_beta, no_nii, no_h_alpha])

        if not dp and line_shape=='manga_dp':
            oiii_h_beta_ratio = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=False, line_shape=line_shape)
            nii_h_alpha_ratio = BPTCategorisation.get_log10_ratio_nii_h_alpha(table, dp=False, line_shape=line_shape)

            oiii_ratio_err = BPTCategorisation.get_log10_ratio_oiii_err(table, dp=False, line_shape=line_shape)
            h_beta_ratio_err = BPTCategorisation.get_log10_ratio_h_beta_err(table, dp=False, line_shape=line_shape)
            nii_ratio_err = BPTCategorisation.get_log10_ratio_nii_err(table, dp=False, line_shape=line_shape)
            h_alpha_ratio_err = BPTCategorisation.get_log10_ratio_h_alpha_err(table, dp=False, line_shape=line_shape)

            no_oiii = BPTCategorisation.classify_first_bpt_single_peak(oiii_ratio_err, nii_h_alpha_ratio)

            no_h_beta = BPTCategorisation.classify_first_bpt_single_peak(h_beta_ratio_err, nii_h_alpha_ratio)

            no_nii = BPTCategorisation.classify_first_bpt_single_peak(oiii_h_beta_ratio, nii_ratio_err)

            no_h_alpha = BPTCategorisation.classify_first_bpt_single_peak(oiii_h_beta_ratio, h_alpha_ratio_err)

            return np.array([no_oiii, no_h_beta, no_nii, no_h_alpha])

        else:
            oiii_h_beta_ratio = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=False, line_shape=line_shape)
            nii_h_alpha_ratio = BPTCategorisation.get_log10_ratio_nii_h_alpha(table, dp=False, line_shape=line_shape)

            oiii_ratio_err = BPTCategorisation.get_log10_ratio_oiii_err(table, dp=False, line_shape=line_shape)
            h_beta_ratio_err = BPTCategorisation.get_log10_ratio_h_beta_err(table, dp=False, line_shape=line_shape)
            nii_ratio_err = BPTCategorisation.get_log10_ratio_nii_err(table, dp=False, line_shape=line_shape)
            h_alpha_ratio_err = BPTCategorisation.get_log10_ratio_h_alpha_err(table, dp=False, line_shape=line_shape)

            no_oiii = BPTCategorisation.classify_first_bpt_single_peak(oiii_ratio_err, nii_h_alpha_ratio)

            no_h_beta = BPTCategorisation.classify_first_bpt_single_peak(h_beta_ratio_err, nii_h_alpha_ratio)

            no_nii = BPTCategorisation.classify_first_bpt_single_peak(oiii_h_beta_ratio, nii_ratio_err)

            no_h_alpha = BPTCategorisation.classify_first_bpt_single_peak(oiii_h_beta_ratio, h_alpha_ratio_err)

            return np.array([no_oiii, no_h_beta, no_nii, no_h_alpha])

    @staticmethod
    def classify_uncertainty_second_bpt(table, dp=True, line_shape='gauss'):

        if dp:
            oiii_h_beta_ratio_1 = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=True, peak=1)
            oiii_h_beta_ratio_2 = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=True, peak=2)
            sii_h_alpha_ratio_1 = BPTCategorisation.get_log10_ratio_sii_h_alpha(table, dp=True, peak=1)
            sii_h_alpha_ratio_2 = BPTCategorisation.get_log10_ratio_sii_h_alpha(table, dp=True, peak=2)

            oiii_ratio_err_1 = BPTCategorisation.get_log10_ratio_oiii_err(table, dp=True, peak=1)
            oiii_ratio_err_2 = BPTCategorisation.get_log10_ratio_oiii_err(table, dp=True, peak=2)
            h_beta_ratio_err_1 = BPTCategorisation.get_log10_ratio_h_beta_err(table, dp=True, peak=1)
            h_beta_ratio_err_2 = BPTCategorisation.get_log10_ratio_h_beta_err(table, dp=True, peak=2)
            h_alpha_ratio_err_1 = BPTCategorisation.get_log10_ratio_h_alpha_err(table, dp=True, peak=1)
            h_alpha_ratio_err_2 = BPTCategorisation.get_log10_ratio_h_alpha_err(table, dp=True, peak=2)
            sii_ratio_err_1 = BPTCategorisation.get_log10_ratio_sii_err(table, dp=True, peak=1)
            sii_ratio_err_2 = BPTCategorisation.get_log10_ratio_sii_err(table, dp=True, peak=2)

            no_oiii = BPTCategorisation.classify_second_bpt_double_peak(oiii_ratio_err_1, oiii_ratio_err_2,
                                                                       sii_h_alpha_ratio_1, sii_h_alpha_ratio_2)

            no_h_beta = BPTCategorisation.classify_second_bpt_double_peak(h_beta_ratio_err_1, h_beta_ratio_err_2,
                                                                         sii_h_alpha_ratio_1, sii_h_alpha_ratio_2)

            no_sii = BPTCategorisation.classify_second_bpt_double_peak(oiii_h_beta_ratio_1, oiii_h_beta_ratio_2,
                                                                      sii_ratio_err_1, sii_ratio_err_2)

            no_h_alpha = BPTCategorisation.classify_second_bpt_double_peak(oiii_h_beta_ratio_1, oiii_h_beta_ratio_2,
                                                                          h_alpha_ratio_err_1, h_alpha_ratio_err_2)

            return np.array([no_oiii, no_h_beta, no_sii, no_h_alpha])

        else:
            oiii_h_beta_ratio = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=False, line_shape=line_shape)
            sii_h_alpha_ratio = BPTCategorisation.get_log10_ratio_sii_h_alpha(table, dp=False, line_shape=line_shape)

            oiii_ratio_err = BPTCategorisation.get_log10_ratio_oiii_err(table, dp=False, line_shape=line_shape)
            h_beta_ratio_err = BPTCategorisation.get_log10_ratio_h_beta_err(table, dp=False, line_shape=line_shape)
            h_alpha_ratio_err = BPTCategorisation.get_log10_ratio_h_alpha_err(table, dp=False, line_shape=line_shape)
            sii_ratio_err = BPTCategorisation.get_log10_ratio_sii_err(table, dp=False, line_shape=line_shape)

            no_oiii = BPTCategorisation.classify_second_bpt_single_peak(oiii_ratio_err, sii_h_alpha_ratio)

            no_h_beta = BPTCategorisation.classify_second_bpt_single_peak(h_beta_ratio_err, sii_h_alpha_ratio)

            no_sii = BPTCategorisation.classify_second_bpt_single_peak(oiii_h_beta_ratio, sii_ratio_err)

            no_h_alpha = BPTCategorisation.classify_second_bpt_single_peak(oiii_h_beta_ratio, h_alpha_ratio_err)

            return np.array([no_oiii, no_h_beta, no_sii, no_h_alpha])

    @staticmethod
    def classify_uncertainty_third_bpt(table, dp=True, line_shape='gauss'):
        if dp:
            oiii_h_beta_ratio_1 = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=True, peak=1)
            oiii_h_beta_ratio_2 = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=True, peak=2)
            oi_h_alpha_ratio_1 = BPTCategorisation.get_log10_ratio_oi_h_alpha(table, dp=True, peak=1)
            oi_h_alpha_ratio_2 = BPTCategorisation.get_log10_ratio_oi_h_alpha(table, dp=True, peak=2)

            oiii_ratio_err_1 = BPTCategorisation.get_log10_ratio_oiii_err(table, dp=True, peak=1)
            oiii_ratio_err_2 = BPTCategorisation.get_log10_ratio_oiii_err(table, dp=True, peak=2)
            h_beta_ratio_err_1 = BPTCategorisation.get_log10_ratio_h_beta_err(table, dp=True, peak=1)
            h_beta_ratio_err_2 = BPTCategorisation.get_log10_ratio_h_beta_err(table, dp=True, peak=2)
            h_alpha_ratio_err_1 = BPTCategorisation.get_log10_ratio_h_alpha_err(table, dp=True, peak=1)
            h_alpha_ratio_err_2 = BPTCategorisation.get_log10_ratio_h_alpha_err(table, dp=True, peak=2)
            oi_ratio_err_1 = BPTCategorisation.get_log10_ratio_oi_err(table, dp=True, peak=1)
            oi_ratio_err_2 = BPTCategorisation.get_log10_ratio_oi_err(table, dp=True, peak=2)

            no_oiii = BPTCategorisation.classify_third_bpt_double_peak(oiii_ratio_err_1, oiii_ratio_err_2,
                                                                       oi_h_alpha_ratio_1, oi_h_alpha_ratio_2)

            no_h_beta = BPTCategorisation.classify_third_bpt_double_peak(h_beta_ratio_err_1, h_beta_ratio_err_2,
                                                                         oi_h_alpha_ratio_1, oi_h_alpha_ratio_2)

            no_oi = BPTCategorisation.classify_third_bpt_double_peak(oiii_h_beta_ratio_1, oiii_h_beta_ratio_2,
                                                                      oi_ratio_err_1, oi_ratio_err_2)

            no_h_alpha = BPTCategorisation.classify_third_bpt_double_peak(oiii_h_beta_ratio_1, oiii_h_beta_ratio_2,
                                                                          h_alpha_ratio_err_1, h_alpha_ratio_err_2)

            return np.array([no_oiii, no_h_beta, no_oi, no_h_alpha])
        else:
            oiii_h_beta_ratio = BPTCategorisation.get_log10_ratio_oiii_h_beta(table, dp=False, line_shape=line_shape)
            oi_h_alpha_ratio = BPTCategorisation.get_log10_ratio_oi_h_alpha(table, dp=False, line_shape=line_shape)

            oiii_ratio_err = BPTCategorisation.get_log10_ratio_oiii_err(table, dp=False, line_shape=line_shape)
            h_beta_ratio_err = BPTCategorisation.get_log10_ratio_h_beta_err(table, dp=False, line_shape=line_shape)
            h_alpha_ratio_err = BPTCategorisation.get_log10_ratio_h_alpha_err(table, dp=False, line_shape=line_shape)
            oi_ratio_err = BPTCategorisation.get_log10_ratio_oi_err(table, dp=False, line_shape=line_shape)

            no_oiii = BPTCategorisation.classify_third_bpt_single_peak(oiii_ratio_err, oi_h_alpha_ratio)

            no_h_beta = BPTCategorisation.classify_third_bpt_single_peak(h_beta_ratio_err, oi_h_alpha_ratio)

            no_oi = BPTCategorisation.classify_third_bpt_single_peak(oiii_h_beta_ratio, oi_ratio_err)

            no_h_alpha = BPTCategorisation.classify_third_bpt_single_peak(oiii_h_beta_ratio, h_alpha_ratio_err)

            return np.array([no_oiii, no_h_beta, no_oi, no_h_alpha])

    @staticmethod
    def classify_first_bpt_double_peak(oiii_h_beta_ratio_1, oiii_h_beta_ratio_2,
                                       nii_h_alpha_ratio_1, nii_h_alpha_ratio_2):

        classification_1 = BPTCategorisation.classify_first_bpt_single_peak(oiii_h_beta_ratio_1, nii_h_alpha_ratio_1)
        classification_2 = BPTCategorisation.classify_first_bpt_single_peak(oiii_h_beta_ratio_2, nii_h_alpha_ratio_2)

        return np.array([classification_1, classification_2])

    @staticmethod
    def classify_second_bpt_double_peak(oiii_h_beta_ratio_1, oiii_h_beta_ratio_2,
                                        sii_h_alpha_ratio_1, sii_h_alpha_ratio_2):

        variable_1 = BPTCategorisation.classify_second_bpt_single_peak(oiii_h_beta_ratio_1, sii_h_alpha_ratio_1)
        variable_2 = BPTCategorisation.classify_second_bpt_single_peak(oiii_h_beta_ratio_2, sii_h_alpha_ratio_2)

        return variable_1, variable_2

    @staticmethod
    def classify_third_bpt_double_peak(oiii_h_beta_ratio_1, oiii_h_beta_ratio_2,
                                       oi_h_alpha_ratio_1, oi_h_alpha_ratio_2):

        variable_1 = BPTCategorisation.classify_third_bpt_single_peak(oiii_h_beta_ratio_1, oi_h_alpha_ratio_1)
        variable_2 = BPTCategorisation.classify_third_bpt_single_peak(oiii_h_beta_ratio_2, oi_h_alpha_ratio_2)

        return variable_1, variable_2

    @staticmethod
    def classify_first_bpt_single_peak(oiii_h_beta_ratio, nii_h_alpha_ratio):
        r"""
        Function for classification with BPT diagrams following
         Kewley et al. 2006 doi:10.1111/j.1365-2966.2006.10859.x using equation in Sect. 3.1
         and Schawinski et al. 2007 doi:10.1111/j.1365-2966.2007.12487.x Using equation 1 in Sect. 2.5.1
        :param oiii_h_beta_ratio: log-value of emission line ratio [OIII]5008 / H$\beta$4863
        :type oiii_h_beta_ratio: array-like
        :param nii_h_alpha_ratio: log-value of emission line ratio [NII]6550 / H$\alpha$6565
        :type nii_h_alpha_ratio: array-like
        :return variable specifying the BPT classification
        :rtype: str
        """

        if oiii_h_beta_ratio.ndim > 1:
            classification = np.zeros(oiii_h_beta_ratio.shape, dtype='S6')
        else:
            classification = np.zeros(len(oiii_h_beta_ratio), dtype='S6')

        # sf
        # eq. (1) (Kewley et al. 2006)
        mask_sf = (oiii_h_beta_ratio < 0.61 / (nii_h_alpha_ratio - 0.05) + 1.3) & (nii_h_alpha_ratio < - 0.1)
        classification[mask_sf] = 'sf'

        # composite
        # eq. (4) and (5) (Kewley et al. 2006)
        mask_comp = (((oiii_h_beta_ratio > (0.61 / (nii_h_alpha_ratio - 0.05) + 1.3)) |
                      ((nii_h_alpha_ratio > - 0.1) & (nii_h_alpha_ratio < 0.4))) &
                     (oiii_h_beta_ratio < (0.61 / (nii_h_alpha_ratio - 0.47) + 1.19)))
        classification[mask_comp] = 'comp'

        # agn
        # eq. (6) (Kewley et al. 2006) with limit of nii_h_alpha_ratio < - 1.2825 and eq. (1) (Schawinski et al. 2007)
        mask_agn = (((oiii_h_beta_ratio > (0.61 / (nii_h_alpha_ratio - 0.05) + 1.3)) & (nii_h_alpha_ratio < - 1.2825)) |
                    ((oiii_h_beta_ratio > 0.61 / (nii_h_alpha_ratio - 0.47) + 1.19) & (nii_h_alpha_ratio > - 1.2825)) |
                    ((oiii_h_beta_ratio > 1.05 * nii_h_alpha_ratio + 0.45) & (nii_h_alpha_ratio > - 0.05)))
        classification[mask_agn] = 'agn'

        # liner
        # eq. (11) (Kewley et al. 2006) and eq. (1) (Schawinski et al. 2007)

        mask_liner = (((oiii_h_beta_ratio > 0.61 / (nii_h_alpha_ratio - 0.47) + 1.19) &
                       (oiii_h_beta_ratio < 1.05 * nii_h_alpha_ratio + 0.45)) |
                      ((nii_h_alpha_ratio > 0.45) & (oiii_h_beta_ratio < 1.05 * nii_h_alpha_ratio + 0.45)))
        classification[mask_liner] = 'liner'

        return classification

    @staticmethod
    def classify_second_bpt_single_peak(oiii_h_beta_ratio, sii_h_alpha_ratio):

        if oiii_h_beta_ratio.ndim > 1:
            classification = np.zeros(oiii_h_beta_ratio.shape, dtype='S6')
        else:
            classification = np.zeros(len(oiii_h_beta_ratio), dtype='S6')

        mask_sf = (oiii_h_beta_ratio < 0.72 / (sii_h_alpha_ratio - 0.32) + 1.3) & (sii_h_alpha_ratio < 0.1)
        classification[mask_sf] = 'sf'

        mask_agn = (((oiii_h_beta_ratio > 0.72 / (sii_h_alpha_ratio - 0.32) + 1.3) & (sii_h_alpha_ratio < - 0.315)) |
                    ((oiii_h_beta_ratio > 1.89 * sii_h_alpha_ratio + 0.76) & (sii_h_alpha_ratio > - 0.315)))
        classification[mask_agn] = 'agn'

        mask_liner = (((oiii_h_beta_ratio > 0.72 / (sii_h_alpha_ratio - 0.32) + 1.3) &
                       (sii_h_alpha_ratio < 0.3) &  (oiii_h_beta_ratio < 1.89 * sii_h_alpha_ratio + 0.76)) |
                      ((oiii_h_beta_ratio < 1.89 * sii_h_alpha_ratio + 0.76) & (sii_h_alpha_ratio > 0.3)))
        classification[mask_liner] = 'liner'
        return classification

    @staticmethod
    def classify_third_bpt_single_peak(oiii_h_beta_ratio, oi_h_alpha_ratio):

        if oiii_h_beta_ratio.ndim > 1:
            classification = np.zeros(oiii_h_beta_ratio.shape, dtype='S6')
        else:
            classification = np.zeros(len(oiii_h_beta_ratio), dtype='S6')

        mask_sf = oiii_h_beta_ratio < 0.73 / (oi_h_alpha_ratio + 0.59) + 1.33
        classification[mask_sf] = 'sf'

        mask_agn = (((oiii_h_beta_ratio > (0.73 / (oi_h_alpha_ratio + 0.59) + 1.33)) | (oi_h_alpha_ratio > -0.59)) &
                    (oiii_h_beta_ratio > 1.18 * oi_h_alpha_ratio + 1.3))
        classification[mask_agn] = 'agn'

        mask_liner = (((oiii_h_beta_ratio > (0.73 / (oi_h_alpha_ratio + 0.59) + 1.33)) | (oi_h_alpha_ratio > -0.59)) &
                      (oiii_h_beta_ratio < 1.18 * oi_h_alpha_ratio + 1.3))
        classification[mask_liner] = 'liner'

        return classification


class BPTFilter:

    def __init__(self):
        r"""
        categorize galaxies using BPT diagrams

        """

    @staticmethod
    def snr_first_bpt(table, dp=True, line_shape='gauss', sn_threshold=3):
        if dp:
            # filter_snr_first_bpt_peak_1 = (((table['flux_6585_1'] / table['flux_6585_1_err']) > sn_threshold) &
            #                            ((table['flux_6565_1'] / table['flux_6565_1_err']) > sn_threshold) &
            #                            ((table['flux_5008_1'] / table['flux_5008_1_err']) > sn_threshold) &
            #                            ((table['flux_4863_1'] / table['flux_4863_1_err']) > sn_threshold))
            # filter_snr_first_bpt_peak_2 = (((table['flux_6585_2'] / table['flux_6585_2_err']) > sn_threshold) &
            #                            ((table['flux_6565_2'] / table['flux_6565_2_err']) > sn_threshold) &
            #                            ((table['flux_5008_2'] / table['flux_5008_2_err']) > sn_threshold) &
            #                            ((table['flux_4863_2'] / table['flux_4863_2_err']) > sn_threshold))
            filter_snr_first_bpt_peak_1 = (((table['flux_1_nii_6585'] / table['flux_1_err_nii_6585']) > sn_threshold) &
                                       ((table['flux_1_h_alpha_6565'] / table['flux_1_err_h_alpha_6565']) > sn_threshold) &
                                       ((table['flux_1_oiii_5008'] / table['flux_1_err_oiii_5008']) > sn_threshold) &
                                       ((table['flux_1_h_beta_4863'] / table['flux_1_err_h_beta_4863']) > sn_threshold))
            filter_snr_first_bpt_peak_2 = (((table['flux_2_nii_6585'] / table['flux_2_err_nii_6585']) > sn_threshold) &
                                       ((table['flux_2_h_alpha_6565'] / table['flux_2_err_h_alpha_6565']) > sn_threshold) &
                                       ((table['flux_2_oiii_5008'] / table['flux_2_err_oiii_5008']) > sn_threshold) &
                                       ((table['flux_2_h_beta_4863'] / table['flux_2_err_h_beta_4863']) > sn_threshold))
            return filter_snr_first_bpt_peak_1, filter_snr_first_bpt_peak_2

        else:
            filter_snr_first_bpt = (table['f6585_nii_flx_%s' % line_shape] > 1.e-4) & \
                                   (table['f6565_h_alpha_flx_%s' % line_shape] > 1.e-4) & \
                                   (table['f4863_h_beta_flx_%s' % line_shape] > 1.e-4) & \
                                   (table['f5008_oiii_flx_%s' % line_shape] > 1.e-4) & \
                                   (table['f5008_oiii_flx_err_%s' % line_shape] > 1.e-4) & \
                                   (table['f6585_nii_flx_err_%s' % line_shape] > 1.e-4) & \
                                   (table['f4863_h_beta_flx_err_%s' % line_shape] > 1.e-4) & \
                                   (table['f6565_h_alpha_flx_err_%s' % line_shape] > 1.e-4) & \
                                   (table['f6585_nii_flx_%s' % line_shape] / table['f6585_nii_flx_err_%s' % line_shape] > sn_threshold) & \
                                   (table['f6565_h_alpha_flx_%s' % line_shape] / table['f6565_h_alpha_flx_err_%s' % line_shape] > sn_threshold) & \
                                   (table['f5008_oiii_flx_%s' % line_shape] / table['f5008_oiii_flx_err_%s' % line_shape] > sn_threshold) & \
                                   (table['f4863_h_beta_flx_%s' % line_shape] / table['f4863_h_beta_flx_err_%s' % line_shape] > sn_threshold)
            return filter_snr_first_bpt

    @staticmethod
    def snr_second_bpt(table, dp=True, line_shape='gauss'):
        if dp:
            # filter_snr_second_bpt_peak_1 = (((table['flux_6718_1'] / table['flux_6718_1_err']) > 3) &
            #                             ((table['flux_6565_1'] / table[
            #                                 'flux_6565_1_err']) > 3) &
            #                             ((table['flux_5008_1'] / table['flux_5008_1_err']) > 3) &
            #                             ((table['flux_4863_1'] / table['flux_4863_1_err']) > 3))
            # filter_snr_second_bpt_peak_2 = (((table['flux_6718_2'] / table['flux_6718_2_err']) > 3) &
            #                             ((table['flux_6565_2'] / table[
            #                                 'flux_6565_2_err']) > 3) &
            #                             ((table['flux_5008_2'] / table['flux_5008_2_err']) > 3) &
            #                             ((table['flux_4863_2'] / table['flux_4863_2_err']) > 3))
            filter_snr_second_bpt_peak_1 = (((table['flux_1_sii_6718'] / table['flux_1_err_sii_6718']) > 3) &
                                        ((table['flux_1_h_alpha_6565'] / table[
                                            'flux_1_err_h_alpha_6565']) > 3) &
                                        ((table['flux_1_oiii_5008'] / table['flux_2_err_oiii_5008']) > 3) &
                                        ((table['flux_1_h_beta_4863'] / table['flux_1_err_h_beta_4863']) > 3))
            filter_snr_second_bpt_peak_2 = (((table['flux_2_sii_6718'] / table['flux_2_err_sii_6718']) > 3) &
                                        ((table['flux_2_h_alpha_6565'] / table[
                                            'flux_2_err_h_alpha_6565']) > 3) &
                                        ((table['flux_2_oiii_5008'] / table['flux_2_err_oiii_5008']) > 3) &
                                        ((table['flux_2_h_beta_4863'] / table['flux_2_err_h_beta_4863']) > 3))
            return filter_snr_second_bpt_peak_1, filter_snr_second_bpt_peak_2
        else:
            filter_snr_second_bpt = (table['f6585_nii_flx_%s' % line_shape] > 1.e-4) & \
                                    (table['f6565_h_alpha_flx_%s' % line_shape] > 1.e-4) & \
                                    (table['f4863_h_beta_flx_%s' % line_shape] > 1.e-4) & \
                                    (table['f5008_oiii_flx_%s' % line_shape] > 1.e-4) & \
                                    ((table['f6718_sii_flx_%s' % line_shape] + table['f6733_sii_flx_%s' % line_shape]) > 1.e-4) & \
                                    (table['f5008_oiii_flx_err_%s' % line_shape] > 1.e-4) & \
                                    (np.sqrt(np.power(table['f6718_sii_flx_err_%s' % line_shape], 2) +
                                             np.power(table['f6733_sii_flx_err_%s' % line_shape], 2)) > 1.e-4) & \
                                    (table['f4863_h_beta_flx_err_%s' % line_shape] > 1.e-4) & \
                                    (table['f6565_h_alpha_flx_err_%s' % line_shape] > 1.e-4) & \
                                    (table['f6565_h_alpha_flx_%s' % line_shape] / table['f6565_h_alpha_flx_err_%s' % line_shape] > 3.) & \
                                    (table['f5008_oiii_flx_%s' % line_shape] / table['f5008_oiii_flx_err_%s' % line_shape] > 3.) & \
                                    ((table['f6718_sii_flx_%s' % line_shape] + table['f6733_sii_flx_%s' % line_shape]) /
                                     (np.sqrt(np.power(table['f6718_sii_flx_err_%s' % line_shape], 2) +
                                              np.power(table['f6733_sii_flx_err_%s' % line_shape], 2))) > 3.) & \
                                    (table['f4863_h_beta_flx_%s' % line_shape] / table['f4863_h_beta_flx_err_%s' % line_shape] > 3.)
            return filter_snr_second_bpt

    @staticmethod
    def snr_third_bpt(table, dp=True, line_shape='gauss'):
        if dp:
            # filter_snr_third_bpt_peak_1 = (((table['flux_6302_1'] / table['flux_6302_1_err']) > 3) &
            #                            ((table['flux_6565_1'] / table['flux_6565_1_err']) > 3) &
            #                            ((table['flux_5008_1'] / table['flux_5008_1_err']) > 3) &
            #                            ((table['flux_4863_1'] / table['flux_4863_1_err']) > 3))
            # filter_snr_third_bpt_peak_2 = (((table['flux_6302_2'] / table['flux_6302_2_err']) > 3) &
            #                            ((table['flux_6565_2'] / table['flux_6565_2_err']) > 3) &
            #                            ((table['flux_5008_2'] / table['flux_5008_2_err']) > 3) &
            #                            ((table['flux_4863_2'] / table['flux_4863_2_err']) > 3))
            filter_snr_third_bpt_peak_1 = (((table['flux_1_oi_6302'] / table['flux_1_err_oi_6302']) > 3) &
                                       ((table['flux_1_h_alpha_6565'] / table['flux_1_err_h_alpha_6565']) > 3) &
                                       ((table['flux_1_oiii_5008'] / table['flux_1_err_oiii_5008']) > 3) &
                                       ((table['flux_1_h_beta_4863'] / table['flux_1_err_h_beta_4863']) > 3))
            filter_snr_third_bpt_peak_2 = (((table['flux_2_oi_6302'] / table['flux_2_err_oi_6302']) > 3) &
                                       ((table['flux_2_h_alpha_6565'] / table['flux_2_err_h_alpha_6565']) > 3) &
                                       ((table['flux_2_oiii_5008'] / table['flux_2_err_oiii_5008']) > 3) &
                                       ((table['flux_2_h_beta_4863'] / table['flux_2_err_h_beta_4863']) > 3))
            return filter_snr_third_bpt_peak_1, filter_snr_third_bpt_peak_2
        else:
            filter_third_bpt = (table['f6585_nii_flx_%s' % line_shape] > 1.e-4) & \
                               (table['f6565_h_alpha_flx_%s' % line_shape] > 1.e-4) & \
                               (table['f4863_h_beta_flx_%s' % line_shape] > 1.e-4) & \
                               (table['f5008_oiii_flx_%s' % line_shape] > 1.e-4) & \
                               (table['f5008_oiii_flx_err_%s' % line_shape] > 1.e-4) & \
                               (table['f6302_oi_flx_err_%s' % line_shape] > 1.e-4) & \
                               (table['f4863_h_beta_flx_err_%s' % line_shape] > 1.e-4) & \
                               (table['f6565_h_alpha_flx_err_%s' % line_shape] > 1.e-4) & \
                               (table['f6302_oi_flx_%s' % line_shape] / table['f6302_oi_flx_err_%s' % line_shape] > 3) & \
                               (table['f6565_h_alpha_flx_%s' % line_shape] / table['f6565_h_alpha_flx_err_%s' % line_shape] > 3) & \
                               (table['f5008_oiii_flx_%s' % line_shape] / table['f5008_oiii_flx_err_%s' % line_shape] > 3) & \
                               (table['f4863_h_beta_flx_%s' % line_shape] / table['f4863_h_beta_flx_err_%s' % line_shape] > 3)
            return filter_third_bpt

    @staticmethod
    def dp_first_bpt(table):
        return ((table['double_peak_flag_nii_6585'] == 1) & (table['double_peak_flag_h_alpha_6565'] == 1) &
                (table['double_peak_flag_oiii_5008'] == 1) & (table['double_peak_flag_h_beta_4863'] == 1))

    @staticmethod
    def dp_second_bpt(table):
        return (((table['double_peak_flag_sii_6718'] == 1) | (table['double_peak_flag_sii_6733'] == 1)) &
                (table['double_peak_flag_h_alpha_6565'] == 1) & (table['double_peak_flag_oiii_5008'] == 1) &
                (table['double_peak_flag_h_beta_4863'] == 1))

    @staticmethod
    def dp_third_bpt(table):
        return ((table['double_peak_flag_oi_6302'] == 1) & (table['double_peak_flag_h_alpha_6565'] == 1) &
                (table['double_peak_flag_oiii_5008'] == 1) & (table['double_peak_flag_h_beta_4863'] == 1))

    @staticmethod
    def dp_flag_h_beta(table):
        return table['double_peak_flag_h_beta_4863'] == 1

    @staticmethod
    def dp_flag_oiii(table):
        return table['double_peak_flag_oiii_5008'] == 1

    @staticmethod
    def dp_flag_oi(table):
        return table['double_peak_flag_oi_6302'] == 1

    @staticmethod
    def dp_flag_h_alpha(table):
        return table['double_peak_flag_h_alpha_6565'] == 1

    @staticmethod
    def dp_flag_nii(table):
        return table['double_peak_flag_nii_6585'] == 1

    @staticmethod
    def dp_flag_sii(table):
        return (table['double_peak_flag_sii_6718'] == 1) | (table['double_peak_flag_sii_6733'] == 1)

    @staticmethod
    def snr_flag_h_beta(table, dp=True, peak=None, line_shape='gauss'):
        if dp:
            if peak:
                # return table['flux_4863_%i' % peak] / table['flux_4863_%i_err' % peak] > 3
                return table['flux_%i_h_beta_4863' % peak] / table['flux_%i_err_h_beta_4863' % peak] > 3
        else:
            filter_snr_first_bpt = (table['f4863_h_beta_flx_%s' % line_shape] > 1.e-4) & \
                                   (table['f4863_h_beta_flx_err_%s' % line_shape] > 1.e-4) & \
                                   (table['f4863_h_beta_flx_%s' % line_shape] /
                                    table['f4863_h_beta_flx_err_%s' % line_shape] > 3)
            return filter_snr_first_bpt

    @staticmethod
    def snr_flag_oiii(table, dp=True, peak=None, line_shape='gauss'):
        if dp:
            if peak:
                # return table['flux_5008_%i' % peak] / table['flux_5008_%i_err' % peak] > 3
                return table['flux_%i_oiii_5008' % peak] / table['flux_%i_err_oiii_5008' % peak] > 3
        else:
            filter_snr_first_bpt = (table['f5008_oiii_flx_%s' % line_shape] > 1.e-4) & \
                                   (table['f5008_oiii_flx_err_%s' % line_shape] > 1.e-4) & \
                                   (table['f5008_oiii_flx_%s' % line_shape] /
                                    table['f5008_oiii_flx_err_%s' % line_shape] > 3)
            return filter_snr_first_bpt

    @staticmethod
    def snr_flag_oi(table, dp=True, peak=None, line_shape='gauss'):
        if dp:
            if peak:
                # return table['flux_6302_%i' % peak] / table['flux_err_%i_6302' % peak] > 3
                return table['flux_%i_oi_6302' % peak] / table['flux_%i_err_oi_6302' % peak] > 3
        else:
            filter_snr_first_bpt = (table['f6302_oi_flx_%s' % line_shape] > 1.e-4) & \
                                   (table['f6302_oi_flx_err_%s' % line_shape] > 1.e-4) & \
                                   (table['f6302_oi_flx_%s' % line_shape] /
                                    table['f6302_oi_flx_err_%s' % line_shape] > 3)
            return filter_snr_first_bpt

    @staticmethod
    def snr_flag_h_alpha(table, dp=True, peak=None, line_shape='gauss'):
        if dp:
            if peak:
                # return table['flux_6565_%i' % peak] / table['flux_6565_%i_err' % peak] > 3
                return table['flux_%i_h_alpha_6565' % peak] / table['flux_%i_err_h_alpha_6565' % peak] > 3
        else:
            filter_snr_first_bpt = (table['f6565_h_alpha_flx_%s' % line_shape] > 1.e-4) & \
                                   (table['f6565_h_alpha_flx_err_%s' % line_shape] > 1.e-4) & \
                                   (table['f6565_h_alpha_flx_%s' % line_shape] /
                                    table['f6565_h_alpha_flx_err_%s' % line_shape] > 3)
            return filter_snr_first_bpt

    @staticmethod
    def snr_flag_nii(table, dp=True, peak=None, line_shape='gauss'):
        if dp:
            if peak:
                # return table['flux_6585_%i' % peak] / table['flux_6585_%i_err' % peak] > 3
                return table['flux_%i_nii_6585' % peak] / table['flux_%i_err_nii_6585' % peak] > 3
        else:
            filter_snr_first_bpt = (table['f6585_nii_flx_%s' % line_shape] > 1.e-4) & \
                                   (table['f6585_nii_flx_err_%s' % line_shape] > 1.e-4) & \
                                   (table['f6585_nii_flx_%s' % line_shape] /
                                    table['f6585_nii_flx_err_%s' % line_shape] > 3)
            return filter_snr_first_bpt

    @staticmethod
    def snr_flag_sii(table, dp=True, peak=None, line_shape='gauss'):
        if dp:
            if peak:
                # return table['flux_6718_%i' % peak] / table['flux_6718_%i_err' % peak] > 3
                return table['flux_%i_sii_6718' % peak] / table['flux_%i_err_sii_6718' % peak] > 3
        else:
            filter_snr_first_bpt = (table['f6718_sii_flx_%s' % line_shape] > 1.e-4) & \
                                   (table['f6718_sii_flx_err_%s' % line_shape] > 1.e-4) & \
                                   (table['f6718_sii_flx_%s' % line_shape] / table['f6718_sii_flx_err_%s' % line_shape] > 3)
            return filter_snr_first_bpt

    @staticmethod
    def dp_ambiguous_peak_1(classification, filter_snr_first_bpt_peak_1, filter_snr_second_bpt_peak_1,
                            filter_snr_third_bpt_peak_1):
        first_second_bpt = ((((classification[:, 0, 0] == 'sf') & (classification[:, 1, 0] == 'agn')) |
                            ((classification[:, 0, 0] == 'sf') & (classification[:, 1, 0] == 'liner')) |
                            ((classification[:, 0, 0] == 'comp') & (classification[:, 1, 0] == 'agn')) |
                            ((classification[:, 0, 0] == 'comp') & (classification[:, 1, 0] == 'liner')) |
                            ((classification[:, 0, 0] == 'agn') & (classification[:, 1, 0] == 'sf')) |
                            ((classification[:, 0, 0] == 'agn') & (classification[:, 1, 0] == 'liner')) |
                            ((classification[:, 0, 0] == 'liner') & (classification[:, 1, 0] == 'sf')) |
                            ((classification[:, 0, 0] == 'liner') & (classification[:, 1, 0] == 'agn'))) *
                            filter_snr_first_bpt_peak_1 * filter_snr_second_bpt_peak_1)

        first_third_bpt = ((((classification[:, 0, 0] == 'sf') & (classification[:, 2, 0] == 'agn')) |
                            ((classification[:, 0, 0] == 'sf') & (classification[:, 2, 0] == 'liner')) |
                            ((classification[:, 0, 0] == 'comp') & (classification[:, 2, 0] == 'agn')) |
                            ((classification[:, 0, 0] == 'comp') & (classification[:, 2, 0] == 'liner')) |
                            ((classification[:, 0, 0] == 'agn') & (classification[:, 2, 0] == 'sf')) |
                            ((classification[:, 0, 0] == 'agn') & (classification[:, 2, 0] == 'liner')) |
                            ((classification[:, 0, 0] == 'liner') & (classification[:, 2, 0] == 'sf')) |
                            ((classification[:, 0, 0] == 'liner') & (classification[:, 2, 0] == 'agn'))) *
                           filter_snr_first_bpt_peak_1 * filter_snr_third_bpt_peak_1)

        second_third_bpt = ((classification[:, 1, 0] != classification[:, 2, 0]) *
                            filter_snr_second_bpt_peak_1 * filter_snr_third_bpt_peak_1)

        return first_second_bpt + first_third_bpt + second_third_bpt

    @staticmethod
    def dp_ambiguous_peak_2(classification, filter_snr_first_bpt_peak_2, filter_snr_second_bpt_peak_2,
                            filter_snr_third_bpt_peak_2):

        first_second_bpt = ((((classification[:, 0, 1] == 'sf') & (classification[:, 1, 1] == 'agn')) |
                             ((classification[:, 0, 1] == 'sf') & (classification[:, 1, 1] == 'liner')) |
                             ((classification[:, 0, 1] == 'comp') & (classification[:, 1, 1] == 'agn')) |
                             ((classification[:, 0, 1] == 'comp') & (classification[:, 1, 1] == 'liner')) |
                             ((classification[:, 0, 1] == 'agn') & (classification[:, 1, 1] == 'sf')) |
                             ((classification[:, 0, 1] == 'agn') & (classification[:, 1, 1] == 'liner')) |
                             ((classification[:, 0, 1] == 'liner') & (classification[:, 1, 1] == 'sf')) |
                             ((classification[:, 0, 1] == 'liner') & (classification[:, 1, 1] == 'agn'))) *
                            filter_snr_first_bpt_peak_2 * filter_snr_second_bpt_peak_2)

        first_third_bpt = ((((classification[:, 0, 1] == 'sf') & (classification[:, 2, 1] == 'agn')) |
                            ((classification[:, 0, 1] == 'sf') & (classification[:, 2, 1] == 'liner')) |
                            ((classification[:, 0, 1] == 'comp') & (classification[:, 2, 1] == 'agn')) |
                            ((classification[:, 0, 1] == 'comp') & (classification[:, 2, 1] == 'liner')) |
                            ((classification[:, 0, 1] == 'agn') & (classification[:, 2, 1] == 'sf')) |
                            ((classification[:, 0, 1] == 'agn') & (classification[:, 2, 1] == 'liner')) |
                            ((classification[:, 0, 1] == 'liner') & (classification[:, 2, 1] == 'sf')) |
                            ((classification[:, 0, 1] == 'liner') & (classification[:, 2, 1] == 'agn'))) *
                           filter_snr_first_bpt_peak_2 * filter_snr_third_bpt_peak_2)

        second_third_bpt = ((classification[:, 1, 1] != classification[:, 2, 1]) *
                            filter_snr_second_bpt_peak_2 * filter_snr_third_bpt_peak_2)

        return first_second_bpt + first_third_bpt + second_third_bpt

    @staticmethod
    def dp_ambiguous_single_peak(classification, filter_snr_first_bpt, filter_snr_second_bpt, filter_snr_third_bpt):
        first_second_bpt = ((((classification[:, 0] == 'sf') & (classification[:, 1] == 'agn')) |
                             ((classification[:, 0] == 'sf') & (classification[:, 1] == 'liner')) |
                             ((classification[:, 0] == 'comp') & (classification[:, 1] == 'agn')) |
                             ((classification[:, 0] == 'comp') & (classification[:, 1] == 'liner')) |
                             ((classification[:, 0] == 'agn') & (classification[:, 1] == 'sf')) |
                             ((classification[:, 0] == 'agn') & (classification[:, 1] == 'liner')) |
                             ((classification[:, 0] == 'liner') & (classification[:, 1] == 'sf')) |
                             ((classification[:, 0] == 'liner') & (classification[:, 1] == 'agn'))) *
                            filter_snr_first_bpt * filter_snr_second_bpt)

        first_third_bpt = ((((classification[:, 0] == 'sf') & (classification[:, 2] == 'agn')) |
                            ((classification[:, 0] == 'sf') & (classification[:, 2] == 'liner')) |
                            ((classification[:, 0] == 'comp') & (classification[:, 2] == 'agn')) |
                            ((classification[:, 0] == 'comp') & (classification[:, 2] == 'liner')) |
                            ((classification[:, 0] == 'agn') & (classification[:, 2] == 'sf')) |
                            ((classification[:, 0] == 'agn') & (classification[:, 2] == 'liner')) |
                            ((classification[:, 0] == 'liner') & (classification[:, 2] == 'sf')) |
                            ((classification[:, 0] == 'liner') & (classification[:, 2] == 'agn'))) *
                           filter_snr_first_bpt * filter_snr_third_bpt)

        second_third_bpt = ((classification[:, 1] != classification[:, 2]) *
                            filter_snr_second_bpt * filter_snr_third_bpt)

        return first_second_bpt + first_third_bpt + second_third_bpt

