# -*- coding: utf-8 -*-
"""
Class to access all kind of data
"""

import numpy as np
import os
from pathlib import Path

from xgaltool import object_identifier

from scipy import constants
from astropy.io import fits
import urllib3


class RCSEDAccess(object_identifier.ObjectIdentifier):

    def __init__(self, **kwargs):
        r"""
        Class to access data of catalogs published by Chilingarian et al. 2017
        """
        super().__init__(**kwargs)

    def get_petrosian_radius(self, radius=50):
        r"""
        function to access petrosin radius. 50 or 90

        :param radius:
        :type band: int
        :return radius in arc seconds
        :rtype: array_like
        """
        if radius == 50:
            return self.table['petror50_r']
        else:
            return self.table['petroR%i_r' % radius]

    def get_rcsed_band_mag(self, band='r', k_correction=True):
        r"""
        Function to access the dust-corrected and k-corrected apparent magnitude
        of different observation bands treated
        by Chilingarian et al. 2017.

        :param band: in AB magnitude. fuv and nuv observed by Galex, u, g, r, i
        and z observed by SDSS and
        Y, J, H and K observed by UKIDSS
        :type band: str
        :return magnitude mag
        :rtype: array_like
        """
        if k_correction:
            return self.table['corrmag_%s' % band] - self.table['kcorr_%s' % band]
        else:
            return self.table['corrmag_%s' % band]

    def get_rcsed_band_mag_err(self, band='r'):
        r"""
        Function to access the dust-corrected error of different observation bands
        treated by Chilingarian et al. 2017.

        :param band: in AB magnitude. fuv and nuv observed by Galex, u, g, r, i and z observed by SDSS and
        Y, J, H and K observed by UKIDSS
        :type band: str
        :return magnitude mag
        :rtype: array_like
        """
        return self.table['corrmag_%s_err' % band]

    def get_rcsed_fib_band_mag(self, band='r'):
        r"""
        Function to access the dust-corrected and k-corrected apparent magnitude
        of different fiber observation bands treated
        by Chilingarian et al. 2017.

        :param band: in AB magnitude. fuv and nuv observed by Galex, u, g, r, i
        and z observed by SDSS and
        Y, J, H and K observed by UKIDSS
        :type band: str
        :return magnitude mag
        :rtype: array_like
        """
        return self.table['corrfibmag_%s' % band] - self.table['kcorrfib_%s' % band]

    def get_rcsed_fib_band_mag_err(self, band='r'):
        r"""
        Function to access the dust-corrected and k-corrected apparent magnitude error of different fiber
        observation bands treated by Chilingarian et al. 2017.

        :param band: in AB magnitude. fuv and nuv observed by Galex, u, g, r, i and z observed by SDSS and
        Y, J, H and K observed by UKIDSS
        :type band: str
        :return magnitude mag
        :rtype: array_like
        """
        return self.table['corrfibmag_%s_err' % band]

    def get_rcsed_band_flux(self, band='fuv', return_format='Jy', k_correction=True):
        # This magnitude system is defined such that, when monochromatic flux f is measured in erg sec^-1 cm^-2 Hz^-1,
        # Oke, J.B. 1974, ApJS, 27, 21
        mag = self.get_rcsed_band_mag(band=band, k_correction=k_correction)
        if return_format == 'Jy':
            return 10 ** (- (mag + 48.6) / 2.5 + 23)
        elif return_format == 'cgs':
            return 10 ** (- (mag + 48.6) / 2.5)
        else:
            raise KeyError('return format must be Jy or cgs')

    def get_rcsed_band_flux_err(self, band='fuv', return_format='Jy', k_correction=False):
        # This magnitude system is defined such that, when monochromatic flux f is measured in erg sec^-1 cm^-2 Hz^-1,
        # Oke, J.B. 1974, ApJS, 27, 21
        mag_err = self.get_rcsed_band_mag_err(band=band)
        mag = self.get_rcsed_band_mag(band=band, k_correction=k_correction)

        if return_format == 'Jy':
            # return np.log(10) / 2.5 * 10 ** (- (mag + 48.6) / 2.5 + 23) * mag_err
            return 10 ** (- (mag - mag_err + 48.6) / 2.5 + 23) -  10 ** (- (mag + 48.6) / 2.5 + 23)
        elif return_format == 'cgs':
            return (10 ** (- (mag - mag_err + 48.6) / 2.5)) - (10 ** (- (mag + 48.6) / 2.5))
            # return np.log(10) / 2.5 * 10 ** (- (mag + 48.6) / 2.5) * mag_err

        else:
            raise KeyError('return format must be Jy or cgs')

    def get_rcsed_b_v(self):
        # Following Fukugita et al. 1996, 1996AJ....111.1748F
        # Eq. (23) g'—r'= 1.05 (B-V)-0.23
        g_r = self.get_rcsed_band_mag(band='g') - self.get_rcsed_band_mag(band='r')
        b_v = (g_r + 0.23) / 1.05
        return b_v

    def get_rcsed_b_v_err(self):
        # Following Fukugita et al. 1996, 1996AJ....111.1748F
        # Eq. (23) g'—r'= 1.05 (B-V)-0.23
        b_v_err = np.sqrt((self.get_rcsed_band_mag_err(band='g')/1.05)**2 +
                          (self.get_rcsed_band_mag_err(band='r')/1.05)**2)

        return b_v_err

    def get_rcsed_continuum_fit_chi2(self, continuum_fit='ssp'):
        if continuum_fit == 'ssp':
            return self.table['ssp_chi2']
        elif continuum_fit == 'exp':
            return self.table['exp_chi2']

    def get_rcsed_continuum_fit_mask(self):
        mask_best_fit_ssp = (self.get_rcsed_continuum_fit_chi2(continuum_fit='ssp') <
                             self.get_rcsed_continuum_fit_chi2(continuum_fit='exp'))
        return mask_best_fit_ssp

    def get_rcsed_emission_line_fit_chi2(self, line_shape='gauss'):
        r"""
        Access chi2/ndf of emission-lines

        :param line_shape: Specify the emission-line fit for the RCSED catalogue (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' but not for the dp sample access
        :type line_shape: str

        :return array with reduced chi2 values
        :rtype: array_like
        """
        return self.table['chi2_%s' % line_shape]

    def get_rcsed_stellar_velocity(self, continuum_fit='best_fit'):
        r"""
        access stellar velocity. We take the two template fits (ssp and exp) into
        account and use the approximation of the better fit

        :return stellar velocity in km/s
        """
        if continuum_fit == 'ssp':
            return self.table['ssp_radvel']
        elif continuum_fit == 'exp':
            return self.table['exp_radvel']
        elif continuum_fit == 'best_fit':
            stellar_velocity = np.zeros(len(self.table))
            mask_best_fit_ssp = self.get_rcsed_continuum_fit_mask()

            stellar_velocity[mask_best_fit_ssp] = self.table[mask_best_fit_ssp]['ssp_radvel']
            stellar_velocity[~mask_best_fit_ssp] = self.table[~mask_best_fit_ssp]['exp_radvel']

            return stellar_velocity
        else:
            raise KeyError('continuum fit must be ssp, exp or best fit')

    def get_rcsed_stellar_velocity_err(self, continuum_fit='best_fit'):
        r"""
        access stellar velocity uncertainty. We take the two template fits (ssp and exp) into
        account and use the approximation of the better fit

        :return stellar velocity uncertainty in km/s
        """
        if continuum_fit == 'ssp':
            return self.table['ssp_radvel_err']
        elif continuum_fit == 'exp':
            return self.table['exp_radvel_err']
        elif continuum_fit == 'best_fit':
            stellar_velocity = np.zeros(len(self.table))
            mask_best_fit_ssp = self.get_rcsed_continuum_fit_mask()

            stellar_velocity[mask_best_fit_ssp] = self.table[mask_best_fit_ssp]['ssp_radvel_err']
            stellar_velocity[~mask_best_fit_ssp] = self.table[~mask_best_fit_ssp]['exp_radvel_err']

            return stellar_velocity
        else:
            raise KeyError('continuum fit must be ssp, exp or best fit')

    def get_rcsed_stellar_velocity_dispersion(self, continuum_fit='best_fit'):
        r"""
        access stellar velocity dispersion (sigma). We take the two template fits (ssp and exp) into
        account and use the approximation of the better fit

        :return stellar velocity dispersion in km/s
        """
        if continuum_fit == 'ssp':
            return self.table['ssp_veldisp']
        elif continuum_fit == 'exp':
            return self.table['exp_veldisp']
        elif continuum_fit == 'best_fit':
            stellar_velocity = np.zeros(len(self.table))
            mask_best_fit_ssp = self.get_rcsed_continuum_fit_mask()

            stellar_velocity[mask_best_fit_ssp] = self.table[mask_best_fit_ssp]['ssp_veldisp']
            stellar_velocity[~mask_best_fit_ssp] = self.table[~mask_best_fit_ssp]['exp_veldisp']

            return stellar_velocity
        else:
            raise KeyError('continuum fit must be ssp, exp or best fit')

    def get_rcsed_stellar_velocity_dispersion_err(self, continuum_fit='best_fit'):
        r"""
        access stellar velocity dispersion uncertainty (sigma_err). We take the two template fits (ssp and exp) into
        account and use the approximation of the better fit

        :return stellar velocity dispersion uncertainty in km/s
        """
        if continuum_fit == 'ssp':
            return self.table['ssp_veldisp_err']
        elif continuum_fit == 'exp':
            return self.table['exp_veldisp_err']
        elif continuum_fit == 'best_fit':
            stellar_velocity = np.zeros(len(self.table))
            mask_best_fit_ssp = self.get_rcsed_continuum_fit_mask()

            stellar_velocity[mask_best_fit_ssp] = self.table[mask_best_fit_ssp]['ssp_veldisp_err']
            stellar_velocity[~mask_best_fit_ssp] = self.table[~mask_best_fit_ssp]['exp_veldisp_err']

            return stellar_velocity
        else:
            raise KeyError('continuum fit must be ssp, exp or best fit')

    def get_rcsed_stellar_metallicity(self, continuum_fit='best_fit'):
        r"""
        Access to the metallicity computed by Chilingarian et al. 2017. We take the two template fits (ssp and exp) into
        account and use the approximation of the better fit

        :return metallicity in [Z/H] dex
        """
        if continuum_fit == 'ssp':
            return self.table['ssp_met']
        elif continuum_fit == 'exp':
            return self.table['exp_met']
        elif continuum_fit == 'best_fit':
            stellar_velocity = np.zeros(len(self.table))
            mask_best_fit_ssp = self.get_rcsed_continuum_fit_mask()

            stellar_velocity[mask_best_fit_ssp] = self.table[mask_best_fit_ssp]['ssp_met']
            stellar_velocity[~mask_best_fit_ssp] = self.table[~mask_best_fit_ssp]['exp_met']

            return stellar_velocity
        else:
            raise KeyError('continuum fit must be ssp, exp or best fit')

    def get_rcsed_stellar_metallicity_err(self, continuum_fit='best_fit'):
        r"""
        Access to the metallicity error computed by Chilingarian et al. 2017. We take the two template fits
        (ssp and exp) into account and use the approximation of the better fit

        :return metallicity error in [Z/H] dex
        """
        if continuum_fit == 'ssp':
            return self.table['ssp_met_err']
        elif continuum_fit == 'exp':
            return self.table['exp_met_err']
        elif continuum_fit == 'best_fit':
            stellar_velocity = np.zeros(len(self.table))
            mask_best_fit_ssp = self.get_rcsed_continuum_fit_mask()

            stellar_velocity[mask_best_fit_ssp] = self.table[mask_best_fit_ssp]['ssp_met_err']
            stellar_velocity[~mask_best_fit_ssp] = self.table[~mask_best_fit_ssp]['exp_met_err']

            return stellar_velocity
        else:
            raise KeyError('continuum fit must be ssp, exp or best fit')

    def get_rcsed_gas_velocity_dispersion(self, line_shape='gauss', line_type='allowed'):
        r"""
        Calculate gas velocity dispersion (sigma)

        :param line_shape: Specified the emission-line fit. For the RCSED catalogue (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
        For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
        ('peak_1') or red-shifted component ('peak_2').
        :type line_shape: str
        :param line_type: Only for gaussian or non-parametric fit. 'allowed' is for Balmer lines and 'forbid' for the
        forbidden emission lines. For the double-peak components there is no distinction between balmer and forbidden
        lines.
        :type line_type: str

        :return gas velocity dispersion in km/s
        """
        if (line_shape == 'gauss') | (line_shape == 'nonpar'):
            if line_type == 'allowed':
                sigma_gas = self.table['allowed_sig_%s' % line_shape]
            elif line_type == 'forbid':
                sigma_gas = self.table['forbid_sig_%s' % line_shape]
            else:
                raise AttributeError('line_type must be allowed for Balmer lines or forbid for forbidden lines')
        else:
            sigma_gas = self.table['sigma_%s_stack' % line_shape[-1:]]

        return sigma_gas

    def get_rcsed_gas_velocity_dispersion_err(self, line_shape='gauss', line_type='allowed'):
        r"""
        Calculate gas velocity dispersion err (sigma_err)

        :param line_shape: Specified the emission-line fit. For the RCSED catalogue (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
        For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
        ('peak_1') or red-shifted component ('peak_2').
        :type line_shape: str
        :param line_type: Only for gaussian or non-parametric fit. 'allowed' is for Balmer lines and 'forbid' for the
        forbidden emission lines. For the double-peak components there is no distinction between balmer and forbidden
        lines.
        :type line_type: str

        :return gas velocity dispersion error in km/s
        """
        if (line_shape == 'gauss') | (line_shape == 'nonpar'):
            if line_type == 'allowed':
                sigma_gas_err = self.table['allowed_sig_err_%s' % line_shape]
            elif line_type == 'forbid':
                sigma_gas_err = self.table['forbid_sig_err_%s' % line_shape]
            else:
                raise AttributeError('line_type must be allowed for Balmer lines or forbid for forbidden lines')
        else:
            sigma_gas_err = self.table['sigma_err_%s_stack' % line_shape[-1:]]

        return sigma_gas_err

    def get_rcsed_age(self):
        r"""
        Access to the galaxy age computed by a simple stellar population Chilingarian et al. 2017.

        :return galaxy age in Gyr
        """
        return self.table['ssp_age'] / 1000

    def get_rcsed_age_err(self):
        r"""
        Access to the galaxy age error computed by a simple stellar population Chilingarian et al. 2017.

        :return galaxy age in Gyr
        """
        return self.table['ssp_age_err'] / 1000

    def get_rcsed_time_scale(self):
        r"""
        Access to the galaxy time_scale computed by an exponential declining star formation history
         Chilingarian et al. 2017.

        :return galaxy time_scale in Gyr
        """
        return self.table['exp_tau'] / 1000

    def get_rcsed_time_scale_err(self):
        r"""
        Access to the galaxy time_scale error computed by an exponential declining star formation history
        Chilingarian et al. 2017.

        :return galaxy time_scale in Gyr
        """
        return self.table['exp_tau_err'] / 1000


class DPAccess(RCSEDAccess):

    def __init__(self, **kwargs):
        r"""
        Class to access data from the catalog published by Maschmann et al. 2020
        """
        super().__init__(**kwargs)

    def get_delta_v(self):
        r"""
        Velocity difference between the blue- and the red-shifted double-peak component

        :return velocity difference in km/s
        :rtype: array_like
        """
        return self.table['delta_v']

    def get_delta_v_err(self):
        r"""
        Error of velocity difference between the blue- and the red-shifted double-peak component

        :return velocity difference in km/s
        :rtype: array_like
        """
        return self.table['delta_v_err']

    def get_mu_single_gauss(self):
        r"""
        Accesses mu parameter of single Gaussian approximation with respect to the SDSS emission-line position.
        Only for double-peak galaxies.

        :return mean velocity of gaussian emission line in km/s
        :rtype: array_like
        """
        return self.table['mu_single']

    def get_mu_err_single_gauss(self):
        r"""
        Accesses error of mu parameter of single Gaussian approximation with respect to the SDSS emission-line position.
        Only for double-peak galaxies.

        :return error on mean velocity of gaussian emission line in km/s
        :rtype: array_like
        """
        return self.table['mu_err_single']

    def get_dp_close_component(self):
        r"""
        function to get the closer emission line component of a DP catalog in units of sigma

        :return mask
        """
        stellar_velocity = self.get_rcsed_stellar_velocity()
        redshift_velocity = self.get_redshift() * constants.c * 1e-3

        mu_1 = self.table['mu_1_stack']
        mu_2 = self.table['mu_2_stack']

        sigma_1 = self.table['sigma_1_stack']
        sigma_2 = self.table['sigma_2_stack']

        stellar_mu_1 = mu_1 + redshift_velocity - stellar_velocity
        stellar_mu_2 = mu_2 + redshift_velocity - stellar_velocity

        first_peak_is_closer = abs(stellar_mu_1 / sigma_1) < abs(stellar_mu_2 / sigma_2)

        return first_peak_is_closer


class StellarAccess(object_identifier.ObjectIdentifier):

    def __init__(self, **kwargs):
        r"""
        Class to access star formation ratios and stellar mass estimates published in the catalog Salim et al. 2016,
        Brinchmann et al. 2004 and Kaufman et al. 2003
        """
        super().__init__(**kwargs)

    def get_log_sed_stellar_mass(self):
        r"""
        Access stellar mass computed from uv/spectral energy distribution (SED) fitting computed by
        Salim et al. 2016

        :return log_10 stellar mass in units of solar masses
        :rtype: array_like
        """
        return self.table['LOGMSTAR']

    def get_log_sed_stellar_mass_err(self):
        r"""
        Access stellar mass error computed from uv/spectral energy distribution (SED) fitting computed by
        Salim et al. 2016

        :return log_10 stellar mass in units of solar masses
        :rtype: array_like
        """
        return self.table['LOGMSTARERR']

    def get_log_mpa_stellar_mass(self):
        r"""
        Access stellar mass computed by Kauffmann et al. 2003. We return the median of the likelihood distribution.

        :return log_10 stellar mass in units of solar masses
        :rtype: array_like
        """
        return self.table['total_stellar_m_MEDIAN']

    def get_log_mpa_stellar_mass_err(self):
        r"""
        Access the 68%-quantile of the stellar mass computed by Kauffmann et al. 2003.
        We return the quantile of the likelihood distribution.

        :return log_10 stellar mass estimated uncertainties in units of solar masses
        :rtype: array_like tuple
        """
        median = self.table['total_stellar_m_MEDIAN']
        p_16 = self.table['total_stellar_m_P16']
        p_84 = self.table['total_stellar_m_P84']

        lower_error = median - p_16
        upper_error = p_84 - median

        return lower_error, upper_error

    def get_log_mpa_fiber_stellar_mass(self):
        r"""
        Access stellar mass computed by Kauffmann et al. 2003. We return the median of the likelihood distribution.

        :return log_10 stellar mass in units of solar masses
        :rtype: array_like
        """
        return self.table['fiber_stellar_m_MEDIAN']

    def get_log_mpa_fiber_stellar_mass_err(self):
        r"""
        Access the 68%-quantile of the stellar mass computed by Kauffmann et al. 2003.
        We return the quantile of the likelihood distribution.

        :return log_10 stellar mass estimated uncertainties in units of solar masses
        :rtype: array_like tuple
        """
        median = self.table['fiber_stellar_m_MEDIAN']
        p_16 = self.table['fiber_stellar_m_P16']
        p_84 = self.table['fiber_stellar_m_P84']

        lower_error = median - p_16
        upper_error = p_84 - median

        return lower_error, upper_error



    def get_log_mpa_sfr(self):
        r"""
        Access star formation ratio (SFR) computed by Brinchmann et al. 2004.
        We return the median of the likelihood distribution.

        :return log_10 SFR in units solar masses per year
        :rtype: array_like
        """
        return self.table['total_sfr_MEDIAN']

    def get_log_mpa_sfr_err(self):
        r"""
        Access 68%-quantile of the star formation ratio (SFR) computed by Brinchmann et al. 2004.
        We return the quantile of the likelihood distribution.
        :return estimated uncertainty of log_10 SFR in solar masses per year
        :rtype: array_like
        """
        median = self.table['total_sfr_MEDIAN']
        p_16 = self.table['total_sfr_P16']
        p_84 = self.table['total_sfr_P84']

        lower_error = median - p_16
        upper_error = p_84 - median

        return lower_error, upper_error

    def get_log_mpa_ssfr(self):
        r"""
        Access star formation ratio (SFR) computed by Brinchmann et al. 2004.
        We return the median of the likelihood distribution.

        :return log_10 SFR in units solar masses per year
        :rtype: array_like
        """
        return self.table['total_ssfr_MEDIAN']

    def get_log_mpa_ssfr_err(self):
        r"""
        Access 68%-quantile of the star formation ratio (SFR) computed by Brinchmann et al. 2004.
        We return the quantile of the likelihood distribution.
        :return estimated uncertainty of log_10 SFR in solar masses per year
        :rtype: array_like
        """
        median = self.table['total_ssfr_MEDIAN']
        p_16 = self.table['total_ssfr_P16']
        p_84 = self.table['total_ssfr_P84']

        lower_error = median - p_16
        upper_error = p_84 - median

        return lower_error, upper_error

    def get_log_mpa_fiber_ssfr(self):
        r"""
        Access specific star formation ratio (sSFR) of the 3"-SDSS fiber computed by Brinchmann et al. 2004.
        We return the median of the likelihood distribution.

        :return log_10 sSFR of the 3"-SDSS fiber in units of stellar masses per year
        :rtype: array_like
        """
        return self.table['fiber_ssfr_MEDIAN']

    def get_log_mpa_fiber_ssfr_err(self):
        r"""
        Access 68%-quantile of the specific star formation ratio (sSFR) of the 3"-SDSS fiber computed by Brinchmann et al. 2004.
        We return the quantile of the likelihood distribution.
        :return estimated uncertainty of log_10 SFR in units of stellar mass per year inside the 3"-SDSS fiber
        :rtype: array_like
        """
        median = self.table['fiber_ssfr_MEDIAN']
        p_16 = self.table['fiber_ssfr_P16']
        p_84 = self.table['fiber_ssfr_P84']

        lower_error = median - p_16
        upper_error = p_84 - median

        return lower_error, upper_error

    def get_log_mpa_fiber_sfr(self):
        r"""
        Access star formation ratio (SFR) of the 3"-SDSS fiber computed by Brinchmann et al. 2004.
        We return the median of the likelihood distribution.

        :return log_10 SFR of the 3"-SDSS fiber in units of solar masses per year
        :rtype: array_like
        """
        return self.table['fiber_sfr_MEDIAN']

    def get_log_mpa_fiber_sfr_err(self):
        r"""
        Access 68%-quantile of the star formation ratio (SFR) of the 3"-SDSS fiber computed by Brinchmann et al. 2004.
        We return the quantile of the likelihood distribution.
        :return estimated uncertainty of log_10 SFR in solar masses per year inside the 3"-SDSS fiber
        :rtype: array_like
        """
        median = self.table['fiber_sfr_MEDIAN']
        p_16 = self.table['fiber_sfr_P16']
        p_84 = self.table['fiber_sfr_P84']

        lower_error = median - p_16
        upper_error = p_84 - median

        return lower_error, upper_error

    def get_log_sed_sfr(self):
        r"""
        Access star formation ratio (SFR) computed from uv/spectral energy distribution (SED) fitting computed by
        Salim et al. 2016

        :return log_10 SFR derived from SED fitting
        :rtype: array_like
        """
        return self.table['LOGSFRSED']

    def get_log_sed_sfr_err(self):
        r"""
        Access error of star formation ratio (SFR) computed from uv/spectral energy distribution (SED) fitting
        computed by Salim et al. 2016

        :return log_10 error of SFR derived from SED fitting
        :rtype: array_like
        """
        return self.table['LOGSFRSEDERR']

    def get_log_sed_ssfr(self):
        r"""
        Access star formation ratio (SSFR) computed from (SED) fitting computed by
        Salim et al. 2016

        :return log_10 SFR derived from SED fitting
        :rtype: array_like
        """
        sed_sfr = self.table['LOGSFRSED']
        m_star = self.table['LOGMSTAR']

        return sed_sfr - m_star

    def get_log_sed_ssfr_err(self):
        r"""
        Access star formation ratio (SFR) computed from uv/spectral energy distribution (SED) fitting computed by
        Salim et al. 2016

        :return log_10 SFR derived from SED fitting
        :rtype: array_like
        """
        sed_sfr_err = self.table['LOGSFRSEDERR']
        m_star_err = self.table['LOGMSTARERR']

        return np.sqrt(sed_sfr_err ** 2 + m_star_err ** 2)

    def get_log_allwise_sfr(self):
        r"""
         Mid-IR star formation rate from WISE (AllWISE catalog) [solar masses / year] computed by Salim et al. 2016

        :return SFR [solar masses / year]
        :rtype: array_like
        """
        return self.table['LOGSFRAW']

    def get_log_unwise_sfr(self):
        r"""
        Mid-IR star formation rate from WISE (unWISE catalog) [solar masses / year] computed by Salim et al. 2016

        :return SFR [solar masses / year]
        :rtype: array_like
        """
        return self.table['LOGSFRUW']


class ImageAccess(object_identifier.ObjectIdentifier):
    def __init__(self, **kwargs):
        r"""
        class to access Hubble Space telescope data
        """
        super().__init__(**kwargs)

    def get_galaxy_snapshot(self, index, mode='sdss'):

        url_string, file_path = self.get_snap_shot_file_path(index, mode=mode)
        fiberid = str(int(self.get_fiberid()[index]))
        # file_name = str(fiberid) + '.jpg'
        # print((file_path / fiberid).with_suffix('.jpg'))
        print(url_string)
        if os.path.isfile((file_path / fiberid).with_suffix('.jpg')):
            print((file_path / fiberid).with_suffix('.jpg'), 'already exists')
        else:
            if mode == 'sdss':
                http = urllib3.PoolManager()
                r = http.request('GET', url_string, preload_content=False)
                if not os.path.isdir(file_path):
                    os.makedirs(file_path)
                with open((file_path / fiberid).with_suffix('.jpg'), 'wb') as out:
                    while True:
                        data = r.read()
                        if not data:
                            break
                        out.write(data)
            else:
                from PIL import Image
                http = urllib3.PoolManager()
                r = http.request('GET', url_string, preload_content=False)
                with open(Path('/tmp') / Path('test_image_%s_%s' % (mode, fiberid)).with_suffix('.jpg'), 'wb') as out:
                    while True:
                        data = r.read()
                        if not data:
                            break
                        out.write(data)
                        try:
                            # img = Image.open(open('/tmp/test_image_%s_%s' % (mode, file_name), 'rb'))
                            img = Image.open(Path('/tmp') / Path('test_image_%s_%s' % (mode, fiberid)).with_suffix('.jpg'))
                            clrs = img.getcolors()
                            if not clrs:
                                if not os.path.isdir(file_path):
                                    os.makedirs(file_path)
                                img.save((file_path / fiberid).with_suffix('.jpg'))
                            else:
                                print('only black image')
                            img.close()
                        except:
                                print('image not readable')
                os.remove(Path('/tmp') / Path('test_image_%s_%s' % (mode, fiberid)).with_suffix('.jpg'))
            r.release_conn()

    def get_legacy_coord_image(self, size, pixel_scale, index=0):

        import matplotlib.image as mpimg

        # download legacy survey images
        url_legacy_g = self.get_observation_image_url(self.get_ra()[index], self.get_dec()[index], size, pixel_scale,
                                                               layer='dr8', bands='g', file_format='fits')
        url_legacy_rgb_jpeg = self.get_observation_image_url(self.get_ra()[index], self.get_dec()[index], size, pixel_scale,
                                                                      layer='dr8', file_format='jpeg')

        file_path = self.get_image_path(index=index)

        file_name_legacy_g = Path('large_field_legacy_%i_%i_%i_pixel_scale_%i_size_%i_offset_g' %
                                 (int(self.get_mjd()[index]), int(self.get_plate()[index]), int(self.get_fiberid()[index]), int(pixel_scale), int(size))).with_suffix('.fits')
        file_name_legacy_rgb = Path('large_field_legacy_%i_%i_%i_pixel_scale_%i_size_%i_offset_rgb' %
                                   (int(self.get_mjd()[index]), int(self.get_plate()[index]), int(self.get_fiberid()[index]), int(pixel_scale), int(size))).with_suffix('.jpeg')
        # get data
        if not os.path.isdir(file_path):
            os.makedirs(file_path)

        self.download_file(file_path, file_name_legacy_g, url_legacy_g, unpack=False)
        self.download_file(file_path, file_name_legacy_rgb, url_legacy_rgb_jpeg, unpack=False)

        g_hdu = fits.open(file_path / file_name_legacy_g)[0]
        g_header = g_hdu.header
        img_rgb = mpimg.imread(file_path / file_name_legacy_rgb)

        return g_header, img_rgb

    def get_hsc_dr2_coord_image(self, size, pixel_scale, index=0):

        import matplotlib.image as mpimg

        # download legacy survey images
        url_legacy_g = self.get_observation_image_url(self.get_ra()[index], self.get_dec()[index], size, pixel_scale,
                                                               layer='hsc-dr2', bands='g', file_format='fits')
        url_legacy_rgb_jpeg = self.get_observation_image_url(self.get_ra()[index], self.get_dec()[index], size, pixel_scale,
                                                                      layer='hsc-dr2', file_format='jpeg')

        file_path = self.get_image_path(index=index)

        file_name_legacy_g = Path('large_field_legacy_%i_%i_%i_pixel_scale_%i_size_%i_offset_g' %
                                 (int(self.get_mjd()[index]), int(self.get_plate()[index]), int(self.get_fiberid()[index]), int(pixel_scale), int(size))).with_suffix('.fits')
        file_name_legacy_rgb = Path('large_field_legacy_%i_%i_%i_pixel_scale_%i_size_%i_offset_rgb' %
                                   (int(self.get_mjd()[index]), int(self.get_plate()[index]), int(self.get_fiberid()[index]), int(pixel_scale), int(size))).with_suffix('.jpeg')
        # get data
        if not os.path.isdir(file_path):
            os.makedirs(file_path)

        self.download_file(file_path, file_name_legacy_g, url_legacy_g, unpack=False)
        self.download_file(file_path, file_name_legacy_rgb, url_legacy_rgb_jpeg, unpack=False)

        g_hdu = fits.open(file_path / file_name_legacy_g)[0]
        g_header = g_hdu.header
        img_rgb = mpimg.imread(file_path / file_name_legacy_rgb)

        return g_header, img_rgb

    def get_lotss_dr1_image(self, size=2 * 60, index=0):
        r"""

        :param size: image size in arc seconds
        :param index: index
        :return:
        """

        url = self.get_lotss_dr1_image_url(size=size, index=index)
        file_path = self.get_image_path(index=index)

        file_name_lotss_dr1_image = Path('lotss_dr1_image_%i_%i_%i_size_%i' % (int(self.get_mjd()[index]),
                                                                       int(self.get_plate()[index]),
                                                                       int(self.get_fiberid()[index]),
                                                                       int(size))).with_suffix('.fits')

        # get data
        if not os.path.isdir(file_path):
            os.makedirs(file_path)

        self.download_file(file_path, file_name_lotss_dr1_image, url, unpack=False)

        hdu = fits.open(file_path / file_name_lotss_dr1_image)[0]
        header = hdu.header
        data = hdu.data

        return header, data

    def get_first_image(self, size=2 * 60, index=0):
        r"""

        :param size: image size in arc seconds
        :param index: index
        :return:
        """

        url = self.get_first_image_url(size=size, index=index)

        file_path = self.get_image_path(index=index)

        file_name_first_image = Path('first_image_%i_%i_%i_size_%i' % (int(self.get_mjd()[index]),
                                                                       int(self.get_plate()[index]),
                                                                       int(self.get_fiberid()[index]),
                                                                       int(size))).with_suffix('.fits')
        # get data
        if not os.path.isdir(file_path):
            os.makedirs(file_path)

        self.download_file(file_path, file_name_first_image, url, unpack=False)

        hdu = fits.open(file_path / file_name_first_image)[0]
        header = hdu.header
        data = hdu.data

        return header, data

    def get_vlass_image(self, size, pixel_scale, index=0):

        import matplotlib.image as mpimg

        # download legacy survey images
        url_legacy_g = self.get_observation_image_url(self.get_ra()[index], self.get_dec()[index], size, pixel_scale,
                                                               layer='vlass1.2', bands='g', file_format='fits')

        file_path = self.get_image_path(index=index)

        file_name = Path('vlass_%i_%i_%i_pixel_scale_%i_size_%i_offset_g' %
                                 (int(self.get_mjd()[index]), int(self.get_plate()[index]), int(self.get_fiberid()[index]), int(pixel_scale), int(size))).with_suffix('.fits')

        # get data
        if not os.path.isdir(file_path):
            os.makedirs(file_path)

        self.download_file(file_path, file_name, url_legacy_g, unpack=False)

        hdu = fits.open(file_path / file_name)[0]
        header = hdu.header
        data = hdu.data

        return data, header

    def load_hst_observation_meta_data(self, search_radius=3/3600, index=0):

        from astroquery.mast import Observations
        ra_dec = '%.6f %.6f' % (self.get_ra()[index], self.get_dec()[index])
        # print(ra_dec)
        # load observation query using ICRS coordinate system
        obs_table = Observations.query_region(ra_dec, radius='%f deg' % search_radius)
        # find hst observation
        hst_observation_mask = obs_table['obs_collection'] == 'HST'
        hst_observation_table = obs_table[hst_observation_mask]

        # compose meta data
        # keywords are explained under https://mast.stsci.edu/api/v0/_c_a_o_mfields.html
        obsid = np.array(hst_observation_table['obsid'])
        proposal_id = np.array(hst_observation_table['proposal_id'])
        proposal_pi = np.array(hst_observation_table['proposal_pi'])
        instrument_name = np.array(hst_observation_table['instrument_name'])
        filters = np.array(hst_observation_table['filters'])
        t_exptime = np.array(hst_observation_table['t_exptime'])

        # the ACS - Advanced Camera for Surveys was installed o the 7th march 2002 (mjd = 52340)
        # and is a high resolution camera which is still today
        # a good reference therefore we exclude all observation made before this date
        # get time of observation in MJD
        start_time = np.array(hst_observation_table['t_min'])
        # create a list of indices which should be deleted
        delete_indices = np.where(start_time < 52340)

        obsid = np.delete(obsid, delete_indices)
        proposal_id = np.delete(proposal_id, delete_indices)
        proposal_pi = np.delete(proposal_pi, delete_indices)
        instrument_name = np.delete(instrument_name, delete_indices)
        filters = np.delete(filters, delete_indices)
        t_exptime = np.delete(t_exptime, delete_indices)
        start_time = np.delete(start_time, delete_indices)

        meta_data = {'obsid': obsid,
                     'proposal_id': proposal_id,
                     'proposal_pi': proposal_pi,
                     'instrument_name': instrument_name,
                     'filters': filters,
                     't_exptime': t_exptime,
                     'start_time': start_time}

        return meta_data

    def download_hst_observation(self, search_radius=3/3600, folder_path=None, index=0):

        from astroquery.mast import Observations

        # get meta data of all observation
        meta_data = self.load_hst_observation_meta_data(search_radius=search_radius, index=index)
        # print(meta_data)
        # check here if hst observation exists otherwise return None.
        if len(meta_data['obsid']) == 0:
            print('there is no HST observation')
            return None

        # folder path
        if folder_path is None:
            folder_path = self.get_image_path(index=index)

        # get observation id
        obsid_list = meta_data['obsid']
        hst_image_file_names = np.zeros(len(obsid_list), dtype='object')
        # print(obsid_list)
        for obsid, i in zip(obsid_list, range(0, len(obsid_list))):

            # print(obsid, i)
            # all keywords of data products are accessible under https://mast.stsci.edu/api/v0/_productsfields.html
            data_products = Observations.get_product_list(obsid)
            data_products.mask = False
            image_mask = data_products['obsID'] == obsid

            if 'DRC' in data_products['productSubGroupDescription'][image_mask]:
                # print('there is corrected data')
                image_mask *= (data_products['productSubGroupDescription'] == 'DRC')
            elif 'DRZ' in data_products['productSubGroupDescription'][image_mask]:
                image_mask *= (data_products['productSubGroupDescription'] == 'DRZ')
            else:
                # there is no reduced HST observation available
                # access HLA data
                image_mask = ((data_products['description'] == 'HLA simple fits science image') &
                              (data_products['productSubGroupDescription'] == 'DRZ') &
                              (data_products['type'] == 'C'))
                # raise ValueError('there is no processed HST observation')

            # print(image_mask)

            # for j in range(0, len(data_products)):
            #     #if usefull_objects[i]:
            #         # print(data_products['type'][i])
            #         #print(data_products[])
            #         print(data_products['description'][j], '!',
            #               data_products['productSubGroupDescription'][j], '!',
            #               data_products['productType'][j], '!',
            #               data_products['type'][j], '!',
            #               data_products['productFilename'][j], '!',
            #               data_products['size'][j])

            if sum(image_mask) == 0:
                continue

            # download hst images
            manifest = Observations.download_products(data_products[image_mask], download_dir=str(folder_path))

              # specify file_names for metadata
            hst_image_file_names[i] = manifest.columns['Local Path'].data[0]

        # check if there was a usable file:
        if np.all((hst_image_file_names) == 0):
            return None

        # update meta data
        meta_data.update({'hst_image_file_names': hst_image_file_names})

        # save meta data
        # make sure the path exists
        if not os.path.isdir(folder_path):
            os.makedirs(folder_path)
        meta_data_file_name = folder_path / 'mastDownload' / 'hst_meta_data.npy'

        np.save(meta_data_file_name, meta_data)

        return meta_data

    def check_for_hst_observation(self, search_radius=3/3600, folder_path=None, index=0):

        if folder_path is None:
            folder_path = self.get_image_path(index=index)
        meta_data_file_name = folder_path / 'mastDownload' / 'hst_meta_data.npy'

        # first check if meta data exists because if so the file already has been downloaded
        if os.path.isfile(meta_data_file_name):
            meta_data = np.load(meta_data_file_name, allow_pickle=True).item()
            return meta_data
        else:
            # check if HST observation exists
            meta_data = self.download_hst_observation(search_radius=search_radius, folder_path=folder_path, index=index)
            if meta_data is not None:
                return meta_data
            else:
                return None

    def plot_hst_image(self, frame_size=0.5, scaling='linear', v_min=0, v_max=1, pixel_cutout=100, band_index=0,
                       folder_path=None, index=0):

        hst_observation_meta_data = self.check_for_hst_observation(folder_path=folder_path, index=index)

        # end function in case there is no observation
        if hst_observation_meta_data is None:
            return None

        # get ra and dec to locate object in image
        ra = self.get_ra()[index]
        dec = self.get_dec()[index]
        print(ra, dec)
        from kapteyn import maputils
        from matplotlib import pyplot as plt

        # create
        fits_image = maputils.FITSimage(hst_observation_meta_data['hst_image_file_names'][band_index], hdunr=1)

        # get pixel coordinates of object
        px, py = fits_image.proj.topixel((ra, dec))

        # if we want to have log scale we just change the image array manually
        if scaling == 'log':
            fits_image.boxdat = np.log10(fits_image.boxdat)

        # calculate the number of pixels in 1 arcminute
        ra_min, dec_min = fits_image.proj.topixel((ra - frame_size / 2 / 60, dec - frame_size / 2 / 60))
        ra_max, dec_max = fits_image.proj.topixel((ra + frame_size / 2 / 60, dec + frame_size / 2 / 60))

        one_arc_min = ra_max - ra_min

        # fits_image.set_limits(pxlim=(int(px - one_arc_min/2), int(px + one_arc_min/2)),
        #                       pylim=(int(py - one_arc_min/2), int(py + one_arc_min/2)))
        #
        fits_image.set_limits(pxlim=(int(px - pixel_cutout), int(px + pixel_cutout)),
                     pylim=(int(py - pixel_cutout), int(py + pixel_cutout)))

        fig = plt.figure()
        frame = fig.add_subplot(1, 1, 1)
        if scaling == 'log':
            annim = fits_image.Annotatedimage(frame, clipmin=np.log10(v_min), clipmax=np.log10(v_max), cmap="binary")
        elif scaling == 'linear':
            annim = fits_image.Annotatedimage(frame, clipmin=v_min, clipmax=v_max, cmap="binary")
        else:
            raise KeyError('scaling nut understand must be linear or log')

        # annim.interact_imagecolors()
        annim.Image()
        grat = annim.Graticule()

        # plot circle around source
        annim.Skypolygon("ellipse", cpos="%.4f deg %.4f deg" % (ra, dec), major=3, minor=3,
                         pa=-30.0, linewidth=1, units='arcsec', fill=False, color='b')

        annim.plot()
        annim.interact_imagecolors()
        annim.interact_toolbarinfo()

        # plt.show()
        plt.savefig('plot_output/HST_instrument_%s_filter_%s_dp_%i.png' % (hst_observation_meta_data['instrument_name'][band_index].replace('/', '_'),
                                                                       hst_observation_meta_data['filters'][band_index],
                                                                       index))
        # print(data_products.colnames)
        #
        #
        # print(data_products[mask_one])
        # print(data_products['productFilename'][mask_one])
        # print(data_products['productSubGroupDescription'][mask_one])
        # # print(data_products['dataURI'][mask_one])
        #
        # if 'DRC' in data_products['productSubGroupDescription'][mask_one]:
        #     print('there is corrected data')
        #     mask_one *= (data_products['productSubGroupDescription'] == 'DRC')
        # elif 'DRZ' in data_products['productSubGroupDescription'][mask_one]:
        #     mask_one *= (data_products['productSubGroupDescription'] == 'DRZ')
        # else:
        #     print('no')
        # mask_one.mask = False
        # print(mask_one)
        #
        # folder_path = self.get_image_path(index=index)
        # manifest = Observations.download_products(data_products[mask_one], download_dir=str(folder_path))
        #
        # print(manifest)

        # print(meta_data)
        # exit()
        # data_products = Observations.get_product_list(obsid)

        # print(data_products[data_products['proposal_id'] == 15444])
        # exit()
        # print(data_products)
        # print(data_products.colnames)
        # file_name_1 = 'hst_%i_'
        # print(data_products['obs_id'])
        #print(ra_dec)
        # #print(filters)
        # description = data_products['description']
        # productSubGroupDescription = data_products['productSubGroupDescription']
        # productType = data_products['productType']
        # type = data_products['type']
        # productFilename = data_products['productFilename']
        #
        # usefull_objects = (#(description == 'DADS DRZ file') &
        #                    #(productSubGroupDescription == 'DRZ') &
        #                    (productType == 'SCIENCE') &
        #                    (type == 'C'))
        #
        # # colname_list = data_products.colnames
        # # print(productType)
        # # exit()
        # for i in range(0, len(data_products)):
        #     if usefull_objects[i]:
        #         # print(data_products['type'][i])
        #         #print(data_products[])
        #         print(data_products['description'][i],
        #               data_products['productSubGroupDescription'][i],
        #               data_products['productType'][i],
        #               data_products['type'][i],
        #               data_products['productFilename'][i],
        #               data_products['proposal_id'][i])
        #
        # exit()
        # folder_path = self.data_path / Path('images/%i/%i/%i/' % (int(self.get_mjd()),
        #                                                           int(self.get_plate()),
        #                                                           int(self.get_fiberid())))
        #
        # manifest = Observations.download_products(data_products, download_dir=str(folder_path))
        #
        # print(manifest)


class EmissionLineAccess(object_identifier.ObjectIdentifier):
    def __init__(self, **kwargs):
        r"""
        class to access images of different surveys and instruments
        """
        super().__init__(**kwargs)

    def get_emission_line_flux(self, line_wavelength, line_shape='gauss'):
        r"""
        Access to the emission-line flux

        :param line_wavelength: line of specific line in angstrom
        :type line_wavelength: int
        :param line_shape: Specified the emission-line fit. For the RCSED catalog (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
        For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
        ('peak_1') or red-shifted component ('peak_2').
        :type line_shape: str

        """
        if (line_shape == 'gauss') | (line_shape == 'nonpar'):
            line_name = self.em_wavelength[line_wavelength]['line_name']
            flux_string = 'f' + str(line_wavelength) + '_' + line_name + '_flx_' + line_shape
            return self.table[flux_string]
        elif (line_shape == 'peak_1') | (line_shape == 'peak_2'):
            flux_string = 'flux_%i_%s' % (line_wavelength, line_shape[-1:])
            return self.table[flux_string]
        elif (line_shape == 'manga_dp_1') | (line_shape == 'manga_dp_2'):
            flux_string = 'flux_%s_%i' % (line_shape[-1:], line_wavelength)
            return self.manga_double_fit_parameters[flux_string]
        elif (line_shape == 'manga_sp'):
            flux_string = 'flux_%i' % (line_wavelength)
            return self.manga_single_fit_parameters[flux_string]
        else:
            raise KeyError('Line shape not understood')

    def get_emission_line_flux_err(self, line_wavelength, line_shape='gauss'):
        r"""
        Access to the emission-line flux error

        :param line_wavelength: line of specific line in angstrom
        :type line_wavelength: int
        :param line_shape: Specified the emission-line fit. For the RCSED catalog (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
        For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
        ('peak_1') or red-shifted component ('peak_2').
        :type line_shape: str

        """
        if (line_shape == 'gauss') | (line_shape == 'nonpar'):
            line_name = self.em_wavelength[line_wavelength]['line_name']
            flux_string = 'f' + str(line_wavelength) + '_' + line_name + '_flx_err_' + line_shape
            return self.table[flux_string]
        elif (line_shape == 'peak_1') | (line_shape == 'peak_2'):
            flux_string = 'flux_%i_%s_err' % (line_wavelength, line_shape[-1:])
            return self.table[flux_string]
        elif (line_shape == 'manga_dp_1') | (line_shape == 'manga_dp_2'):
            flux_string = 'flux_%s_%i_err' % (line_shape[-1:], line_wavelength)
            return self.manga_double_fit_parameters[flux_string]
        elif (line_shape == 'manga_sp'):
            flux_string = 'flux_%i_err' % (line_wavelength)
            return self.manga_single_fit_parameters[flux_string]
        else:
            raise KeyError('Line shape not understood')

    def get_emission_line_snr(self, line_wavelength, line_shape='gauss'):
        r"""
        Access to the S/N of emission-lines

        :param line_wavelength: line of specific line in angstrom
        :type line_wavelength: int
        :param line_shape: Specified the emission-line fit. For the RCSED catalog (Chilingarian et al.2017) this
        can be 'gauss' or 'nonpar' for a gaussian or a non-parametric line approximation respectively.
        For Maschmann et al. (in prep.) this is dedicated to the double gaussian fit and denotes the blue-shifted
        ('peak_1') or red-shifted component ('peak_2').
        :type line_shape: str
        :return S/N of assumed with an emission-line fit
        :rtype: array_like
        """
        return (self.get_emission_line_flux(line_wavelength=line_wavelength, line_shape=line_shape) /
                self.get_emission_line_flux_err(line_wavelength=line_wavelength, line_shape=line_shape))

    def get_hi_line_spec(self, hi_catalog='alfalfa', index=0):
        if hi_catalog == 'xgass':
            file_path = self.data_path / 'tables' / 'xgass' / 'xGASS_FITS'
            spec_file_name = 'G%i.fits' % self.table['GASS'][index]
            if not os.path.isfile(file_path / spec_file_name):
                file_path = self.get_hi_object_file_path(index)
                spec_file_name, url = self.get_radio_hi_source_identifier(only_file_name=True, hi_catalog=hi_catalog,
                                                                          index=index)
                if not os.path.isfile(file_path / spec_file_name):
                    spec_file_name, url = self.get_radio_hi_source_identifier(only_file_name=False,
                                                                              hi_catalog=hi_catalog, index=index)
                    self.download_file(file_path=file_path, file_name=spec_file_name, url=url)

        else:
            file_path = self.get_hi_object_file_path(index)
            spec_file_name, url = self.get_radio_hi_source_identifier(only_file_name=True, hi_catalog=hi_catalog,
                                                                      index=index)
            if not os.path.isfile(file_path / spec_file_name):
                spec_file_name, url = self.get_radio_hi_source_identifier(only_file_name=True, hi_catalog=hi_catalog,
                                                                          suffix='txt', index=index)
                if not os.path.isfile(file_path / spec_file_name):
                    spec_file_name, url = self.get_radio_hi_source_identifier(only_file_name=False, hi_catalog=hi_catalog,
                                                                              index=index)
                    self.download_file(file_path=file_path, file_name=spec_file_name, url=url)

        print(file_path / spec_file_name)
        if spec_file_name[-4:] == 'fits':
            hdu = self.open_hdu(file_path=file_path, file_name=spec_file_name, data_type='hi', index=index)
            data_array = np.array([list(item) for item in hdu[1].data])
            # print(data_array.shape)
            velocity = data_array[:, 0]
            frequency = data_array[:, 1]
            flux = data_array[:, 2]
        else:
            # print(file_path / spec_file_name)
            # spec_file_name = spec_file_name[:-4] + 'txt'
            print(file_path / spec_file_name)
            if not os.path.isfile(file_path / spec_file_name):
                self.download_hi_line_spectrum_file(index=index)
            data = np.genfromtxt(file_path / spec_file_name)
            # print(data)
            # print(data.shape)
            velocity = data[:, 0]
            frequency = data[:, 3]
            flux = data[:, 1]

        return velocity, frequency, flux

    def get_co_flux(self, co_line_hierarchy=None, limit_flag=None, line_ratios=None):
        r"""
        Function to compute the limits of CO luminosity and detection flag flags
        :param invert_limit_flag:
        :param co_line_hierarchy:
        :param limit_flag:
        :return:
        """

        if line_ratios is None:
            line_ratios = [1, 0.77, 0.5, 0.42]

        if co_line_hierarchy is None:
            co_line_hierarchy = ['flux_co10', 'flux_co21', 'flux_co32', 'flux_co43']

        if limit_flag is None:
            limit_flag = ['limit_flag_co10', 'limit_flag_co21', 'limit_flag_co32', 'limit_flag_co43']

        co_flux = np.zeros(len(self.table)) * np.nan
        # co_limit_flag = np.zeros(len(self.table)) * np.nan

        selected_co_flux = np.zeros(len(self.table), dtype=bool)
        selected_co_limit_flag = np.zeros(len(self.table), dtype=bool)

        for co_line, flag, ratio in zip(co_line_hierarchy, limit_flag, line_ratios):
            if co_line not in self.col_names:
                continue

            detected_co_flux = self.table[flag] == 0
            non_detected_co_flux = self.table[flag] == 1

            # print(detected_co_flux)
            co_flux[detected_co_flux * ~selected_co_flux] = self.table[co_line][detected_co_flux * ~selected_co_flux] / ratio

            selected_co_flux[detected_co_flux * ~selected_co_flux] = True

            co_flux[non_detected_co_flux * ~selected_co_flux * ~selected_co_limit_flag] = self.table[co_line][non_detected_co_flux * ~selected_co_flux * ~selected_co_limit_flag] / ratio

            selected_co_limit_flag[non_detected_co_flux * ~selected_co_limit_flag] = True

        # overwrite limit flag if other band was detected
        selected_co_limit_flag[selected_co_flux] = False

        return co_flux, np.invert(selected_co_limit_flag)

    def get_co_flux_err(self, co_line_hierarchy=None, limit_flag=None, line_ratios=None):
        r"""
        Function to compute the limits of CO flux and detection flag flags
        :param invert_limit_flag:
        :param co_line_hierarchy:
        :param limit_flag:
        :return:
        """

        if line_ratios is None:
            line_ratios = [1, 0.77, 0.5, 0.42]

        if co_line_hierarchy is None:
            co_line_hierarchy = ['flux_co10_err', 'flux_co21_err', 'flux_co32_err', 'flux_co43_err']

        if limit_flag is None:
            limit_flag = ['limit_flag_co10', 'limit_flag_co21', 'limit_flag_co32', 'limit_flag_co43']

        co_flux_err = np.zeros(len(self.table)) * np.nan
        # co_limit_flag = np.zeros(len(self.table)) * np.nan

        selected_co_flux = np.zeros(len(self.table), dtype=bool)
        selected_co_limit_flag = np.zeros(len(self.table), dtype=bool)

        for co_line, flag, ratio in zip(co_line_hierarchy, limit_flag, line_ratios):
            if co_line not in self.col_names:
                continue

            detected_co_flux = self.table[flag] == 0
            non_detected_co_flux = self.table[flag] == 1

            # print(detected_co_flux)
            co_flux_err[detected_co_flux * ~selected_co_flux] = self.table[co_line][detected_co_flux * ~selected_co_flux] / ratio

            selected_co_flux[detected_co_flux * ~selected_co_flux] = True

            co_flux_err[non_detected_co_flux * ~selected_co_flux * ~selected_co_limit_flag] = self.table[co_line][non_detected_co_flux * ~selected_co_flux * ~selected_co_limit_flag] / ratio

            selected_co_limit_flag[non_detected_co_flux * ~selected_co_limit_flag] = True

        return co_flux_err

    def get_co_rel_err(self, co_line_hierarchy=None, limit_flag=None, line_ratios=None):
        co_flux = self.get_co_flux(co_line_hierarchy=co_line_hierarchy, limit_flag=limit_flag,
                                   line_ratios=line_ratios)[0]
        co_flux_err = self.get_co_flux_err(co_line_hierarchy=co_line_hierarchy, limit_flag=limit_flag,
                                           line_ratios=line_ratios)
        return co_flux_err / co_flux

    def get_co_luminosity(self, co_line_hierarchy=None, limit_flag=None, line_ratios=None):
        r"""
        Function to compute the limits of CO luminosity and detection flag flags
        :param invert_limit_flag:
        :param co_line_hierarchy:
        :param limit_flag:
        :return:
        """

        if line_ratios is None:
            line_ratios = [1, 0.77, 0.5, 0.42]

        if co_line_hierarchy is None:
            co_line_hierarchy = ['L_co10', 'L_co21', 'L_co32', 'L_co43']

        if limit_flag is None:
            limit_flag = ['limit_flag_co10', 'limit_flag_co21', 'limit_flag_co32', 'limit_flag_co43']

        co_luminosity = np.zeros(len(self.table)) * np.nan
        # co_limit_flag = np.zeros(len(self.table)) * np.nan

        selected_co_luminosity = np.zeros(len(self.table), dtype=bool)
        selected_co_limit_flag = np.zeros(len(self.table), dtype=bool)

        for co_line, flag, ratio in zip(co_line_hierarchy, limit_flag, line_ratios):
            if co_line not in self.col_names:
                continue

            detected_co_luminosity = self.table[flag] == 0
            non_detected_co_luminosity = self.table[flag] == 1

            # print(detected_co_luminosity)
            co_luminosity[detected_co_luminosity * ~selected_co_luminosity] = self.table[co_line][detected_co_luminosity * ~selected_co_luminosity] / ratio

            selected_co_luminosity[detected_co_luminosity * ~selected_co_luminosity] = True

            co_luminosity[non_detected_co_luminosity * ~selected_co_luminosity * ~selected_co_limit_flag] = self.table[co_line][non_detected_co_luminosity * ~selected_co_luminosity * ~selected_co_limit_flag] / ratio

            selected_co_limit_flag[non_detected_co_luminosity * ~selected_co_limit_flag] = True

        # overwrite limit flag if other band was detected
        selected_co_limit_flag[selected_co_luminosity] = False

        return co_luminosity, np.invert(selected_co_limit_flag)

    def get_co_luminosity_err(self, co_line_hierarchy=None, limit_flag=None, line_ratios=None):
        r"""
        Function to compute the limits of CO luminosity and detection flag flags
        :param invert_limit_flag:
        :param co_line_hierarchy:
        :param limit_flag:
        :return:
        """

        if line_ratios is None:
            line_ratios = [1, 0.77, 0.5, 0.42]

        if co_line_hierarchy is None:
            co_line_hierarchy = ['L_co10_err', 'L_co21_err', 'L_co32_err', 'L_co43_err']

        if limit_flag is None:
            limit_flag = ['limit_flag_co10', 'limit_flag_co21', 'limit_flag_co32', 'limit_flag_co43']

        co_luminosity_err = np.zeros(len(self.table)) * np.nan
        # co_limit_flag = np.zeros(len(self.table)) * np.nan

        selected_co_luminosity = np.zeros(len(self.table), dtype=bool)
        selected_co_limit_flag = np.zeros(len(self.table), dtype=bool)

        for co_line, flag, ratio in zip(co_line_hierarchy, limit_flag, line_ratios):
            if co_line not in self.col_names:
                continue

            detected_co_luminosity = self.table[flag] == 0
            non_detected_co_luminosity = self.table[flag] == 1

            # print(detected_co_luminosity)
            co_luminosity_err[detected_co_luminosity * ~selected_co_luminosity] = self.table[co_line][detected_co_luminosity * ~selected_co_luminosity] / ratio

            selected_co_luminosity[detected_co_luminosity * ~selected_co_luminosity] = True

            co_luminosity_err[non_detected_co_luminosity * ~selected_co_luminosity * ~selected_co_limit_flag] = self.table[co_line][non_detected_co_luminosity * ~selected_co_luminosity * ~selected_co_limit_flag] / ratio

            selected_co_limit_flag[non_detected_co_luminosity * ~selected_co_limit_flag] = True

        return co_luminosity_err

    def get_co_line_flux(self, co_line='co10'):
        if co_line == 'co10':
            return self.table['flux_co10']
        elif co_line == 'co21':
            return self.table['flux_co21']
        elif co_line == 'co32':
            return self.table['flux_co32']
        elif co_line == 'co43':
            return self.table['flux_co43']

    def get_co_line_flux_err(self, co_line='co10'):
        if co_line == 'co10':
            return self.table['flux_co10_err']
        elif co_line == 'co21':
            return self.table['flux_co21_err']
        elif co_line == 'co32':
            return self.table['flux_co32_err']
        elif co_line == 'co43':
            return self.table['flux_co43_err']

    def get_co_line_luminosity(self, co_line='co10'):
        if co_line == 'co10':
            return self.table['L_co10']
        elif co_line == 'co21':
            return self.table['L_co21']
        elif co_line == 'co32':
            return self.table['L_co32']
        elif co_line == 'co43':
            return self.table['L_co43']

    def get_co_line_luminosity_err(self, co_line='co10'):
        if co_line == 'co10':
            return self.table['L_co10_err']
        elif co_line == 'co21':
            return self.table['L_co21_err']
        elif co_line == 'co32':
            return self.table['L_co32_err']
        elif co_line == 'co43':
            return self.table['L_co43_err']

    def get_co_limit_flag(self, co_line='co10'):
        if co_line == 'co10':
            return self.table['limit_flag_co10'] == 1
        elif co_line == 'co21':
            return self.table['limit_flag_co21'] == 1
        elif co_line == 'co32':
            return self.table['limit_flag_co32'] == 1
        elif co_line == 'co43':
            return self.table['limit_flag_co43'] == 1


class EmissionLineRatio(EmissionLineAccess):
    def __init__(self, **kwargs):
        r"""
        class to access emission line ratios and limits of ratios used for diagnostic diagrams such as BPT diagrams.
        """
        super().__init__(**kwargs)

    def get_log10_ratio_oiii_h_beta(self, line_shape='gauss'):
        r"""
        logarithmic emission line ratio of the [OIII]5008 and Hbeta4863 emission line for the y axis of BPT diagrams
        """
        return np.log10(self.get_emission_line_flux(line_wavelength=5008, line_shape=line_shape) /
                        self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape))

    def get_log10_ratio_oiii_h_beta_err(self, line_shape='gauss'):
        r"""
        logarithmic emission line ratio of the [OIII]5008 and Hbeta4863 emission line for the y axis of BPT diagrams
        """
        oiii_5008_flux = self.get_emission_line_flux(line_wavelength=5008, line_shape=line_shape)
        oiii_5008_flux_err = self.get_emission_line_flux_err(line_wavelength=5008, line_shape=line_shape)
        h_beta_flux = self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape)
        h_beta_flux_err = self.get_emission_line_flux_err(line_wavelength=4863, line_shape=line_shape)

        ratio_err = np.sqrt((oiii_5008_flux_err / (oiii_5008_flux * np.log(10)))**2 +
                            (h_beta_flux_err / (h_beta_flux * np.log(10)))**2)

        return ratio_err

    def get_log10_ratio_nii_h_alpha(self, line_shape='gauss'):
        r"""
        logarithmic emission line ratio of the [NII]6585 and Halpha6565 emission line for the x axis of the first BPT
         diagram
        """
        return np.log10(self.get_emission_line_flux(line_wavelength=6585, line_shape=line_shape) /
                        self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape))

    def get_log10_ratio_nii_h_alpha_err(self, line_shape='gauss'):
        r"""
        logarithmic emission line ratio err of the [NII]6585 and Halpha6565 emission line for the x axis of the first
         BPT diagram
        """
        nii_6585_flux = self.get_emission_line_flux(line_wavelength=6585, line_shape=line_shape)
        nii_6585_flux_err = self.get_emission_line_flux_err(line_wavelength=6585, line_shape=line_shape)
        h_alpha_flux = self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape)
        h_alpha_flux_err = self.get_emission_line_flux_err(line_wavelength=6565, line_shape=line_shape)

        ratio_err = np.sqrt((nii_6585_flux_err / (nii_6585_flux * np.log(10)))**2 +
                            (h_alpha_flux_err / (h_alpha_flux * np.log(10)))**2)

        return ratio_err

    def get_log10_ratio_sii_h_alpha(self, line_shape='gauss'):
        r"""
        logarithmic emission line ratio of the ([SII]6718 + [SII]6733) and Halpha6565 emission line
         for the x axis of the second BPT diagram
        """
        return np.log10((self.get_emission_line_flux(line_wavelength=6718, line_shape=line_shape) +
                         self.get_emission_line_flux(line_wavelength=6733, line_shape=line_shape)) /
                        self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape))

    def get_log10_ratio_oi_h_alpha(self, line_shape='gauss'):
        r"""
        logarithmic emission line ratio of the [OI]6302 and Halpha6565 emission line for the x axis of the third
         BPT diagram
        """
        return np.log10(self.get_emission_line_flux(line_wavelength=6302, line_shape=line_shape) /
                        self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape))

    def get_log10_ratio_oiii_limit_h_beta(self, line_shape='gauss', sigma_limit=3):
        r"""
        This function computes logarithmic emission line ratio of the [OIII]5008 error and Hbeta4863 flux
         for the y axis of BPT diagrams. This can be used as a limit approximation if [OIII] is not detected
        """
        return np.log10(sigma_limit * self.get_emission_line_flux_err(line_wavelength=5008, line_shape=line_shape) /
                        self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape))

    def get_log10_ratio_oiii_h_beta_limit(self, line_shape='gauss', sigma_limit=3):
        r"""
        This function computes logarithmic emission line ratio of the [OIII]5008 flux and Hbeta4863 error
         for the y axis of BPT diagrams. This can be used as a limit approximation if Hbeta4863 is not detected
        """
        return np.log10(self.get_emission_line_flux(line_wavelength=5008, line_shape=line_shape) /
                        (sigma_limit * self.get_emission_line_flux_err(line_wavelength=4863, line_shape=line_shape)))

    def get_log10_ratio_nii_limit_h_alpha(self, line_shape='gauss', sigma_limit=3):
        r"""
        This function computes logarithmic emission line ratio of the [NII]6585 error and Halpha6565 flux
         for the x axis of the first BPT diagram. This can be used as a limit approximation if [NII]6585 is not detected
        """
        return np.log10(sigma_limit * self.get_emission_line_flux_err(line_wavelength=6585, line_shape=line_shape) /
                        self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape))

    def get_log10_ratio_nii_h_alpha_limit(self, line_shape='gauss', sigma_limit=3):
        r"""
        This function computes logarithmic emission line ratio of the [NII]6585 flux and Halpha6565 error
         for the x axis of the first BPT diagram. This can be used as a limit approximation if Halpha6565 is not
         detected
        """
        return np.log10(self.get_emission_line_flux(line_wavelength=6585, line_shape=line_shape) /
                        (sigma_limit * self.get_emission_line_flux_err(line_wavelength=6565, line_shape=line_shape)))

    def get_log10_ratio_sii_limit_h_alpha(self, line_shape='gauss', sigma_limit=3):
        r"""
        This function computes logarithmic emission line ratio of the ([SII]6718 + [SII]6733) error and Halpha6565 Flux
         for the x axis of the second BPT diagram. This can be used as a limit approximation if ([SII]6718 + [SII]6733)
         is not detected
        """
        return np.log10(sigma_limit * (self.get_emission_line_flux_err(line_wavelength=6718, line_shape=line_shape) +
                                       self.get_emission_line_flux_err(line_wavelength=6733, line_shape=line_shape)) /
                        self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape))

    def get_log10_ratio_sii_h_alpha_limit(self, line_shape='gauss', sigma_limit=3):
        r"""
        This function computes logarithmic emission line ratio of the ([SII]6718 + [SII]6733) flux and Halpha6565 error
         for the x axis of the second BPT diagram. This can be used as a limit approximation if Halpha6565
         is not detected
        """
        return np.log10((self.get_emission_line_flux(line_wavelength=6718, line_shape=line_shape) +
                         self.get_emission_line_flux(line_wavelength=6733, line_shape=line_shape)) /
                        (sigma_limit * self.get_emission_line_flux_err(line_wavelength=6565, line_shape=line_shape)))

    def get_log10_ratio_oi_limit_h_alpha(self, line_shape='gauss', sigma_limit=3):
        r"""
        This function computes logarithmic emission line ratio of the [OI]6302 error and Halpha6565 flux
         for the x axis of the third BPT diagram. This can be used as a limit approximation if [OI]6302
         is not detected

        """
        return np.log10(sigma_limit * self.get_emission_line_flux_err(line_wavelength=6302, line_shape=line_shape) /
                        self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape))

    def get_snr_mask_bpt(self, bpt='first', line_shape='gauss', snr_limit=3, lower_flux_limit=1e-4,
                         lower_flux_err_limit=1e-4):

        # get S/N cut for y axis
        snr_mask_y_axis = (
                # mask too low emission line flux values and flux err values
                # (this can be due the case for fits at the parameter limits)
                (self.get_emission_line_flux(line_wavelength=4863, line_shape=line_shape) > lower_flux_limit) &
                (self.get_emission_line_flux(line_wavelength=5008, line_shape=line_shape) > lower_flux_limit) &
                (self.get_emission_line_flux_err(line_wavelength=4863, line_shape=line_shape) > lower_flux_err_limit) &
                (self.get_emission_line_flux_err(line_wavelength=5008, line_shape=line_shape) > lower_flux_err_limit) &
                # mask too low S/N values
                (self.get_emission_line_snr(line_wavelength=5008, line_shape=line_shape) > snr_limit) &
                (self.get_emission_line_snr(line_wavelength=4863, line_shape=line_shape) > snr_limit))

        # get S/N cut for x axis
        # Halpha mask. This line is represented in all three BPT diagrams
        snr_halpha = ((self.get_emission_line_flux(line_wavelength=6565, line_shape=line_shape) > lower_flux_limit) &
                      (self.get_emission_line_flux_err(line_wavelength=6565, line_shape=line_shape) >
                       lower_flux_err_limit) &
                      (self.get_emission_line_snr(line_wavelength=6565, line_shape=line_shape) > snr_limit))

        if bpt == 'first':
            snr_mask_x_axis = (
                    # mask too low emission line flux values and flux err values
                    # (this can be due the case for fits at the parameter limits)
                    (self.get_emission_line_flux(line_wavelength=6585, line_shape=line_shape) > lower_flux_limit) &
                    (self.get_emission_line_flux_err(line_wavelength=6585, line_shape=line_shape) > lower_flux_err_limit) &
                    # mask too low S/N values
                    (self.get_emission_line_snr(line_wavelength=6585, line_shape=line_shape) > snr_limit))

        elif bpt == 'second':
            snr_mask_x_axis = (
                    # mask too low emission line flux values and flux err values
                    # (this can be due the case for fits at the parameter limits)
                    ((self.get_emission_line_flux(line_wavelength=6718, line_shape=line_shape) +
                      self.get_emission_line_flux(line_wavelength=6733, line_shape=line_shape)) > lower_flux_limit) &
                    (np.sqrt(self.get_emission_line_flux_err(line_wavelength=6718, line_shape=line_shape) ** 2 +
                             self.get_emission_line_flux_err(line_wavelength=6733, line_shape=line_shape) ** 2) >
                     lower_flux_err_limit) &
                    # mask too low S/N values
                    (((self.get_emission_line_flux(line_wavelength=6718, line_shape=line_shape) +
                      self.get_emission_line_flux(line_wavelength=6733, line_shape=line_shape)) /
                     np.sqrt(self.get_emission_line_flux_err(line_wavelength=6718, line_shape=line_shape) ** 2 +
                             self.get_emission_line_flux_err(line_wavelength=6733, line_shape=line_shape) ** 2)) >
                     snr_limit))

        elif bpt == 'third':
            snr_mask_x_axis = (
                    # mask too low emission line flux values and flux err values
                    # (this can be due the case for fits at the parameter limits)
                    (self.get_emission_line_flux(line_wavelength=6302, line_shape=line_shape) > lower_flux_limit) &
                    (self.get_emission_line_flux_err(line_wavelength=6302, line_shape=line_shape) > lower_flux_err_limit) &
                    # mask too low S/N values
                    (self.get_emission_line_snr(line_wavelength=6302, line_shape=line_shape) > snr_limit))
        else:
            raise KeyError('did not understand bpt keyword must be first, second or thierd')

        return snr_mask_y_axis * snr_halpha * snr_mask_x_axis


class XrayAccess(object_identifier.ObjectIdentifier):
    def __init__(self, **kwargs):
        r"""
        """
        super().__init__(**kwargs)

    def get_chandra_band_flux(self, band='b', return_format='Jy'):

        if band not in ['b', 'h', 'm', 's', 'u', 'w']:
            raise KeyError('chandra band not understand must be b, h, m, s, u or w')

        if return_format == 'Jy':
            return self.table['Favg%s' % band] / ((self.chandra_band_pass_filter_borders[band]['max'] -
                                                        self.chandra_band_pass_filter_borders[band]['min']) *
                                                       constants.e / constants.h) * 10 ** 23


        elif return_format == 'cgs':
            return self.table['Favg%s' % band] / ((self.chandra_band_pass_filter_borders[band]['max'] -
                                                            self.chandra_band_pass_filter_borders[band]['min']) *
                                                           constants.e / constants.h)
        else:
            raise KeyError('return format must be Jy or cgs')

    def get_xmm_newton_band_flux(self, band='band8', return_format='Jy'):
        if band != 'band8':
            raise KeyError('XMM Newton band not understand. only band8 is available')

        if return_format == 'Jy':
            return self.table['Flux8'] / ((self.xmm_newton_band_pass_filter_borders[band]['max'] -
                                                    self.xmm_newton_band_pass_filter_borders[band]['min']) *
                                                   constants.e / constants.h) * 10 ** 23
        elif return_format == 'cgs':
            return self.table['Flux8'] / ((self.xmm_newton_band_pass_filter_borders[band]['max'] -
                                                self.xmm_newton_band_pass_filter_borders[band]['min']) *
                                               constants.e / constants.h)
        else:
            raise KeyError('return format must be Jy or cgs')


class InfraredAccess(object_identifier.ObjectIdentifier):
    def __init__(self, **kwargs):
        r"""
        class to access Hubble Space telescope data
        """
        super().__init__(**kwargs)

    def get_wise_vega_mag(self, band='W1'):
        # returns WISE mag in Vega mag
        # look
        # http://wise2.ipac.caltech.edu/docs/release/prelim/expsup/sec4_3g.html

        if wise_filter == 'W1':
            return self.table['W1mag']
        elif wise_filter == 'W2':
            return self.table['W2mag']
        elif wise_filter == 'W3':
            return self.table['W3mag']
        elif wise_filter == 'W4':
            return self.table['W4mag']
        else:
            raise KeyError('this band does not exist! choose W1, W2, W3 or W4')

    def get_wise_ab_mag(self, band='W1'):
        # returns WISE mag in AB mag
        # look
        # http://wise2.ipac.caltech.edu/docs/release/prelim/expsup/sec4_3g.html

        if band == 'W1':
            return self.table['W1mag'] + 2.699
        elif band == 'W2':
            return self.table['W2mag'] + 3.339
        elif band == 'W3':
            return self.table['W3mag'] + 5.174
        elif band == 'W4':
            return self.table['W4mag'] + 6.620
        else:
            raise KeyError('this band does not exist! choose W1, W2, W3 or W4')

    def get_wise_ab_mag_err(self, band='W1'):
        # returns WISE mag in AB mag
        # look
        # http://wise2.ipac.caltech.edu/docs/release/prelim/expsup/sec4_3g.html

        if band == 'W1':
            return self.table['e_W1mag'] # + 2.699
        elif band == 'W2':
            return self.table['e_W2mag'] # + 3.339
        elif band == 'W3':
            return self.table['e_W3mag'] # + 5.174
        elif band == 'W4':
            return self.table['e_W4mag'] # + 6.620
        else:
            raise KeyError('this band does not exist! choose W1, W2, W3 or W4')

    def get_wise_band_flux(self, band='W1', return_format='Jy'):
        # This magnitude system is defined such that, when monochromatic flux f is measured in erg sec^-1 cm^-2 Hz^-1,
        # Oke, J.B. 1974, ApJS, 27, 21
        mag = self.get_wise_ab_mag(band=band)
        if return_format == 'Jy':
            return 10 ** (- (mag + 48.6) / 2.5 + 23)
        elif return_format == 'cgs':
            return 10 ** (- (mag + 48.6) / 2.5)
        else:
            raise KeyError('return format must be Jy or cgs')

    def get_wise_band_flux_err(self, band='W1', return_format='Jy'):
        # This magnitude system is defined such that, when monochromatic flux f is measured in erg sec^-1 cm^-2 Hz^-1,
        # Oke, J.B. 1974, ApJS, 27, 21
        mag = self.get_wise_ab_mag(band=band)
        mag_err = self.get_wise_ab_mag_err(band=band)

        if return_format == 'Jy':
            return (10 ** (- (mag - mag_err + 48.6) / 2.5 + 23)) - (10 ** (- (mag + 48.6) / 2.5 + 23))
            # return - np.log(10) / 2.5 * 10 ** (- (mag + 48.6) / 2.5 + 23) * mag_err

        elif return_format == 'cgs':
            return (10 ** (- (mag - mag_err + 48.6) / 2.5)) - (10 ** (- (mag + 48.6) / 2.5))
            # return - np.log(10) / 2.5 * 10 ** (- (mag + 48.6) / 2.5) * mag_err

        else:
            raise KeyError('return format must be Jy or cgs')

    def get_iras_flux(self, band='12um', return_format='Jy'):
        r"""
        Access flux observed by IRAS see Fullmer 1989  1989IRASG....c...0F
        we return the estimated integrated flux
        :return flux
        :rtype: array_like
        """
        if band not in ['12mu', '25mu', '60mu', '100mu']:
            raise KeyError('you choose the wrong band. IRAS has only 12mu, 25mu, 60mu and 100mu')
        if return_format == 'Jy':
            return self.table['Fnu_%s' % band[:-2]]
        elif return_format == 'cgs':
            return self.table['Fnu_%s' % band[:-2]] * (10 ** -23)
        else:
            raise KeyError('return format must be Jy or cgs')

    def get_iras_limit_flag(self, band='12um'):
        r"""
        Access flux observed by IRAS see Fullmer 1989  1989IRASG....c...0F
        we return the estimated integrated flux
        :return flux
        :rtype: array_like
        """
        if band not in ['12mu', '25mu', '60mu', '100mu']:
            raise KeyError('you choose the wrong band. IRAS has only 12mu, 25mu, 60mu and 100mu')

        return self.table['FQual_%s' % band[:-2]] == 'L'


    def get_herschel_flux(self, band='100mu', return_format='Jy'):
        r"""
        Access flux observed by Herschel see Maddox et al 2018, oliver et al 2012 and Viero et al 2014
        we return the estimated integrated flux
        :return flux
        :rtype: array_like
        """
        if band not in ['100mu', '160mu', '250mu', '350mu', '500mu']:
            raise KeyError('you choose the wrong band. Herschel has only 100mu, 160mu, 250mu, 350mu and 500mu')

        if return_format == 'Jy':
            return self.table['F%s' % band[:-2]] * 1e-3
        elif return_format == 'cgs':
            return self.table['F%s' % band[:-2]] * (10 ** -26)
        else:
            raise KeyError('return format must be Jy or cgs')

    def get_herschel_flux_err(self, band='100mu', return_format='Jy'):
        r"""
        Access flux observed by Herschel see Maddox et al 2018, oliver et al 2012 and Viero et al 2014
        we return the estimated integrated flux
        :return flux
        :rtype: array_like
        """
        if band not in ['100mu', '160mu', '250mu', '350mu', '500mu']:
            raise KeyError('you choose the wrong band. Herschel has only 100mu, 160mu, 250mu, 350mu and 500mu')

        if return_format == 'Jy':
            return self.table['e_F%s' % band[:-2]] * 1e-3
        elif return_format == 'cgs':
            return self.table['e_F%s' % band[:-2]] * (10 ** -26)
        else:
            raise KeyError('return format must be Jy or cgs')


class RadioAccess(object_identifier.ObjectIdentifier):
    def __init__(self, **kwargs):
        r"""
        """
        super().__init__(**kwargs)

    def get_radio_continuum_flux(self, observation, flux_estimation='peak', unit_system='Jy'):
        r"""
        Access to radio continuum flux provided from
        VLASS at 3 GHz see
        FIRST at 1.4 GHz see White et al. (1997) DOI:10.1086/303564
        NVSS at 1.4 GHz see Condon et al. (1998) DOI:10.1086/300337
        LOFAR at 150MHz see Schimwell et al. (2017) DOI: 10.1051/0004-6361/201629313

        :param observation: `vlass´ for 3GHz, `first´ for 1.4GHz `nvss´ for 1.4GHz or `lofar´ for 150MHz
        :param flux_estimation: `peak´ for peak flux or `int´ for integrated flux
        :param unit_system: Jy or cgs

        :return flux
        :rtype: array_like
        """

        # check if observation and flux_estimation is consistent
        if flux_estimation + '_' + observation not in ['peak_vlass', 'int_vlass',
                                                       'peak_first', 'int_first',
                                                       'int_nvss',
                                                       'peak_lofar', 'int_lofar']:
            raise KeyError('The flux estimation you have choosen is not possible. '
                           'It must be on of the following combination:'
                           'flux_estimation = peak + observation = vlass, first or lofar  or'
                           'flux_estimation = int + observation = vlass, first, nvss or lofar')

        if unit_system == 'Jy':
            return self.table['f_%s_%s' % (flux_estimation, observation)] * 1e-3
        elif unit_system == 'cgs':
            return self.table['f_%s_%s' % (flux_estimation, observation)] * (10 ** -26)
        else:
            raise KeyError('unit_system must be Jy or cgs')

    def get_radio_continuum_flux_err(self, observation, flux_estimation='peak', unit_system='Jy'):
        r"""
        Access to radio continuum flux err. Parameter description see get_radio_continuum_flux
        """

        # check if observation and flux_estimation is consistent
        if flux_estimation + '_' + observation not in ['peak_vlass', 'int_vlass',
                                                       'peak_first', 'int_first',
                                                       'int_nvss',
                                                       'peak_lofar', 'int_lofar']:
            raise KeyError('The flux estimation you have choosen is not possible. '
                           'It must be on of the following combination:'
                           'flux_estimation = peak + observation = vlass, first or lofar  or'
                           'flux_estimation = int + observation = vlass, first, nvss or lofar')

        if unit_system == 'Jy':
            if observation == 'first':
                return self.table['rms_first'] * 1e-3
            else:
                return self.table['f_%s_err_%s' % (flux_estimation, observation)] * 1e-3
        elif unit_system == 'cgs':
            if flux_estimation == 'first':
                return self.table['rms_first'] * (10 ** -26)
            else:
                return self.table['f_%s_err_%s' % (flux_estimation, observation)] * (10 ** -26)
        else:
            raise KeyError('unit_system must be Jy or cgs')

    def get_radio_continuum_snr(self, observation, flux_estimator='peak'):
        r"""
        Access to radio continuum SNR using get_radio_continuum_flux and get_radio_continuum_flux_err
        see there in for docstring.
        """

        if observation == 'first':
            if flux_estimator == 'int':
                raise KeyError('There is no error estimation for integrated flux in the FIRST catalog')
            # white et al. 1997 DOI:10.1086/303564 section 4.4
            snr = (self.table['f_peak_first'] - 0.25) / self.table['rms_first']
        elif observation == 'nvss':
            if flux_estimator == 'peak':
                raise KeyError('There is only the integrated flux density of available in the NVSS catalog')
            snr = self.table['f_int_nvss'] / self.table['f_int_err_nvss']
        elif observation == 'vlass':
            snr = self.table['f_%s_vlass' % flux_estimator] / self.table['f_%s_err_vlass' % flux_estimator]
        elif observation == 'lofar':
            snr = self.table['f_%s_lofar' % flux_estimator] / self.table['f_%s_err_lofar' % flux_estimator]
        else:
            raise KeyError('The observation must be first, nvss, vlass or lofar')

        return snr

    def get_vlass_flux(self, flux_estimation='int', unit_system='Jy'):
        r"""
        Access the flux observed by VLASS
        :param flux_estimation: peak for peak flux or int for integrated flux
        :param unit_system: Jy or cgs
        :return flux error
        :rtype: array_like
        """
        return self.get_radio_continuum_flux(observation='vlass', flux_estimation=flux_estimation,
                                             unit_system=unit_system)

    def get_vlass_flux_err(self, flux_estimation='int', unit_system='Jy'):
        r"""
        Access error on the flux observed by VLASS see get_vlass_flux for parameter description"""
        return self.get_radio_continuum_flux_err(observation='vlass', flux_estimation=flux_estimation,
                                                 unit_system=unit_system)

    def get_first_flux(self, flux_estimation='int', unit_system='Jy'):
        r"""
        Access the flux observed by FIRST see White et al. (1997) DOI:10.1086/303564
        :param flux_estimation: peak for peak flux or int for integrated flux
        :param unit_system: Jy or cgs
        :return flux error
        :rtype: array_like
        """
        return self.get_radio_continuum_flux(observation='first', flux_estimation=flux_estimation,
                                             unit_system=unit_system)

    def get_first_flux_err(self, unit_system='Jy'):
        r"""
        Access error on the flux observed by FIRST see get_first_flux for parameter description
        """
        if unit_system == 'Jy':
            return self.table['rms_first'] * 1e-3
        elif unit_system == 'cgs':
            return self.table['rms_first'] * (10 ** -26)

    def get_nvss_flux(self, flux_estimation='int', unit_system='Jy'):
        r"""
        Access the flux observed by NVSS see Condon et al. (1998) DOI:10.1086/300337
        :param flux_estimation: peak for peak flux or int for integrated flux
        :param unit_system: Jy or cgs
        :return flux error
        :rtype: array_like
        """
        return self.get_radio_continuum_flux(observation='nvss', flux_estimation=flux_estimation,
                                             unit_system=unit_system)

    def get_nvss_flux_err(self, flux_estimation='int', unit_system='Jy'):
        r"""
        Access error on the flux observed by NVSS see get_nvss_flux for parameter description
        """
        return self.get_radio_continuum_flux_err(observation='nvss', flux_estimation=flux_estimation,
                                                 unit_system=unit_system)

    def get_lofar_flux(self, flux_estimation='int', unit_system='Jy'):
        r"""
        Access the flux observed by LOFAR see Schimwell et al. (2017) DOI: 10.1051/0004-6361/201629313
        """
        return self.get_radio_continuum_flux(observation='lofar', flux_estimation=flux_estimation,
                                             unit_system=unit_system)

    def get_lofar_flux_err(self, flux_estimation='int', unit_system='Jy'):
        r"""
        Access error on the flux observed by LOFAR see get_lofar_flux for parameter description
        """
        return self.get_radio_continuum_flux_err(observation='lofar', flux_estimation=flux_estimation,
                                                 unit_system=unit_system)

    def get_radio_rel_err(self, observation, flux_estimation='int'):
        r"""
        Access relativ error of radio observation
        """
        radio_flux = self.get_radio_continuum_flux(observation=observation, flux_estimation=flux_estimation,
                                                   unit_system='Jy')
        radio_flux_err = self.get_radio_continuum_flux_err(observation=observation, flux_estimation=flux_estimation,
                                                           unit_system='Jy')

        return radio_flux_err / radio_flux

    def get_vla_rel_err(self, flux_estimation='int'):
        r"""
        this function uses the FIRST and NVSS catalog to estimate the flux at 1.4 GHz.
        We prefer the FIRST catalog since it is deeper.
        parameters like get_radio_luminosity()
        """
        radio_vla_flux = np.zeros(len(self.table)) * np.nan
        radio_vla_flux_err = np.zeros(len(self.table)) * np.nan

        first_flux = self.get_radio_continuum_flux(observation='first', flux_estimation=flux_estimation,
                                                   unit_system='Jy')
        first_flux_err = self.get_radio_continuum_flux_err(observation='first', flux_estimation=flux_estimation,
                                                           unit_system='Jy')
        nvss_flux = self.get_radio_continuum_flux(observation='nvss', flux_estimation=flux_estimation, unit_system='Jy')
        nvss_flux_err = self.get_radio_continuum_flux_err(observation='nvss', flux_estimation=flux_estimation,
                                                          unit_system='Jy')

        reasonable_first_values = np.invert(np.isnan(first_flux))
        reasonable_nvss_values = np.invert(np.isnan(nvss_flux))

        radio_vla_flux[reasonable_first_values] = first_flux[reasonable_first_values]
        radio_vla_flux_err[reasonable_first_values] = first_flux_err[reasonable_first_values]

        radio_vla_flux[reasonable_nvss_values * ~reasonable_first_values] = nvss_flux[reasonable_nvss_values *
                                                                                      ~reasonable_first_values]
        radio_vla_flux_err[reasonable_nvss_values * ~reasonable_first_values] = nvss_flux_err[reasonable_nvss_values *
                                                                                              ~reasonable_first_values]

        return radio_vla_flux_err / radio_vla_flux


class DataAccess(DPAccess, StellarAccess, ImageAccess, EmissionLineRatio, XrayAccess,
                 InfraredAccess, RadioAccess):

    def __init__(self, **kwargs):
        r"""

        """
        super().__init__(**kwargs)

    def get_classification_mask(self):
        r"""
        !!!!! work in progress
        at the moment only morphology masks
        :return mask for galaxy table.
        :rtype: array_like
        """
        # load dictionary of morphology masks
        morph_mask_dict = np.load((Path(self.data_path / self.file_name).parents[0]
                                   / 'subsets' / 'morphology' / 'morph').with_suffix('.npy'), allow_pickle=True).item()

        return morph_mask_dict

    def get_morphology_MaNGA_classification_mask(self, VAC_version):
        r"""
        only MaNGA-VAC-based morphology classification
        :return morphology dict for galaxy table.
        :rtype: array_like
        """
        if VAC_version == 'DR15' :
            dictname = 'morph_dict_dr15'
        if VAC_version == 'DR17' :
            dictname == 'morph_dict_dr17'
        # load dictionary of morphology masks
        morph_mask_dict = np.load((Path(self.data_path / self.file_name).parents[0]
                                   / 'subsets' / 'morphology' / dictname).with_suffix('.npy'), allow_pickle=True).item()

        return morph_mask_dict

    @staticmethod
    def get_designation(ra, dec):
        from astropy import units as u
        from astropy.coordinates import SkyCoord

        c = SkyCoord(ra=ra * u.degree, dec=dec * u.degree)
        ra_coords = c.ra.hms
        dec_coords = c.dec.dms
        d = dec_coords.d
        if d > 0:
            d_string = '+%02i' % d
        else:
            if d == 0:
                if dec_coords.m > 0:
                    d_string = '+%02i' % d
                else:
                    if dec_coords.m == 0:
                        if dec_coords.s >= 0:
                            d_string = '+%02i' % d
                        else:
                            d_string = '-%02i' % abs(d)
                    else:
                        d_string = '-%02i' % abs(d)
            else:
                d_string = '-%02i' % abs(d)

        # print 'J%02i%02i%05.2f%s%02i%02.2f' % (
        # ra_coords.h, abs(ra_coords.m), abs(ra_coords.s), d_string, abs(dec_coords.m), abs(dec_coords.s))
        # exit()
        return 'J%02i%02i%05.2f%s%02i%04.1f' % (
        ra_coords.h, abs(ra_coords.m), abs(ra_coords.s), d_string, abs(dec_coords.m), abs(dec_coords.s))

    @staticmethod
    def get_designation_coord(ra, dec, precision=3):

        from astropy import units as u
        from astropy.coordinates import SkyCoord

        c = SkyCoord(ra=ra * u.degree, dec=dec * u.degree)
        ra_coords = c.ra.hms
        dec_coords = c.dec.dms
        d = dec_coords.d
        if d >= 0:
            d_string = '+%02i' % d
        else:
            d_string = '-%02i' % abs(d)

        if precision == 3:
            return '%02i:%02i:%06.3f %s:%02i:%06.3f' % (
            ra_coords.h, abs(ra_coords.m), abs(ra_coords.s), d_string, abs(dec_coords.m), abs(dec_coords.s))
        elif precision == 0:
            return '%02i:%02i:%.0f %s:%02i:%.0f' % (
            ra_coords.h, abs(ra_coords.m), abs(ra_coords.s), d_string, abs(dec_coords.m), abs(dec_coords.s))

    @staticmethod
    def transform_coordinate(ra, dec):
        from astropy import units as u
        from astropy.coordinates import SkyCoord

        c = SkyCoord(ra=ra * u.degree, dec=dec * u.degree)
        ra_coords = c.ra.hms
        dec_coords = c.dec.dms
        d = dec_coords.d
        if d >= 0:
            d_string = '%02i' % d
        else:
            d_string = '%02i' % abs(d)
        # print ra_coords
        # print dec_coords
        # print 'J%02i%02i%05.2f%s%02i%02.2f' % (
        # ra_coords.h, abs(ra_coords.m), abs(ra_coords.s), d_string, abs(dec_coords.m), abs(dec_coords.s))
        # exit()
        ra_new = '%02i:%02i:%05.3f' % (ra_coords.h, abs(ra_coords.m), abs(ra_coords.s))
        dec_new = '%s:%02i:%04.3f' % (d_string, abs(dec_coords.m), abs(dec_coords.s))

        return ra_new, dec_new

    def fiber_diameter_array(self, x):
        return 1 / self.cosmology.arcsec_per_kpc_comoving(x).value * 3

    def get_comoving_distance(self, z):
        return self.cosmology.comoving_distance(z).value

    def degree_to_comoving_distance(self, degree, z):
        return degree * 3600 / self.cosmology.arcsec_per_kpc_comoving(z).value

    def arcsec2comoving_distance(self, arcsec, z):
        return arcsec / self.cosmology.arcsec_per_kpc_comoving(z).value

